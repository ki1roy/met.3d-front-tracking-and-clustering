# For cartopy image generation
import matplotlib.pyplot as plt
import io
import os

from urllib.request import urlopen, Request
# from PIL import Image

#For netcdf library support
from netCDF4 import Dataset as netcdf_dataset
from numpy import dtype
import numpy as np
import pickle
import xarray as xr
from scipy.stats import wasserstein_distance


gpropVarMatr = np.array([], ndmin=2)

g_time = ""
g_nem = ""
g_filename_prefix = ""
g_var_names = []

current_front_dict = {}

g_tfpStats = np.array([], ndmin=1)
g_strengthStats = np.array([], ndmin=1)
g_areaStats = np.array([], ndmin=1)
g_slopeStats = np.array([], ndmin=1)
g_stats_meta = np.array([], ndmin=1)


def receive_properties(propVarMatr):
    global gpropVarMatr

    print(propVarMatr.shape, flush=True)

    gpropVarMatr = propVarMatr

    return "Metrics data received."


def receive_metadata(time, mem, var_names, filename_prefix, ):
    global g_time
    global g_nem
    global g_var_names
    global g_filename_prefix

    g_time = time
    g_nem = mem
    g_var_names = var_names.split(",")
    g_filename_prefix = filename_prefix

    print(g_time, flush=True)
    print(g_nem, flush=True)
    print(g_filename_prefix, flush=True)

    return "Meta data received."


def receive_front_statistic(tfp, strength, area, slope, vector_meta):
    global g_tfpStats
    global g_strengthStats
    global g_areaStats
    global g_slopeStats
    global g_stats_meta

    g_tfpStats = tfp
    g_strengthStats = strength
    g_areaStats = area
    g_slopeStats = slope
    g_stats_meta = vector_meta

    return "Statistical data received."


def save_to_pickle():
    global current_front_dict

    filename = g_filename_prefix + "-" + g_time + "-" + g_nem + ".pickle"
    d = {}
    start = 0
    end = 0

    for i in range(0, gpropVarMatr.shape[0]):
        d[i] = {}
        end += g_stats_meta[i]

        d[i]["tfp_stats"] = g_tfpStats[start:end]
        d[i]["strength_stats"] = g_strengthStats
        d[i]["area_stats"] = g_areaStats
        d[i]["slope_stats"] = g_slopeStats

        c = 0
        for n in g_var_names:
            d[i][n] = gpropVarMatr[i][c]
            c += 1

        start += g_stats_meta[i]

#    d["tfp_stats"] = g_tfpStats
#    d["strength_stats"] = g_strengthStats
#    d["area_stats"] = g_areaStats
#    d["slope_stats"] = g_slopeStats
#    d["stats_meta"] = g_stats_meta

    current_front_dict = d

    with open(filename, 'wb') as handle:
        pickle.dump(d, handle, protocol=pickle.HIGHEST_PROTOCOL)

    return "Saved to pickle file."


def open_pickle(filename):
    with open(filename, 'rb') as f:
        loaded_dict = pickle.load(f)

    return loaded_dict


def track_front(tfp):
    front_nr = get_min_emd(tfp, current_front_dict, "tfp_stats")
    print("Most similar front is front Nr: " + str(front_nr))
    return str(front_nr)


def get_min_emd(test_data, against_dicts, var_name):
    min_wd = 99999.9
    dict_nr = 999
    i = 0
    for d in against_dicts.keys():
        res = wasserstein_distance(test_data, against_dicts[d][var_name])
        if res < min_wd:
            min_wd = res
            dict_nr = i
        i += 1

    return dict_nr
