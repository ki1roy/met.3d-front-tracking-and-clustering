import os
import ftplib
import bz2
import pickle
import logging
import cdo
import time

zip_file_name = "icon_global_icosahedral_time-invariant_2020083100_CLAT.grib2.bz2"
queried_dataset_path = "/mnt/sdb1/Data/casestudies/ICON/20200831"
unzip_file_name = "icon_global_icosahedral_time-invariant_2020083100_CLAT.grib2"
variable = "CLAT"

os.chdir(queried_dataset_path)
print(queried_dataset_path)
print(os.getcwd())

try:
    # Uncompress the files of each 'variable' and for given 'grid type', using bz2 module.
    with open(unzip_file_name, 'wb') as unzip_file_pointer, bz2.BZ2File(zip_file_name,
                                                                        'rb') as zip_file_pointer:
        print(zip_file_name)
        print(os.getcwd())
        for data in iter(lambda: zip_file_pointer.read(), b''):
            unzip_file_pointer.write(data)
        unzip_file_pointer.close()
        zip_file_pointer.close()
        time.sleep(3)
except (IOError, EOFError):
    logging.error("Unable to uncompress file '%s'." % variable)
    unzip_file_pointer.close()
    zip_file_pointer.close()
    os.unlink(unzip_file_name)

