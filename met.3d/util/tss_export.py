# For cartopy image generation
import matplotlib.pyplot as plt
import io
import os

from urllib.request import urlopen, Request
# from PIL import Image

#For netcdf library support
from netCDF4 import Dataset as netcdf_dataset
from numpy import dtype
import numpy as np
import xarray as xr

# time series similarities
import sys
sys.path.append("path/to/tssanalysis/enstools.timeseriessimilarity/enstools/")
import timeseriessimilarity.distanceMeasure as dm
import timeseriessimilarity.wrappers as wr
import timeseriessimilarity.tsPlotter as tp

# Pass off to PIL.
from PIL import Image



glatVec = []
glonVec = []
glevelVec = []
gdataVec = []

glonMatr = np.array([], ndmin=2)
glatMatr = np.array([], ndmin=2)
glevMatr = np.array([], ndmin=2)
gauxVarMatr = np.array([], ndmin=2)
gtimeVec = np.array([], ndmin=1)

g_init_time = ""
g_valid_time = ""
g_time_unit = ""
g_aux_var_name = ""


def receive_trajectories(lonMatr, latMatr, levMatr, auxVarMatr, timeVec):
    global glonMatr
    global glatMatr
    global glevMatr
    global gauxVarMatr
    global gtimeVec

    print(lonMatr.shape, flush=True)
    print(latMatr.shape, flush=True)
    print(levMatr.shape, flush=True)
    print(auxVarMatr.shape, flush=True)
    print(timeVec.shape, flush=True)
    
    glonMatr = lonMatr
    glatMatr = latMatr
    glevMatr = levMatr
    gauxVarMatr = auxVarMatr
    gtimeVec = timeVec

    return "Trajcetory data received."


def receive_metadata(initTime, validTime, timeUnit, auxVarName):
    global g_init_time
    global g_valid_time
    global g_time_unit
    global g_aux_var_name

    g_init_time = initTime
    g_valid_time = validTime
    g_time_unit = timeUnit
    g_aux_var_name = auxVarName

    print(g_init_time, flush=True)
    print(g_valid_time, flush=True)
    print(g_time_unit, flush=True)
    print(g_aux_var_name, flush=True)
    return "Meta data received."


def plot_data():
    # ds = to_xarray(write_to_netcdf=True)
    ds = to_xarray2(write_to_netcdf=True)

    enstools_analysis_plot(ds, varname=g_aux_var_name)

    return "Data plot created."
    

def to_xarray(write_to_netcdf=True):
    ds = xr.Dataset(
        data_vars=dict(
            lon=(["ensemble", "trajectory", "time"], np.expand_dims(glonMatr, axis=0)),
            lat=(["ensemble", "trajectory", "time"], np.expand_dims(glatMatr, axis=0)),
            pressure=(["ensemble", "trajectory", "time"], np.expand_dims(glevMatr, axis=0)),
            auxvar=(["ensemble", "trajectory", "time"], np.expand_dims(gauxVarMatr, axis=0)),
            time=(["time"], gtimeVec),

        ),
        attrs=dict(description="Trajectories exported from Met.3D via the python interface"),
    )

    # set attributes
    ds['lon'].attrs = dict(_FillValue=np.nan,
                           units="longitude",
                           standard_name="longitude",
                           )

    ds['lat'].attrs = dict(_FillValue=np.nan,
                           units="latitude",
                           standard_name="latitude",
                           )

    ds['pressure'].attrs = dict(_FillValue=np.nan,
                                standard_name="air_pressure",
                                long_name="pressure",
                                units="hPa",
                                axis="Z",
                                positive="down")

    ds['auxvar'].attrs = dict(_FillValue=np.nan,
                              auxiliary_data="yes",
                              standard_name="auxvar",
                              long_name="auxiliary variable",
                              units="unknown"
                              )

    ds['time'].attrs = dict(_FillValue=np.nan,
                            units="hours since 2021-07-26 12:00:00",
                            trajectory_starttime="2021-08-10 12:00:00",
                            forecast_inittime="2021-06-27 00:00:00",
                            standard_name="time",
                            long_name="time"
                            )
    if write_to_netcdf:
        print("Write data to netcdf", flush=True)
        ds.to_netcdf("testtrajectoyexport.nc")
        print("Data written to Netcdf", flush=True)

    return ds


def to_xarray2(write_to_netcdf=True):
    ds = xr.Dataset(
        data_vars=dict(
            lon=(["ens", "time"], glonMatr),
            lat=(["ens", "time"], glatMatr),
            pressure=(["ens", "time"], glevMatr),
            auxvar=(["ens", "time"], gauxVarMatr),
            time=(["time"], gtimeVec),
            ens=(["ens"], np.arange(0, glonMatr.shape[0], 1))
        ),
        attrs=dict(description="Trajectories exported from Met.3D via the python interface"),
    )

    ds = ds.rename({'auxvar': g_aux_var_name})

    print(g_aux_var_name, flush=True)
    # set attributes
    ds['lon'].attrs = dict(_FillValue=np.nan,
                           units="longitude",
                           standard_name="longitude",
                           )

    ds['lat'].attrs = dict(_FillValue=np.nan,
                           units="latitude",
                           standard_name="latitude",
                           )

    ds['pressure'].attrs = dict(_FillValue=np.nan,
                                standard_name="air_pressure",
                                long_name="pressure",
                                units="hPa",
                                axis="Z",
                                positive="down")

    ds[g_aux_var_name].attrs = dict(_FillValue=np.nan,
                              auxiliary_data="yes",
                              standard_name=g_aux_var_name,
                              long_name=g_aux_var_name,
                              units="unknown"
                              )

    ds['time'].attrs = dict(_FillValue=np.nan,
                            units=g_time_unit,
                            trajectory_starttime=g_init_time,
                            forecast_inittime=g_valid_time,
                            standard_name="time",
                            long_name="time"
                            )

    ds['ens'].attrs = dict(units="number",
                           standard_name="ens",
                           long_name="ensemble",
                           )

    if write_to_netcdf:
        print("Write data to netcdf", flush=True)
        ds.to_netcdf("testtrajectoyexport2.nc")
        print("Data written to Netcdf", flush=True)

    return ds


def enstools_analysis_plot(ds, varname="T"):
    print("Create TSS plot", flush=True)
    vis = tp.tsPlotter(ds, varname, 60, colors="magma")
    w, h, buf = vis.visualize_the_ensemble_cpp(view=0)
    print("Create TSS plot", flush=True)
    plot = Image.frombytes("RGBA", (w, h), buf)
    plot.show()
    return w, h, buf

