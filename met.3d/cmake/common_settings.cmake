##########################################################################################
#  This file is a cmake script containing common settings for all systems
#
#  Copyright 2016-2021 Marc Rautenhaus
#  Copyright 2016 Michael Kern
#
##########################################################################################

#NOTE (04Oct2021, mr) -- the following should be revised.
# See: https://stackoverflow.com/questions/29191855/what-is-the-proper-way-to-use-pkg-config-from-cmake
# See: https://cmake.org/cmake/help/latest/module/UsePkgConfig.html#module:UsePkgConfig
# See: https://cmake.org/cmake/help/latest/module/FindPkgConfig.html#module:FindPkgConfig

if (UNIX)

    include(FindPkgConfig)

    # Use pkg_config to detect INCLUDE/LIBRARY directories
    function(use_pkg_config pkg_name)

        if (${${pkg_name}_FOUND})
            return()
        endif()

        set(PREFIX Package)
        pkg_search_module(${PREFIX} ${pkg_name})

        if (${PREFIX}_FOUND)
            message(STATUS "Found pkg_config file for package ${pkg_name}")
        endif()

        set(PKG_LIBRARY_DIRS ${${PREFIX}_LIBRARY_DIRS} PARENT_SCOPE)
        set(PKG_INCLUDE_DIRS ${${PREFIX}_INCLUDE_DIRS} PARENT_SCOPE)
    endfunction()

endif (UNIX)
