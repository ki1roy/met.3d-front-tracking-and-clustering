/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2018 Marc Rautenhaus
**  Copyright 2015      Michael Kern
**  Copyright 2017-2018 Bianca Tost
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
******************************************************************************/

// Compute shader that computes normal curve line vertices from given
// initial points.

/*****************************************************************************
 ***                             CONSTANTS
 *****************************************************************************/

const int MAX_ISOSURFACES = 10;

const int VALUE_MODE_LENGTH = 0;
const int VALUE_MODE_VALUE = 1;
const int VALUE_MODE_AVG_DIFFERENCE = 2;
const int VALUE_MODE_TOTAL_DIFFERENCE = 3;

// Vertical level type; see structuredgrid.h.
const int SURFACE_2D = 0;
const int PRESSURE_LEVELS_3D = 1;
const int HYBRID_SIGMA_PRESSURE_3D = 2;
const int POTENTIAL_VORTICITY_2D = 3;
const int LOG_PRESSURE_LEVELS_3D = 4;
const int AUXILIARY_PRESSURE_3D = 5;

const float DELTA_LAT_KM = 111.2;
const float M_PI = 3.1415926535897932384626433832795;

/*****************************************************************************
 ***                             UNIFORMS
 *****************************************************************************/

// Normal curve vertex element.
struct InitPointNC
{
    vec4 position;
};

struct EndPosLengthNC
{
    vec4 position;
};


// Shader buffer storage object of inti points for normal curves.
layout (std430, binding=0) buffer NormalCurveIntegrationBuffer
{
    InitPointNC startVertex[];
};

// Shader buffer storage object of computed normal curve vertices.
layout (std430, binding=1) writeonly buffer NormalCurveLineBuffer
{
    EndPosLengthNC endVertex[];
};


// matrices
// ========
// texture holding front location equation
uniform sampler3D dataVolume; // SIGMA_HYBRID_PRESSURE | PRESSURE_LEVELS // fle grid
uniform sampler2D surfacePressure; // SIGMA_HYBRID_PRESSURE
uniform sampler1D hybridCoefficients; // SIGMA_HYBRID_PRESSURE
uniform sampler1D pressureTable; // FOR PRESSURE_LEVELS only
uniform sampler2D pressureTexCoordTable2D; // HYBRID_SIGMA
uniform sampler3D auxPressureField3D_hPa; // AUXILIARY_PRESSURE_3D
uniform sampler1D transferFunction;
uniform sampler1D lonLatLevAxes;

// texture holding gradient x of detection variable
uniform sampler3D dataVolumeDVgradX; // SIGMA_HYBRID_PRESSURE | PRESSURE_LEVELS // integration / detection var grid
uniform sampler2D surfacePressureDVgradX; // SIGMA_HYBRID_PRESSURE
uniform sampler1D hybridCoefficientsDVgradX; // SIGMA_HYBRID_PRESSURE
uniform sampler1D pressureTableDVgradX; // FOR PRESSURE_LEVELS only
uniform sampler2D pressureTexCoordTable2DDVgradX; // HYBRID_SIGMA
uniform sampler3D auxPressureField3DDVgradX_hPa; // AUXILIARY_PRESSURE_3D
uniform sampler1D transferFunctionDVgradX;
uniform sampler1D lonLatLevAxesDVgradX;

// texture holding gradient x of detection variable

uniform sampler3D dataVolumeDVgradY; // SIGMA_HYBRID_PRESSURE | PRESSURE_LEVELS // integration / detection var grid
uniform sampler2D surfacePressureDVgradY; // SIGMA_HYBRID_PRESSURE
uniform sampler1D hybridCoefficientsDVgradY; // SIGMA_HYBRID_PRESSURE
uniform sampler1D pressureTableDVgradY; // FOR PRESSURE_LEVELS only
uniform sampler2D pressureTexCoordTable2DDVgradY; // HYBRID_SIGMA
uniform sampler3D auxPressureField3DDVgradY_hPa; // AUXILIARY_PRESSURE_3D
uniform sampler1D transferFunctionDVgradY;
uniform sampler1D lonLatLevAxesDVgradY;

// scalars
// =======

uniform float   integrationStepSize; // Integration step size
uniform float   minValue;
uniform uint    bisectionSteps;
uniform int     numNormalCurves;


// Pressure conversion to WorldZ
// ==============
uniform vec2    pToWorldZParams;

// Normal Curve specific
// =====================

uniform float   isoValueStop;

// BBox
uniform vec2 minBBox;
uniform vec2 maxBBox;

// dummy variables for included shaders. Not needed to set them.
uniform int numIsoValues;
uniform float isoValues[1];
uniform mat4 mvpMatrix;




/*****************************************************************************
 ***                             INCLUDES
 *****************************************************************************/
// include missing values
#include "shared/msharedconstants.h"
// include global definitions
#include "volume_defines.glsl"
// include global structs
#include "volume_global_structs_utils.glsl"
// include hybrid model volume sampling methods
#include "volume_hybrid_utils.glsl"
// include model level volume with auxiliary pressure field sampling methods
#include "volume_auxiliarypressure_utils.glsl"
// include pressure levels volume sampling methods
#include "volume_pressure_utils.glsl"
// defines subroutines and auxiliary ray-casting functions
#include "volume_sample_utils.glsl"


// uniform definitions after include statements
uniform DataVolumeExtent dataExtentDVgradX;
uniform DataVolumeExtent dataExtentDVgradY;
/*****************************************************************************
 ***                              UTILS
 *****************************************************************************/

vec2 sample2DGradientsAtPos(in vec3 pos)
{
    vec2 gradientXY;
    // case PRESSURE_LEVEL_3D
    if (dataExtentDVgradX.levelType == 0)
    {
         gradientXY.x = samplePressureLevelVolumeAtPos(dataVolumeDVgradX, dataExtentDVgradX,
                                                       pressureTableDVgradX, pos);
         gradientXY.y = samplePressureLevelVolumeAtPos(dataVolumeDVgradY, dataExtentDVgradY,
                                                       pressureTableDVgradY, pos);
    }
    // case HYBRID_SIGMA_PRESSURE_3D
    else if (dataExtentDVgradX.levelType == 1)
    {
        gradientXY.x = sampleHybridSigmaVolumeAtPos(dataVolumeDVgradX, dataExtentDVgradX,
                                                    surfacePressureDVgradX, hybridCoefficientsDVgradX,
                                                    pos);
        gradientXY.y = sampleHybridSigmaVolumeAtPos(dataVolumeDVgradY, dataExtentDVgradY,
                                                    surfacePressureDVgradY, hybridCoefficientsDVgradY,
                                                    pos);
    }
    // case AUXILIARY_PRESSURE_3D
    else if (dataExtentDVgradX.levelType == 2)
    {
        gradientXY.x = sampleAuxiliaryPressureVolumeAtPos(dataVolumeDVgradX, dataExtentDVgradX,
                                                          auxPressureField3DDVgradX_hPa, pos);
        gradientXY.y = sampleAuxiliaryPressureVolumeAtPos(dataVolumeDVgradY, dataExtentDVgradY,
                                                          auxPressureField3DDVgradY_hPa, pos);
    }
    return gradientXY;
}

// Correct the position of any detected iso-surface by using the
// bisection algorithm.
vec3 bisectionCorrection(in vec3 position,
                         in vec3 prevPosition)
{
    vec3 centerPosition;
    float locatorCenter = 0.0;

    for (int i = 0; i < bisectionSteps; ++i)
    {
        centerPosition = (position + prevPosition) / 2.0;
        locatorCenter = sampleDataAtPos(centerPosition);

        if (locatorCenter <= 0)
        {
            position = centerPosition;
        }
        else
        {
            prevPosition = centerPosition;
        }
    }

    centerPosition = (position + prevPosition) / 2.0;
    return centerPosition;
}


void integrateAlongGradient(uint pointIndex)
{
    int index = int(pointIndex);
    vec4 normalCurve;
    const int maxNumIterations = int(3000 / (DELTA_LAT_KM * integrationStepSize));

    float normalCurveLength_km = 0; // KM
    vec3 currentPos = startVertex[index].position.xyz;
    // Compute z-world coordinate
    float worldZ = (log(currentPos.z) - pToWorldZParams.x) * pToWorldZParams.y;
    currentPos.z = worldZ;

    vec3 gradient = vec3(.0, .0, .0);
    vec3 prevPos = vec3(.0, .0, .0);

    // write current heigth to normal curve end position
    endVertex[index].position.z = currentPos.z;

    float tfp = startVertex[index].position.w;
    int direction = (tfp < 0) ? -1 : 1;

    for (int cc = 0; cc < maxNumIterations; ++cc)
    {
        gradient.xy = sample2DGradientsAtPos(currentPos);
        gradient = normalize(gradient);
        prevPos = currentPos;
        currentPos -= gradient * integrationStepSize * direction;

        // if next position out of boundary or in an area where
        // fle is not defined return normal curve vertices.
        if(sampleDataAtPos(currentPos) < minValue
           || currentPos.x < minBBox.x || currentPos.x > maxBBox.x
           || currentPos.y < minBBox.y || currentPos.y > maxBBox.y)
        {
            endVertex[index].position.w = normalCurveLength_km;
            endVertex[index].position.xy = currentPos.xy;
            return;
        }

        // check if next position is out of frontal zone, that is, if the
        // fle field is larger or equal to zero
        if (sampleDataAtPos(currentPos) <= isoValueStop)
        {
            // compute a bisection correction
            currentPos = bisectionCorrection(currentPos,
                                             prevPos);
            // calculate length of normal curve segment and add to total length
            vec3 diffPos = currentPos - prevPos;
            float deltaLonKM = DELTA_LAT_KM * cos(currentPos.y / 180.0 * M_PI);
            diffPos.x = diffPos.x * deltaLonKM;
            diffPos.y = diffPos.y * DELTA_LAT_KM;
            diffPos.z = 0;
            normalCurveLength_km += length(diffPos);
            endVertex[index].position.xy = currentPos.xy;
            endVertex[index].position.w = normalCurveLength_km;
            return;
        }

        // calculate length of normal curve segment and add to total length
        vec3 diffPos = currentPos - prevPos;
        float deltaLonKM = DELTA_LAT_KM * cos(currentPos.y / 180.0 * M_PI);
        diffPos.x = diffPos.x * deltaLonKM;
        diffPos.y = diffPos.y * DELTA_LAT_KM;
        diffPos.z = 0;
        normalCurveLength_km += length(diffPos);
    }

    endVertex[index].position.xy = currentPos.xy;
    endVertex[index].position.w = normalCurveLength_km;
}


/*****************************************************************************
 ***                           COMPUTE SHADER
 *****************************************************************************/

// Shader that integrates only in one certain direction
shader CSsingleIntegration()
{
    uint pointIndex = gl_GlobalInvocationID.x;

    if (pointIndex >= numNormalCurves) { return; }

//    computeLine(pointIndex);
    integrateAlongGradient(pointIndex);
}

/*****************************************************************************
 ***                             PROGRAMS
 *****************************************************************************/

program SingleIntegration
{
    cs(430)=CSsingleIntegration() : in(local_size_x = 32);
};
