/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MSEQUENCERTABLEMODEL_H
#define MSEQUENCERTABLEMODEL_H

// standard library imports

// related third party imports
#include <QAbstractTableModel>
#include <QMessageBox>
#include <QTableView>

// local application imports
#include "mcamerasequence.h"

namespace Met3D
{

/** Column count of this table model. Must be the same length as @ref Columns */
#define COLUMN_COUNT 8

/**
  This class implements a table model to show the keys for an animation sequence
  to the user.
 */
class MSequencerTableModel : public QAbstractTableModel
{
Q_OBJECT

public:
    /**
      Enum specifying the colums of this table model.
     */
    enum Columns
    {
        LON,
        LAT,
        Z,
        PITCH,
        YAW,
        ROLL,
        TELEPORT_TRANSITION,
        NEXT_TIMESTEP
    };

    explicit MSequencerTableModel(QObject *parent = nullptr);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const;

    int columnCount(const QModelIndex &parent = QModelIndex()) const;

    bool insertRows(int position, int rows, const QModelIndex &index, QList<MSequenceKey *> *);

    bool removeRows(int row, int count, const QModelIndex &parent);

    bool moveRows(const QModelIndex &sourceParent, int sourceRow, int count, const QModelIndex &destinationParent,
                  int destinationChild) override;

    QVariant data(const QModelIndex &index, int role) const;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role);

    Qt::ItemFlags flags(const QModelIndex &index) const;

    /**
      Set the sequence currently displayed in this table model.
      @param sequence A pointer to the sequence that should be displayed in this table model.
     */
    void setSequence(MCameraSequence *sequence);

    /**
       @return The sequence currently displayed in this table model.
     */
    MCameraSequence *getSequence() const;

    /**
      @return The corresponding key from the camera sequence for the row @param row of the table.
     */
    MSequenceKey *getKeyForRow(int row) const;

    /**
      Update displayed contents of the specified @param row.
     */
    void updateRow(QModelIndex &row);

private:
    /**
      The currently displayed camera sequence.
     */
    MCameraSequence *sequence;
};

}

#endif // MSEQUENCERTABLEMODEL_H
