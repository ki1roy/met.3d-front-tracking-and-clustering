/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022-2023 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "mcamerasequence.h"

// standard library imports

// related third party imports
#include <QtXml/QDomDocument>
#include <QFileInfo>
#include <log4cplus/loggingmacros.h>

// local application imports
#include "gxfw/msystemcontrol.h"
#include "util/mutil.h"
#include "util/mexception.h"

namespace Met3D
{

MCameraSequence::MCameraSequence(bool registerInSysControl) :
        sequenceName("Sequence"),
        loopSequence(false),
        sequenceRuntime(10)
{
    if (registerInSysControl)
    {
        registerSequence();
    }
}


MCameraSequence::~MCameraSequence()
{
    for (int i = 0; i < keys.length(); i++)
    {
        delete keys[i];
    }

    keys.clear();
}


void MCameraSequence::saveToFile(QString fileName)
{
    QDomDocument document("CameraSequence");
    QDomElement seq_el = document.createElement("CameraSequence");
    document.appendChild(seq_el);
    seq_el.setAttribute("name", sequenceName);
    seq_el.setAttribute("loop", (uint) loopSequence);
    seq_el.setAttribute("runtime", (double) sequenceRuntime);

        for (MSequenceKey *key : keys)
        {
            QDomElement key_el = document.createElement("SequenceKey");
            seq_el.appendChild(key_el);

            key_el.setAttribute("lon", key->position.x());
            key_el.setAttribute("lat", key->position.y());
            key_el.setAttribute("z", key->position.z());
            key_el.setAttribute("pitch", key->rotation.x());
            key_el.setAttribute("yaw", key->rotation.y());
            key_el.setAttribute("roll", key->rotation.z());
            key_el.setAttribute("transition", (uint) key->transition);
            key_el.setAttribute("advanceTimestep", key->advanceTimestep);
        }

    QFile file(fileName);
    file.open(QIODevice::WriteOnly);
    QTextStream textStream(&file);
    document.save(textStream, 2);
    file.close();
    this->lastFileName = QString(fileName);

}


void MCameraSequence::loadFromFile(QString fileName)
{
    // Check if file exists.
    if (!QFile::exists(fileName))
    {
        QString msg = QString("ERROR: Cannot open camera sequence file %1").arg(fileName);
        LOG4CPLUS_ERROR(mlog, msg.toStdString());
        throw MInitialisationError(msg.toStdString(), __FILE__, __LINE__);
    }

    this->lastFileName = fileName;

    QFile file;
    file.setFileName(fileName);
    file.open(QFile::ReadOnly);

    QDomDocument document;
    document.setContent(file.readAll());

    file.close();

    QDomElement seq_el = document.elementsByTagName("CameraSequence").at(0).toElement();
    sequenceName = seq_el.attribute("name");
    loopSequence = (bool) seq_el.attribute("loop").toUInt();
    sequenceRuntime = (float) seq_el.attribute("runtime").toDouble();

    if (sequenceName.isEmpty())
    {
        sequenceName = "Sequence";
    }

    this->keys.clear();

    QDomNodeList sequence_key_nodes = document.elementsByTagName("SequenceKey");
    for (int i = 0; i < sequence_key_nodes.count(); i++)
    {
        QDomElement key_el = sequence_key_nodes.at(i).toElement();

        MSequenceKey *key = new MSequenceKey;
        key->position.setX(key_el.attribute("lon").toDouble());
        key->position.setY(key_el.attribute("lat").toDouble());
        key->position.setZ(key_el.attribute("z").toDouble());
        key->rotation.setX(key_el.attribute("pitch").toDouble());
        key->rotation.setY(key_el.attribute("yaw").toDouble());
        key->rotation.setZ(key_el.attribute("roll").toDouble());
        key->transition = (MCameraTransition) key_el.attribute("transition").toUInt();
        key->advanceTimestep = (bool) key_el.attribute("advanceTimestep").toInt();

        this->keys.append(key);
    }
}


void MCameraSequence::saveToSettings(QSettings *settings) const
{
    settings->setValue("name", sequenceName);
    settings->setValue("loop", loopSequence);
    settings->setValue("runtime", sequenceRuntime);
    settings->beginWriteArray("SequenceKeys");
    MSequenceKey *key;
    for (int i = 0; i < this->keys.length(); ++i)
    {
        settings->setArrayIndex(i);
        key = keys[i];

        settings->setValue("lon", key->position.x());
        settings->setValue("lat", key->position.y());
        settings->setValue("z", key->position.z());
        settings->setValue("pitch", key->rotation.x());
        settings->setValue("yaw", key->rotation.y());
        settings->setValue("roll", key->rotation.z());
        settings->setValue("transition", (uint) key->transition);
        settings->setValue("advanceTimeStep", key->advanceTimestep);
    }

    settings->endArray();
}


void MCameraSequence::loadFromSettings(QSettings *settings)
{
    this->keys.clear();

    sequenceName = settings->value("name", "Sequence").toString();
    loopSequence = settings->value("loop", false).toBool();
    sequenceRuntime = settings->value("runtime", 10).toFloat();

    int keyCount = settings->beginReadArray("SequenceKeys");

    for (int i = 0; i < keyCount; i++)
    {
        settings->setArrayIndex(i);

        MSequenceKey *key = new MSequenceKey;

        key->position.setX(settings->value("lon").toFloat());
        key->position.setY(settings->value("lat").toFloat());
        key->position.setZ(settings->value("z").toFloat());
        key->rotation.setX(settings->value("pitch").toFloat());
        key->rotation.setY(settings->value("yaw").toFloat());
        key->rotation.setZ(settings->value("roll").toFloat());
        key->transition = (MCameraTransition) settings->value("transition").toUInt();
        key->advanceTimestep = settings->value("advanceTimeStep").toBool();

        keys.append(key);
    }

    settings->endArray();
}


QString MCameraSequence::getFileName() const
{
    if (lastFileName.length() == 0)
        return nullptr;
    return lastFileName;
}


void MCameraSequence::registerSequence()
{
    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();

    sysMC->registerCameraSequence(this);
}

}
