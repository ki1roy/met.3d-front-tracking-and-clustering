/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022 Andreas Beckert
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "pythontssactor.h"
#include "gxfw/mscenecontrol.h"

using namespace std;
namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MPythonTSSActor::MPythonTSSActor()
    : MActor(),
      trackedVar(nullptr),
      pyTSSScriptName(""),
      netCDFFilename(""),
      lineWidth(2),
      pointSize(8),
      vboTrackedPositions(nullptr),
      currentEnsMember(0),
      pyFullAttributesScriptName(""),
      saveFullAttributesFilename(""),
      autoFullAttributesExportProperty(nullptr),
      autoFullAttributesExport(false),
      autoFrontTracking(false)
{
    // enablePicking(true);

    // Create and initialise QtProperties for the GUI.
    // ===============================================
    beginInitialiseQtProperties();

    setActorType(staticActorType());
    setName(getActorType());

    lineWidthProperty = addProperty(DOUBLE_PROPERTY,
                                    "line width",
                                    actorPropertiesSupGroup);

    pointSizeProperty = addProperty(DOUBLE_PROPERTY,
                                    "point size",
                                    actorPropertiesSupGroup);

    exportVarSelectionProperty = addProperty(CLICK_PROPERTY,
                                             "select external variables",
                                             actorPropertiesSupGroup);
    exportVarNameProperty = addProperty(STRING_PROPERTY,
                                        "selected variables",
                                        actorPropertiesSupGroup);
    properties->mString()->setValue(exportVarNameProperty,
                                    exportVarName);
    exportVarNameProperty->setEnabled(false);


    // Property group: Python TSS Interface
    removeLastTrackedPositionProperty = addProperty(CLICK_PROPERTY, "remove last postition",
                                        actorPropertiesSupGroup);

    resetTrackingProperty = addProperty(CLICK_PROPERTY, "reset tracking",
                                        actorPropertiesSupGroup);

    pyTSSScriptSelectionProperty = addProperty(CLICK_PROPERTY,
                                               "select python file",
                                               actorPropertiesSupGroup);

    pyTSSScriptNameProperty = addProperty(STRING_PROPERTY,
                                          "python file name",
                                          actorPropertiesSupGroup);
    properties->mString()->setValue(pyTSSScriptNameProperty, pyTSSScriptName);
    pyTSSScriptNameProperty->setEnabled(false);

    netCDFFilenameProperty = addProperty(STRING_PROPERTY,
                                         "NetCDF Filename",
                                         actorPropertiesSupGroup);
    properties->mString()->setValue(netCDFFilenameProperty, netCDFFilename);

    executePyTSSInterfaceProperty = addProperty(CLICK_PROPERTY,
                                                "execute python export",
                                                actorPropertiesSupGroup);


    pyFullAttributesScriptSelectionProperty = addProperty(CLICK_PROPERTY,
                                                          "select attribute export py file",
                                                          actorPropertiesSupGroup);

    pyFullAttributesScriptNameProperty = addProperty(STRING_PROPERTY,
                                                     "attribute py file name",
                                                     actorPropertiesSupGroup);
    properties->mString()->setValue(pyFullAttributesScriptNameProperty, pyFullAttributesScriptName);
    pyFullAttributesScriptNameProperty->setEnabled(false);

    saveFullAttributesFilenameProperty = addProperty(STRING_PROPERTY,
                                                     "Export filename prefix",
                                                     actorPropertiesSupGroup);
    properties->mString()->setValue(saveFullAttributesFilenameProperty, saveFullAttributesFilename);

    executePyFullAttributesInterfaceProperty = addProperty(CLICK_PROPERTY,
                                                           "execute attribute python export",
                                                           actorPropertiesSupGroup);

    autoFullAttributesExportProperty = addProperty(BOOL_PROPERTY, "auto stats export",
                                                   actorPropertiesSupGroup);
    properties->mBool()->setValue(autoFullAttributesExportProperty, autoFullAttributesExport);

    autoFrontTrackingProperty = addProperty(BOOL_PROPERTY, "auto front tracking",
                                            actorPropertiesSupGroup);
    properties->mBool()->setValue(autoFrontTrackingProperty, autoFrontTracking);

    endInitialiseQtProperties();
}

MPythonTSSActor::~MPythonTSSActor()
{
}


#define SHADER_VERTEX_ATTRIBUTE 0

void MPythonTSSActor::reloadShaderEffects()
{
    LOG4CPLUS_DEBUG(mlog, "loading shader programs" << flush);

    beginCompileShaders(1);

    compileShadersFromFileWithProgressDialog(
                glLinesShader,
                "src/glsl/simple_coloured_geometry.fx.glsl");

    endCompileShaders();
}


void MPythonTSSActor::saveConfiguration(QSettings *settings)
{
    settings->beginGroup(MPythonTSSActor::getSettingsID());
    // python tss interface
    settings->setValue("pyScriptName", pyTSSScriptName);
    settings->setValue("netCDFFilename", netCDFFilename);
    settings->setValue("lineWidth", lineWidth);
    settings->setValue("pointSize", pointSize);
    settings->setValue("pyFullAttributesScriptName", pyFullAttributesScriptName);
    settings->setValue("saveFullAttributesFilename", saveFullAttributesFilename);
    settings->setValue("autoFullAttributesExport", autoFullAttributesExport);
    settings->setValue("autoFrontTracking", autoFrontTracking);
    settings->endGroup(); // MPythonTSSActor
}


void MPythonTSSActor::loadConfiguration(QSettings *settings)
{
    settings->beginGroup(MPythonTSSActor::getSettingsID());
    // python tss interface
    properties->mString()->setValue(pyTSSScriptNameProperty,
                                    settings->value("pyScriptName").toString());
    properties->mString()->setValue(netCDFFilenameProperty,
                                    settings->value("netCDFFilename").toString());
    lineWidth = settings->value("lineWidth").toFloat();
    properties->mDouble()->setValue(lineWidthProperty, lineWidth);
    pointSize = settings->value("pointSize").toFloat();
    properties->mDouble()->setValue(pointSizeProperty, pointSize);

    properties->mString()->setValue(pyFullAttributesScriptNameProperty,
                                    settings->value("pyFullAttributesScriptName").toString());
    properties->mString()->setValue(saveFullAttributesFilenameProperty,
                                    settings->value("saveFullAttributesFilename").toString());
    autoFullAttributesExport = settings->value("autoFullAttributesExport").toBool();
    properties->mBool()->setValue(autoFullAttributesExportProperty, autoFullAttributesExport);
    autoFrontTracking = settings->value("autoFrontTracking").toBool();
    properties->mBool()->setValue(autoFrontTrackingProperty, autoFrontTracking);

    settings->endGroup();
}


void MPythonTSSActor::updateLabels()
{
    removeAllLabels();
    labels = QList<MLabel*>();

    MGLResourcesManager* glRM = MGLResourcesManager::getInstance();
    MTextManager* tm = glRM->getTextManager();

    foreach (MPythonTSSActor::VariableTracking v, trackingList)
    {
        if(v.member == currentEnsMember)
        {
            positionLabel = tm->addText(v.label, MTextManager::LONLATP,
                                        v.position.x(), v.position.y(), v.position.z(),
                                        properties->mInt()->value(labelSizeProperty),
                                        v.color, MTextManager::BASELINELEFT,
                                        properties->mBool()->value(labelBBoxProperty),
                                        properties->mColor()->value(labelBBoxColourProperty)
                                        );
            labels.append(positionLabel);
        }
    }
    emit labelChanged();
    emitActorChangedSignal();
}


QVector<MPythonTSSActor::VariableTracking> MPythonTSSActor::popTracksOfIdentifier(int mem, QDateTime time)
{
    QVector<MPythonTSSActor::VariableTracking> tracksEqualIdentifier;
    int t = 0;
    foreach (VariableTracking tracks, trackingList)
    {
        if (tracks.member == mem && tracks.time == time)
        {
            tracksEqualIdentifier.append(tracks);
            trackingList.removeAt(t);
        }
        t += 1;
    }
    updateLabels();
    sortTrackingList();
    return tracksEqualIdentifier;
}


bool MPythonTSSActor::triggerAnalysisHorizontalSection(QVector3D pos)
{
    if (!trackedVar || !trackedVar->grid) {return false;}

    float value = trackedVar->grid->interpolateValue(pos);
    int mem = trackedVar->getEnsembleMember();
    QDateTime time = trackedVar->getSynchronizationControl()->validDateTime();
    updateDisplayedEnsMember(mem);
    saveTrackedPosition(pos, value, mem, time);
    return true;
}


bool MPythonTSSActor::triggerAnalysis3DFronts(QVector3D pos, QMap<QString, float> metrics, int mem, QDateTime time)
{
    updateDisplayedEnsMember(mem);
    save3DFrontMetrics(pos, metrics, mem, time);
    updateLabels();
    return true;
}


bool MPythonTSSActor::initFrontMetaAttributes(int mem, QDateTime time)
{
    attributes.time = std::move(time);
    attributes.member = mem;
    return true;
}


bool MPythonTSSActor::addFrontPatchAttributes(QMap<QString, float> metrics)
{
    attributes.metrics.append(metrics);
    return true;
}


void MPythonTSSActor::removeLastTrackedPosition()
{
    if (trackingList.size() > 0)
    {
        trackingList.removeLast();
    }

    sortTrackingList();
    updateLabels();
    emitActorChangedSignal();
}


void MPythonTSSActor::removeLast3DFrontMetrics()
{
    if (trackingList.size() > 0)
    {
        trackingList.removeLast();
    }

    sortTrackingList();
    updateLabels();
    emitActorChangedSignal();
}


void MPythonTSSActor::resetTracking()
{

    trackingList.clear();
    sortedTracks.clear();
    updateLabels();
    emitRemoveTrackingSignal();
    emitActorChangedSignal();
}

void MPythonTSSActor::updateDisplayedEnsMember(int ens)
{
    currentEnsMember = ens;
    updateLabels();
}


void MPythonTSSActor::initializeActorResources()
{
    MGLResourcesManager* glRM = MGLResourcesManager::getInstance();
    bool loadShaders = false;

    loadShaders |= glRM->generateEffectProgram("simple_coloured_geometry",
                                               glLinesShader);

    if (loadShaders) reloadShaderEffects();
}


void MPythonTSSActor::onQtPropertyChanged(QtProperty *property)
{
    // python tss interface properties
    if (property == lineWidthProperty)
    {
        lineWidth = properties->mDouble()->value(
                lineWidthProperty);
    }
    else if (property == pointSizeProperty)
    {
        pointSize = properties->mDouble()->value(
                pointSizeProperty);
    }
    else if (property == exportVarSelectionProperty)
    {
        selectAuxVarExportToPython();
    }
    else if (property == exportVarNameProperty)
    {
        exportVarName = properties->mString()->value(
                exportVarNameProperty);
    }
    else if (property == removeLastTrackedPositionProperty)
    {
        removeLastTrackedPosition();
    }
    else if (property == resetTrackingProperty)
    {
        resetTracking();
    }
    else if (property == pyTSSScriptSelectionProperty)
    {
        properties->mString()->setValue(pyTSSScriptNameProperty, selectPythonFile());
    }
    else if (property == pyTSSScriptNameProperty)
    {
        pyTSSScriptName = properties->mString()->value(pyTSSScriptNameProperty);

    }
    else if (property == netCDFFilenameProperty)
    {
        netCDFFilename = properties->mString()->value(netCDFFilenameProperty);
    }
    else if (property == executePyTSSInterfaceProperty)
    {
        requestPythonTSSExport();
    }
        // attribute property changes
    else if (property == pyFullAttributesScriptSelectionProperty)
    {
        properties->mString()->setValue(pyFullAttributesScriptNameProperty, selectPythonFile());
    }
    else if (property == pyFullAttributesScriptNameProperty)
    {
        pyFullAttributesScriptName = properties->mString()->value(pyFullAttributesScriptNameProperty);

    }
    else if (property == saveFullAttributesFilenameProperty)
    {
        saveFullAttributesFilename = properties->mString()->value(saveFullAttributesFilenameProperty);
    }
    else if (property == executePyFullAttributesInterfaceProperty)
    {
        requestPythonTrackingAndFullAttributesExport(true);
    }
    else if (property == autoFullAttributesExportProperty)
    {
        autoFullAttributesExport = properties->mBool()->value(autoFullAttributesExportProperty);
        emitActorChangedSignal();
    }
    else if (property == autoFrontTrackingProperty)
    {
        autoFrontTracking = properties->mBool()->value(autoFrontTrackingProperty);
        emitActorChangedSignal();
    }
}


void MPythonTSSActor::renderToCurrentContext(MSceneViewGLWidget *sceneView)
{
    renderTracking(sceneView);
}


void MPythonTSSActor::renderTransparencyToCurrentContext(MSceneViewGLWidget *sceneView)
{
    Q_UNUSED(sceneView);
}

void MPythonTSSActor::emitRemoveTrackingSignal()
{
    emit removeTrackingSignal();
}



/******************************************************************************
***                          PRIVATE METHODS                                ***
*******************************************************************************/

void MPythonTSSActor::saveTrackedPosition(QVector3D pos, float value, int mem, QDateTime time)
{
    // look for the first 2D variable in the list and save the value at the position in the tracked value list

    VariableTracking tracked;

    QMap<QString, float> metric;
    metric["var"] = value;

    tracked.position = pos;
    tracked.metrics = metric;
    tracked.member = mem;
    tracked.time = time;
    tracked.label = QString("%1: %2").arg(mem).arg(time.toString("MM-dd hh:mm"));
    tracked.color = getEnsMemColor(mem);

    trackingList.append(tracked);
    sortTrackingList();
}


void MPythonTSSActor::save3DFrontMetrics(QVector3D pos, QMap<QString, float> metrics, int mem, QDateTime time)
{
    // look for the first 2D variable in the list and save the value at the position in the tracked value list

    VariableTracking tracked;

    tracked.position = pos;
    tracked.metrics = metrics;
    tracked.member = mem;
    tracked.time = time;
    tracked.label = QString("%1: %2").arg(mem).arg(time.toString("MM-dd hh:mm"));
    tracked.color = getEnsMemColor(mem);

    trackingList.append(tracked);

    sortTrackingList();
}


QString MPythonTSSActor::selectPythonFile(QString filename)
{
    if (filename.isEmpty())
    {
        filename = QFileDialog::getOpenFileName(
                MGLResourcesManager::getInstance(),
                "Select python file",
                MSystemManagerAndControl::getInstance()->getMet3DBaseDir().absolutePath(),
                "Python files (*.py)");

        if (filename.isEmpty())
        {
            return {""};
        }
    }
    else
    {
        if (!QFile::exists(filename))
        {
            return {""};
        }
    }


    LOG4CPLUS_DEBUG(mlog, "Selected Python script file "
                    << filename.toStdString());
    return filename;
}


void MPythonTSSActor::requestPythonTSSExport()
{
    if(trackingList.size() < 1)
    {
        return;
    }

    QList<QDateTime> uniqueTimeSteps = getUniqueTimeSteps();
    QList<int> uniqueMembers = getUniqueMembers();

    // get keys
    QList<QString> varNames = trackingList[0].metrics.keys();

    Eigen::MatrixXf lonvec(uniqueMembers.size(), uniqueTimeSteps.size());
    Eigen::MatrixXf latvec(uniqueMembers.size(), uniqueTimeSteps.size());
    Eigen::MatrixXf presvec(uniqueMembers.size(), uniqueTimeSteps.size());
    Eigen::MatrixXf auxVar(varNames.size(), uniqueMembers.size() * uniqueTimeSteps.size());

    Eigen::VectorXf timesteps(uniqueTimeSteps.size());



    for (int m = 0; m < uniqueMembers.size(); m++)
    {
        for (int t = 0; t < uniqueTimeSteps.size(); t++)
        {
            foreach (VariableTracking elem, trackingList)
            {
                if (elem.member == uniqueMembers[m]
                        && elem.time == uniqueTimeSteps[t])
                {
                    lonvec(m, t) = elem.position.x();
                    latvec(m, t) = elem.position.y();
                    presvec(m, t) = elem.position.z();
                    int varNum = 0;
                    foreach (QString k, varNames)
                    {
                        auxVar(varNum, m * uniqueTimeSteps.size() + t) = elem.metrics.value(k);
                        varNum++;
                    }
                }
            }
        }
    }


    // Prepare some meta data as string, such as init time and aux var name
    QString initT = uniqueTimeSteps[0].toString("yyyy-MM-dd hh:mm:ss");
    QString validT = uniqueTimeSteps[0].toString("yyyy-MM-dd hh:mm:ss");
    int timeDiff = uniqueTimeSteps[0].secsTo(uniqueTimeSteps[1]);

    QString unit;
    int timeDiffInUnits = 1;

    if ((timeDiff / 86400) >= 1.)
    {
        unit = "days since";
        timeDiffInUnits = int(timeDiff / 86400);
    }
    else if ((timeDiff / 3600) >= 1.)
    {
        unit = "hours since";
        timeDiffInUnits = int(timeDiff / 3600);
    }
    else if ((timeDiff == 60) >= 1.)
    {
        unit = "minutes since";
        timeDiffInUnits = int(timeDiff / 60);
    }
    else if ((timeDiff == 1) >= 1.)
    {
        unit = "seconds since";
        timeDiffInUnits = int(timeDiff / 1);
    }
    else
    {
        unit = "unknown time unit";
        timeDiffInUnits = 1;
    }


    int exportTime = 0;
    for (int t = 0; t < uniqueTimeSteps.size(); t++)
    {
        timesteps(t) = exportTime;
        exportTime += timeDiffInUnits;
    }


    QString timeUnit = QString("%1 %2").arg(unit).arg(validT);

    // pybind
    py::module pyExport;
    const char *fileName = qPrintable(pyTSSScriptName);
    std::experimental::filesystem::path p(fileName);
    const char *pybindName = qPrintable(QString::fromStdString(p.stem().string()));
    pyExport = py::module::import(pybindName);

    py::object trajectory_export;
    py::object meta_export;
    py::object plot_created;

    QString exportVarNamesString = "";
            foreach (QString s, varNames)
        {
            exportVarNamesString.append(s).append(",");
        }
    exportVarNamesString.chop(1);

    meta_export = pyExport.attr("receive_metadata")(qPrintable(initT), qPrintable(validT), qPrintable(timeUnit),
                                                    qPrintable(exportVarNamesString), qPrintable(netCDFFilename));
    LOG4CPLUS_DEBUG(mlog, "\tTrajectory metadata export Status : " << meta_export.cast<std::string>()
                                                                   << flush);

    trajectory_export = pyExport.attr("receive_trajectories")(lonvec, latvec, presvec, auxVar, timesteps);
    LOG4CPLUS_DEBUG(mlog, "\tTrajectory export status : " << trajectory_export.cast<std::string>()
                                                          << flush);

    plot_created = pyExport.attr("plot_data")();
    LOG4CPLUS_DEBUG(mlog, "\tTrajectory plot status : " << plot_created.cast<std::string>()
                                                        << flush);
}


int MPythonTSSActor::requestPythonTrackingAndFullAttributesExport(bool full_export)
{
    if (attributes.metrics.empty())
    {
        return false;
    }
    int numObjects = attributes.metrics.size();
    int numProperties = attributes.metrics[0].size();

    Eigen::MatrixXf properties(numObjects, numProperties);

    for (int m = 0; m < numObjects; m++)
    {
        int pos = 0;
        for (auto p: attributes.metrics[m])
        {
            properties(m, pos) = p;
            pos++;
        }
    }

    auto timeKey = attributes.time.toString("yyyy-MM-dd hh:mm:ss");
    auto memberKey = QString::number(attributes.member);

    int statLengths = frontalStats.tfpStats.length();

    if ((statLengths != frontalStats.strengthStats.length()) ||
            (statLengths != frontalStats.areaStats.length()) ||
            (statLengths != frontalStats.slopeStats.length()))
    {
        LOG4CPLUS_DEBUG(mlog, "Front statistic export not possible, "
                              "expected same size of statistical values but size differs.");
        return false;
    }


    int metaLength = frontalStats.vectorMeta.length();

    Eigen::VectorXf tfp_stats(statLengths);
    Eigen::VectorXf strength_stats(statLengths);
    Eigen::VectorXf area_stats(statLengths);
    Eigen::VectorXf slope_stats(statLengths);

    Eigen::VectorXi vector_meta(metaLength);

    for (int i = 0; i < statLengths; i++)
    {
        tfp_stats(i) = frontalStats.tfpStats.at(i);
        strength_stats(i) = frontalStats.strengthStats.at(i);
        area_stats(i) = frontalStats.areaStats.at(i);
        slope_stats(i) = frontalStats.slopeStats.at(i);
    }

    for (int i = 0; i < metaLength; i++)
    {
        vector_meta(i) = frontalStats.vectorMeta.at(i);
    }

    // pybind
    py::module pyExport;
    const char *fileName = qPrintable(pyFullAttributesScriptName);
    std::experimental::filesystem::path path(fileName);

    if (pyExport)
    {
        pyExport.reload(); // If already loaded reload, to take any new edits
        // in the python module
    }
    else
    {
        // import python module
        const char *pybindName = qPrintable(QString::fromStdString(path.stem().string()));
        try
        {
            pyExport = py::module::import(pybindName);
        }
        catch (py::error_already_set &e)
        {
            if (e.matches(PyExc_ModuleNotFoundError))
            {
                LOG4CPLUS_ERROR(
                        mlog, "Input file "
                        << fileName
                        << " not found! Check Met.3D build directory!");
                return false;
            }
            else
            {
                LOG4CPLUS_ERROR(mlog, e.what());
                return false;
            }
        }
    }


    py::object meta_export;
    py::object property_export;
    py::object stats_export;
    py::object save_to_pickle;
    py::object track_front;

    QList<QString> varNames = attributes.metrics[0].keys();
    QString exportVarNamesString = "";
    for (const auto &s: varNames)
    {
        exportVarNamesString.append(s).append(",");
    }
    exportVarNamesString.chop(1);

    // meta data export, including identifier.
    meta_export = pyExport
            .attr("receive_metadata")(qPrintable(timeKey), qPrintable(memberKey), qPrintable(exportVarNamesString),
                                      qPrintable(saveFullAttributesFilename));
    LOG4CPLUS_DEBUG(mlog, "\tStatistic metadata export Status : " << meta_export.cast<std::string>()
                                                                  << flush);

    property_export = pyExport.attr("receive_properties")(properties);
    LOG4CPLUS_DEBUG(mlog, "\tProperty export status : " << property_export.cast<std::string>()
                                                        << flush);

    stats_export = pyExport.attr("receive_front_statistic")(tfp_stats, strength_stats, area_stats, slope_stats,
                                                            vector_meta);
    LOG4CPLUS_DEBUG(mlog, "\tData export status : " << stats_export.cast<std::string>()
                                                    << flush);

    if (autoFullAttributesExport || full_export)
    {
        save_to_pickle = pyExport.attr("save_to_pickle")();
        LOG4CPLUS_DEBUG(mlog, "\tStatistics saved to pickle successful : " << save_to_pickle.cast<std::string>()
                                                                           << flush);
    }

    if (autoFrontTracking)
    {
        Eigen::VectorXf prev_tfp_stats(prevFrontalTFPStats.length());
        Eigen::VectorXf prevCentroid_xy(2);
        Eigen::VectorXf centroids_x(attributes.metrics.length());
        Eigen::VectorXf centroids_y(attributes.metrics.length());

        prevCentroid_xy(0) = prevCentroid.x();
        prevCentroid_xy(1) = prevCentroid.y();

        for (int i = 0; i < prevFrontalTFPStats.length(); i++)
        {
            prev_tfp_stats(i) = prevFrontalTFPStats.at(i);
        }

        for (int i = 0; i < attributes.metrics.length(); i++)
        {
            centroids_x(i) = attributes.metrics[i]["centroid_x"];
            centroids_y(i) = attributes.metrics[i]["centroid_y"];
        }

        track_front = pyExport.attr("track_front")(prev_tfp_stats, prevCentroid_xy, centroids_x, centroids_y);
        LOG4CPLUS_DEBUG(mlog, "\tFound similar front : " << track_front.cast<std::string>()
                                                         << flush);
        string track_nr = track_front.cast<std::string>();

        return stoi(track_nr);
    }

    return -999;
}


QList<QDateTime> MPythonTSSActor::getUniqueTimeSteps()
{
    QList<QDateTime> uniqueTimeSteps;
            foreach (VariableTracking elem, trackingList)
        {
            if (!uniqueTimeSteps.contains(elem.time))
            {
                uniqueTimeSteps.append(elem.time);
            }
        }
    return uniqueTimeSteps;
}


QList<int> MPythonTSSActor::getUniqueMembers()
{
    {
        QList<int> uniqueMembers;
        foreach (VariableTracking elem, trackingList)
        {
            if (!uniqueMembers.contains(elem.member))
            {
                uniqueMembers.append(elem.member);
            }
        }
        return uniqueMembers;
    }
}


void MPythonTSSActor::renderTracking(MSceneViewGLWidget *sceneView)
{
    if (trackingList.empty())
    {
        return;
    }

    glLinesShader->bindProgram("Pressure");
    glLinesShader->setUniformValue(
                "mvpMatrix",
                *(sceneView->getModelViewProjectionMatrix()));
    glLinesShader->setUniformValue(
                "pToWorldZParams", sceneView->pressureToWorldZParameters());
    for (auto &sortedTrack: sortedTracks)
    {
        QVector3D color = sortedTrack.first();
        QVector4D col = QVector4D(color, 1);
        QVector<QVector3D> renderPoints = sortedTrack;
        renderPoints.pop_front();

        glLinesShader->setUniformValue(
                "colour", col);

        int nPos = renderPoints.size();
        const QString vboIDs = QString("cycloneTracks#%1").arg(myID);
        uploadVec3ToVertexBuffer(renderPoints, vboIDs, &vboTrackedPositions,
                                 sceneView);

        glBindBuffer(GL_ARRAY_BUFFER, vboTrackedPositions->getVertexBufferObject()); CHECK_GL_ERROR;
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(QVector3D),
                              nullptr); CHECK_GL_ERROR;

        glDepthFunc(GL_ALWAYS);
        if (nPos > 1)
        {
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            CHECK_GL_ERROR;
            glLineWidth(lineWidth);
            CHECK_GL_ERROR;
            glDrawArrays(GL_LINE_STRIP, 0, nPos);
            CHECK_GL_ERROR;
        }

        glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
        CHECK_GL_ERROR;
        glPointSize(pointSize);
        CHECK_GL_ERROR;
        glDrawArrays(GL_POINTS, 0, nPos);
        CHECK_GL_ERROR;

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        CHECK_GL_ERROR;
        glDepthFunc(GL_LESS);
    }
}


void MPythonTSSActor::sortTrackingList()
{
    sortedTracks = QVector<QVector<QVector3D>>();
    QList<int> uniqueMembers = getUniqueMembers();
    std::sort(uniqueMembers.begin(), uniqueMembers.end(),
              [](const int &a, const int &b)
              { return a < b; });

    for (auto mem: uniqueMembers)
    {
        QVector<QVector3D> memberTrackPoints;
        QColor color = getEnsMemColor(mem);
        memberTrackPoints.append(QVector3D(color.red() / 255, color.green() / 255, color.blue() / 255));
        for (auto &i: trackingList)
        {
            if (i.member == mem)
            {
                memberTrackPoints.append(i.position);
            }
        }
        sortedTracks.append(memberTrackPoints);
    }
}


QColor MPythonTSSActor::getEnsMemColor(int mem)
{
    int code = mem % 10;

    switch(code)
    {
    case 0:
        return {"black"};
    case 1:
        return {"red"};
    case 2:
        return {"cyan"};
    case 3:
        return {"magenta"};
    case 4:
        return {"green"};
    case 5:
        return {"yellow"};
    case 6:
        return {"blue"};
    case 7:
        return {"gray"};
    case 8:
        return {"darkCyan"};
    case 9:
        return {"darkMagenta"};
    default:
        return {"black"};
    }
}


void MPythonTSSActor::selectAuxVarExportToPython()
{
    // Selecting multiple variables from an actor
    MVariableSelectionDialog dlg;
    if (variables.empty())
    { return; }

    QList<QString> availVars;
    for (auto var: variables)
    {
        availVars.append(var->variableName);
    }

    dlg.setAvailableVariables(availVars);
    if (dlg.exec() == QDialog::Accepted)
    {
        QList<QString> selVariables = dlg.getSelectedVariables();
        if (!selVariables.isEmpty())
        {
            exportVarName = selVariables.first();
            properties->mString()->setValue(exportVarNameProperty,
                                            exportVarName);

        }
    }

    for (auto var: variables)
    {
        if (var->variableName == exportVarName)
        {
            trackedVar = var;
        }
    }
}


void MPythonTSSActor::clearStatistic()
{
    attributes = AttributeList();
    frontalStats = StatsList();
}


void MPythonTSSActor::addTfpStats(const QVector<float> &stats)
{
    frontalStats.tfpStats.append(stats);
    frontalStats.vectorMeta.append(stats.length());
}


void MPythonTSSActor::addStrengthStats(const QVector<float> &stats)
{
    frontalStats.strengthStats.append(stats);
}


void MPythonTSSActor::addAreaStats(const QVector<float> &stats)
{
    frontalStats.areaStats.append(stats);
}


void MPythonTSSActor::addSlopeStats(const QVector<float> &stats)
{
    frontalStats.slopeStats.append(stats);
}


bool MPythonTSSActor::autoExportActive() const
{
    return autoFullAttributesExport;
}


void MPythonTSSActor::initTFPStartStats(QVector<float> vector, QVector2D centroid_xy)
{
    prevFrontalTFPStats = std::move(vector);
    prevCentroid = centroid_xy;

}


} // namespace Met3D
