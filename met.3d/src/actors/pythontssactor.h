/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022 Andreas Beckert
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef PYTHONTSSACTOR_H
#define PYTHONTSSACTOR_H

// standard library imports
// standard library imports
#include <iostream>
#include <math.h>
#include <experimental/filesystem>
#include <memory> // for use of shared_ptr

// related third party imports
#include <QObject>
#include <QFileDialog>
#include <log4cplus/loggingmacros.h>
#include <GL/glew.h>
#include <QtProperty>

#pragma push_macro("slots")
#undef slots

#include "Python.h"

#pragma pop_macro("slots")

#include <pybind11/eigen.h>

// local application imports
#include "util/mutil.h"
#include "util/mexception.h"
#include "gxfw/mglresourcesmanager.h"
#include "gxfw/msceneviewglwidget.h"
#include "gxfw/variableselectiondialog.h"
#include "gxfw/gl/typedvertexbuffer.h"
#include "data/structuredgrid.h"
#include "actors/spatial1dtransferfunction.h"
#include "gxfw/nwpactorvariable.h"
#include "gxfw/selectactordialog.h"

class MGLResourcesManager;
class MSceneViewGLWidget;

namespace py = pybind11;
namespace Met3D
{

class MNWPHorizontalSectionActor;

/**
  @brief MPythonTSSActor creates, saves and handles the export of time series for the time
  series similarity analysis
  */
class MPythonTSSActor : public MActor
{
Q_OBJECT

public:
    MPythonTSSActor();

    ~MPythonTSSActor();

    struct VariableTracking
    {
        QVector3D position;
        QMap<QString, float> metrics;
        int member;
        QDateTime time;
        QString label = QString();
        QColor color = QColor(0, 0, 0);
    };

    struct AttributeList
    {
        int member;
        QDateTime time;
        QVector<QMap<QString, float>> metrics;
        QVector<QVector<float>> tfp;
        QVector<QVector<float>> frontalStrength;
        QVector<QVector<float>> area;
        QVector<QVector<float>> slope;
    };

    struct StatsList
    {
        QVector<float> tfpStats;
        QVector<float> strengthStats;
        QVector<float> areaStats;
        QVector<float> slopeStats;
        QVector<int> vectorMeta;
    };


    static QString staticActorType()
    { return "Python time series similarity interface"; }


    void reloadShaderEffects();


    QString getSettingsID() override
    { return "PythonTSSActor"; }


    void saveConfiguration(QSettings *settings) override;

    void loadConfiguration(QSettings *settings) override;

    QVector<VariableTracking> popTracksOfIdentifier(int mem, QDateTime time);

    bool triggerAnalysisHorizontalSection(QVector3D pos);

    bool triggerAnalysis3DFronts(QVector3D pos, QMap<QString, float> metrics, int mem, QDateTime time);

    bool initFrontMetaAttributes(int mem, QDateTime time);

    bool addFrontPatchAttributes(QMap<QString, float> metrics);


    void setVerticalPosition(float pressure_hPa)
    { verticalPosition_hPa = pressure_hPa; }


    void set2DTrackedVariable(MNWP2DHorizontalActorVariable *var)
    { trackedVar = var; }


    void setActorVariables(QList<MNWPActorVariable *> vars)
    { variables = vars; }


    void removeLastTrackedPosition();

    void removeLast3DFrontMetrics();

    void resetTracking();

    void updateDisplayedEnsMember(int ens);

    void clearStatistic();

    void addTfpStats(const QVector<float> &stats);

    void addStrengthStats(const QVector<float> &stats);

    void addAreaStats(const QVector<float> &stats);

    void addSlopeStats(const QVector<float> &stats);

    bool autoExportActive() const;

    int requestPythonTrackingAndFullAttributesExport(bool full_export = false);

    void initTFPStartStats(QVector<float> vector, QVector2D centroid_xy);

signals:

    void removeTrackingSignal();

    void labelChanged();

protected:
    void initializeActorResources() override;

    void onQtPropertyChanged(QtProperty *property) override;

    void renderToCurrentContext(MSceneViewGLWidget *sceneView) override;

    void renderTransparencyToCurrentContext(MSceneViewGLWidget *sceneView) override;

    void emitRemoveTrackingSignal();

private:
    // python interface methods
    void saveTrackedPosition(QVector3D pos, float value, int mem, QDateTime time);

    void save3DFrontMetrics(QVector3D pos, QMap<QString, float> metrics, int mem, QDateTime time);

    QString selectPythonFile(QString filename = "");

    void requestPythonTSSExport();

    QList<QDateTime> getUniqueTimeSteps();

    QList<int> getUniqueMembers();

    void renderTracking(MSceneViewGLWidget *sceneView);

    void sortTrackingList();

    bool variantLessThan(const int p1, const int p2)
    { return p1 < p2; }

    static QColor getEnsMemColor(int mem);

    void selectAuxVarExportToPython();

    void updateLabels();

    std::shared_ptr<GL::MShaderEffect> glLinesShader;

    MNWPActorVariable *trackedVar;
    QList<VariableTracking> trackingList;
    QVector<QVector<QVector3D>> sortedTracks;

    // new property group
    QtProperty *pyTSSInterfacePropertyGroup;

    // select aux var property
    QtProperty *exportVarSelectionProperty;
    QtProperty *exportVarNameProperty;
    QString exportVarName;

    // select python file property
    QtProperty *pyTSSScriptSelectionProperty;
    QtProperty *pyTSSScriptNameProperty;
    QString pyTSSScriptName;

    QtProperty *netCDFFilenameProperty;
    QString netCDFFilename;

    QtProperty *lineWidthProperty;
    float lineWidth;

    QtProperty *pointSizeProperty;
    float pointSize;

    // execute and export via python interface
    QtProperty *executePyTSSInterfaceProperty;

    QtProperty *removeLastTrackedPositionProperty;

    QtProperty *resetTrackingProperty;

    GL::MVertexBuffer *vboTrackedPositions;

    float verticalPosition_hPa;

    QList<MNWPActorVariable *> variables;

    int currentEnsMember;

    // Full attribute list export to create distributions in python
    // select python file property
    QtProperty *pyFullAttributesScriptSelectionProperty;
    QtProperty *pyFullAttributesScriptNameProperty;
    QString pyFullAttributesScriptName;

    // execute and export via python interface
    QtProperty *executePyFullAttributesInterfaceProperty;

    QtProperty *saveFullAttributesFilenameProperty;
    QString saveFullAttributesFilename;

    // auto save full front statistics properties
    QtProperty *autoFullAttributesExportProperty;
    bool autoFullAttributesExport;

    // auto save full front statistics properties
    QtProperty *autoFrontTrackingProperty;
    bool autoFrontTracking;

    AttributeList attributes;

    StatsList frontalStats;

    // stats of previous batch
    QVector<float> prevFrontalTFPStats;
    QVector2D prevCentroid;
};

} // namespace Met3D
#endif // PYTHONTSSACTOR_H
