/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2018 Marc Rautenhaus
**  Copyright 2018 Michael Kern
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "frontdetectionactor.h"
#include "util/metroutines.h"

#include <QFileDialog>

// related third party imports
#include <netcdf>

using namespace Met3D;
using namespace netCDF;
using namespace netCDF::exceptions;

#include <log4cplus/loggingmacros.h>

#define qNaN (std::numeric_limits<float>::quiet_NaN())


/******************************************************************************
***                             PUBLIC METHODS                              ***
*******************************************************************************/


MFrontDetectionActor::MFrontDetectionActor()
        : MNWPMultiVarActor(),
          MBoundingBoxInterface(this, MBoundingBoxConnectionType::VOLUME),
          suppressUpdates(false),
          isComputing(false),
          applySettingsClickProperty(nullptr),
          autoComputeProperty(nullptr),
          autoCompute(false),
          computeOnGPUProperty(nullptr),
          computeOnGPU(false),
          useGeopotentialHeightProperty(nullptr),
          useGeopotentialHeight(false),
          inputVarGroupProperty(nullptr),
          detectionVariableIndexProperty(nullptr),
          detectionVariableIndex(0),
          detectionVariable(nullptr),
          windUVarIndexProp(nullptr),
          windUVariableIndex(0),
          windUVar(nullptr),
          windVVarIndexProp(nullptr),
          windVVariableIndex(0),
          windVVar(nullptr),
          zVarIndexProp(nullptr),
          zVariableIndex(0),
          zVar(nullptr),
          displayOptionsGroupProperty(nullptr),
          render3DFrontProperty(nullptr),
          render3DFront(false),
          render3DNCProperty(nullptr),
          render3DNC(false),
          render2DFrontProperty(nullptr),
          render2DFront(false),
          render2DNCProperty(nullptr),
          render2DNC(false),
          frontElevationProperty(nullptr),
          frontElevation2d_hPa(850.0),
          filterGroupProperty(nullptr),
          fpmaDistanceProperty(nullptr),
          fpmaDistanceValue_km(75.),
          transferFunctionTFPProperty(nullptr),
          transferFunctionTFP(nullptr),
          transferFunctionTFPTexUnit(-1),
          transferFunctionFSProperty(nullptr),
          transferFunctionFS(nullptr),
          transferFunctionFSTexUnit(-1),
          genericFilterGroupProperty(nullptr),
          addNormalCurveFilterProperty(nullptr),
          optionalFilterProperty(nullptr),
          transferFunctionABZProperty(nullptr),
          transferFunctionABZ(nullptr),
          transferFunctionABZTexUnit(-1),
          transferFunctionBreadthProperty(nullptr),
          transferFunctionBreadth(nullptr),
          transferFunctionBreadthTexUnit(-1),
          transferFunctionSlopeProperty(nullptr),
          transferFunctionSlope(nullptr),
          transferFunctionSlopeTexUnit(-1),
          fleIsoProperty(nullptr),
          fleIsoValue(0.0),
          appearanceGroupProperty(nullptr),
          shadingGroupProperty(nullptr),
          shadingModeProperty(nullptr),
          shadingMode(0),
          shadingVariableIndexProperty(nullptr),
          shadingVariableIndex(0),
          shadingVariable(nullptr),
          shadingVarModeProperty(nullptr),
          shadingVarMode(CHANGE_PER_100KM),
          transferFunctionShadingProperty(nullptr),
          transferFunctionShading(nullptr),
          transferFunctionShadingTexUnit(-1),
          frontsTubeRadiusProp(nullptr),
          frontsTubeRadius(0.1),
          showFrontTypesProperty(nullptr),
          showFrontTypes(false),
          lightShadowGroupProperty(nullptr),
          shadowHeightProperty(nullptr),
          shadowHeight(1045),
          shadowColorProperty(nullptr),
          shadowColor(QColor(70, 70, 70, 150)),
          lightingModeProperty(nullptr),
          lightingMode(0),
          normalCurvesGroupProp(nullptr),
          trigger3DNormalCurveFilter(false),
          trigger2DNormalCurveFilter(false),
          normalCurvesTubeRadiusProp(nullptr),
          normalCurvesTubeRadius(0.05),
          ncShadingVarIndexProp(nullptr),
          ncShadingVariableIndex(0),
          ncShadingVar(nullptr),
          seedPointSpacingProp(nullptr),
          seedPointSpacing(2),
          seedPointSpacingZProp(nullptr),
          seedPointSpacingZ(100),
          ncShadingModeProperty(nullptr),
          ncShadingMode(0),
        // Settings front selection
          frontSelectionGroupProp(nullptr),
          enableSelectionProperty(nullptr),
          enableSelection(false),
          selectTFPProperty(nullptr),
          selectTFP(0.0),
          selectStrengthProperty(nullptr),
          selectStrength(0.0),
          selectBottomPressureProperty(nullptr),
          selectBottomPressure(950),
          selectTopPressureProperty(nullptr),
          selectTopPressure(500),
          selectFrontTypeModeProperty(nullptr),
          selectFrontTypeMode(0),
          hideNonSelectedFrontsProperty(nullptr),
          hideNonSelectedFronts(false),
          showDataStatisticsProperty(nullptr),
          significantDigitsProperty(nullptr),
          significantDigits(0),
          histogramDisplayModeProperty(nullptr),

          transferFunctionNCShadingProperty(nullptr),
          transferFunctionNCShading(nullptr),
          transferFunctionNCShadingTexUnit(-1),
          useTFPTransferFunction(false),
          useFSTransferFunction(false),
          useABZTransferFunction(false),
          useSlopeTransferFunction(false),
          useBreadthTransferFunction(false),
          fleSource(nullptr),
          tfpSource(nullptr),
          abzSource(nullptr),
          partialDerivFilter(nullptr),
          frontDetection2DSource(nullptr),
          frontDetection3DSourceCPU(nullptr),
          frontDetection3DSourceGPU(nullptr),
          frontSurfaceSelection(nullptr),
          frontLineSelection(nullptr),
          frontMesh3D(nullptr),
          frontLines2D(nullptr),
          normalCurves3D(nullptr),
          normalCurves2D(nullptr),

          fronts3DSurfaceShader(nullptr),
          frontTubeShaderOIT(nullptr),
          normalCurvesTubeShaderOIT(nullptr),

          vbo3DFronts(nullptr),
          ibo3DFronts(nullptr),
          vbo3DFrontsShading(nullptr),

          vbo3DNormalCurves(nullptr),
          ibo3DNormalCurves(nullptr),
          vbo3DNormalCurvesShading(nullptr),

          vbo2DFronts(nullptr),
          ibo2DFronts(nullptr),
          vbo2DFrontsShading(nullptr),

          vbo2DNormalCurves(nullptr),
          ibo2DNormalCurves(nullptr),
          vbo2DNormalCurvesShading(nullptr),

          alphaShading3DFronts(0),
          alphaShading3DNormalCurves(0),

          alphaShading2DFronts(0),
          alphaShading2DNormalCurves(0),

          recomputeAlpha3DFronts(true),
          recomputeShading3DFronts(true),
          recomputeAlpha3DNormalCurves(true),
          recomputeShading3DNormalCurves(true),
          recomputeAlpha2DFronts(true),
          recomputeShading2DFronts(true),
          recomputeAlpha2DNormalCurves(true),
          recomputeShading2DNormalCurves(true),
          restartIndex(std::numeric_limits<u_int32_t>::max()),

          fronts2dReleased(true),
          fronts3dReleased(true),
          fronts3dGPUReleased(true),

          vboPositionCross(nullptr),
          useSelectionShading(false),
          updateSelectedShading(false),
          currentSelectedFront(nullptr)
{
    bBoxConnection =
            new MBoundingBoxConnection(this,
                                       MBoundingBoxConnectionType::VOLUME);


    // Disable picking, will be enabled when front selection is enabled
    enablePicking(false);

    // Transfer function.
    // Scan currently available actors for transfer functions. Add TFs to
    // the list displayed in the combo box of the transferFunctionProperty.
    QStringList availableTFs;
    availableTFs << "None";
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
    for (MActor *mactor: glRM->getActors())
    {
        if (MTransferFunction1D * tf = dynamic_cast<MTransferFunction1D *>(mactor))
        {
            availableTFs << tf->transferFunctionName();
        }
    }

    beginInitialiseQtProperties();

    setActorType("Front Detection Actor");
    setName(getActorType());


    /**************************************************************************
                            MAIN PROPERTIES
    **************************************************************************/

    // auto compute active fronts, a front is active when its render[2/3]DFront
    // property is activated
    autoComputeProperty = addProperty(BOOL_PROPERTY, "automatic update",
                                      actorPropertiesSupGroup);
    properties->mBool()->setValue(autoComputeProperty, autoCompute);

    computeOnGPUProperty = addProperty(BOOL_PROPERTY, "compute normal curve integration on GPU",
                                       actorPropertiesSupGroup);
    properties->mBool()->setValue(computeOnGPUProperty, computeOnGPU);

    useGeopotentialHeightProperty = addProperty(BOOL_PROPERTY, "compute slope with geopotential height?",
                                                actorPropertiesSupGroup);
    useGeopotentialHeightProperty->setToolTip(
            "Use geopotential height for as geometric height. If false, the standard ICAO atmosphere is used");
    properties->mBool()->setValue(useGeopotentialHeightProperty, useGeopotentialHeight);

    // compute active fronts, a front is active when its render[2/3]DFront
    // property is activated
    applySettingsClickProperty = addProperty(CLICK_PROPERTY, "update",
                                             actorPropertiesSupGroup);

    /**************************************************************************
                            INPUT VAR PROPERTIES
    **************************************************************************/
    inputVarGroupProperty = addProperty(GROUP_PROPERTY,
                                        "input variables",
                                        actorPropertiesSupGroup);

    detectionVariableIndexProperty = addProperty(
            ENUM_PROPERTY, "detection variable", inputVarGroupProperty);
    detectionVariableIndexProperty->setToolTip(
            "example: wet-bulb potential temperature");

    windUVarIndexProp = addProperty(
            ENUM_PROPERTY, "eastward wind (optional)", inputVarGroupProperty);
    windUVarIndexProp->setToolTip("u-wind component");

    windVVarIndexProp = addProperty(
            ENUM_PROPERTY, "northward wind (optional)", inputVarGroupProperty);
    windVVarIndexProp->setToolTip("v-wind component");

    zVarIndexProp = addProperty(
            ENUM_PROPERTY, "geopotential height (optional)", inputVarGroupProperty);
    zVarIndexProp->setToolTip("variable to calculating the slope of 3D front surfaces");

    /**************************************************************************
                            DISPLAY OPTIONS
    **************************************************************************/

    displayOptionsGroupProperty = addProperty(GROUP_PROPERTY,
                                              "display options",
                                              actorPropertiesSupGroup);

    render3DFrontProperty = addProperty(BOOL_PROPERTY, "3D fronts",
                                        displayOptionsGroupProperty);
    properties->mBool()->setValue(render3DFrontProperty, render3DFront);

    render3DNCProperty = addProperty(BOOL_PROPERTY, "3D normal curves",
                                     displayOptionsGroupProperty);
    properties->mBool()->setValue(render3DNCProperty, render3DNC);

    render2DFrontProperty = addProperty(BOOL_PROPERTY, "2D fronts",
                                        displayOptionsGroupProperty);
    properties->mBool()->setValue(render2DFrontProperty, render2DFront);

    render2DNCProperty = addProperty(BOOL_PROPERTY, "2D normal curves",
                                     displayOptionsGroupProperty);
    properties->mBool()->setValue(render2DNCProperty, render2DNC);

    frontElevationProperty = addProperty(DECORATEDDOUBLE_PROPERTY,
                                         "elevation (2d)",
                                         displayOptionsGroupProperty);
    properties->setDDouble(frontElevationProperty, frontElevation2d_hPa,
                           1., 1050., 2, 0.1, " hPa");

    /**************************************************************************
                            FILTER OPTIONS
    **************************************************************************/
    filterGroupProperty = addProperty(GROUP_PROPERTY,
                                      "filter options",
                                      actorPropertiesSupGroup);

    fpmaDistanceProperty = addProperty(DECORATEDDOUBLE_PROPERTY,
                                       "5-point-mean-axis distance",
                                       filterGroupProperty);
    properties->setDDouble(fpmaDistanceProperty, fpmaDistanceValue_km,
                           0., 1000., 1, 5, " km");
    fpmaDistanceProperty->setToolTip(
            "Set the distance between the 5-point-mean-axis. \n"
            "The distance ideally corresponds to the smoothing radius.");

    transferFunctionTFPProperty = addProperty(ENUM_PROPERTY,
                                              "transfer function TFP filter",
                                              filterGroupProperty);
    properties->mEnum()->setEnumNames(transferFunctionTFPProperty, availableTFs);
    transferFunctionTFPProperty->setToolTip(
            "Set alpha value of transfer function for fuzzy filtering.");

    transferFunctionFSProperty = addProperty(ENUM_PROPERTY,
                                             "transfer function frontal strength filter",
                                             filterGroupProperty);
    properties->mEnum()->setEnumNames(transferFunctionFSProperty, availableTFs);
    transferFunctionFSProperty->setToolTip(
            "Set alpha values of transfer function for fuzzy filtering.");

    genericFilterGroupProperty = addProperty(GROUP_PROPERTY,
                                             "generic filter",
                                             filterGroupProperty);

    addNormalCurveFilterProperty = addProperty(CLICK_PROPERTY,
                                               "add normal curve filter",
                                               genericFilterGroupProperty);
    addNormalCurveFilterProperty->setToolTip(
            "At least one generic filter is recommended, \n"
            "which filters according to the frontal strength \n"
            "(change per 100 km of detection variable).\n"
            "Replacement of ABZ filter of Hewson (1998)");

    optionalFilterProperty = addProperty(GROUP_PROPERTY,
                                         "optional filter",
                                         filterGroupProperty);

    transferFunctionABZProperty = addProperty(ENUM_PROPERTY,
                                              "transfer function abz filter",
                                              optionalFilterProperty);
    properties->mEnum()->setEnumNames(transferFunctionABZProperty, availableTFs);
    transferFunctionABZProperty->setToolTip(
            "Set alpha value of transfer function for fuzzy filtering. \n"
            "ABZ filter after Hewson (1998), estimates frontal strength");

    transferFunctionBreadthProperty = addProperty(ENUM_PROPERTY,
                                                  "transfer function breadth filter",
                                                  optionalFilterProperty);
    properties->mEnum()->setEnumNames(transferFunctionBreadthProperty, availableTFs);
    transferFunctionBreadthProperty->setToolTip(
            "Set alpha value of transfer function for fuzzy filtering. \n"
            "Breadth of frontal zone");

    transferFunctionSlopeProperty = addProperty(ENUM_PROPERTY,
                                                "transfer function filter slope (3d)",
                                                optionalFilterProperty);
    properties->mEnum()->setEnumNames(transferFunctionSlopeProperty, availableTFs);
    transferFunctionSlopeProperty->setToolTip(
            "Set alpha value of transfer function for fuzzy filtering. \n"
            "Slope of frontal frontal surface (only for 3D Fronts)");

    /**************************************************************************
                            FRONT APPEARANCE PROPERTIES
    **************************************************************************/
    appearanceGroupProperty = addProperty(GROUP_PROPERTY,
                                          "front appearance",
                                          actorPropertiesSupGroup);
    shadingGroupProperty = addProperty(GROUP_PROPERTY,
                                       "shading",
                                       appearanceGroupProperty);

    QStringList shadingModes;
    shadingModes << "Shading Var" << "Pressure" << "TFP" << "ABZ"
                 << "Slope" << "Breadth of Frontal Zone";

    shadingModeProperty = addProperty(ENUM_PROPERTY, "shading mode", shadingGroupProperty);
    properties->mEnum()->setEnumNames(shadingModeProperty, shadingModes);
    properties->mEnum()->setValue(shadingModeProperty, shadingMode);

    shadingVariableIndexProperty = addProperty(
            ENUM_PROPERTY, "shading variable", shadingGroupProperty);

    QStringList shadingVarModes;
    shadingVarModes << filterTypeToString(ABSOLUTE_CHANGE)
                    << filterTypeToString(CHANGE_PER_100KM)
                    << filterTypeToString(VALUE_AT_VERTEX);

    shadingVarModeProperty = addProperty(ENUM_PROPERTY, "shading variable mode",
                                         shadingGroupProperty);
    properties->mEnum()->setEnumNames(shadingVarModeProperty, shadingVarModes);
    properties->mEnum()->setValue(shadingVarModeProperty, shadingVarMode);


    transferFunctionShadingProperty = addProperty(ENUM_PROPERTY,
                                                  "transfer function shading",
                                                  shadingGroupProperty);
    properties->mEnum()->setEnumNames(transferFunctionShadingProperty,
                                      availableTFs);

    frontsTubeRadiusProp = addProperty(
            DOUBLE_PROPERTY, "fronts tube radius (2d)", appearanceGroupProperty);
    properties->setDouble(frontsTubeRadiusProp, frontsTubeRadius, 0.01, 1.0, 2, 0.01);

    appearanceGroupProperty->addSubProperty(bBoxConnection->getProperty());

    showFrontTypesProperty = addProperty(BOOL_PROPERTY, "show warm/cold fronts (3d)",
                                         appearanceGroupProperty);
    properties->mBool()->setValue(showFrontTypesProperty, showFrontTypes);

    lightShadowGroupProperty = addProperty(GROUP_PROPERTY,
                                           "light and shadow",
                                           appearanceGroupProperty);


    shadowColorProperty = addProperty(COLOR_PROPERTY, "shadow color", lightShadowGroupProperty);
    properties->mColor()->setValue(shadowColorProperty, shadowColor);

    shadowHeightProperty = addProperty(DOUBLE_PROPERTY, "shadow elevation", lightShadowGroupProperty);
    properties->setDouble(shadowHeightProperty, shadowHeight, 0, 1050, 0, 5);

    QStringList modesLst;
    modesLst << "double-sided" << "single-sided" << "double-sided + headlight" << "single-sided + headlight";
    lightingModeProperty = addProperty(ENUM_PROPERTY, "lighting mode", lightShadowGroupProperty);
    properties->mEnum()->setEnumNames(lightingModeProperty, modesLst);
    properties->mEnum()->setValue(lightingModeProperty, lightingMode);


    /**************************************************************************
                            Normal curves
    **************************************************************************/

    normalCurvesGroupProp = addProperty(GROUP_PROPERTY, "normal curve settings",
                                        actorPropertiesSupGroup);

    normalCurvesTubeRadiusProp = addProperty(
            DOUBLE_PROPERTY, "normal curves tube radius", normalCurvesGroupProp);
    properties->setDouble(normalCurvesTubeRadiusProp, normalCurvesTubeRadius,
                          0.01, 1.0, 2, 0.01);

    seedPointSpacingProp = addProperty(DOUBLE_PROPERTY,
                                       "seed point spacing",
                                       normalCurvesGroupProp);
    properties->setDouble(seedPointSpacingProp, seedPointSpacing,
                          0.1, 100, 1, 0.1);

    seedPointSpacingZProp = addProperty(INT_PROPERTY,
                                        "seed point spacing (z) [hPa]",
                                        normalCurvesGroupProp);
    properties->setInt(seedPointSpacingZProp, seedPointSpacingZ,
                       10, 500, 10);

    QStringList ncShadingList;
    ncShadingList << "normalCurveLength (km)" << "valueAtFrontline" << "changePer100km"
                  << "absoluteChangeWithinFrontalZone";
    ncShadingModeProperty = addProperty(ENUM_PROPERTY, "nc shading mode",
                                        normalCurvesGroupProp);
    properties->mEnum()->setEnumNames(ncShadingModeProperty, ncShadingList);
    properties->mEnum()->setValue(ncShadingModeProperty, ncShadingMode);

    ncShadingVarIndexProp = addProperty(
            ENUM_PROPERTY, "nc shading variable", normalCurvesGroupProp);

    transferFunctionNCShadingProperty = addProperty(ENUM_PROPERTY,
                                                    "transfer function nc shading",
                                                    normalCurvesGroupProp);
    properties->mEnum()->setEnumNames(transferFunctionNCShadingProperty,
                                      availableTFs);

    /**************************************************************************
                            Selection properties
    **************************************************************************/
    frontSelectionGroupProp = addProperty(GROUP_PROPERTY, "front selection settings",
                                          actorPropertiesSupGroup);

    enableSelectionProperty = addProperty(BOOL_PROPERTY, "enable front selection",
                                          frontSelectionGroupProp);
    properties->mBool()->setValue(enableSelectionProperty, enableSelection);

    selectTFPProperty = addProperty(DOUBLE_PROPERTY, "tfp threshold", frontSelectionGroupProp);
    properties->setDouble(selectTFPProperty, selectTFP, 0.0, 10.0, 2, 0.1);

    selectStrengthProperty = addProperty(DOUBLE_PROPERTY, "strength threshold", frontSelectionGroupProp);
    properties->setDouble(selectStrengthProperty, selectStrength, 0.0, 10.0, 2, 0.1);

    selectBottomPressureProperty = addProperty(INT_PROPERTY, "bottom pressure", frontSelectionGroupProp);
    properties->setInt(selectBottomPressureProperty, selectBottomPressure, 1, 1050, 5);
    selectTopPressureProperty = addProperty(INT_PROPERTY, "top pressure", frontSelectionGroupProp);
    properties->setInt(selectTopPressureProperty, selectTopPressure, 1, 1050, 5);

    QStringList selectTypeModes;
    selectTypeModes << "both" << "cold" << "warm";
    selectFrontTypeModeProperty = addProperty(ENUM_PROPERTY, "front type",
                                              frontSelectionGroupProp);
    properties->mEnum()->setEnumNames(selectFrontTypeModeProperty, selectTypeModes);
    properties->mEnum()->setValue(selectFrontTypeModeProperty, selectFrontTypeMode);

    hideNonSelectedFrontsProperty = addProperty(BOOL_PROPERTY, "hide non selected fronts",
                                                frontSelectionGroupProp);
    properties->mBool()->setValue(hideNonSelectedFrontsProperty, hideNonSelectedFronts);

    showDataStatisticsProperty = addProperty(
            CLICK_PROPERTY, "show statistics", frontSelectionGroupProp);

    significantDigitsProperty = addProperty(INT_PROPERTY, "significant digits",
                                            frontSelectionGroupProp);
    properties->setInt(significantDigitsProperty, 0, 0, 10, 1);
    significantDigitsProperty->setToolTip("Digits considered for computation of the selected front value "
                                          "distribution.");

    histogramDisplayModeProperty = addProperty(ENUM_PROPERTY, "histogram display",
                                               frontSelectionGroupProp);
    QStringList histogramDisplayModes;
    histogramDisplayModes << "relative frequencies" << "absolute vertex count";
    properties->mEnum()->setEnumNames(histogramDisplayModeProperty, histogramDisplayModes);

    /**************************************************************************
                            Python TSS properties
    **************************************************************************/
    // Keep an instance of the tracking actor as subactor get all tracking properties
    // needed to track cyclone paths or other things.
    pythonTSSActor = new MPythonTSSActor();
    pythonTSSActor->setName("python tss interface");
    actorPropertiesSupGroup->addSubProperty(pythonTSSActor->getPropertyGroup());

    connect(glRM, SIGNAL(actorCreated(MActor * )),
            SLOT(onActorCreated(MActor * )));
    connect(glRM, SIGNAL(actorDeleted(MActor * )),
            SLOT(onActorDeleted(MActor * )));
    connect(glRM, SIGNAL(actorRenamed(MActor * , QString)),
            SLOT(onActorRenamed(MActor * , QString)));

    endInitialiseQtProperties();

}


MFrontDetectionActor::~MFrontDetectionActor()
{
//    if (frontSurfaceSelection)
//    {
//        curFilter->releaseData(frontSurfaceSelection);
//    }
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

#define SHADER_VERTEX_ATTRIBUTE 0


void MFrontDetectionActor::reloadShaderEffects()
{
    LOG4CPLUS_DEBUG(mlog, "loading shader programs");

    beginCompileShaders(4);

    compileShadersFromFileWithProgressDialog(
            boundingBoxShader,
            "src/glsl/simple_coloured_geometry.fx.glsl");
    compileShadersFromFileWithProgressDialog(
            fronts3DSurfaceShader, "src/glsl/fronts3d_geometry.fx.glsl");
    compileShadersFromFileWithProgressDialog(
            frontTubeShaderOIT,
            "src/glsl/tube_frontlines_OIT.fx.glsl");
    compileShadersFromFileWithProgressDialog(
            normalCurvesTubeShaderOIT,
            "src/glsl/tube_normalcurves_OIT.fx.glsl");

    endCompileShaders();
}


void MFrontDetectionActor::saveConfiguration(QSettings *settings)
{
    MNWPMultiVarActor::saveConfiguration(settings);

    settings->beginGroup(getSettingsID());

    MBoundingBoxInterface::saveConfiguration(settings);


    settings->setValue("render3DFront", render3DFront);
    settings->setValue("render2DFront", render2DFront);
    settings->setValue("render3DNC", render3DNC);
    settings->setValue("render2DNC", render2DNC);
    settings->setValue("autoCompute", autoCompute);
    settings->setValue("computeOnGPU", computeOnGPU);
    settings->setValue("useGeopotentialHeight", useGeopotentialHeight);
    settings->setValue("frontElevation2d_hPa", frontElevation2d_hPa);
    settings->setValue("detectionVariableIndex", detectionVariableIndex);
    settings->setValue("fpmaDistanceValue", fpmaDistanceValue_km);
    settings->setValue("useTFPTransferFunction", useTFPTransferFunction);
    settings->setValue("useFSTransferFunction", useFSTransferFunction);
    settings->setValue("useABZTransferFunction", useABZTransferFunction);
    settings->setValue("useSlopeTransferFunction", useSlopeTransferFunction);
    settings->setValue("useBreadthTransferFunction", useBreadthTransferFunction);

    settings->beginWriteArray("filterTfsFrontDetection",
                              normalCurvesFilterSetList.size());
    int index = 0;
    for (auto filter: normalCurvesFilterSetList)
    {
        settings->setArrayIndex(index);
        settings->setValue("enabled", filter->enabled);
        settings->setValue("variable", filter->variableIndex);
        settings->setValue("transferFunctionProperty", properties->getEnumItem(
                filter->transferFunctionProperty));
        settings->setValue("type", filterTypeToString(filter->type));
        index++;
    }
    settings->endArray(); // "filterTfsFrontDetection"

    settings->setValue("showFrontTypes", showFrontTypes);
    settings->setValue("shadingMode", shadingMode);
    settings->setValue("shadingVarMode", filterTypeToString(shadingVarMode));
    settings->setValue("frontsTubeRadius", frontsTubeRadius);

    settings->setValue("shadingVariableIndex", shadingVariableIndex);
    settings->setValue("windUVariableIndex", windUVariableIndex);
    settings->setValue("windVVariableIndex", windVVariableIndex);
    settings->setValue("zVariableIndex", zVariableIndex);

    settings->setValue("normalCurvesTubeRadius", normalCurvesTubeRadius);
    settings->setValue("ncShadingVariableIndex", ncShadingVariableIndex);
    settings->setValue("seedPointSpacing", seedPointSpacing);
    settings->setValue("seedPointSpacingZ", seedPointSpacingZ);
    settings->setValue("ncShadingMode", ncShadingMode);

    settings->setValue("enableSelection", enableSelection);
    settings->setValue("selectTFP", selectTFP);
    settings->setValue("selectStrength", selectStrength);
    settings->setValue("selectBottomPressure", selectBottomPressure);
    settings->setValue("selectTopPressure", selectTopPressure);
    settings->setValue("selectFrontTypeMode", selectFrontTypeMode);
    settings->setValue("hideNonSelectedFronts", hideNonSelectedFronts);


    settings->setValue("transferFunctionShading",
                       properties->getEnumItem(
                               transferFunctionShadingProperty));
    settings->setValue("transferFunctionTFPProperty",
                       properties->getEnumItem(
                               transferFunctionTFPProperty));
    settings->setValue("transferFunctionFSProperty",
                       properties->getEnumItem(
                               transferFunctionFSProperty));
    settings->setValue("transferFunctionABZProperty",
                       properties->getEnumItem(
                               transferFunctionABZProperty));
    settings->setValue("transferFunctionSlopeProperty",
                       properties->getEnumItem(
                               transferFunctionSlopeProperty));
    settings->setValue("transferFunctionBreadthProperty",
                       properties->getEnumItem(
                               transferFunctionBreadthProperty));
    settings->setValue("transferFunctionNCShadingProperty",
                       properties->getEnumItem(
                               transferFunctionNCShadingProperty));

    settings->setValue("shadowColor", shadowColor);
    settings->setValue("shadowHeight", shadowHeight);
    settings->setValue("lightingMode", lightingMode);

    // Store the properties of the graticule subactor in a separate subgroup.
    settings->beginGroup("SubActor_PythonTSS");
    pythonTSSActor->saveActorConfiguration(settings);
    settings->endGroup();

    settings->endGroup();
}


void MFrontDetectionActor::loadConfiguration(QSettings *settings)
{
    MNWPMultiVarActor::loadConfiguration(settings);

    suppressUpdates = true;

    settings->beginGroup(getSettingsID());

    MBoundingBoxInterface::loadConfiguration(settings);


    // ********************* MAIN PROPERTIES *********************
    render3DFront = settings->value("render3DFront").toBool();
    properties->mBool()->setValue(render3DFrontProperty, render3DFront);

    render2DFront = settings->value("render2DFront").toBool();
    properties->mBool()->setValue(render2DFrontProperty, render2DFront);

    render3DNC = settings->value("render3DNC").toBool();
    properties->mBool()->setValue(render3DNCProperty, render3DNC);

    render2DNC = settings->value("render2DNC").toBool();
    properties->mBool()->setValue(render2DNCProperty, render2DNC);

    autoCompute = settings->value("autoCompute").toBool();
    properties->mBool()->setValue(autoComputeProperty, autoCompute);

    computeOnGPU = settings->value("computeOnGPU").toBool();
    properties->mBool()->setValue(computeOnGPUProperty, computeOnGPU);

    useGeopotentialHeight = settings->value("useGeopotentialHeight").toBool();
    properties->mBool()->setValue(useGeopotentialHeightProperty, useGeopotentialHeight);

    frontElevation2d_hPa = settings->value("frontElevation2d_hPa").toDouble();
    properties->mDDouble()->setValue(frontElevationProperty,
                                     frontElevation2d_hPa);

    detectionVariableIndex = settings->value("detectionVariableIndex").toInt();
    properties->mInt()->setValue(detectionVariableIndexProperty,
                                 detectionVariableIndex);

    fpmaDistanceValue_km = settings->value("fpmaDistanceValue").toDouble();
    properties->mDDouble()->setValue(fpmaDistanceProperty,
                                     fpmaDistanceValue_km);


    // ********************* APPEARANCE PROPERTIES *********************

    useTFPTransferFunction = settings->value("useTFPTransferFunction").toBool();

    useFSTransferFunction = settings->value("useFSTransferFunction").toBool();

    useABZTransferFunction = settings->value("useABZTransferFunction").toBool();

    useSlopeTransferFunction =
            settings->value("useSlopeTransferFunction").toBool();

    useBreadthTransferFunction =
            settings->value("useBreadthTransferFunction").toBool();

    int numFilterSet = settings->beginReadArray("filterTfsFrontDetection");
    for (int i = 0; i < numFilterSet; i++)
    {
        settings->setArrayIndex(i);
        bool enabled = settings->value("enabled", false).toBool();
        int varIndex = settings->value("variable", 0).toInt();
        QString tfName = settings->value("transferFunctionProperty", "").toString();
        filterType type = stringToFilterType(settings->value("type").toString());
        loadNormalCurvesFilter(i, enabled, varIndex, tfName, type);
    }
    settings->endArray();

    showFrontTypes = settings->value("showFrontTypes").toBool();

    shadingMode = settings->value("shadingMode").toInt();
    properties->mInt()->setValue(shadingModeProperty, shadingMode);

    shadingVarMode = stringToFilterType(
            settings->value("shadingVarMode").toString());
    properties->mInt()->setValue(shadingVarModeProperty, shadingVarMode);

    frontsTubeRadius = settings->value("frontsTubeRadius", 0.3).toFloat();
    properties->mDouble()->setValue(frontsTubeRadiusProp, frontsTubeRadius);

    shadingVariableIndex = settings->value("shadingVariableIndex").toInt();
    properties->mInt()->setValue(shadingVariableIndexProperty,
                                 shadingVariableIndex);

    windUVariableIndex = settings->value("windUVariableIndex").toInt();
    properties->mInt()->setValue(windUVarIndexProp,
                                 windUVariableIndex);

    windVVariableIndex = settings->value("windVVariableIndex").toInt();
    properties->mInt()->setValue(windVVarIndexProp,
                                 windVVariableIndex);

    zVariableIndex = settings->value("zVariableIndex").toInt();
    properties->mInt()->setValue(zVarIndexProp,
                                 zVariableIndex);

//    normalCurvesEnabled = settings->value("normalCurvesEnabled").toBool();
//    properties->mBool()->setValue(normalCurvesEnabledProp, normalCurvesEnabled);

    normalCurvesTubeRadius = settings->value("normalCurvesTubeRadius", 0.2).toFloat();
    properties->mDouble()->setValue(normalCurvesTubeRadiusProp, normalCurvesTubeRadius);

    ncShadingVariableIndex = settings->value("ncShadingVariableIndex").toInt();
    properties->mInt()->setValue(ncShadingVarIndexProp,
                                 ncShadingVariableIndex);

    seedPointSpacing = settings->value("seedPointSpacing").toFloat();
    properties->mDouble()->setValue(seedPointSpacingProp, seedPointSpacing);

    seedPointSpacingZ = settings->value("seedPointSpacingZ", 100).toInt();
    properties->mInt()->setValue(seedPointSpacingZProp, seedPointSpacingZ);

    ncShadingMode = settings->value("ncShadingMode").toInt();
    properties->mEnum()->setValue(ncShadingModeProperty, ncShadingMode);
    if (ncShadingMode == 0)
    { ncShadingVarIndexProp->setEnabled(false); }

// front selection properties
    enableSelection = settings->value("enableSelection").toBool();
    properties->mBool()->setValue(enableSelectionProperty, enableSelection);

    selectTFP = settings->value("selectTFP").toFloat();
    properties->mDouble()->setValue(selectTFPProperty, selectTFP);

    selectStrength = settings->value("selectStrength").toFloat();
    properties->mDouble()->setValue(selectStrengthProperty, selectStrength);

    selectBottomPressure = settings->value("selectBottomPressure").toInt();
    properties->mInt()->setValue(selectBottomPressureProperty, selectBottomPressure);
    selectTopPressure = settings->value("selectTopPressure").toInt();
    properties->mInt()->setValue(selectTopPressureProperty, selectTopPressure);

    selectFrontTypeMode = settings->value("selectFrontTypeMode").toInt();
    properties->mEnum()->setValue(selectFrontTypeModeProperty, selectFrontTypeMode);

    hideNonSelectedFronts = settings->value("hideNonSelectedFronts").toBool();
    properties->mBool()->setValue(hideNonSelectedFrontsProperty, hideNonSelectedFronts);

    QString tfName = settings->value("transferFunctionShading", "None").toString();
    if (!setTransferFunction(transferFunctionShadingProperty, tfName))
    {
    }

    tfName = settings->value("transferFunctionTFPProperty", "None").toString();
    if (!setTransferFunction(transferFunctionTFPProperty, tfName))
    {
    }

    tfName = settings->value("transferFunctionFSProperty", "None").toString();
    if (!setTransferFunction(transferFunctionFSProperty, tfName))
    {
    }

    tfName = settings->value("transferFunctionABZProperty", "None").toString();
    if (!setTransferFunction(transferFunctionABZProperty, tfName))
    {
    }

    tfName = settings->value("transferFunctionSlopeProperty", "None").toString();
    if (!setTransferFunction(transferFunctionSlopeProperty, tfName))
    {
    }

    tfName = settings->value("transferFunctionBreadthProperty", "None").toString();
    if (!setTransferFunction(transferFunctionBreadthProperty, tfName))
    {
    }

    tfName = settings->value("transferFunctionNCShadingProperty", "None").toString();
    if (!setTransferFunction(transferFunctionNCShadingProperty, tfName))
    {
    }

    setTransferFunctionFromProperty(transferFunctionShadingProperty,
                                    &transferFunctionShading);
    setTransferFunctionFromProperty(transferFunctionTFPProperty,
                                    &transferFunctionTFP);
    setTransferFunctionFromProperty(transferFunctionFSProperty,
                                    &transferFunctionFS);
    setTransferFunctionFromProperty(transferFunctionABZProperty,
                                    &transferFunctionABZ);
    setTransferFunctionFromProperty(transferFunctionSlopeProperty,
                                    &transferFunctionSlope);
    setTransferFunctionFromProperty(transferFunctionBreadthProperty,
                                    &transferFunctionBreadth);
    setTransferFunctionFromProperty(transferFunctionNCShadingProperty,
                                    &transferFunctionNCShading);

    shadowColor = settings->value("shadowColor",
                                  QColor(70, 70, 70)).value<QColor>();
    properties->mColor()->setValue(shadowColorProperty, shadowColor);

    shadowHeight = settings->value("shadowHeight", 1045.0).toFloat();
    properties->mDouble()->setValue(shadowHeightProperty, shadowHeight);

    lightingMode = settings->value("lightingMode").toInt();
    properties->mEnum()->setValue(lightingModeProperty, lightingMode);

    if (!variables.empty())
    {
        detectionVariable = static_cast<MNWP3DVolumeActorVariable *>(
                variables.at(detectionVariableIndex));
        //potTemperatureVar = static_cast<MNWP3DVolumeActorVariable*>(
        //        variables.at(potTemperatureVariableIndex));
        shadingVariable = static_cast<MNWP3DVolumeActorVariable *>(
                variables.at(shadingVariableIndex));
        windUVar = static_cast<MNWP3DVolumeActorVariable *>(
                variables.at(windUVariableIndex));
        windVVar = static_cast<MNWP3DVolumeActorVariable *>(
                variables.at(windVVariableIndex));
        zVar = static_cast<MNWP3DVolumeActorVariable *>(
                variables.at(zVariableIndex));
        ncShadingVar = static_cast<MNWP3DVolumeActorVariable *>(
                variables.at(ncShadingVariableIndex));
    }


    // Store the properties of the graticule subactor in a separate subgroup.
    settings->beginGroup("SubActor_PythonTSS");
    pythonTSSActor->loadActorConfiguration(settings);
    settings->endGroup();

    if (pythonTSSActor->getName().isEmpty())
    {
        pythonTSSActor->setName("python tss interface");
    }
    // Old sessions have stored the graticule properties in a different
    // subgroup; restore vertical position to avoid user confusion.

    settings->endGroup();

    suppressUpdates = false;
    //requestFrontLines();
    emitActorChangedSignal();
}


bool MFrontDetectionActor::setTransferFunction(QtProperty *tfProp, const QString &tfName)
{
    QStringList tfNames = properties->mEnum()->enumNames(tfProp);
    int tfIndex = tfNames.indexOf(tfName);

    if (tfIndex >= 0)
    {
        properties->mEnum()->setValue(tfProp, tfIndex);
        return true;
    }

    // Set transfer function property to "None".
    properties->mEnum()->setValue(tfProp, 0);

    return false; // The given tf name could not be found.
}


void MFrontDetectionActor::setTransferFunctionFromProperty(
        QtProperty *tfProp, MTransferFunction1D **transferFunc)
{
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    QString tfName = properties->getEnumItem(tfProp);

    if (tfName == "None")
    {
        *transferFunc = nullptr;
        return;
    }

    // Find the selected transfer function in the list of actors from the
    // resources manager. Not very efficient, but works well enough for the
    // small number of actors at the moment..
    for (MActor *actor: glRM->getActors())
    {
        if (MTransferFunction1D * tf = dynamic_cast<MTransferFunction1D *>(actor))
        {
            if (tf->transferFunctionName() == tfName)
            {
                *transferFunc = tf;
                return;
            }

        }
    }
}


MFrontDetectionActor::NormalCurvesFilterSettings::
NormalCurvesFilterSettings(MFrontDetectionActor *a,
                           uint8_t filterNumber,
                           QList<QString> variableNames,
                           bool enabled,
                           int variableIndex,
                           MTransferFunction1D *transferFunction,
                           filterType type,
                           int textureUnitTransferFunction)
        : enabled(enabled),
          variableIndex(variableIndex),
          transferFunction(transferFunction),
          textureUnitTransferFunction(textureUnitTransferFunction),
          varNameList(variableNames),
          type(type)
{
    //MActor *a = this;
    MQtProperties *properties = a->getQtProperties();

    a->beginInitialiseQtProperties();

    QString propertyTitle = QString("nc filter #%1").arg(filterNumber);

    groupProperty = a->addProperty(GROUP_PROPERTY, propertyTitle);

    enabledProperty = a->addProperty(BOOL_PROPERTY, "enabled", groupProperty);
    properties->mBool()->setValue(enabledProperty, enabled);

    variableProperty = a->addProperty(ENUM_PROPERTY,
                                      "filter variable",
                                      groupProperty);

    int tmpVarIndex = variableIndex;

    properties->mEnum()->setEnumNames(
            variableProperty,
            varNameList);
    properties->mEnum()->setValue(
            variableProperty,
            tmpVarIndex);

    // Transfer function.
    // Scan currently available actors for transfer functions. Add TFs to
    // the list displayed in the combo box of the transferFunctionProperty.
    QStringList availableTFs;
    availableTFs << "None";
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
    for (MActor *mactor: glRM->getActors())
    {
        if (MTransferFunction1D * tf = dynamic_cast<MTransferFunction1D *>(mactor))
        {
            availableTFs << tf->transferFunctionName();
        }
    }

    int tfIndex = -1;
    if (transferFunction != nullptr)
    {
        for (int i = 0; i < availableTFs.size(); i++)
        {
            if (availableTFs[i] == transferFunction->transferFunctionName())
            {
                tfIndex = i;
                break;
            }
        }
    }
    transferFunctionProperty = a->addProperty(ENUM_PROPERTY,
                                              "transfer function",
                                              groupProperty);
    properties->mEnum()->setEnumNames(transferFunctionProperty, availableTFs);
    properties->mEnum()->setValue(transferFunctionProperty, tfIndex);
    transferFunctionProperty->setToolTip("This transfer function is used "
                                         "for mapping frontal strength to "
                                         "frontal's colour.");
    QStringList typeModes;

    typeModes << filterTypeToString(ABSOLUTE_CHANGE)
              << filterTypeToString(CHANGE_PER_100KM)
              << filterTypeToString(VALUE_AT_VERTEX);

    typeProperty = a->addProperty(ENUM_PROPERTY, "filterType", groupProperty);
    properties->mEnum()->setEnumNames(typeProperty, typeModes);
    properties->mEnum()->setValue(typeProperty, type);

    removeProperty = a->addProperty(CLICK_PROPERTY, "remove", groupProperty);

    a->endInitialiseQtProperties();
}


void MFrontDetectionActor::addNormalCurvesFilter(
        int8_t filterNumber)
{
    QList<QString> variableList;
    for (int vi = 0; vi < variables.size(); vi++)
    {
        MNWPActorVariable *var = variables.at(vi);

        variableList << var->variableName;
    }
    int varIndex = -1;
    if (varNameList.size() > 0)
    {
        varIndex = 0;
    }
    NormalCurvesFilterSettings *ncFilterSettings =
            new NormalCurvesFilterSettings(this, filterNumber, variableList,
                                           false, varIndex);
    normalCurvesFilterSetList.append(ncFilterSettings);
    genericFilterGroupProperty->addSubProperty(
            normalCurvesFilterSetList.back()->groupProperty);

    if (ncFilterSettings->enabled)
    {
        bool isConnected;
        isConnected = connect(ncFilterSettings->transferFunction,
                              SIGNAL(actorChanged()),
                              this, SLOT(recomputeFrontsAlpha));
        assert(isConnected);
    }
}


void MFrontDetectionActor::loadNormalCurvesFilter(
        int8_t filterNumber, bool enabled, int varIndex, QString tfName,
        filterType type)
{
    QList<QString> variableList;
    for (int vi = 0; vi < variables.size(); vi++)
    {
        MNWPActorVariable *var = variables.at(vi);

        variableList << var->variableName;
    }
    if (varIndex > varNameList.size())
    {
        varIndex = -1;
    }

    MTransferFunction1D *tfFilter = nullptr;
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
    for (MActor *actor: glRM->getActors())
    {
        if (MTransferFunction1D * tf = dynamic_cast<MTransferFunction1D *>(actor))
        {
            if (tf->transferFunctionName() == tfName)
            {
                tfFilter = tf;

                NormalCurvesFilterSettings *ncFilterSettings =
                        new NormalCurvesFilterSettings(this, filterNumber, variableList,
                                                       enabled, varIndex, tfFilter, type);
                normalCurvesFilterSetList.append(ncFilterSettings);
                genericFilterGroupProperty->addSubProperty(
                        normalCurvesFilterSetList.back()->groupProperty);

                if (ncFilterSettings->enabled)
                {
                    bool isConnected;
                    isConnected = connect(ncFilterSettings->transferFunction,
                                          SIGNAL(actorChanged()),
                                          this, SLOT(recomputeFrontsAlpha()));
                    assert(isConnected);
                }
            }
        }
    }
}


bool MFrontDetectionActor::removeNormalCurvesFilter(
        NormalCurvesFilterSettings *filter)
{
    int index = normalCurvesFilterSetList.indexOf(filter);
    // Don't ask for confirmation during application start.
    if (MSystemManagerAndControl::getInstance()->applicationIsInitialized())
    {
        QMessageBox yesNoBox;
        yesNoBox.setWindowTitle("Delete filter");
        yesNoBox.setText(QString("Do you really want to delete "
                                 "filter #%1").arg(index + 1));
        yesNoBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        yesNoBox.setDefaultButton(QMessageBox::No);

        if (yesNoBox.exec() != QMessageBox::Yes)
        {
            return false;
        }
    }

    // disconnect filter connection to this actor
    disconnect(filter->transferFunction, 0, this, 0);

    // Redraw is only needed if filter sets to delete are enabled.
    bool needsRedraw = filter->enabled;
    MQtProperties *properties = this->getQtProperties();
    properties->mBool()->setValue(filter->enabledProperty,
                                  false);
    genericFilterGroupProperty->removeSubProperty(
            filter->groupProperty);
    normalCurvesFilterSetList.remove(index);

    QString text = filter->groupProperty->propertyName();
    text = text.mid(0, text.indexOf("#") + 1);
    // Rename filter sets after deleted filter to
    // close gap left by deleted filter sets.
    for (; index < normalCurvesFilterSetList.size(); index++)
    {
        filter->groupProperty->setPropertyName(
                text + QString::number(index + 1));
    }
    return needsRedraw;
}


MNWPActorVariable *MFrontDetectionActor::createActorVariable(
        const MSelectableDataSource &dataSource)
{
    MNWP3DVolumeActorVariable *newVar = new MNWP3DVolumeActorVariable(this);

    newVar->dataSourceID = dataSource.dataSourceID;
    newVar->levelType = dataSource.levelType;
    newVar->variableName = dataSource.variableName;

    return newVar;
}


const QList<MVerticalLevelType> MFrontDetectionActor::supportedLevelTypes()
{
    return (QList<MVerticalLevelType>()
            << HYBRID_SIGMA_PRESSURE_3D << PRESSURE_LEVELS_3D << AUXILIARY_PRESSURE_3D);
}


void MFrontDetectionActor::onBoundingBoxChanged()
{
    labels.clear();
    if (suppressActorUpdates())
    {
        return;
    }
    // Switching to no bounding box only needs a redraw, but no recomputation
    // because it disables rendering of the actor.
    if (bBoxConnection->getBoundingBox() == nullptr)
    {
        emitActorChangedSignal();
        return;
    }

    emitActorChangedSignal();
}


/******************************************************************************
***                               PUBLIC SLOTS                              ***
*******************************************************************************/


void MFrontDetectionActor::asynchronous3DFrontsAvailable(MDataRequest request)
{
    releaseData();

    if (computeOnGPU)
    {
//        frontSurfaceSelection = frontDetection3DSourceGPU->getData(request);
        frontSurfaceSelection = frontDetection3DFilter->getData(request);
        fronts3dGPUReleased = false;
    }
    else
    {
        frontSurfaceSelection = frontDetection3DFilter->getData(request);
//        frontSurfaceSelection = frontDetection3DSource->getData(request);
        fronts3dReleased = false;
    }

    frontMesh3D = frontSurfaceSelection->getTriangleMeshSelection();
    normalCurves3D = frontSurfaceSelection->getNormalCurvesSelection();
    frontTrianglePatches = frontSurfaceSelection->getTrianglePatches();

    num3DFrontVertices = frontMesh3D->getNumVertices();

    vbo3DFronts = frontMesh3D->getVertexBuffer();
    ibo3DFronts = frontMesh3D->getIndexBuffer();

    applySettingsClickProperty->setEnabled(true);
    actorPropertiesSupGroup->setEnabled(true);

    // disable normal curves rendering option, when fronts computed on GPU
    if (computeOnGPU)
    { render3DNCProperty->setEnabled(false); }
    else
    { render3DNCProperty->setEnabled(true); }

    suppressUpdates = false;
    isComputing = false;

    trigger3DNormalCurveFilter = true;

    recomputeAlpha3DFronts = true;
    recomputeShading3DFronts = true;
    recomputeAlpha3DNormalCurves = true;
    recomputeShading3DNormalCurves = true;

    useSelectionShading = false;

    if (enableSelection)
    {
        triggerFrontAttributeAnalysis();
    }

    if (pythonTSSActor->autoExportActive() && enableSelection)
    {
        int batchNR = pythonTSSActor->requestPythonTrackingAndFullAttributesExport();

        if (batchNR >= 0 || batchNR < frontTrianglePatches->getNumPatches())
        {
            currentSelectedFront = frontTrianglePatches->getTrianglePatch(batchNR);
            QMap<QString, float> metrics = calcMetricOfFrontalPatch(currentSelectedFront);
            int mem = detectionVariable->getEnsembleMember();
            QDateTime datetime = detectionVariable->getSynchronizationControl()->validDateTime();

            QVector3D lonLatP = QVector3D(metrics["centroid_x"], metrics["centroid_y"], metrics["centroid_z"]);

            pythonTSSActor->triggerAnalysis3DFronts(lonLatP, metrics, mem, datetime);
            updateSelectedShading = true;
            useSelectionShading = true;
            pythonTSSActor->initTFPStartStats(tfpStats, QVector2D(lonLatP.x(), lonLatP.y()));
        }
    }

    emitActorChangedSignal();
  synchronizationControl->synchronizationCompleted(this);

//    detectionVariable->getSynchronizationControl()->emitSaveImageSupport();
}


void MFrontDetectionActor::asynchronous2DFrontsAvailable(
        MDataRequest request)
{
    releaseData();

    frontLineSelection = frontDetection2DSource->getData(request);

    frontLines2D = frontLineSelection->getLineSelection();
    normalCurves2D = frontLineSelection->getNormalCurvesSelection();

    vbo2DFronts = frontLines2D->getVertexBuffer();
    ibo2DFronts = frontLines2D->getIndexBuffer();

    num2DFrontVertices = frontLines2D->getNumVertices();

    applySettingsClickProperty->setEnabled(true);
    actorPropertiesSupGroup->setEnabled(true);

    isComputing = false;
    suppressUpdates = false;

    trigger2DNormalCurveFilter = true;

    recomputeAlpha2DFronts = true;
    recomputeShading2DFronts = true;
    recomputeAlpha2DNormalCurves = true;
    recomputeShading2DNormalCurves = true;

    fronts2dReleased = false;

    emitActorChangedSignal();
  synchronizationControl->synchronizationCompleted(this);
}


void MFrontDetectionActor::prepareAvailableDataForRendering()
{

}


void MFrontDetectionActor::updateSelectedFronts()
{
    for (auto &alphaShading3DFront: alphaShading3DFronts)
    {
        alphaShading3DFront[2] = 0.0;
    }
    useSelectionShading = false;

    emitActorChangedSignal();
}


void MFrontDetectionActor::updateLabels()
{
    labels = pythonTSSActor->getLabelsToRender();
}


void MFrontDetectionActor::releaseData()
{
    if (!fronts2dReleased)
    {
        // ToDo release data
        frontLines2D->releaseIndexBuffer();
        frontLines2D->releaseVertexBuffer();

        normalCurves2D->releaseVertexBuffer();
        normalCurves2D->releaseIndexBuffer();

        frontDetection2DSource->releaseData(frontLineSelection);
        //frontDetection2DSource->releaseData(frontLineSelection);

        MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
        glRM->releaseGPUItem(vbo2DFronts);
        glRM->releaseGPUItem(ibo2DFronts);

        fronts2dReleased = true;
    }

    if (!fronts3dReleased)
    {
        MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
        glRM->releaseGPUItem(vbo3DFronts);
        glRM->releaseGPUItem(ibo3DFronts);

        frontMesh3D->releaseIndexBuffer();
        frontMesh3D->releaseVertexBuffer();

        normalCurves3D->releaseVertexBuffer();
        normalCurves3D->releaseIndexBuffer();

//        frontDetection3DSource->releaseData(frontSurfaceSelection);
        frontDetection3DFilter->releaseData(frontSurfaceSelection);

        fronts3dReleased = true;
    }

    if (!fronts3dGPUReleased)
    {
        MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
        glRM->releaseGPUItem(vbo3DFronts);
        glRM->releaseGPUItem(ibo3DFronts);

        frontMesh3D->releaseIndexBuffer();
        frontMesh3D->releaseVertexBuffer();

        frontDetection3DFilter->releaseData(frontSurfaceSelection);

        fronts3dGPUReleased = true;
    }
}


void MFrontDetectionActor::onAddActorVariable(MNWPActorVariable *var)
{
    varNameList << var->variableName;

    // Temporarily save variable indices.
    int tmpVarIndex = detectionVariableIndex;
    int tmpShadingVarIndex = shadingVariableIndex;
    int tmpWindUVarIndex = windUVariableIndex;
    int tmpWindVVarIndex = windVVariableIndex;
    int tmpZVarIndex = zVariableIndex;
    int tmpNCShadingVarIndex = ncShadingVariableIndex;

    // Update enum lists.
    properties->mEnum()->setEnumNames(detectionVariableIndexProperty, varNameList);
    properties->mEnum()->setValue(detectionVariableIndexProperty, tmpVarIndex);
    properties->mEnum()->setEnumNames(shadingVariableIndexProperty, varNameList);
    properties->mEnum()->setValue(shadingVariableIndexProperty, tmpShadingVarIndex);
    properties->mEnum()->setEnumNames(windUVarIndexProp, varNameList);
    properties->mEnum()->setValue(windUVarIndexProp, tmpWindUVarIndex);
    properties->mEnum()->setEnumNames(windVVarIndexProp, varNameList);
    properties->mEnum()->setValue(windVVarIndexProp, tmpWindVVarIndex);
    properties->mEnum()->setEnumNames(zVarIndexProp, varNameList);
    properties->mEnum()->setValue(zVarIndexProp, tmpZVarIndex);
    properties->mEnum()->setEnumNames(ncShadingVarIndexProp, varNameList);
    properties->mEnum()->setValue(ncShadingVarIndexProp, tmpNCShadingVarIndex);

    for (auto filter: normalCurvesFilterSetList)
    {
        filter->varNameList << var->variableName;
        int tmpVarIndex = filter->variableIndex;

        properties->mEnum()->setEnumNames(
                filter->variableProperty,
                filter->varNameList);
        properties->mEnum()->setValue(
                filter->variableProperty,
                tmpVarIndex);
    }

    refreshEnumsProperties(nullptr);
}


void MFrontDetectionActor::onDeleteActorVariable(MNWPActorVariable *var)
{
    int i = variables.indexOf(var);

    // Update variableIndex and shadingVariableIndex if these point to
    // the removed variable or to one with a lower index.
    if (i <= detectionVariableIndex)
    {
        detectionVariableIndex = std::max(-1, detectionVariableIndex - 1);
    }
    if (i <= shadingVariableIndex)
    {
        shadingVariableIndex = std::max(-1, shadingVariableIndex - 1);
    }
    if (i <= windUVariableIndex)
    {
        windUVariableIndex = std::max(-1, windUVariableIndex - 1);
    }
    if (i <= windVVariableIndex)
    {
        windVVariableIndex = std::max(-1, windVVariableIndex - 1);
    }
    if (i <= zVariableIndex)
    {
        zVariableIndex = std::max(-1, zVariableIndex - 1);
    }
    if (i <= ncShadingVariableIndex)
    {
        ncShadingVariableIndex = std::max(-1, ncShadingVariableIndex - 1);
    }

    // Temporarily save variable indices.
    int tmpVarIndex = detectionVariableIndex;
    int tmpShadingVarIndex = shadingVariableIndex;
    int tmpWindUVarIndex = windUVariableIndex;
    int tmpWindVVarIndex = windVVariableIndex;
    int tmpZVarIndex = zVariableIndex;
    int tmpNCShadingVarIndex = ncShadingVariableIndex;

    // Remove the variable name from the enum lists.
    varNameList.removeAt(i);

    // Update enum lists.
    properties->mEnum()->setEnumNames(detectionVariableIndexProperty, varNameList);
    properties->mEnum()->setValue(detectionVariableIndexProperty, tmpVarIndex);
    properties->mEnum()->setEnumNames(shadingVariableIndexProperty, varNameList);
    properties->mEnum()->setValue(shadingVariableIndexProperty, tmpShadingVarIndex);
    properties->mEnum()->setEnumNames(windUVarIndexProp, varNameList);
    properties->mEnum()->setValue(windUVarIndexProp, tmpWindUVarIndex);
    properties->mEnum()->setEnumNames(windVVarIndexProp, varNameList);
    properties->mEnum()->setValue(windVVarIndexProp, tmpWindVVarIndex);
    properties->mEnum()->setEnumNames(zVarIndexProp, varNameList);
    properties->mEnum()->setValue(zVarIndexProp, tmpZVarIndex);
    properties->mEnum()->setEnumNames(ncShadingVarIndexProp, varNameList);
    properties->mEnum()->setValue(ncShadingVarIndexProp, tmpNCShadingVarIndex);

    for (auto *filter: normalCurvesFilterSetList)
    {
        if (i <= filter->variableIndex)
        {
            filter->variableIndex =
                    std::max(-1, filter->variableIndex - 1);
        }
        int tempVarIndex = filter->variableIndex;

        // Remove the variable name from the enum lists.
        filter->varNameList.removeAt(i);

        // Update enum lists.
        properties->mEnum()->setEnumNames(
                filter->variableProperty,
                filter->varNameList);

        properties->mEnum()->setValue(
                filter->variableProperty,
                tempVarIndex);

    }

    refreshEnumsProperties(var);
}


void MFrontDetectionActor::onChangeActorVariable(MNWPActorVariable *var)
{
    int varIndex = variables.indexOf(var);

    varNameList.replace(varIndex, var->variableName);

    int tmpDetectVarIdx = detectionVariableIndex;
    int tmpUWindVarIdx = windUVariableIndex;
    int tmpVWindVarIdx = windVVariableIndex;
    int tmpZVarIdx = zVariableIndex;
    int tmpShadingVarIdx = shadingVariableIndex;
    int tmpNCShadingVarIdx = ncShadingVariableIndex;

    enableActorUpdates(false);

    properties->mEnum()->setEnumNames(detectionVariableIndexProperty, varNameList);
    properties->mEnum()->setEnumNames(windUVarIndexProp, varNameList);
    properties->mEnum()->setEnumNames(windVVarIndexProp, varNameList);
    properties->mEnum()->setEnumNames(zVarIndexProp, varNameList);
    properties->mEnum()->setEnumNames(shadingVariableIndexProperty, varNameList);
    properties->mEnum()->setEnumNames(ncShadingVarIndexProp, varNameList);


    properties->mEnum()->setValue(detectionVariableIndexProperty, tmpDetectVarIdx);
    properties->mEnum()->setValue(windUVarIndexProp, tmpUWindVarIdx);
    properties->mEnum()->setValue(windVVarIndexProp, tmpVWindVarIdx);
    properties->mEnum()->setValue(zVarIndexProp, tmpZVarIdx);
    properties->mEnum()->setValue(shadingVariableIndexProperty, tmpShadingVarIdx);
    properties->mEnum()->setValue(ncShadingVarIndexProp, tmpNCShadingVarIdx);


    for (auto filter: normalCurvesFilterSetList)
    {
        filter->varNameList.replace(varIndex, var->variableName);
        int tmpVarIndex = filter->variableIndex;

        properties->mEnum()->setEnumNames(
                filter->variableProperty,
                filter->varNameList);
        properties->mEnum()->setValue(
                filter->variableProperty,
                tmpVarIndex);
    }
    enableActorUpdates(true);
    refreshEnumsProperties(nullptr);
}


void MFrontDetectionActor::onActorCreated(MActor *actor)
{
    // If the new actor is a transfer function, add it to the list of
    // available transfer functions.
    if (MTransferFunction1D * tf = dynamic_cast<MTransferFunction1D *>(actor))
    {
        // Don't render while the properties are being updated.
        enableEmissionOfActorChangedSignal(false);

        int indexS = properties->mEnum()->value(transferFunctionShadingProperty);
        int indexTFP = properties->mEnum()->value(transferFunctionTFPProperty);
        int indexFS = properties->mEnum()->value(transferFunctionFSProperty);
        int indexABZ = properties->mEnum()->value(transferFunctionABZProperty);
        int indexSlope = properties->mEnum()->value(transferFunctionSlopeProperty);
        int indexBreadth = properties->mEnum()->value(transferFunctionBreadthProperty);
        int indexNCShading = properties->mEnum()->value(transferFunctionNCShadingProperty);

        QStringList availableTFs = properties->mEnum()->enumNames(transferFunctionTFPProperty);
        availableTFs << tf->transferFunctionName();

        properties->mEnum()->setEnumNames(transferFunctionShadingProperty, availableTFs);
        properties->mEnum()->setEnumNames(transferFunctionTFPProperty, availableTFs);
        properties->mEnum()->setEnumNames(transferFunctionFSProperty, availableTFs);
        properties->mEnum()->setEnumNames(transferFunctionABZProperty, availableTFs);
        properties->mEnum()->setEnumNames(transferFunctionSlopeProperty, availableTFs);
        properties->mEnum()->setEnumNames(transferFunctionBreadthProperty, availableTFs);
        properties->mEnum()->setEnumNames(transferFunctionNCShadingProperty, availableTFs);

        properties->mEnum()->setValue(transferFunctionShadingProperty, indexS);
        properties->mEnum()->setValue(transferFunctionTFPProperty, indexTFP);
        properties->mEnum()->setValue(transferFunctionFSProperty, indexFS);
        properties->mEnum()->setValue(transferFunctionABZProperty, indexABZ);
        properties->mEnum()->setValue(transferFunctionSlopeProperty, indexSlope);
        properties->mEnum()->setValue(transferFunctionBreadthProperty, indexBreadth);
        properties->mEnum()->setValue(transferFunctionNCShadingProperty, indexNCShading);

        for (auto *filter: normalCurvesFilterSetList)
        {
            availableTFs.clear();
            int index = properties->mEnum()->value(
                    filter->transferFunctionProperty);
            availableTFs = properties->mEnum()->enumNames(
                    filter->transferFunctionProperty);
            availableTFs << tf->transferFunctionName();
            properties->mEnum()->setEnumNames(filter->transferFunctionProperty,
                                              availableTFs);
            properties->mEnum()->setValue(
                    filter->transferFunctionProperty, index);
        }

        enableEmissionOfActorChangedSignal(true);
    }
}


void MFrontDetectionActor::onActorDeleted(MActor *actor)
{
    // If the deleted actor is a transfer function, remove it from the list of
    // available transfer functions.
    if (MTransferFunction1D * tf = dynamic_cast<MTransferFunction1D *>(actor))
    {
        enableEmissionOfActorChangedSignal(false);

        int indexS = properties->mEnum()->value(transferFunctionShadingProperty);
        int indexTFP = properties->mEnum()->value(transferFunctionTFPProperty);
        int indexFS = properties->mEnum()->value(transferFunctionFSProperty);
        int indexABZ = properties->mEnum()->value(transferFunctionABZProperty);
        int indexSlope = properties->mEnum()->value(transferFunctionSlopeProperty);
        int indexBreadth = properties->mEnum()->value(transferFunctionBreadthProperty);
        int indexNCShading = properties->mEnum()->value(transferFunctionNCShadingProperty);

        QStringList availableTFs =
                properties->mEnum()->enumNames(transferFunctionTFPProperty);
        availableTFs << tf->transferFunctionName();

        // If the deleted transfer function is currently connected to this
        // variable, set current transfer function to "None" (index 0).
        if (availableTFs.at(indexS) == tf->getName())
        { indexS = 0; }
        if (availableTFs.at(indexTFP) == tf->getName())
        { indexTFP = 0; }
        if (availableTFs.at(indexFS) == tf->getName())
        { indexFS = 0; }
        if (availableTFs.at(indexSlope) == tf->getName())
        { indexSlope = 0; }
        if (availableTFs.at(indexBreadth) == tf->getName())
        { indexBreadth = 0; }
        if (availableTFs.at(indexNCShading) == tf->getName())
        { indexNCShading = 0; }

        availableTFs.removeOne(tf->getName());

        properties->mEnum()->setEnumNames(transferFunctionShadingProperty, availableTFs);
        properties->mEnum()->setEnumNames(transferFunctionTFPProperty, availableTFs);
        properties->mEnum()->setEnumNames(transferFunctionFSProperty, availableTFs);
        properties->mEnum()->setEnumNames(transferFunctionABZProperty, availableTFs);
        properties->mEnum()->setEnumNames(transferFunctionSlopeProperty, availableTFs);
        properties->mEnum()->setEnumNames(transferFunctionBreadthProperty, availableTFs);
        properties->mEnum()->setEnumNames(transferFunctionNCShadingProperty, availableTFs);

        properties->mEnum()->setValue(transferFunctionShadingProperty, indexS);
        properties->mEnum()->setValue(transferFunctionTFPProperty, indexTFP);
        properties->mEnum()->setValue(transferFunctionFSProperty, indexFS);
        properties->mEnum()->setValue(transferFunctionABZProperty, indexABZ);
        properties->mEnum()->setValue(transferFunctionSlopeProperty, indexSlope);
        properties->mEnum()->setValue(transferFunctionBreadthProperty, indexBreadth);
        properties->mEnum()->setValue(transferFunctionNCShadingProperty, indexNCShading);

        for (auto *filter: normalCurvesFilterSetList)
        {
            QString tFName = properties->getEnumItem(filter->transferFunctionProperty);
            availableTFs = properties->mEnum()->enumNames(
                    filter->transferFunctionProperty);
            availableTFs.removeOne(tf->getName());
            // Get the current index of the transfer function selected. If the
            // transfer function is the one to be deleted, the selection is set to
            // 'None'.
            int index = availableTFs.indexOf(tFName);

            properties->mEnum()->setEnumNames(filter->transferFunctionProperty,
                                              availableTFs);
            properties->mEnum()->setValue(
                    filter->transferFunctionProperty, index);
        }

        enableEmissionOfActorChangedSignal(true);
    }
}


void MFrontDetectionActor::onActorRenamed(MActor *actor,
                                          QString oldName)
{
    // If the renamed actor is a transfer function, change its name in the list
    // of available transfer functions.
    if (MTransferFunction1D * tf = dynamic_cast<MTransferFunction1D *>(actor))
    {
        // Don't render while the properties are being updated.
        enableEmissionOfActorChangedSignal(false);

        int indexS = properties->mEnum()->value(transferFunctionShadingProperty);
        int indexTFP = properties->mEnum()->value(transferFunctionTFPProperty);
        int indexFS = properties->mEnum()->value(transferFunctionFSProperty);
        int indexABZ = properties->mEnum()->value(transferFunctionABZProperty);
        int indexSlope = properties->mEnum()->value(transferFunctionSlopeProperty);
        int indexBreadth = properties->mEnum()->value(transferFunctionBreadthProperty);
        int indexNCShading = properties->mEnum()->value(transferFunctionNCShadingProperty);

        QStringList availableTFs = properties->mEnum()->enumNames(transferFunctionTFPProperty);

        // Replace affected entry.
        availableTFs[availableTFs.indexOf(oldName)] = tf->getName();

        properties->mEnum()->setEnumNames(transferFunctionShadingProperty, availableTFs);
        properties->mEnum()->setEnumNames(transferFunctionTFPProperty, availableTFs);
        properties->mEnum()->setEnumNames(transferFunctionFSProperty, availableTFs);
        properties->mEnum()->setEnumNames(transferFunctionABZProperty, availableTFs);
        properties->mEnum()->setEnumNames(transferFunctionSlopeProperty, availableTFs);
        properties->mEnum()->setEnumNames(transferFunctionBreadthProperty, availableTFs);
        properties->mEnum()->setEnumNames(transferFunctionNCShadingProperty, availableTFs);

        properties->mEnum()->setValue(transferFunctionShadingProperty, indexS);
        properties->mEnum()->setValue(transferFunctionTFPProperty, indexTFP);
        properties->mEnum()->setValue(transferFunctionFSProperty, indexFS);
        properties->mEnum()->setValue(transferFunctionABZProperty, indexABZ);
        properties->mEnum()->setValue(transferFunctionSlopeProperty, indexSlope);
        properties->mEnum()->setValue(transferFunctionBreadthProperty, indexBreadth);
        properties->mEnum()->setValue(transferFunctionNCShadingProperty, indexNCShading);

        for (auto *filter: normalCurvesFilterSetList)
        {
            int index = properties->mEnum()->value(
                    filter->transferFunctionProperty);
            availableTFs = properties->mEnum()->enumNames(
                    filter->transferFunctionProperty);

            // Replace affected entry.
            availableTFs[availableTFs.indexOf(oldName)] = tf->getName();

            properties->mEnum()->setEnumNames(
                    filter->transferFunctionProperty,
                    availableTFs);
            properties->mEnum()->setValue(
                    filter->transferFunctionProperty, index);
        }

        enableEmissionOfActorChangedSignal(true);
    }
}


void MFrontDetectionActor::dataFieldChangedEvent()
{
  if (autoCompute)
    {
        if (render2DFront) triggerAsynchronous2DFrontsRequest();
        if (render3DFront) triggerAsynchronous3DFrontsRequest();
    }

}


void MFrontDetectionActor::updateShadow()
{
    emitActorChangedSignal();
}


/******************************************************************************
***                             PRIVATE METHODS                             ***
*******************************************************************************/


void MFrontDetectionActor::initializeActorResources()
{
    // Parent initialisation.
    MNWPMultiVarActor::initializeActorResources();

    updatePositionCrossGeometry((QVector3D(0., 0., 1050.)));

    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    if (!variables.empty())
    {
        detectionVariable = static_cast<MNWP3DVolumeActorVariable *>(
                variables.at(detectionVariableIndex));
        shadingVariable = static_cast<MNWP3DVolumeActorVariable *>(
                variables.at(shadingVariableIndex));
        windUVar = static_cast<MNWP3DVolumeActorVariable *>(
                variables.at(windUVariableIndex));
        windVVar = static_cast<MNWP3DVolumeActorVariable *>(
                variables.at(windVVariableIndex));
        zVar = static_cast<MNWP3DVolumeActorVariable *>(
                variables.at(zVariableIndex));
        ncShadingVar = static_cast<MNWP3DVolumeActorVariable *>(
                variables.at(ncShadingVariableIndex));
    }

    varNameList.clear();
    for (int vi = 0; vi < variables.size(); vi++)
    {
        MNWPActorVariable *var = variables.at(vi);
        varNameList << var->variableName;
    }

    properties->mEnum()->setEnumNames(detectionVariableIndexProperty, varNameList);
    properties->mEnum()->setValue(detectionVariableIndexProperty, detectionVariableIndex);
    properties->mEnum()->setEnumNames(shadingVariableIndexProperty, varNameList);
    properties->mEnum()->setValue(shadingVariableIndexProperty, shadingVariableIndex);
    properties->mEnum()->setEnumNames(windUVarIndexProp, varNameList);
    properties->mEnum()->setValue(windUVarIndexProp, windUVariableIndex);
    properties->mEnum()->setEnumNames(windVVarIndexProp, varNameList);
    properties->mEnum()->setValue(windVVarIndexProp, windVVariableIndex);
    properties->mEnum()->setEnumNames(zVarIndexProp, varNameList);
    properties->mEnum()->setValue(zVarIndexProp, zVariableIndex);
    properties->mEnum()->setEnumNames(ncShadingVarIndexProp, varNameList);
    properties->mEnum()->setValue(ncShadingVarIndexProp, ncShadingVariableIndex);

    bool loadShaders = false;

    loadShaders |= glRM->generateEffectProgram("multiactor_bbox",
                                               boundingBoxShader);
    loadShaders |= glRM->generateEffectProgram("fronts3d_geometry",
                                               fronts3DSurfaceShader);
    loadShaders |= glRM->generateEffectProgram("tube_frontlines_OIT",
                                               frontTubeShaderOIT);
    loadShaders |= glRM->generateEffectProgram("tube_normalcurves_OIT",
                                               normalCurvesTubeShaderOIT);
    if (loadShaders) reloadShaderEffects();

    // Create filter / source pipeline
    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    MAbstractScheduler *scheduler = sysMC->getScheduler("MultiThread");
    MAbstractMemoryManager *memoryManager = sysMC->getMemoryManager("NWP");

    fleSource = std::make_shared<MFrontLocationEquationSource>();
    fleSource->setScheduler(scheduler);
    fleSource->setMemoryManager(memoryManager);

    tfpSource = std::make_shared<MThermalFrontParameterSource>();
    tfpSource->setScheduler(scheduler);
    tfpSource->setMemoryManager(memoryManager);

    abzSource = std::make_shared<MAdjacentBaroclinicZoneSource>();
    abzSource->setScheduler(scheduler);
    abzSource->setMemoryManager(memoryManager);

    partialDerivFilter = std::make_shared<MPartialDerivativeFilter>();
    partialDerivFilter->setScheduler(scheduler);
    partialDerivFilter->setMemoryManager(memoryManager);

    frontDetection2DSource = std::make_shared<MFrontDetection2DSource>();
    frontDetection2DSource->setScheduler(scheduler);
    frontDetection2DSource->setMemoryManager(memoryManager);

    frontDetection3DSourceCPU = std::make_shared<MFrontDetection3DSourceCPU>();
    frontDetection3DSourceCPU->setScheduler(scheduler);
    frontDetection3DSourceCPU->setMemoryManager(memoryManager);

    frontDetection3DFilter = std::make_shared<MFrontDetection3DFilter>();
    frontDetection3DFilter->setScheduler(scheduler);
    frontDetection3DFilter->setMemoryManager(memoryManager);

    frontDetection3DSourceGPU = std::make_shared<MFrontDetection3DSourceGPU>();
    frontDetection3DSourceGPU->setScheduler(scheduler);
    frontDetection3DSourceGPU->setMemoryManager(memoryManager);

    transferFunctionShadingTexUnit = assignTextureUnit();
    transferFunctionTFPTexUnit = assignTextureUnit();
    transferFunctionFSTexUnit = assignTextureUnit();
    transferFunctionABZTexUnit = assignTextureUnit();
    transferFunctionSlopeTexUnit = assignTextureUnit();
    transferFunctionBreadthTexUnit = assignTextureUnit();
    transferFunctionNCShadingTexUnit = assignTextureUnit();

    sampleSubroutines.resize(MVerticalLevelType::SIZE_LEVELTYPES);
    normalCompSubroutines.resize(MVerticalLevelType::SIZE_LEVELTYPES);

//    bool connected = connect(frontDetection3DSource.get(),
//            SIGNAL(dataRequestCompleted(MDataRequest)),
//            this, SLOT(asynchronous3DFrontsAvailable(MDataRequest)));
//    assert(connected);

    bool connected = connect(frontDetection3DFilter.get(),
                             SIGNAL(dataRequestCompleted(MDataRequest)),
                             this, SLOT(asynchronous3DFrontsAvailable(MDataRequest)));
    assert(connected);

//    connected = connect(frontDetection3DSourceGPU.get(),
//            SIGNAL(dataRequestCompleted(MDataRequest)),
//            this, SLOT(asynchronous3DFrontsAvailable(MDataRequest)));
//    assert(connected);

    connected = connect(frontDetection2DSource.get(),
                        SIGNAL(dataRequestCompleted(MDataRequest)),
                        this, SLOT(asynchronous2DFrontsAvailable(MDataRequest)));
    assert(connected);

    // connect python interface
    connected = connect(pythonTSSActor,
                        SIGNAL(removeTrackingSignal()),
                        this, SLOT(updateSelectedFronts()));

    assert(connected);

    connected = connect(pythonTSSActor,
                        SIGNAL(labelChanged()),
                        this, SLOT(updateLabels()));

    assert(connected);


    sampleSubroutines[PRESSURE_LEVELS_3D]
            << "samplePressureLevel"
            << "pressureLevelGradient";

    sampleSubroutines[HYBRID_SIGMA_PRESSURE_3D]
            << "sampleHybridLevel"
            << "hybridLevelGradient";

    sampleSubroutines[AUXILIARY_PRESSURE_3D]
            << "sampleAuxiliaryPressure"
            << "auxiliaryPressureGradient";

    normalCompSubroutines[PRESSURE_LEVELS_3D]
            << "samplePressureLevel"
            << "pressureLevelGradient";

    normalCompSubroutines[HYBRID_SIGMA_PRESSURE_3D]
            << "sampleHybridLevel"
            << "hybridLevelGradient";

    normalCompSubroutines[AUXILIARY_PRESSURE_3D]
            << "sampleAuxiliaryPressure"
            << "auxiliaryPressureGradient";

    pythonTSSActor->initialize();

  synchronizeWith(sysMC->getSyncControl("Synchronization"), false);
}


void MFrontDetectionActor::onQtPropertyChanged(QtProperty *property)
{
    if (suppressUpdates)
    { return; }

    // TODO: how to handle auto computation?
    // Parent signal processing.
    MNWPMultiVarActor::onQtPropertyChanged(property);

    // ********************* MAIN PROPERTIES *********************
    if (property == autoComputeProperty)
    {
        autoCompute = properties->mBool()->value(autoComputeProperty);
        emitActorChangedSignal();
    }

    else if (property == computeOnGPUProperty)
    {
        computeOnGPU = properties->mBool()->value(computeOnGPUProperty);
        emitActorChangedSignal();
    }

    else if (property == useGeopotentialHeightProperty)
    {
        useGeopotentialHeight = properties->mBool()->value(useGeopotentialHeightProperty);
        emitActorChangedSignal();
    }

    else if (property == render3DFrontProperty)
    {
        render3DFront = properties->mBool()->value(render3DFrontProperty);
        emitActorChangedSignal();
    }

    else if (property == render3DNCProperty)
    {
        render3DNC = properties->mBool()->value(render3DNCProperty);
        trigger3DNormalCurveFilter = true;
        emitActorChangedSignal();
    }

    else if (property == render2DFrontProperty)
    {
        render2DFront = properties->mBool()->value(render2DFrontProperty);
        emitActorChangedSignal();
    }

    else if (property == render2DNCProperty)
    {
        render2DNC = properties->mBool()->value(render2DNCProperty);
        trigger2DNormalCurveFilter = true;
        emitActorChangedSignal();
    }

    else if (property == frontElevationProperty)
    {
        frontElevation2d_hPa = properties->mDDouble()
                                         ->value(frontElevationProperty);
        emitActorChangedSignal();
    }

        // need to ask if applySettingsClickProperty is enabled, because
        // otherwise at each change of applySettingsClickProperty the
        // front request will be triggered. Setting
        // applySettingsClickProperty to true or false would trigger
        // a new front request.
    else if (property == applySettingsClickProperty
            && applySettingsClickProperty->isEnabled())
    {

        if (render3DFront) triggerAsynchronous3DFrontsRequest();
        if (render2DFront) triggerAsynchronous2DFrontsRequest();
        //emitActorChangedSignal();
    }

    else if (property == detectionVariableIndexProperty)
    {
        detectionVariableIndex = properties->mEnum()
                                           ->value(detectionVariableIndexProperty);
        if (detectionVariableIndex < 0) return;

        if (detectionVariableIndex >= variables.size())
        {
            detectionVariableIndex = variables.size() - 1;
            properties->mEnum()->setValue(detectionVariableIndexProperty,
                                          detectionVariableIndex);
        }

        detectionVariable = static_cast<MNWP3DVolumeActorVariable *>(
                variables[detectionVariableIndex]);
        emitActorChangedSignal();
    }

    else if (property == fpmaDistanceProperty)
    {
        fpmaDistanceValue_km = properties->mDDouble()
                                         ->value(fpmaDistanceProperty);
        emitActorChangedSignal();
    }

        // ********************* APPEARANCE PROPERTIES ********************

    else if (property == frontsTubeRadiusProp)
    {
        frontsTubeRadius = properties->mDouble()->value(frontsTubeRadiusProp);
        emitActorChangedSignal();
    }

    else if (property == showFrontTypesProperty)
    {
        showFrontTypes = properties->mBool()->value(showFrontTypesProperty);
        emitActorChangedSignal();
    }

    else if (property == shadingModeProperty)
    {
        shadingMode = properties->mEnum()->value(shadingModeProperty);
        if (shadingMode != 0)
        {
            shadingVarModeProperty->setEnabled(false);
            shadingVariableIndexProperty->setEnabled(false);
        }
        else
        {
            shadingVarModeProperty->setEnabled(true);
            shadingVariableIndexProperty->setEnabled(true);
        }
        recomputeShading3DFronts = true;
        recomputeShading3DNormalCurves = true;
        recomputeShading2DFronts = true;
        recomputeShading2DNormalCurves = true;
        emitActorChangedSignal();
    }

    else if (property == shadingVarModeProperty)
    {
        shadingVarMode = stringToFilterType(
                properties->getEnumItem(shadingVarModeProperty));
        recomputeShading3DFronts = true;
        recomputeShading3DNormalCurves = true;
        recomputeShading2DFronts = true;
        recomputeShading2DNormalCurves = true;
        emitActorChangedSignal();
    }

    else if (property == shadingVariableIndexProperty)
    {
        shadingVariableIndex = properties->mEnum()->value(shadingVariableIndexProperty);
        if (shadingVariableIndex < 0) return;

        if (shadingVariableIndex >= variables.size())
        {
            shadingVariableIndex = variables.size() - 1;
            properties->mEnum()->setValue(shadingVariableIndexProperty, shadingVariableIndex);
        }

        shadingVariable = static_cast<MNWP3DVolumeActorVariable *>(
                variables[shadingVariableIndex]);
        recomputeShading3DFronts = true;
        recomputeShading2DFronts = true;
        emitActorChangedSignal();
    }

    else if (property == windUVarIndexProp)
    {
        windUVariableIndex = properties->mEnum()->value(windUVarIndexProp);
        if (windUVariableIndex < 0) return;

        if (windUVariableIndex >= variables.size())
        {
            windUVariableIndex = variables.size() - 1;
            properties->mEnum()->setValue(windUVarIndexProp, windUVariableIndex);
        }
        windUVar = static_cast<MNWP3DVolumeActorVariable *>(
                variables.at(windUVariableIndex));
        emitActorChangedSignal();
    }

    else if (property == windVVarIndexProp)
    {
        windVVariableIndex = properties->mEnum()->value(windVVarIndexProp);
        if (windVVariableIndex < 0) return;

        if (windVVariableIndex >= variables.size())
        {
            windVVariableIndex = variables.size() - 1;
            properties->mEnum()->setValue(windVVarIndexProp, windVVariableIndex);
        }
        windVVar = static_cast<MNWP3DVolumeActorVariable *>(
                variables.at(windVVariableIndex));
        emitActorChangedSignal();
    }

    else if (property == zVarIndexProp)
    {
        zVariableIndex = properties->mEnum()->value(zVarIndexProp);
        if (zVariableIndex < 0) return;

        if (zVariableIndex >= variables.size())
        {
            zVariableIndex = variables.size() - 1;
            properties->mEnum()->setValue(zVarIndexProp, zVariableIndex);
        }
        zVar = static_cast<MNWP3DVolumeActorVariable *>(
                variables.at(zVariableIndex));
        emitActorChangedSignal();
    }

    else if (property == normalCurvesTubeRadiusProp)
    {
        normalCurvesTubeRadius = properties->mDouble()->value(normalCurvesTubeRadiusProp);
        emitActorChangedSignal();
    }


    else if (property == ncShadingVarIndexProp)
    {
        ncShadingVariableIndex = properties->mEnum()->value(ncShadingVarIndexProp);
        if (ncShadingVariableIndex < 0) return;

        if (ncShadingVariableIndex >= variables.size())
        {
            ncShadingVariableIndex = variables.size() - 1;
            properties->mEnum()->setValue(ncShadingVarIndexProp, ncShadingVariableIndex);
        }
        ncShadingVar = static_cast<MNWP3DVolumeActorVariable *>(
                variables.at(ncShadingVariableIndex));

        trigger2DNormalCurveFilter = true;
        trigger3DNormalCurveFilter = true;

        recomputeShading2DNormalCurves = true;
        recomputeShading3DNormalCurves = true;
        emitActorChangedSignal();
    }

    else if (property == seedPointSpacingProp)
    {
        seedPointSpacing = properties->mDouble()->value(seedPointSpacingProp);
        trigger2DNormalCurveFilter = true;
        trigger3DNormalCurveFilter = true;
        emitActorChangedSignal();
    }

    else if (property == seedPointSpacingZProp)
    {
        seedPointSpacingZ = properties->mInt()->value(seedPointSpacingZProp);
        trigger3DNormalCurveFilter = true;
        emitActorChangedSignal();
    }

    else if (property == ncShadingModeProperty)
    {
        ncShadingMode = properties->mEnum()->value(ncShadingModeProperty);
        recomputeShading2DNormalCurves = true;
        recomputeShading3DNormalCurves = true;
        if (ncShadingMode == 0)
        { ncShadingVarIndexProp->setEnabled(false); }
        else
        { ncShadingVarIndexProp->setEnabled(true); }
        emitActorChangedSignal();
    }

    else if (property == transferFunctionShadingProperty
            || property == transferFunctionTFPProperty
            || property == transferFunctionFSProperty
            || property == transferFunctionABZProperty
            || property == transferFunctionSlopeProperty
            || property == transferFunctionBreadthProperty
            || property == transferFunctionNCShadingProperty)
    {
        setTransferFunctionFromProperty(transferFunctionShadingProperty,
                                        &transferFunctionShading);
        setTransferFunctionFromProperty(transferFunctionTFPProperty,
                                        &transferFunctionTFP);
        setTransferFunctionFromProperty(transferFunctionFSProperty,
                                        &transferFunctionFS);
        setTransferFunctionFromProperty(transferFunctionABZProperty,
                                        &transferFunctionABZ);
        setTransferFunctionFromProperty(transferFunctionSlopeProperty,
                                        &transferFunctionSlope);
        setTransferFunctionFromProperty(transferFunctionBreadthProperty,
                                        &transferFunctionBreadth);
        setTransferFunctionFromProperty(transferFunctionNCShadingProperty,
                                        &transferFunctionNCShading);
        useTFPTransferFunction = (transferFunctionTFP != nullptr);
        useFSTransferFunction = (transferFunctionFS != nullptr);
        useABZTransferFunction = (transferFunctionABZ != nullptr);
        useSlopeTransferFunction = (transferFunctionSlope != nullptr);
        useBreadthTransferFunction = (transferFunctionBreadth != nullptr);

        if (transferFunctionShading != nullptr)

            if (suppressActorUpdates())
            {
                return;
            }

        emitActorChangedSignal();
    }

    else if (property == shadowColorProperty
            || property == shadowHeightProperty
            || property == lightingModeProperty)
    {
        shadowColor = properties->mColor()->value(shadowColorProperty);
        shadowHeight = properties->mDouble()->value(shadowHeightProperty);
        lightingMode = properties->mEnum()->value(lightingModeProperty);
        emitActorChangedSignal();
    }

    else if (property == addNormalCurveFilterProperty)
    {
        enableEmissionOfActorChangedSignal(false);
        int filterNumber =
                normalCurvesFilterSetList.size();
        addNormalCurvesFilter(filterNumber + 1);
        enableEmissionOfActorChangedSignal(true);
        emitActorChangedSignal();
    }

        // front selection settings:
    else if (property == enableSelectionProperty)
    {
        enableSelection = properties->mBool()->value(enableSelectionProperty);
        // Enable picking for the scene view's analysis mode. See
        if (enableSelection)
        {
            enablePicking(true);
        }
        else
        {
            enablePicking(false);
        }
        emitActorChangedSignal();
    }

    else if (property == selectTFPProperty)
    {
        selectTFP = properties->mDouble()->value(selectTFPProperty);
        emitActorChangedSignal();
    }
    else if (property == selectStrengthProperty)
    {
        selectStrength = properties->mDouble()->value(selectStrengthProperty);
        emitActorChangedSignal();
    }
    else if (property == selectBottomPressureProperty)
    {
        selectBottomPressure = properties->mInt()->value(selectBottomPressureProperty);
        emitActorChangedSignal();
    }
    else if (property == selectTopPressureProperty)
    {
        selectTopPressure = properties->mInt()->value(selectTopPressureProperty);
        emitActorChangedSignal();
    }
    else if (property == selectFrontTypeModeProperty)
    {
        selectFrontTypeMode = properties->mEnum()->value(selectFrontTypeModeProperty);
        emitActorChangedSignal();
    }
    else if (property == hideNonSelectedFrontsProperty)
    {
        hideNonSelectedFronts = properties->mBool()->value(hideNonSelectedFrontsProperty);
        emitActorChangedSignal();
    }


        // Show data statistics of the variable.
    else if (property == showDataStatisticsProperty)
    {
        if (suppressActorUpdates())
        {
            return;
        }
        runStatisticalAnalysis(significantDigits,
                               properties->mEnum()->value(histogramDisplayModeProperty));
    }

    else if (property == significantDigitsProperty)
    {
        significantDigits = properties->mInt()->value(significantDigitsProperty);
        emitActorChangedSignal();
    }

    //NormalCurvesFilterSettings *ncFilterSet = nullptr;
    for (NormalCurvesFilterSettings *filter:
            normalCurvesFilterSetList)
    {
        if (property == filter->enabledProperty)
        {
            filter->enabled = properties->mBool()->value(
                    filter->enabledProperty);
            if (filter->enabled)
            {
                bool isConnected;
                isConnected = connect(filter->transferFunction,
                                      SIGNAL(actorChanged()),
                                      this, SLOT(recomputeFrontsAlpha()));
                //assert(isConnected);
            }
            else
            {
                disconnect(filter->transferFunction, 0, this, 0);
            }
            recomputeAlpha3DFronts = true;
            recomputeAlpha3DNormalCurves = true;
            recomputeAlpha2DFronts = true;
            recomputeAlpha2DNormalCurves = true;
            emitActorChangedSignal();
        }
        else if (property == filter->variableProperty)
        {
            filter->variableIndex = properties->mEnum()
                                              ->value(filter->variableProperty);
            recomputeAlpha3DFronts = true;
            recomputeAlpha3DNormalCurves = true;
            recomputeAlpha2DFronts = true;
            recomputeAlpha2DNormalCurves = true;
            emitActorChangedSignal();
        }
        else if (property == filter->transferFunctionProperty)
        {
            if (filter->enabled)
            {
                disconnect(filter->transferFunction, 0, this, 0);
            }
            setNCFilterTransferFunctionFromProperty(filter);
            if (filter->enabled)
            {
                bool isConnected;
                isConnected = connect(filter->transferFunction,
                                      SIGNAL(actorChanged()),
                                      this, SLOT(recomputeFrontsAlpha()));
                //assert(isConnected);
            }
            recomputeAlpha3DFronts = true;
            recomputeAlpha2DFronts = true;
            recomputeAlpha2DNormalCurves = true;
            recomputeAlpha3DNormalCurves = true;
            emitActorChangedSignal();
        }
        else if (property == filter->typeProperty)
        {
            filter->type = stringToFilterType(
                    properties->getEnumItem(filter->typeProperty));
            recomputeAlpha3DFronts = true;
            recomputeAlpha3DNormalCurves = true;
            recomputeAlpha2DFronts = true;
            recomputeAlpha2DNormalCurves = true;
            emitActorChangedSignal();
        }
        else if (property == filter->removeProperty)
        {
            bool redraw = removeNormalCurvesFilter(filter);
            if (redraw)
            {
                recomputeAlpha3DFronts = true;
                recomputeAlpha3DNormalCurves = true;
                recomputeAlpha2DFronts = true;
                recomputeAlpha2DNormalCurves = true;
            }
            emitActorChangedSignal();
        }
    }
}


void MFrontDetectionActor::recomputeFrontsAlpha()
{
    recomputeAlpha3DFronts = true;
    recomputeAlpha3DNormalCurves = true;
    recomputeAlpha2DFronts = true;
    recomputeAlpha2DNormalCurves = true;
    emitActorChangedSignal();
}


// unused, since use OIT render methods
void MFrontDetectionActor::renderToCurrentContext(
        MSceneViewGLWidget *sceneView)
{
    // In analysis mode, render a cross at the position where the user
    // has clicked.
    if (sceneView->analysisModeEnabled()) renderPositionCross(sceneView);
    // Q_UNUSED(sceneView);

    // Render track lines
    // =====================
    pythonTSSActor->render(sceneView);
}


// for translucent objects, this method is called twice
void MFrontDetectionActor::renderTransparencyToCurrentContext(
        MSceneViewGLWidget *sceneView)
{
    // calling render methods with OIT
    render3DFrontSurfaceOIT(sceneView);
    render3DNormalCurvesOIT(sceneView);
    render2DFrontTubesOIT(sceneView);
    render2DNormalCurvesOIT(sceneView);
    //sceneView->getSynchronizationControl()->emitSaveImageSupport();
}


// for translucent objects, this method is called twice
void MFrontDetectionActor::render3DFrontSurfaceOIT(
        MSceneViewGLWidget *sceneView)
{
    if (transferFunctionShading == nullptr
            || !frontSurfaceSelection
            || !render3DFront
            || num3DFrontVertices < 1
            )
    {
        return;
    }

    if (alphaShading3DFronts.size() < 1
            || alphaShading3DFronts.size() != num3DFrontVertices)
    {
        alphaShading3DFronts = QVector<QVector3D>(num3DFrontVertices, QVector3D(1.0, 1.0, 0.0));
        recomputeAlpha3DFronts = true;
        recomputeShading3DFronts = true;
    }

    if (recomputeAlpha3DFronts)
    {
        for (int i = 0; i < num3DFrontVertices; i++)
        {
            alphaShading3DFronts[i][0] = 1;
        }
        for (auto filter: normalCurvesFilterSetList)
        {
            if (!filter->enabled || filter->transferFunction == nullptr) continue;
            MStructuredGrid *f = variables.at(filter->variableIndex)->grid;
            switch (filter->type)
            {
            case ABSOLUTE_CHANGE:
            {
#pragma omp parallel for
                for (int p = 0; p < num3DFrontVertices; p++)
                {
                    QVector3D startPos = frontMesh3D->getVertex(p).position;
                    QVector3D endPos = frontMesh3D->getVertex(p).nCEnd;
                    float vStart = f->interpolateValue(startPos);
                    float vEnd = f->interpolateValue(endPos);
                    float filterValue = vStart - vEnd;
                    alphaShading3DFronts[p][0] *=
                            filter->transferFunction->getColorValue(
                                    filterValue).alphaF();
                }
                break;
            }
            case CHANGE_PER_100KM:
            {
#pragma omp parallel for
                for (int p = 0; p < num3DFrontVertices; p++)
                {
                    QVector3D startPos = frontMesh3D->getVertex(p).position;
                    QVector3D endPos = frontMesh3D->getVertex(p).nCEnd;
                    float vStart = f->interpolateValue(startPos);
                    float vEnd = f->interpolateValue(endPos);
                    float filterValue = (vStart - vEnd) / frontMesh3D->getVertex(p).breadth * 100;
                    alphaShading3DFronts[p][0] *=
                            filter->transferFunction->getColorValue(
                                    filterValue).alphaF();
                }
                break;
            }
            case VALUE_AT_VERTEX:
            {
#pragma omp parallel for
                for (int p = 0; p < num3DFrontVertices; p++)
                {
                    QVector3D startPos = frontMesh3D->getVertex(p).position;
                    float vStart = f->interpolateValue(startPos);
                    alphaShading3DFronts[p][0] *=
                            filter->transferFunction->getColorValue(
                                    vStart).alphaF();
                }
                break;
            }
            }
        }
        recomputeAlpha3DFronts = false;
    }

    if (recomputeShading3DFronts)
    {
        if (shadingMode == 0)
        {
            if (shadingVariableIndex < 0) return;
            MStructuredGrid *f = variables.at(shadingVariableIndex)->grid;
            switch (shadingVarMode)
            {
            case ABSOLUTE_CHANGE:
            {
#pragma omp parallel for
                for (int p = 0; p < num3DFrontVertices; p++)
                {
                    QVector3D startPos = frontMesh3D->getVertex(p).position;
                    QVector3D endPos = frontMesh3D->getVertex(p).nCEnd;
                    float vStart = f->interpolateValue(startPos);
                    float vEnd = f->interpolateValue(endPos);
                    alphaShading3DFronts[p][1] = vStart - vEnd;
                }
                break;
            }
            case CHANGE_PER_100KM:
            {
#pragma omp parallel for
                for (int p = 0; p < num3DFrontVertices; p++)
                {
                    QVector3D startPos = frontMesh3D->getVertex(p).position;
                    QVector3D endPos = frontMesh3D->getVertex(p).nCEnd;
                    float vStart = f->interpolateValue(startPos);
                    float vEnd = f->interpolateValue(endPos);
                    alphaShading3DFronts[p][1] = (vStart - vEnd)
                            / frontMesh3D->getVertex(p).breadth * 100;
                }
                break;
            }
            case VALUE_AT_VERTEX:
            {
#pragma omp parallel for
                for (int p = 0; p < num3DFrontVertices; p++)
                {
                    QVector3D startPos = frontMesh3D->getVertex(p).position;
                    alphaShading3DFronts[p][1] = f->interpolateValue(startPos);
                }
                break;
            }
            }
        }
        else if (shadingMode == 1)
        {
#pragma omp parallel for
            for (int p = 0; p < num3DFrontVertices; p++)
            {
                alphaShading3DFronts[p][1] = frontMesh3D->getVertex(p).position.z();
            }
        }
        else if (shadingMode == 2)
        {
#pragma omp parallel for
            for (int p = 0; p < num3DFrontVertices; p++)
            {
                alphaShading3DFronts[p][1] = frontMesh3D->getVertex(p).tfp;
            }
        }
        else if (shadingMode == 3)
        {
#pragma omp parallel for
            for (int p = 0; p < num3DFrontVertices; p++)
            {
                alphaShading3DFronts[p][1] = frontMesh3D->getVertex(p).abz;
            }
        }
        else if (shadingMode == 4)
        {
#pragma omp parallel for
            for (int p = 0; p < num3DFrontVertices; p++)
            {
                alphaShading3DFronts[p][1] = frontMesh3D->getVertex(p).slope;
            }
        }
        else if (shadingMode == 5)
        {
#pragma omp parallel for
            for (int p = 0; p < num3DFrontVertices; p++)
            {
                alphaShading3DFronts[p][1] = frontMesh3D->getVertex(p).breadth;
            }
        }
        recomputeShading3DFronts = false;
    }

    if (updateSelectedShading)
    {
        for (uint32_t t = 0; t < currentSelectedFront->getNumTriangles(); t++)
        {
            Geometry::MTriangle tri = currentSelectedFront->getTriangle(t);
            alphaShading3DFronts[tri.indices[0]][2] = 1.0;
            alphaShading3DFronts[tri.indices[1]][2] = 1.0;
            alphaShading3DFronts[tri.indices[2]][2] = 1.0;
        }
        updateSelectedShading = false;
    }

    float dataMinZ;
    float dataMaxZ;
    QVector3D minBBox;
    QVector3D maxBBox;

    if (bBoxConnection->getBoundingBox() == nullptr)
    {
        dataMinZ = static_cast<float>(sceneView->worldZfromPressure(
                detectionVariable->grid->getBottomDataVolumePressure_hPa()));
        dataMaxZ = static_cast<float>(sceneView->worldZfromPressure(
                detectionVariable->grid->getTopDataVolumePressure_hPa()));
        minBBox = QVector3D(detectionVariable->grid->getWestDataVolumeCorner_lon(),
                            detectionVariable->grid->getSouthDataVolumeCorner_lat(),
                            dataMinZ);
        maxBBox = QVector3D(detectionVariable->grid->getEastDataVolumeCorner_lon(),
                            detectionVariable->grid->getNorthDataVolumeCorner_lat(),
                            dataMaxZ);
    }
    else
    {
        dataMinZ = static_cast<float>(
                sceneView->worldZfromPressure(bBoxConnection->bottomPressure_hPa()));
        dataMaxZ = static_cast<float>(
                sceneView->worldZfromPressure(bBoxConnection->topPressure_hPa()));
        minBBox = QVector3D(bBoxConnection->westLon(), bBoxConnection->southLat(), dataMinZ);
        maxBBox = QVector3D(bBoxConnection->eastLon(), bBoxConnection->northLat(), dataMaxZ);
    }


    for (auto i = 0; i < 2; ++i)
    {
        if (sceneView->shadowMappingInProgress())
        {
            fronts3DSurfaceShader->bindProgram("TriangleFilteringShadowMap");
        }
        else
        {
            fronts3DSurfaceShader->bindProgram((i == 0) ? "TriangleFilteringShadow"
                                                        : "TriangleFilteringOIT");
        }

        fronts3DSurfaceShader->setUniformValue("mvpMatrix",
                                               *(sceneView->getModelViewProjectionMatrix()));
        fronts3DSurfaceShader->setUniformValue("pToWorldZParams",
                                               sceneView->pressureToWorldZParameters());
        fronts3DSurfaceShader->setUniformValue("lightDirection",
                                               sceneView->getLightDirection());
        fronts3DSurfaceShader->setUniformValue("cameraPosition",
                                               sceneView->getCamera()->getOrigin());

        fronts3DSurfaceShader->setUniformValue("bboxMax", maxBBox);
        fronts3DSurfaceShader->setUniformValue("bboxMin", minBBox);

        fronts3DSurfaceShader->setUniformValue("displayFrontTypes", showFrontTypes);

        fronts3DSurfaceShader->setUniformValue("useTFPFilter", useTFPTransferFunction);
        fronts3DSurfaceShader->setUniformValue("useFSFilter", useFSTransferFunction);
        fronts3DSurfaceShader->setUniformValue("useABZFilter", useABZTransferFunction);
        fronts3DSurfaceShader->setUniformValue("useSlopeFilter", useSlopeTransferFunction);
        fronts3DSurfaceShader->setUniformValue("useBreadthFilter",
                                               useBreadthTransferFunction);

        float shadowHeightWorldZ = sceneView->worldZfromPressure(shadowHeight);
        fronts3DSurfaceShader->setUniformValue("shadowColor", shadowColor);
        fronts3DSurfaceShader->setUniformValue("shadowHeight",
                                               shadowHeightWorldZ);

        fronts3DSurfaceShader->setUniformValue("shadingMode", shadingMode);

        fronts3DSurfaceShader->setUniformValue("lightingMode", lightingMode);

        fronts3DSurfaceShader->setUniformValue("useSelectionShading", useSelectionShading);

        fronts3DSurfaceShader->setUniformValue("hideNonSelectedFronts", hideNonSelectedFronts);

        if (transferFunctionShading)
        {
            transferFunctionShading->getTexture()->bindToTextureUnit(transferFunctionShadingTexUnit);
            CHECK_GL_ERROR;
            fronts3DSurfaceShader->setUniformValue("tfShading", transferFunctionShadingTexUnit);
            CHECK_GL_ERROR;
            fronts3DSurfaceShader->setUniformValue(
                    "tfShadingMinMax", QVector2D(
                            transferFunctionShading->getMinimumValue(),
                            transferFunctionShading->getMaximumValue()));
        }
        if (transferFunctionTFP != nullptr)
        {
            transferFunctionTFP->getTexture()->bindToTextureUnit(transferFunctionTFPTexUnit);
            fronts3DSurfaceShader->setUniformValue("tfTFP", transferFunctionTFPTexUnit);
            CHECK_GL_ERROR;

            fronts3DSurfaceShader->setUniformValue("tfTFPMinMax",
                                                   QVector2D(transferFunctionTFP->getMinimumValue(),
                                                             transferFunctionTFP->getMaximumValue()));
        }

        if (transferFunctionFS)
        {
            transferFunctionFS->getTexture()->bindToTextureUnit(transferFunctionFSTexUnit);
            fronts3DSurfaceShader->setUniformValue("tfFS", transferFunctionFSTexUnit);
            CHECK_GL_ERROR;

            fronts3DSurfaceShader->setUniformValue("tfFSMinMax",
                                                   QVector2D(transferFunctionFS->getMinimumValue(),
                                                             transferFunctionFS->getMaximumValue()));
        }

        if (transferFunctionABZ != nullptr)
        {
            transferFunctionABZ->getTexture()->bindToTextureUnit(transferFunctionABZTexUnit);
            fronts3DSurfaceShader->setUniformValue("tfABZ", transferFunctionABZTexUnit);
            CHECK_GL_ERROR;

            fronts3DSurfaceShader->setUniformValue("tfABZMinMax",
                                                   QVector2D(transferFunctionABZ->getMinimumValue(),
                                                             transferFunctionABZ->getMaximumValue()));
        }

        if (transferFunctionSlope != nullptr)
        {
            transferFunctionSlope->getTexture()->bindToTextureUnit(transferFunctionSlopeTexUnit);
            fronts3DSurfaceShader->setUniformValue("tfSlope", transferFunctionSlopeTexUnit);
            CHECK_GL_ERROR;

            fronts3DSurfaceShader->setUniformValue("tfSlopeMinMax",
                                                   QVector2D(transferFunctionSlope->getMinimumValue(),
                                                             transferFunctionSlope->getMaximumValue()));
        }

        if (transferFunctionBreadth != nullptr)
        {
            transferFunctionBreadth->getTexture()->bindToTextureUnit(transferFunctionBreadthTexUnit);
            fronts3DSurfaceShader->setUniformValue("tfBreadth", transferFunctionBreadthTexUnit);
            CHECK_GL_ERROR;

            fronts3DSurfaceShader->setUniformValue("tfBreadthMinMax",
                                                   QVector2D(transferFunctionBreadth->getMinimumValue(),
                                                             transferFunctionBreadth->getMaximumValue()));
        }

        fronts3DSurfaceShader->setUniformValue(
                "inShadowMappingMode", sceneView->shadowMappingInProgress());
        fronts3DSurfaceShader->setUniformValue(
                "lightMatrix", *sceneView->getLightOrthoProjectionMatrix());

        sceneView->getShadowMap()->bindToTextureUnit(
                static_cast<GLuint>(sceneView->getShadowMapTexUnit()));
        fronts3DSurfaceShader->setUniformValue(
                "shadowMap", sceneView->getShadowMapTexUnit());
        CHECK_GL_ERROR;

        sceneView->setOITUniforms(fronts3DSurfaceShader);

        const QString vboIDa = QString("frontsurface_vbo_shading#%1").arg(myID);
        uploadVec3ToVertexBuffer(alphaShading3DFronts, vboIDa, &vbo3DFrontsShading,
                                 sceneView);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                     ibo3DFronts->getIndexBufferObject());
        CHECK_GL_ERROR;
        glBindBuffer(GL_ARRAY_BUFFER,
                     vbo3DFronts->getVertexBufferObject());
        CHECK_GL_ERROR;

        int fontMeshVertexSize = sizeof(Geometry::FrontMeshVertex);
        long bytePosition = 0;

        // vertex
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, fontMeshVertexSize,
                              nullptr);
        CHECK_GL_ERROR;
        // normals
        bytePosition += 3 * sizeof(float);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, fontMeshVertexSize,
                              (const GLvoid *) bytePosition);
        CHECK_GL_ERROR;
        // tfp
        bytePosition += 9 * sizeof(float); // skip normal curves end vertex and normalZ vertex
        glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, fontMeshVertexSize,
                              (const GLvoid *) bytePosition);
        CHECK_GL_ERROR;
        //abz
        bytePosition += sizeof(float);
        glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, fontMeshVertexSize,
                              (const GLvoid *) bytePosition);
        CHECK_GL_ERROR;
        //strength
        bytePosition += sizeof(float);
        glVertexAttribPointer(4, 1, GL_FLOAT, GL_FALSE, fontMeshVertexSize,
                              (const GLvoid *) bytePosition);
        CHECK_GL_ERROR;
        // type (warm o. cold)
        bytePosition += sizeof(float);
        glVertexAttribPointer(5, 1, GL_FLOAT, GL_FALSE, fontMeshVertexSize,
                              (const GLvoid *) bytePosition);
        CHECK_GL_ERROR;
        // breadth
        bytePosition += sizeof(float);
        glVertexAttribPointer(6, 1, GL_FLOAT, GL_FALSE, fontMeshVertexSize,
                              (const GLvoid *) bytePosition);
        CHECK_GL_ERROR;
        // slope
        bytePosition += sizeof(float);
        glVertexAttribPointer(7, 1, GL_FLOAT, GL_FALSE, fontMeshVertexSize,
                              (const GLvoid *) bytePosition);
        CHECK_GL_ERROR;

        //alpha
        glBindBuffer(GL_ARRAY_BUFFER, vbo3DFrontsShading->getVertexBufferObject());
        CHECK_GL_ERROR;
        glVertexAttribPointer(8, 1, GL_FLOAT, GL_FALSE, sizeof(QVector3D),
                              nullptr);
        CHECK_GL_ERROR;
        //shading
        bytePosition = sizeof(float);
        glVertexAttribPointer(9, 1, GL_FLOAT, GL_FALSE, sizeof(QVector3D),
                              (const GLvoid *) bytePosition);
        CHECK_GL_ERROR;
        bytePosition += sizeof(float);
        glVertexAttribPointer(10, 1, GL_FLOAT, GL_FALSE, sizeof(QVector3D),
                              (const GLvoid *) bytePosition);
        CHECK_GL_ERROR;

        //color


        glEnableVertexAttribArray(0);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(1);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(2);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(3);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(4);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(5);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(6);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(7);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(8);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(9);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(10);
        CHECK_GL_ERROR;

        // Create linked list for fragments
        glPolygonMode(GL_FRONT_AND_BACK, (renderAsWireFrame) ? GL_LINE : GL_FILL);
        CHECK_GL_ERROR;
        glDrawElements(GL_TRIANGLES, frontMesh3D->getNumTriangleIndices(),
                       GL_UNSIGNED_INT, nullptr);
        CHECK_GL_ERROR;

        glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
        glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }
    // 1) Render shadows
    // 2) Render transparent triangle geometry
}


void MFrontDetectionActor::renderPositionCross(
        MSceneViewGLWidget *sceneView)
{
    setBoundingBoxShaderVars(sceneView);

    vboPositionCross->attachToVertexAttribute(SHADER_VERTEX_ATTRIBUTE);

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    CHECK_GL_ERROR;
    glLineWidth(2);
    CHECK_GL_ERROR;

    glDrawArrays(GL_LINES, 0, 6);
    CHECK_GL_ERROR;
}


void MFrontDetectionActor::setBoundingBoxShaderVars(
        MSceneViewGLWidget *sceneView)
{
    boundingBoxShader->bindProgram("Pressure");
    boundingBoxShader->setUniformValue(
            "mvpMatrix", *(sceneView->getModelViewProjectionMatrix()));
    boundingBoxShader->setUniformValue(
            "pToWorldZParams", sceneView->pressureToWorldZParameters());
    boundingBoxShader->setUniformValue(
            "colour", QColor(Qt::black));
}


void MFrontDetectionActor::updatePositionCrossGeometry(
        QVector3D worldSpacePosition)
{
    float size = 2.;
    QVector<QVector3D> vertices;
    QVector3D &p = worldSpacePosition;
    vertices.append(QVector3D(p.x() - size, p.y(), p.z()));
    vertices.append(QVector3D(p.x() + size, p.y(), p.z()));
    vertices.append(QVector3D(p.x(), p.y() - size, p.z()));
    vertices.append(QVector3D(p.x(), p.y() + size, p.z()));
    vertices.append(QVector3D(p.x(), p.y(), p.z() - 40.));
    vertices.append(QVector3D(p.x(), p.y(), p.z() + 40.));

    const QString vboID = QString("vbo_positioncross_actor#%1").arg(myID);
    uploadVec3ToVertexBuffer(vertices, vboID, &vboPositionCross);
}


bool MFrontDetectionActor::triggerAnalysisOfObjectAtPos(
        MSceneViewGLWidget *sceneView, float clipX, float clipY,
        float clipRadius, bool wIsPressed)
{
    if (bBoxConnection->getBoundingBox() == nullptr)
    {
        LOG4CPLUS_DEBUG(mlog, "No bounding box is set.");
        return false;
    }
    if (frontMesh3D == nullptr)
    {
        LOG4CPLUS_DEBUG(mlog, "No variable selected.");
        return false;
    }

    Q_UNUSED(clipRadius);
    LOG4CPLUS_DEBUG(mlog, "triggering isosurface analysis.");

    QVector3D mousePosClipSpace = QVector3D(clipX, clipY, 0.);
    QVector3D mousePosWorldSpace = sceneView->clipSpaceToLonLatWorldZ(
            mousePosClipSpace);

    QVector3D rayOrigin = sceneView->getCamera()->getOrigin();
    QVector3D rayDirection = (mousePosWorldSpace - rayOrigin).normalized();

    // Compute the intersection points of the ray with the volume bounding
    // box. If the ray does not intersect with the box discard this fragment.
    QVector3D volumeTopNECrnr(
            bBoxConnection->eastLon(), bBoxConnection->northLat(),
            sceneView->worldZfromPressure(bBoxConnection->topPressure_hPa()));
    QVector3D volumeBottomSWCrnr(
            bBoxConnection->westLon(), bBoxConnection->southLat(),
            sceneView->worldZfromPressure(bBoxConnection->bottomPressure_hPa()));
    QVector2D lambdaNearFar;

    bool rayIntersectsRenderVolume = rayBoxIntersection(
            rayOrigin, rayDirection, volumeBottomSWCrnr, volumeTopNECrnr,
            &lambdaNearFar);
    if (!rayIntersectsRenderVolume)
    {
        LOG4CPLUS_DEBUG(mlog, "mouse position outside render volume.");
        return false;
    }

    // If the value for lambdaNear is < 0 the camera is located inside the
    // bounding box. It makes no sense to start the ray traversal behind the
    // camera, hence move lambdaNear to 0 to start in front of the camera.
    if (lambdaNearFar.x() < 0.01)
    {
        lambdaNearFar.setX(0.01);
    }

    QVector3D rayPosition;

    bool foundIntersection = false;

    // check which triangle intersects with ray originating from mouse position
    // if triangle is found, check which triangle batch contains this triangle
    // update the index and vortex buffer for rendering -> only render selected frontal surface
    // calculate frontal metrices for this triangle batch
    // send and save the result inside the pythonTSSActor
    for (int i = 0; i < frontMesh3D->getNumTriangles(); i++)
    {
        QVector3D node0 = frontMesh3D->getVertex(frontMesh3D->getTriangle(i).indices[0]).position;
        QVector3D node1 = frontMesh3D->getVertex(frontMesh3D->getTriangle(i).indices[1]).position;
        QVector3D node2 = frontMesh3D->getVertex(frontMesh3D->getTriangle(i).indices[2]).position;

        node0.setZ(sceneView->worldZfromPressure(node0.z()));
        node1.setZ(sceneView->worldZfromPressure(node1.z()));
        node2.setZ(sceneView->worldZfromPressure(node2.z()));

        QVector<QVector3D> triangleVertices = {node0, node1, node2};
        foundIntersection = rayIntersectsTriangle(rayOrigin, rayDirection, triangleVertices,
                                                  rayPosition);
        if (foundIntersection)
        {
            QVector3D lonLatP = rayPosition;
            lonLatP.setZ(sceneView->pressureFromWorldZ(rayPosition.z()));
            LOG4CPLUS_DEBUG_FMT(mlog, "Front hit at position %.2f "
                                      "deg/%.2f deg/%.2f hPa",
                                lonLatP.x(), lonLatP.y(), lonLatP.z());
            updatePositionCrossGeometry(lonLatP);

            QMap<QString, float> metrics;
            // find this triangle in the one of the triangle patches:
            Geometry::MTriangle tRef = frontMesh3D->getTriangle(i);

            // start new impl
            if (alphaShading3DFronts.size() < 1
                    || alphaShading3DFronts.size() != num3DFrontVertices)
            {
                alphaShading3DFronts = QVector<QVector3D>(num3DFrontVertices, QVector3D(1.0, 1.0, 0.0));
                recomputeAlpha3DFronts = true;
                recomputeShading3DFronts = true;
            }
            // end new impl

            for (uint32_t j = 0; j < frontTrianglePatches->getNumPatches(); j++)
            {
                currentSelectedFront = frontTrianglePatches->getTrianglePatch(j);
                for (uint32_t k = 0; k < currentSelectedFront->getNumTriangles(); k++)
                {
                    if (currentSelectedFront->getTriangle(k).indices[0] == tRef.indices[0]
                            || currentSelectedFront->getTriangle(k).indices[1] == tRef.indices[1]
                            || currentSelectedFront->getTriangle(k).indices[2] == tRef.indices[2])
                    {
                        metrics = calcMetricOfFrontalPatch(currentSelectedFront);

                        int mem = detectionVariable->getEnsembleMember();
                        QDateTime datetime = detectionVariable->getSynchronizationControl()->validDateTime();

                        if (wIsPressed)
                        {
                            QVector<MPythonTSSActor::VariableTracking> sameIdentifierTracks =
                                    pythonTSSActor->popTracksOfIdentifier(mem, datetime);

                            float value;
                            for (QString k: metrics.keys())
                            {
                                if (k.contains("min"))
                                {
                                    value = metrics[k];
                                    if (k.contains("slope") && k.contains("neg"))
                                    {
                                        for (MPythonTSSActor::VariableTracking track: sameIdentifierTracks)
                                        {
                                            if (track.metrics[k] > value)
                                            { value = track.metrics[k]; }
                                        }
                                    }
                                    else
                                    {
                                        for (MPythonTSSActor::VariableTracking track: sameIdentifierTracks)
                                        {
                                            if (track.metrics[k] < value)
                                            { value = track.metrics[k]; }
                                        }
                                    }
                                    metrics[k] = value;
                                }

                                else if (k.contains("max"))
                                {
                                    value = metrics[k];
                                    if (k.contains("slope") && k.contains("neg"))
                                    {
                                        for (MPythonTSSActor::VariableTracking track: sameIdentifierTracks)
                                        {
                                            if (track.metrics[k] < value)
                                            { value = track.metrics[k]; }
                                        }
                                    }
                                    else
                                    {
                                        for (MPythonTSSActor::VariableTracking track: sameIdentifierTracks)
                                        {
                                            if (track.metrics[k] > value)
                                            { value = track.metrics[k]; }
                                        }
                                    }
                                    metrics[k] = value;
                                }
                                else if ((k.contains("average") || k.contains("centroid")) && !k.contains("slope"))
                                {
                                    value = metrics[k] * metrics["totalFrontArea_m2"];
                                    float total_area = metrics["totalFrontArea_m2"];
                                    for (MPythonTSSActor::VariableTracking track: sameIdentifierTracks)
                                    {
                                        value += track.metrics[k] * track.metrics["totalFrontArea_m2"];
                                        total_area += track.metrics["totalFrontArea_m2"];
                                    }
                                    metrics[k] = value / total_area;
                                }
                            }

                            float lengthXYAvg = metrics["slope_normal_lengthXY_average"] * metrics["totalFrontArea_m2"];
                            float lengthZAvg = metrics["slope_normal_lengthZ_average"] * metrics["totalFrontArea_m2"];
                            float total_area = metrics["totalFrontArea_m2"];

                            for (MPythonTSSActor::VariableTracking track: sameIdentifierTracks)
                            {
                                lengthXYAvg += track.metrics["slope_normal_lengthXY_average"]
                                        * track.metrics["totalFrontArea_m2"];
                                lengthZAvg += track.metrics["slope_normal_lengthZ_average"]
                                        * track.metrics["totalFrontArea_m2"];
                                total_area += track.metrics["totalFrontArea_m2"];
                            }
                            metrics["slope_normal_lengthXY_average"] = lengthXYAvg / total_area;
                            metrics["slope_normal_lengthZ_average"] = lengthZAvg / total_area;
                            metrics["slope_average"] = lengthXYAvg / lengthZAvg;


                            value = metrics["totalFrontArea_m2"];
                            for (MPythonTSSActor::VariableTracking track: sameIdentifierTracks)
                            {
                                value += track.metrics["totalFrontArea_m2"];
                            }
                            metrics["totalFrontArea_m2"] = value;

                            value = metrics["numberOfFronts"];
                            for (MPythonTSSActor::VariableTracking track: sameIdentifierTracks)
                            {
                                value += track.metrics["numberOfFronts"];
                            }
                            metrics["numberOfFronts"] = value;

                            LOG4CPLUS_DEBUG_FMT(mlog, "Values were composed of is %.0f fronts",
                                                metrics["numberOfFronts"]);

                        }

                        lonLatP = QVector3D(metrics["centroid_x"], metrics["centroid_y"], metrics["centroid_z"]);

                        pythonTSSActor->triggerAnalysis3DFronts(lonLatP, metrics, mem, datetime);

                        // start new impl


                        for (uint32_t t = 0; t < currentSelectedFront->getNumTriangles(); t++)
                        {
                            Geometry::MTriangle tri = currentSelectedFront->getTriangle(t);
                            alphaShading3DFronts[tri.indices[0]][2] = 1.0;
                            alphaShading3DFronts[tri.indices[1]][2] = 1.0;
                            alphaShading3DFronts[tri.indices[2]][2] = 1.0;
                        }
                        useSelectionShading = true;
                        updateSelectedShading = true;
                        // end new impl
                        // for frontal tracking
                        pythonTSSActor->initTFPStartStats(tfpStats, QVector2D(lonLatP.x(), lonLatP.y()));
                        emitActorChangedSignal();
                        return true;
                    }
                }
            }
        }
    }
    // calculate frontal metrices for this triangle batch
    // send and save the result inside the pythonTSSActor

    // If we arrive here no isosurface has been hit.
    LOG4CPLUS_DEBUG(mlog, "no isosurface could be identified at mouse position.");
    return false;
}


bool MFrontDetectionActor::rayBoxIntersection(
        QVector3D rayOrigin, QVector3D rayDirection,
        QVector3D boxCrnr1, QVector3D boxCrnr2, QVector2D *tNearFar)
{
    float tnear = 0.;
    float tfar = 0.;

    QVector3D rayDirInv = QVector3D(1. / rayDirection.x(), 1. / rayDirection.y(),
                                    1. / rayDirection.z());
    if (rayDirInv.x() >= 0.0)
    {
        tnear = (boxCrnr1.x() - rayOrigin.x()) * rayDirInv.x();
        tfar = (boxCrnr2.x() - rayOrigin.x()) * rayDirInv.x();
    }
    else
    {
        tnear = (boxCrnr2.x() - rayOrigin.x()) * rayDirInv.x();
        tfar = (boxCrnr1.x() - rayOrigin.x()) * rayDirInv.x();
    }

    if (rayDirInv.y() >= 0.0)
    {
        tnear = max(tnear, float((boxCrnr1.y() - rayOrigin.y()) * rayDirInv.y()));
        tfar = min(tfar, float((boxCrnr2.y() - rayOrigin.y()) * rayDirInv.y()));
    }
    else
    {
        tnear = max(tnear, float((boxCrnr2.y() - rayOrigin.y()) * rayDirInv.y()));
        tfar = min(tfar, float((boxCrnr1.y() - rayOrigin.y()) * rayDirInv.y()));
    }

    if (rayDirInv.z() >= 0.0)
    {
        tnear = max(tnear, float((boxCrnr1.z() - rayOrigin.z()) * rayDirInv.z()));
        tfar = min(tfar, float((boxCrnr2.z() - rayOrigin.z()) * rayDirInv.z()));
    }
    else
    {
        tnear = max(tnear, float((boxCrnr2.z() - rayOrigin.z()) * rayDirInv.z()));
        tfar = min(tfar, float((boxCrnr1.z() - rayOrigin.z()) * rayDirInv.z()));
    }

    tNearFar->setX(tnear);
    tNearFar->setY(tfar);
    return (tnear < tfar);
}


bool MFrontDetectionActor::rayIntersectsTriangle(
        QVector3D rayOrigin, QVector3D rayDirection,
        QVector<QVector3D> triangleVertices, QVector3D &intersectionPoint)
{
    const float EPSILON = 0.0000001;
    QVector3D vertex0 = triangleVertices.at(0);
    QVector3D vertex1 = triangleVertices.at(1);
    QVector3D vertex2 = triangleVertices.at(2);
    QVector3D edge1, edge2, h, s, q;
    float a, f, u, v;
    edge1 = vertex1 - vertex0;
    edge2 = vertex2 - vertex0;
    h = QVector3D::crossProduct(rayDirection, edge2);
    a = QVector3D::dotProduct(edge1, h);
    if (a > -EPSILON && a < EPSILON)
        return false;    // This ray is parallel to this triangle.
    f = 1.0 / a;
    s = rayOrigin - vertex0;
    u = f * QVector3D::dotProduct(s, h);
    if (u < 0.0 || u > 1.0) return false;
    q = QVector3D::crossProduct(s, edge1);
    v = f * QVector3D::dotProduct(rayDirection, q);
    if (v < 0.0 || u + v > 1.0) return false;
    // At this stage we can compute t to find out where the intersection point is on the line.
    float t = f * QVector3D::dotProduct(edge2, q);
    if (t > EPSILON) // ray intersection
    {
        intersectionPoint = rayOrigin + rayDirection * t;
        return true;
    }
    else // This means that there is a line intersection but not a ray intersection.
        return false;
}


QMap<QString, float> MFrontDetectionActor::calcMetricOfFrontalPatch(MTriangleSelection *p)
{
    float totalFrontArea_m2 = .0;

    float tfpAvg = .0;
    float tfpMax = .0;

    float strengthAvg_100km = .0;
    float strengthMax_100km = .0;

    float lengthXYAvg = .0;
    float lengthZAvg = .0;
    float slopeMinPos = 999.9;
    float slopeMaxPos = 0.0;
    float slopeMinNeg = -999.9;
    float slopeMaxNeg = -0.0;

    float x_centroid = .0;
    float y_centroid = .0;
    float z_centroid = .0;

    tfpStats = QVector<float>(p->getNumTriangles());
    strengthStats = QVector<float>(p->getNumTriangles());
    areaStats = QVector<float>(p->getNumTriangles());
    slopeStats = QVector<float>(p->getNumTriangles());


    for (uint32_t i = 0; i < p->getNumTriangles(); i++)
    {
        int i1 = p->getTriangle(i).indices[0];
        int i2 = p->getTriangle(i).indices[1];
        int i3 = p->getTriangle(i).indices[2];

        Geometry::FrontMeshVertex v1 = frontMesh3D->getVertex(i1);
        Geometry::FrontMeshVertex v2 = frontMesh3D->getVertex(i2);
        Geometry::FrontMeshVertex v3 = frontMesh3D->getVertex(i3);

        QVector3D pos1 = v1.position;
        QVector3D pos2 = v2.position;
        QVector3D pos3 = v3.position;

//        QVector3D normalZ1 = v1.normalZ;
//        QVector3D normalZ2 = v2.normalZ;
//        QVector3D normalZ3 = v3.normalZ;

        // compute triangle area
        float triangleArea_m2 = .0;
        if (useGeopotentialHeight)
        {
            triangleArea_m2 = calcualteTriangleAreaWithGeopotentialHeight(pos1, pos2, pos3);
            totalFrontArea_m2 += triangleArea_m2;
        }
        else
        {
            triangleArea_m2 = frontMesh3D->calculateAreaOfTriangle_standardICAO(i1, i2, i3);
            totalFrontArea_m2 += triangleArea_m2;
        }
        // calc triangle area


        // calculate centroid of triangle
        x_centroid += (pos1.x() + pos2.x() + pos3.x()) * triangleArea_m2 / 3;
        y_centroid += (pos1.y() + pos2.y() + pos3.y()) * triangleArea_m2 / 3;
        z_centroid += (pos1.z() + pos2.z() + pos3.z()) * triangleArea_m2 / 3;

        // TFP
        // average
        tfpAvg += (v1.tfp + v2.tfp + v3.tfp) / 3 * triangleArea_m2;
        // calc tfp max
        if (v1.tfp > tfpMax)
        { tfpMax = v1.tfp; }
        if (v2.tfp > tfpMax)
        { tfpMax = v2.tfp; }
        if (v3.tfp > tfpMax)
        { tfpMax = v3.tfp; }

        // STRENGTH
        // strength average
        float strength1_100km = v1.strength / v1.breadth * 100;
        float strength2_100km = v2.strength / v2.breadth * 100;
        float strength3_100km = v3.strength / v3.breadth * 100;
        strengthAvg_100km += (strength1_100km + strength2_100km + strength3_100km) / 3 * triangleArea_m2;
        // strength max
        if (strength1_100km > strengthMax_100km)
        { strengthMax_100km = strength1_100km; }
        if (strength2_100km > strengthMax_100km)
        { strengthMax_100km = strength2_100km; }
        if (strength3_100km > strengthMax_100km)
        { strengthMax_100km = strength3_100km; }

//        // the average frontal slope is calculated as the average normal for each triangle vortex weighted with the area of the triangle.
//        // The normal of each triangle is added to the overall average and later divided by the total front area.
//        normalZAvg += (normalZ1 + normalZ2 + normalZ3) / 3 * triangleArea_m2;
//
//
//
//        // New calculation of frontal slope
//        //todo check computation
//        QVector3D normalZAvgNew = (normalZ1 + normalZ2 + normalZ3) / 3;
//        lengthXYAvg += std::sqrt(
//                normalZAvgNew.x() * normalZAvgNew.x() + normalZAvgNew.y() * normalZAvgNew.y()) * triangleArea_m2;
//        lengthZAvg += -normalZAvgNew.z() * triangleArea_m2;

        // Here we cant take the maximum of the frontal slope. The slope of a front is defined as the tangent of the angle
        // between the normal and the xy-plane. However, if the frontal surface is exact vertical, the slope is infinity (no defined).
        // Therefore it makes more sense to look at the tangent between the normal of the frontal surface and the z-axis. This is equal to
        // the inverse of the slope.

        // frontal slope of triangles.
        // First, convert spherical coordinates to cartesian coordinates.
        // Second, rotate the cartesian coordinate system, so that the z-axis is in line with the first vertex of the triangle.
        // Third, compute slope of triangle.

        // transformation
        QVector3D v1Cart = MetRoutinesExperimental::lonLatPressureToGlobalCart(v1.position);
        QVector3D v2Cart = MetRoutinesExperimental::lonLatPressureToGlobalCart(v2.position);
        QVector3D v3Cart = MetRoutinesExperimental::lonLatPressureToGlobalCart(v3.position);

        // rotation
        QVector3D v1CartRot = MetRoutinesExperimental::cartesianRotation(v1Cart, v1.position.x(), v1.position.y());
        QVector3D v2CartRot = MetRoutinesExperimental::cartesianRotation(v2Cart, v1.position.x(), v1.position.y());
        QVector3D v3CartRot = MetRoutinesExperimental::cartesianRotation(v3Cart, v1.position.x(), v1.position.y());

        // slope
        QVector3D u = v2CartRot - v1CartRot;
        QVector3D v = v3CartRot - v1CartRot;

        QVector3D normalsNew = QVector3D::crossProduct(u, v);

        float lengthXY = std::sqrt(normalsNew.x() * normalsNew.x() + normalsNew.y() * normalsNew.y());
        float lengthZ = normalsNew.z();
        float slope = lengthXY / lengthZ;

        lengthXYAvg += lengthXY * triangleArea_m2;
        lengthZAvg += lengthZ * triangleArea_m2;

        if (slope >= 0.)
        {
            if (slope > slopeMaxPos) slopeMaxPos = slope;
            if (slope < slopeMinPos) slopeMinPos = slope;
        }
        else
        {
            if (slope < slopeMaxNeg) slopeMaxNeg = slope;
            if (slope > slopeMinNeg) slopeMinNeg = slope;
        }

        // stats for histogram:
        tfpStats[int(i)] = (v1.tfp + v2.tfp + v3.tfp) / 3;
        strengthStats[int(i)] = (strength1_100km + strength2_100km + strength3_100km) / 3;
        // slopeStats[int(i)] = slope;
        slopeStats[int(i)] = std::atan(lengthZ / lengthXY) * 180 / M_PI;
        //slopeStats[int(i)] =
        //        (std::acos(lengthXY / std::sqrt(lengthXY * lengthXY + lengthZ * lengthZ)) * 180 / M_PI) * -1;
        areaStats[int(i)] = triangleArea_m2 / 1000000.;
    }

    tfpAvg = tfpAvg / totalFrontArea_m2;
    strengthAvg_100km = strengthAvg_100km / totalFrontArea_m2;

    const float slopeAvg = lengthXYAvg / lengthZAvg;

    QMap<QString, float> metrics;
    metrics["centroid_x"] = x_centroid / totalFrontArea_m2;
    metrics["centroid_y"] = y_centroid / totalFrontArea_m2;
    metrics["centroid_z"] = z_centroid / totalFrontArea_m2;

    metrics["tfp_average"] = tfpAvg;
    metrics["tfp_max"] = tfpMax;
    metrics["strength_average"] = strengthAvg_100km;
    metrics["strength_max"] = strengthMax_100km;
    metrics["slope_average"] = slopeAvg;
    metrics["slope_min_pos"] = slopeMinPos;
    metrics["slope_max_pos"] = slopeMaxPos;
    metrics["slope_min_neg"] = slopeMinNeg;
    metrics["slope_max_neg"] = slopeMaxNeg;
    metrics["slope_normal_lengthXY_average"] = lengthXYAvg / totalFrontArea_m2;
    metrics["slope_normal_lengthZ_average"] = lengthZAvg / totalFrontArea_m2;
    metrics["totalFrontArea_m2"] = totalFrontArea_m2;
    metrics["numberOfFronts"] = 1;

    LOG4CPLUS_DEBUG_FMT(mlog, "Total number of selected triangles %i", p->getNumTriangles());

    LOG4CPLUS_DEBUG_FMT(mlog, "Average TFP of frontal surface is %.3f", tfpAvg);
    LOG4CPLUS_DEBUG_FMT(mlog, "Average strength of frontal surface is %.3f (k/100km)", strengthAvg_100km);
    LOG4CPLUS_DEBUG_FMT(mlog, "Max strength of frontal surface is %.3f (k/100km)", strengthMax_100km);

    LOG4CPLUS_DEBUG_FMT(mlog, "Average slope of frontal surface is %.4f", slopeAvg);
    LOG4CPLUS_DEBUG_FMT(mlog, "Max positive slope of frontal surface is %.4f", slopeMaxPos);
    LOG4CPLUS_DEBUG_FMT(mlog, "Min positive slope of frontal surface is %.4f", slopeMinPos);
    LOG4CPLUS_DEBUG_FMT(mlog, "Max negative slope of frontal surface is %.4f", slopeMaxNeg);
    LOG4CPLUS_DEBUG_FMT(mlog, "Min negative slope of frontal surface is %.4f", slopeMinNeg);


    LOG4CPLUS_DEBUG_FMT(mlog, "Average length XY frontal surface is %.10f", lengthXYAvg / totalFrontArea_m2);
    LOG4CPLUS_DEBUG_FMT(mlog, "Average length Z frontal surface is %.10f", lengthZAvg / totalFrontArea_m2);

    LOG4CPLUS_DEBUG_FMT(mlog, "Total selected area is %.3f (km^2)", totalFrontArea_m2 / 1000000);

    return metrics;
}


float MFrontDetectionActor::calcualteTriangleAreaWithGeopotentialHeight(QVector3D pos1, QVector3D pos2, QVector3D pos3)
{
    // transform vertices to cartesian coordinates
    QVector3D a = MetRoutinesExperimental::lonLatMetreToGlobalCart(pos1.x(), pos1.y(),
                                                                   zVar->grid->interpolateValue(pos1));
    QVector3D b = MetRoutinesExperimental::lonLatMetreToGlobalCart(pos2.x(), pos2.y(),
                                                                   zVar->grid->interpolateValue(pos2));
    QVector3D c = MetRoutinesExperimental::lonLatMetreToGlobalCart(pos3.x(), pos3.y(),
                                                                   zVar->grid->interpolateValue(pos3));

    QVector3D ab = b - a;
    QVector3D ac = c - a;

    return 0.5 * QVector3D::crossProduct(ab, ac).length();
}


void MFrontDetectionActor::render3DNormalCurvesOIT(
        MSceneViewGLWidget *sceneView)
{
    // ########################################################################
    // ### CHECK IF ALL VALUES ARE VALID
    // ########################################################################

    if (transferFunctionNCShading == nullptr
            || !frontSurfaceSelection
            || !render3DNC)
    {
        return;
    }

    if (trigger3DNormalCurveFilter)
    {
        filter3DNormalCurves();
        trigger3DNormalCurveFilter = false;
    }

    if (!normalCurves3D
            || normalCurves3D->getNumNormalCurveSegments() < 1)
    {
        return;
    }

    int numNormalCurveSegments = normalCurves3D->getNumNormalCurveSegments();

    if (alphaShading3DNormalCurves.size() < 1
            || alphaShading3DNormalCurves.size() != numNormalCurveSegments)
    {
        alphaShading3DNormalCurves =
                QVector<QVector2D>(numNormalCurveSegments, QVector2D(1.0, 1.0));
        recomputeAlpha3DNormalCurves = true;
        recomputeShading3DNormalCurves = true;
    }

    if (recomputeAlpha3DNormalCurves)
    {
        for (int i = 0; i < numNormalCurveSegments; i++)
        {
            alphaShading3DNormalCurves[i][0] = 1;
        }
        for (auto filter: normalCurvesFilterSetList)
        {
            if (!filter->enabled || filter->transferFunction == nullptr) continue;
            MStructuredGrid *aGrid = variables.at(filter->variableIndex)->grid;
            switch (filter->type)
            {
            case ABSOLUTE_CHANGE:
            {
                for (int i = 0; i < numNormalCurveSegments; i++)
                {
                    Geometry::NormalCurveVertex nc = normalCurves3D->getNormalCurveSegment(i);
                    float vStart = aGrid->interpolateValue(nc.start);
                    float vEnd = aGrid->interpolateValue(nc.end);
                    float filterValue = vStart - vEnd;
                    alphaShading3DNormalCurves[i][0] *=
                            filter->transferFunction->getColorValue(
                                    filterValue).alphaF();
                }
                break;
            }
            case CHANGE_PER_100KM:
            {
                for (int i = 0; i < numNormalCurveSegments; i++)
                {
                    Geometry::NormalCurveVertex nc = normalCurves3D->getNormalCurveSegment(i);
                    float vStart = aGrid->interpolateValue(nc.start);
                    float vEnd = aGrid->interpolateValue(nc.end);
                    float filterValue = (vStart - vEnd)
                            / nc.breadth * 100.;
                    alphaShading3DNormalCurves[i][0] *=
                            filter->transferFunction->getColorValue(
                                    filterValue).alphaF();
                }
                break;
            }
            case VALUE_AT_VERTEX:
            {
                for (int i = 0; i < numNormalCurveSegments; i++)
                {
                    Geometry::NormalCurveVertex nc = normalCurves3D->getNormalCurveSegment(i);
                    float vStart = aGrid->interpolateValue(nc.start);
                    alphaShading3DNormalCurves[i][0] *=
                            filter->transferFunction->getColorValue(
                                    vStart).alphaF();
                }
                break;
            }
            }
        }
        recomputeAlpha3DNormalCurves = false;
    }

    if (recomputeShading3DNormalCurves)
    {
        if (ncShadingMode == 1
                || ncShadingMode == 2
                || ncShadingMode == 3)
        {
            if (ncShadingVariableIndex < 0) return;
            MStructuredGrid *sGrid = variables.at(ncShadingVariableIndex)->grid;
            if (ncShadingMode == 1) // shading value
            {
                for (int i = 0; i < numNormalCurveSegments; i++)
                {
                    Geometry::NormalCurveVertex nc = normalCurves3D->getNormalCurveSegment(i);
                    float v = sGrid->interpolateValue(nc.start);
                    alphaShading3DNormalCurves[i][1] = v;
                }
            }
            else if (ncShadingMode == 2) // "changePer100km"
            {
                for (int i = 0; i < numNormalCurveSegments; i++)
                {
                    Geometry::NormalCurveVertex nc = normalCurves3D->getNormalCurveSegment(i);
                    float vStart = sGrid->interpolateValue(nc.start);
                    float vEnd = sGrid->interpolateValue(nc.end);
                    float shadingValue = (vStart - vEnd)
                            / nc.breadth * 100.;
                    alphaShading3DNormalCurves[i][1] = shadingValue;
                }
            }
            else if (ncShadingMode == 3) // "absoluteChangeWithinFrontalZone"
            {
                for (int i = 0; i < numNormalCurveSegments; i++)
                {
                    Geometry::NormalCurveVertex nc = normalCurves3D->getNormalCurveSegment(i);
                    float vStart = sGrid->interpolateValue(
                            nc.start);
                    float vEnd = sGrid->interpolateValue(
                            nc.end);
                    alphaShading3DNormalCurves[i][1] = vStart - vEnd;
                }
            }
        }
        recomputeShading3DNormalCurves = false;
    }

    float dataMinZ;
    float dataMaxZ;
    QVector3D minBBox;
    QVector3D maxBBox;

    if (bBoxConnection->getBoundingBox() == nullptr)
    {
        dataMinZ = static_cast<float>(sceneView->worldZfromPressure(
                detectionVariable->grid->getBottomDataVolumePressure_hPa()));
        dataMaxZ = static_cast<float>(sceneView->worldZfromPressure(
                detectionVariable->grid->getTopDataVolumePressure_hPa()));
        minBBox = QVector3D(detectionVariable->grid->getWestDataVolumeCorner_lon(),
                            detectionVariable->grid->getSouthDataVolumeCorner_lat(),
                            dataMinZ);
        maxBBox = QVector3D(detectionVariable->grid->getEastDataVolumeCorner_lon(),
                            detectionVariable->grid->getNorthDataVolumeCorner_lat(),
                            dataMaxZ);
    }
    else
    {
        dataMinZ = static_cast<float>(
                sceneView->worldZfromPressure(bBoxConnection->bottomPressure_hPa()));
        dataMaxZ = static_cast<float>(
                sceneView->worldZfromPressure(bBoxConnection->topPressure_hPa()));
        minBBox = QVector3D(bBoxConnection->westLon(), bBoxConnection->southLat(), dataMinZ);
        maxBBox = QVector3D(bBoxConnection->eastLon(), bBoxConnection->northLat(), dataMaxZ);
    }

    // ########################################################################
    // ### BIND BUFFER AND SET UNIFORM VALUES IN GLSL
    // ########################################################################

    for (auto i = 0; i < 2; ++i)
    {
        if (sceneView->shadowMappingInProgress())
        {
            normalCurvesTubeShaderOIT->bindProgram("TubeFilteringShadowMap3D");
        }
        else
        {
            normalCurvesTubeShaderOIT->bindProgram((i == 0) ? "TubeFilteringShadow3D"
                                                            : "TubeFilteringOIT3D");
        }

        normalCurvesTubeShaderOIT->setUniformValue(
                "pToWorldZParams", sceneView->pressureToWorldZParameters());

        normalCurvesTubeShaderOIT->setUniformValue(
                "mvpMatrix", *(sceneView->getModelViewProjectionMatrix()));
        CHECK_GL_ERROR;
        normalCurvesTubeShaderOIT->setUniformValue(
                "cameraPosition", sceneView->getCamera()->getOrigin());
        CHECK_GL_ERROR;

        // Lighting direction from scene view.
        normalCurvesTubeShaderOIT->setUniformValue(
                "lightDirection", sceneView->getLightDirection());
        CHECK_GL_ERROR;

        normalCurvesTubeShaderOIT->setUniformValue("bboxMax", maxBBox);
        normalCurvesTubeShaderOIT->setUniformValue("bboxMin", minBBox);

        transferFunctionNCShading->getTexture()->bindToTextureUnit(
                transferFunctionNCShadingTexUnit);
        normalCurvesTubeShaderOIT->setUniformValue(
                "transferFunction", transferFunctionNCShadingTexUnit);

        normalCurvesTubeShaderOIT->setUniformValue(
                "tfMinimum", transferFunctionNCShading->getMinimumValue());
        normalCurvesTubeShaderOIT->setUniformValue(
                "tfMaximum", transferFunctionNCShading->getMaximumValue());

        normalCurvesTubeShaderOIT->setUniformValue(
                "normalized", false);

        normalCurvesTubeShaderOIT->setUniformValue("tubeRadius", normalCurvesTubeRadius);
        CHECK_GL_ERROR;

        float shadowHeightWorldZ = sceneView->worldZfromPressure(shadowHeight);
        normalCurvesTubeShaderOIT->setUniformValue("shadowColor", shadowColor);
        normalCurvesTubeShaderOIT->setUniformValue("shadowHeight",
                                                   shadowHeightWorldZ);

        normalCurvesTubeShaderOIT->setUniformValue("useTFPFilter", useTFPTransferFunction);
        normalCurvesTubeShaderOIT->setUniformValue("useABZFilter", useABZTransferFunction);
        normalCurvesTubeShaderOIT->setUniformValue("useFSFilter", useFSTransferFunction);
        normalCurvesTubeShaderOIT->setUniformValue("useBreadthFilter", useBreadthTransferFunction);

        if (transferFunctionTFP)
        {
            transferFunctionTFP->getTexture()->bindToTextureUnit(transferFunctionTFPTexUnit);
            normalCurvesTubeShaderOIT->setUniformValue("tfTFP", transferFunctionTFPTexUnit);
            CHECK_GL_ERROR;

            normalCurvesTubeShaderOIT->setUniformValue("tfTFPMinMax",
                                                       QVector2D(transferFunctionTFP->getMinimumValue(),
                                                                 transferFunctionTFP->getMaximumValue()));
        }

        if (transferFunctionABZ)
        {
            transferFunctionABZ->getTexture()->bindToTextureUnit(transferFunctionABZTexUnit);
            normalCurvesTubeShaderOIT->setUniformValue("tfABZ", transferFunctionABZTexUnit);
            CHECK_GL_ERROR;

            normalCurvesTubeShaderOIT->setUniformValue("tfABZMinMax",
                                                       QVector2D(transferFunctionABZ->getMinimumValue(),
                                                                 transferFunctionABZ->getMaximumValue()));
        }

        if (transferFunctionFS)
        {
            transferFunctionFS->getTexture()->bindToTextureUnit(transferFunctionFSTexUnit);
            normalCurvesTubeShaderOIT->setUniformValue("tfFS", transferFunctionFSTexUnit);
            CHECK_GL_ERROR;

            normalCurvesTubeShaderOIT->setUniformValue("tfFSMinMax",
                                                       QVector2D(transferFunctionFS->getMinimumValue(),
                                                                 transferFunctionFS->getMaximumValue()));
        }

        if (transferFunctionBreadth)
        {
            transferFunctionBreadth->getTexture()->bindToTextureUnit(transferFunctionBreadthTexUnit);
            normalCurvesTubeShaderOIT->setUniformValue("tfBreadth",
                                                       transferFunctionBreadthTexUnit);
            CHECK_GL_ERROR;
            normalCurvesTubeShaderOIT->setUniformValue(
                    "tfBreadthMinMax",
                    QVector2D(transferFunctionBreadth->getMinimumValue(),
                              transferFunctionBreadth->getMaximumValue()));
        }

        normalCurvesTubeShaderOIT->setUniformValue(
                "inShadowMappingMode", sceneView->shadowMappingInProgress());

        normalCurvesTubeShaderOIT->setUniformValue(
                "shadingMode", ncShadingMode);

        sceneView->getShadowMap()->bindToTextureUnit(
                static_cast<GLuint>(sceneView->getShadowMapTexUnit()));
        normalCurvesTubeShaderOIT->setUniformValue(
                "shadowMap", sceneView->getShadowMapTexUnit());
        CHECK_GL_ERROR;

        sceneView->setOITUniforms(normalCurvesTubeShaderOIT);
        // ########################################################################
        // ### GET RESOURCE MANAGER AND UPLOAD DATA TO VERTEX BUFFER
        // ########################################################################

        const QString vboIDs = QString("normal_curves_vbo_shading#%1").arg(myID);
        uploadVec2ToVertexBuffer(alphaShading3DNormalCurves, vboIDs,
                                 &vbo3DNormalCurvesShading, sceneView);

        //front vertices
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                     ibo3DNormalCurves->getIndexBufferObject());
        CHECK_GL_ERROR;
        glBindBuffer(GL_ARRAY_BUFFER,
                     vbo3DNormalCurves->getVertexBufferObject());
        CHECK_GL_ERROR;

        int normalCurveVertexSize = sizeof(Geometry::NormalCurveVertex);
        long bytePosition = 0;

        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, normalCurveVertexSize,
                              nullptr);
        CHECK_GL_ERROR;

        bytePosition += 9 * sizeof(float); // tfp, 9 because of start and end points  (2 * QVector3D)
        glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, normalCurveVertexSize,
                              (const GLvoid *) bytePosition);
        CHECK_GL_ERROR;

        bytePosition += sizeof(float); // abz
        glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, normalCurveVertexSize,
                              (const GLvoid *) bytePosition);
        CHECK_GL_ERROR;

        bytePosition += sizeof(float); // strength
        glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, normalCurveVertexSize,
                              (const GLvoid *) bytePosition);
        CHECK_GL_ERROR;

        bytePosition += sizeof(float); // type
        glVertexAttribPointer(4, 1, GL_FLOAT, GL_FALSE, normalCurveVertexSize,
                              (const GLvoid *) bytePosition);
        CHECK_GL_ERROR;

        bytePosition += sizeof(float); // breadth
        glVertexAttribPointer(5, 1, GL_FLOAT, GL_FALSE, normalCurveVertexSize,
                              (const GLvoid *) bytePosition);
        CHECK_GL_ERROR;

        glBindBuffer(GL_ARRAY_BUFFER, vbo3DNormalCurvesShading->getVertexBufferObject());
        CHECK_GL_ERROR;
        glVertexAttribPointer(6, 1, GL_FLOAT, GL_FALSE, sizeof(QVector2D),
                              nullptr);
        CHECK_GL_ERROR;

        glVertexAttribPointer(7, 1, GL_FLOAT, GL_FALSE, sizeof(QVector2D),
                              (const GLvoid *) sizeof(float));
        CHECK_GL_ERROR;

        glEnableVertexAttribArray(0);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(1);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(2);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(3);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(4);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(5);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(6);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(7);
        CHECK_GL_ERROR;

        glPrimitiveRestartIndex(restartIndex);
        glEnable(GL_PRIMITIVE_RESTART);

        glEnable(GL_CULL_FACE);
        CHECK_GL_ERROR;
        glCullFace(GL_FRONT);

        glPolygonMode(GL_BACK, GL_FILL);
        CHECK_GL_ERROR;
        glDrawElements(GL_LINE_STRIP_ADJACENCY, numNormalCurveSegments, GL_UNSIGNED_INT, nullptr);
        CHECK_GL_ERROR;

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        CHECK_GL_ERROR;
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        CHECK_GL_ERROR;
        glDisable(GL_CULL_FACE);
        CHECK_GL_ERROR;
        glDisable(GL_PRIMITIVE_RESTART);
    }
}


void MFrontDetectionActor::render2DFrontTubesOIT(
        MSceneViewGLWidget *sceneView)
{
    // ########################################################################
    // ### CHECK IF ALL VALUES ARE VALID
    // ########################################################################
    if (transferFunctionShading == nullptr
            || !frontLineSelection
            || !render2DFront
            || num2DFrontVertices < 1)
    {
        return;
    }


    MNWP2DHorizontalActorVariable *varThermal =
            static_cast<MNWP2DHorizontalActorVariable *>(
                    variables.at(detectionVariableIndex)
            );

    if (!varThermal->hasData())
    {
        return;
    }

    // Don't render 2d front lines if horizontal slice position is outside the
    // data domain.

    if (varThermal->grid->getLevelType() != SURFACE_2D &&
            (varThermal->grid->getBottomDataVolumePressure_hPa() < frontElevation2d_hPa
                    || varThermal->grid->getTopDataVolumePressure_hPa() > frontElevation2d_hPa))
    {
        return;
    }


    if (alphaShading2DFronts.size() < 1
            || alphaShading2DFronts.size() != num2DFrontVertices)
    {
        alphaShading2DFronts = QVector<QVector2D>(num2DFrontVertices, QVector2D(1.0, 1.0));
        recomputeAlpha2DFronts = true;
        recomputeShading2DFronts = true;
    }


    if (recomputeAlpha2DFronts)
    {
        for (int i = 0; i < num2DFrontVertices; i++)
        {
            alphaShading2DFronts[i][0] = 1;
        }
        for (auto filter: normalCurvesFilterSetList)
        {
            if (!filter->enabled || filter->transferFunction == nullptr) continue;
            MStructuredGrid *aGrid = variables.at(filter->variableIndex)->grid;
            switch (filter->type)
            {
            case ABSOLUTE_CHANGE:
            {
                for (int i = 0; i < num2DFrontVertices; i++)
                {
                    float vStart = aGrid->interpolateValue(
                            frontLines2D->getVertex(i).position);
                    float vEnd = aGrid->interpolateValue(
                            frontLines2D->getVertex(i).nCEnd);
                    float filterValue = vStart - vEnd;
                    alphaShading2DFronts[i][0] *=
                            filter->transferFunction->getColorValue(
                                    filterValue).alphaF();
                }
                break;
            }
            case CHANGE_PER_100KM:
            {
                for (int i = 0; i < num2DFrontVertices; i++)
                {
                    float vStart = aGrid->interpolateValue(
                            frontLines2D->getVertex(i).position);
                    float vEnd = aGrid->interpolateValue(
                            frontLines2D->getVertex(i).nCEnd);
                    float filterValue = (vStart - vEnd)
                            / frontLines2D->getVertex(i).breadth * 100.;
                    alphaShading2DFronts[i][0] *=
                            filter->transferFunction->getColorValue(
                                    filterValue).alphaF();
                }
                break;
            }
            case VALUE_AT_VERTEX:
            {
                for (int i = 0; i < num2DFrontVertices; i++)
                {
                    float vStart = aGrid->interpolateValue(
                            frontLines2D->getVertex(i).position);
                    alphaShading2DFronts[i][0] *=
                            filter->transferFunction->getColorValue(
                                    vStart).alphaF();
                }
                break;
            }
            }
        }
        recomputeAlpha2DFronts = false;
    }

    if (recomputeShading2DFronts)
    {
        if (shadingMode == 0
                || shadingMode == 4)
        {
            if (shadingVariableIndex < 0) return;
            MStructuredGrid *sGrid = variables.at(shadingVariableIndex)->grid;
            switch (shadingVarMode)
            {
            case ABSOLUTE_CHANGE:
            {
                for (int i = 0; i < num2DFrontVertices; i++)
                {
                    float vStart = sGrid->interpolateValue(
                            frontLines2D->getVertex(i).position);
                    float vEnd = sGrid->interpolateValue(
                            frontLines2D->getVertex(i).nCEnd);
                    alphaShading2DFronts[i][1] = vStart - vEnd;
                }
                break;
            }
            case CHANGE_PER_100KM:
            {
                for (int i = 0; i < num2DFrontVertices; i++)
                {
                    float vStart = sGrid->interpolateValue(
                            frontLines2D->getVertex(i).position);
                    float vEnd = sGrid->interpolateValue(
                            frontLines2D->getVertex(i).nCEnd);
                    alphaShading2DFronts[i][1] = (vStart - vEnd)
                            / frontLines2D->getVertex(i).breadth * 100.;
                }
                break;
            }
            case VALUE_AT_VERTEX:
            {
                for (int i = 0; i < num2DFrontVertices; i++)
                {
                    alphaShading2DFronts[i][1] = sGrid->interpolateValue(
                            frontLines2D->getVertex(i).position);
                }
                break;
            }
            }
        }
        else if (shadingMode == 1)
        {
            for (int i = 0; i < num2DFrontVertices; i++)
            {
                alphaShading2DFronts[i][1] = frontElevation2d_hPa;
            }
        }
        else if (shadingMode == 2)
        {
            for (int i = 0; i < num2DFrontVertices; i++)
            {
                alphaShading2DFronts[i][1] = frontLines2D->getVertex(i).tfp;
            }
        }
        else if (shadingMode == 3)
        {
            for (int i = 0; i < num2DFrontVertices; i++)
            {
                alphaShading2DFronts[i][1] = frontLines2D->getVertex(i).abz;
            }
        }
        else if (shadingMode == 5)
        {
            for (int i = 0; i < num2DFrontVertices; i++)
            {
                alphaShading2DFronts[i][1] = frontLines2D->getVertex(i).breadth;
            }
        }
        recomputeShading2DFronts = false;
    }

    float dataMinZ;
    float dataMaxZ;
    QVector3D minBBox;
    QVector3D maxBBox;

    if (bBoxConnection->getBoundingBox() == nullptr)
    {
        dataMinZ = static_cast<float>(sceneView->worldZfromPressure(
                detectionVariable->grid->getBottomDataVolumePressure_hPa()));
        dataMaxZ = static_cast<float>(sceneView->worldZfromPressure(
                detectionVariable->grid->getTopDataVolumePressure_hPa()));
        minBBox = QVector3D(detectionVariable->grid->getWestDataVolumeCorner_lon(),
                            detectionVariable->grid->getSouthDataVolumeCorner_lat(),
                            dataMinZ);
        maxBBox = QVector3D(detectionVariable->grid->getEastDataVolumeCorner_lon(),
                            detectionVariable->grid->getNorthDataVolumeCorner_lat(),
                            dataMaxZ);
    }
    else
    {
        dataMinZ = static_cast<float>(
                sceneView->worldZfromPressure(bBoxConnection->bottomPressure_hPa()));
        dataMaxZ = static_cast<float>(
                sceneView->worldZfromPressure(bBoxConnection->topPressure_hPa()));
        minBBox = QVector3D(bBoxConnection->westLon(), bBoxConnection->southLat(), dataMinZ);
        maxBBox = QVector3D(bBoxConnection->eastLon(), bBoxConnection->northLat(), dataMaxZ);
    }

    float worldZPos = sceneView->worldZfromPressure(frontElevation2d_hPa);
    // ########################################################################
    // ### BIND BUFFER AND SET UNIFORM VALUES IN GLSL
    // ########################################################################

    for (auto i = 0; i < 2; ++i)
    {
        if (sceneView->shadowMappingInProgress())
        {
            frontTubeShaderOIT->bindProgram("TubeFilteringShadowMap");
        }
        else
        {
            frontTubeShaderOIT->bindProgram((i == 0) ? "TubeFilteringShadow"
                                                     : "TubeFilteringOIT");
        }

        frontTubeShaderOIT->setUniformValue(
                "worldZPos",
                worldZPos);
        frontTubeShaderOIT->setUniformValue(
                "mvpMatrix", *(sceneView->getModelViewProjectionMatrix()));
        CHECK_GL_ERROR;
        frontTubeShaderOIT->setUniformValue(
                "cameraPosition", sceneView->getCamera()->getOrigin());
        CHECK_GL_ERROR;

        // Lighting direction from scene view.
        frontTubeShaderOIT->setUniformValue(
                "lightDirection", sceneView->getLightDirection());
        CHECK_GL_ERROR;

        frontTubeShaderOIT->setUniformValue("bboxMax", maxBBox);
        frontTubeShaderOIT->setUniformValue("bboxMin", minBBox);

        transferFunctionShading->getTexture()->bindToTextureUnit(
                transferFunctionShadingTexUnit);
        frontTubeShaderOIT->setUniformValue(
                "transferFunction", transferFunctionShadingTexUnit);

        frontTubeShaderOIT->setUniformValue(
                "tfMinimum", transferFunctionShading->getMinimumValue());
        frontTubeShaderOIT->setUniformValue(
                "tfMaximum", transferFunctionShading->getMaximumValue());

        frontTubeShaderOIT->setUniformValue(
                "normalized", false);

        frontTubeShaderOIT->setUniformValue("tubeRadius", frontsTubeRadius);
        CHECK_GL_ERROR;

        float shadowHeightWorldZ = sceneView->worldZfromPressure(shadowHeight);
        frontTubeShaderOIT->setUniformValue("shadowColor", shadowColor);
        frontTubeShaderOIT->setUniformValue("shadowHeight",
                                            shadowHeightWorldZ);
        frontTubeShaderOIT->setUniformValue("displayFrontTypes", showFrontTypes);
        frontTubeShaderOIT->setUniformValue("useTFPFilter", useTFPTransferFunction);
        frontTubeShaderOIT->setUniformValue("useABZFilter", useABZTransferFunction);
        frontTubeShaderOIT->setUniformValue("useBreadthFilter", useBreadthTransferFunction);
        frontTubeShaderOIT->setUniformValue("useFSFilter", useFSTransferFunction);

        if (transferFunctionTFP)
        {
            transferFunctionTFP->getTexture()->bindToTextureUnit(transferFunctionTFPTexUnit);
            frontTubeShaderOIT->setUniformValue("tfTFP", transferFunctionTFPTexUnit);
            CHECK_GL_ERROR;

            frontTubeShaderOIT->setUniformValue("tfTFPMinMax",
                                                QVector2D(transferFunctionTFP->getMinimumValue(),
                                                          transferFunctionTFP->getMaximumValue()));
        }

        if (transferFunctionABZ)
        {
            transferFunctionABZ->getTexture()->bindToTextureUnit(transferFunctionABZTexUnit);
            frontTubeShaderOIT->setUniformValue("tfABZ", transferFunctionABZTexUnit);
            CHECK_GL_ERROR;

            frontTubeShaderOIT->setUniformValue("tfABZMinMax",
                                                QVector2D(transferFunctionABZ->getMinimumValue(),
                                                          transferFunctionABZ->getMaximumValue()));
        }


        if (transferFunctionFS)
        {
            transferFunctionFS->getTexture()->bindToTextureUnit(transferFunctionFSTexUnit);
            frontTubeShaderOIT->setUniformValue("tfFS", transferFunctionFSTexUnit);
            CHECK_GL_ERROR;

            frontTubeShaderOIT->setUniformValue("tfFSMinMax",
                                                QVector2D(transferFunctionFS->getMinimumValue(),
                                                          transferFunctionFS->getMaximumValue()));
        }

        if (transferFunctionBreadth)
        {
            transferFunctionBreadth->getTexture()->bindToTextureUnit(transferFunctionBreadthTexUnit);
            frontTubeShaderOIT->setUniformValue("tfBreadth",
                                                transferFunctionBreadthTexUnit);
            CHECK_GL_ERROR;
            frontTubeShaderOIT->setUniformValue(
                    "tfBreadthMinMax",
                    QVector2D(transferFunctionBreadth->getMinimumValue(),
                              transferFunctionBreadth->getMaximumValue()));
        }

        frontTubeShaderOIT->setUniformValue(
                "inShadowMappingMode", sceneView->shadowMappingInProgress());

        sceneView->getShadowMap()->bindToTextureUnit(
                static_cast<GLuint>(sceneView->getShadowMapTexUnit()));
        frontTubeShaderOIT->setUniformValue(
                "shadowMap", sceneView->getShadowMapTexUnit());
        CHECK_GL_ERROR;

        sceneView->setOITUniforms(frontTubeShaderOIT);
        // ########################################################################
        // ### GET RESOURCE MANAGER AND UPLOAD DATA TO VERTEX BUFFER
        // ########################################################################

        const QString vboIDs = QString("frontline_vbo_shading#%1").arg(myID);
        uploadVec2ToVertexBuffer(alphaShading2DFronts, vboIDs, &vbo2DFrontsShading,
                                 sceneView);

        //front vertices
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                     ibo2DFronts->getIndexBufferObject());
        CHECK_GL_ERROR;
        glBindBuffer(GL_ARRAY_BUFFER,
                     vbo2DFronts->getVertexBufferObject());
        CHECK_GL_ERROR;

        int fontLineVertexSize = sizeof(Geometry::FrontLineVertex);
        long bytePosition = 0;

        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, fontLineVertexSize,
                              nullptr);
        CHECK_GL_ERROR;

        bytePosition += 6 * sizeof(float); // tfp 6 because there of ncEnd point (QVector3D)
        glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, fontLineVertexSize,
                              (const GLvoid *) bytePosition);
        CHECK_GL_ERROR;

        bytePosition += sizeof(float); // abz
        glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, fontLineVertexSize,
                              (const GLvoid *) bytePosition);
        CHECK_GL_ERROR;

        bytePosition += sizeof(float); // strength
        glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, fontLineVertexSize,
                              (const GLvoid *) bytePosition);
        CHECK_GL_ERROR;

        bytePosition += sizeof(float); // type
        glVertexAttribPointer(4, 1, GL_FLOAT, GL_FALSE, fontLineVertexSize,
                              (const GLvoid *) bytePosition);
        CHECK_GL_ERROR;

        bytePosition += sizeof(float); // breadth
        glVertexAttribPointer(5, 1, GL_FLOAT, GL_FALSE, fontLineVertexSize,
                              (const GLvoid *) bytePosition);
        CHECK_GL_ERROR;

        glBindBuffer(GL_ARRAY_BUFFER, vbo2DFrontsShading->getVertexBufferObject());
        CHECK_GL_ERROR;
        glVertexAttribPointer(6, 1, GL_FLOAT, GL_FALSE, sizeof(QVector2D),
                              nullptr);
        CHECK_GL_ERROR;

        glVertexAttribPointer(7, 1, GL_FLOAT, GL_FALSE, sizeof(QVector2D),
                              (const GLvoid *) sizeof(float));
        CHECK_GL_ERROR;

        glEnableVertexAttribArray(0);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(1);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(2);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(3);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(4);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(5);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(6);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(7);
        CHECK_GL_ERROR;

//        u_int32_t restartIndex = std::numeric_limits<u_int32_t>::max();
        glPrimitiveRestartIndex(restartIndex);
        glEnable(GL_PRIMITIVE_RESTART);

        glEnable(GL_CULL_FACE);
        CHECK_GL_ERROR;
        glCullFace(GL_FRONT);

        glPolygonMode(GL_BACK, GL_FILL);
        CHECK_GL_ERROR;
        glDrawElements(GL_LINE_STRIP_ADJACENCY, num2DFrontVertices,
                       GL_UNSIGNED_INT, nullptr);
        CHECK_GL_ERROR;

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        CHECK_GL_ERROR;
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        CHECK_GL_ERROR;
        glDisable(GL_CULL_FACE);
        CHECK_GL_ERROR;
        glDisable(GL_PRIMITIVE_RESTART);

    }

}


void MFrontDetectionActor::render2DNormalCurvesOIT(
        MSceneViewGLWidget *sceneView)
{
    // ########################################################################
    // ### CHECK IF ALL VALUES ARE VALID
    // ########################################################################
    if (transferFunctionNCShading == nullptr
            || !frontLineSelection
            || !render2DNC)
    {
        return;
    }

    if (trigger2DNormalCurveFilter)
    {
        filter2DNormalCurves();
        trigger2DNormalCurveFilter = false;
    }

    if (!normalCurves2D
            || normalCurves2D->getNumNormalCurveSegments() < 1)
    {
        return;
    }


    MNWP2DHorizontalActorVariable *varThermal =
            static_cast<MNWP2DHorizontalActorVariable *>(
                    variables.at(detectionVariableIndex)
            );

    // Don't render if horizontal slice position is outside the
    // data domain.
    if (varThermal->grid->getLevelType() != SURFACE_2D &&
            (varThermal->grid->getBottomDataVolumePressure_hPa() < frontElevation2d_hPa
                    || varThermal->grid->getTopDataVolumePressure_hPa() > frontElevation2d_hPa))
    {
        return;
    }

    int numNormalCurveSegments = normalCurves2D->getNumNormalCurveSegments();

    if (alphaShading2DNormalCurves.size() < 1
            || alphaShading2DNormalCurves.size() != numNormalCurveSegments)
    {
        alphaShading2DNormalCurves = QVector<QVector2D>(numNormalCurveSegments, QVector2D(1.0, 1.0));
        recomputeAlpha2DNormalCurves = true;
        recomputeShading2DNormalCurves = true;
    }

    if (recomputeAlpha2DNormalCurves)
    {
        for (int i = 0; i < numNormalCurveSegments; i++)
        {
            alphaShading2DNormalCurves[i][0] = 1;
        }
        for (auto filter: normalCurvesFilterSetList)
        {
            if (!filter->enabled || filter->transferFunction == nullptr) continue;
            MStructuredGrid *aGrid = variables.at(filter->variableIndex)->grid;
            switch (filter->type)
            {
            case ABSOLUTE_CHANGE:
            {
                for (int i = 0; i < numNormalCurveSegments; i++)
                {
                    Geometry::NormalCurveVertex nc = normalCurves2D->getNormalCurveSegment(i);
                    float vStart = aGrid->interpolateValue(nc.start);
                    float vEnd = aGrid->interpolateValue(nc.end);
                    float filterValue = vStart - vEnd;
                    alphaShading2DNormalCurves[i][0] *=
                            filter->transferFunction->getColorValue(
                                    filterValue).alphaF();
                }
                break;
            }
            case CHANGE_PER_100KM:
            {
                for (int i = 0; i < numNormalCurveSegments; i++)
                {
                    Geometry::NormalCurveVertex nc = normalCurves2D->getNormalCurveSegment(i);
                    float vStart = aGrid->interpolateValue(nc.start);
                    float vEnd = aGrid->interpolateValue(nc.end);
                    float filterValue = (vStart - vEnd) / nc.breadth * 100.;
                    alphaShading2DNormalCurves[i][0] *=
                            filter->transferFunction->getColorValue(
                                    filterValue).alphaF();
                }
                break;
            }
            case VALUE_AT_VERTEX:
            {
                for (int i = 0; i < numNormalCurveSegments; i++)
                {
                    Geometry::NormalCurveVertex nc = normalCurves2D->getNormalCurveSegment(i);
                    float vStart = aGrid->interpolateValue(nc.start);
                    alphaShading2DNormalCurves[i][0] *=
                            filter->transferFunction->getColorValue(
                                    vStart).alphaF();
                }
                break;
            }
            }
        }
        recomputeAlpha2DNormalCurves = false;
    }

    if (recomputeShading2DNormalCurves)
    {
        if (ncShadingMode == 1
                || ncShadingMode == 2
                || ncShadingMode == 3)
        {
            if (ncShadingVariableIndex < 0) return;
            MStructuredGrid *sGrid = variables.at(ncShadingVariableIndex)->grid;
            if (ncShadingMode == 1) // "valueAtFrontline"
            {
                for (int i = 0; i < numNormalCurveSegments; i++)
                {
                    Geometry::NormalCurveVertex nc = normalCurves2D->getNormalCurveSegment(i);
                    float v = sGrid->interpolateValue(nc.start);
                    alphaShading2DNormalCurves[i][1] = v;
                }
            }
            else if (ncShadingMode == 2) // "changePer100km"
            {
                for (int i = 0; i < numNormalCurveSegments; i++)
                {
                    Geometry::NormalCurveVertex nc = normalCurves2D->getNormalCurveSegment(i);
                    float vStart = sGrid->interpolateValue(nc.start);
                    float vEnd = sGrid->interpolateValue(nc.end);
                    float shadingValue = (vStart - vEnd) / nc.breadth * 100.;
                    alphaShading2DNormalCurves[i][1] = shadingValue;
                }
            }
            else if (ncShadingMode == 3) // "absoluteChangeWithinFrontalZone"
            {
                for (int i = 0; i < numNormalCurveSegments; i++)
                {
                    Geometry::NormalCurveVertex nc = normalCurves2D->getNormalCurveSegment(i);
                    float vStart = sGrid->interpolateValue(nc.start);
                    float vEnd = sGrid->interpolateValue(nc.end);
                    alphaShading2DNormalCurves[i][1] = vStart - vEnd;
                }
            }
        }
        recomputeShading2DNormalCurves = false;
    }


    float dataMinZ;
    float dataMaxZ;
    QVector3D minBBox;
    QVector3D maxBBox;

    if (bBoxConnection->getBoundingBox() == nullptr)
    {
        dataMinZ = static_cast<float>(sceneView->worldZfromPressure(
                detectionVariable->grid->getBottomDataVolumePressure_hPa()));
        dataMaxZ = static_cast<float>(sceneView->worldZfromPressure(
                detectionVariable->grid->getTopDataVolumePressure_hPa()));
        minBBox = QVector3D(detectionVariable->grid->getWestDataVolumeCorner_lon(),
                            detectionVariable->grid->getSouthDataVolumeCorner_lat(),
                            dataMinZ);
        maxBBox = QVector3D(detectionVariable->grid->getEastDataVolumeCorner_lon(),
                            detectionVariable->grid->getNorthDataVolumeCorner_lat(),
                            dataMaxZ);
    }
    else
    {
        dataMinZ = static_cast<float>(
                sceneView->worldZfromPressure(bBoxConnection->bottomPressure_hPa()));
        dataMaxZ = static_cast<float>(
                sceneView->worldZfromPressure(bBoxConnection->topPressure_hPa()));
        minBBox = QVector3D(bBoxConnection->westLon(), bBoxConnection->southLat(), dataMinZ);
        maxBBox = QVector3D(bBoxConnection->eastLon(), bBoxConnection->northLat(), dataMaxZ);
    }

//    float worldZPos = sceneView->worldZfromPressure(frontElevation2d_hPa);
    // ########################################################################
    // ### BIND BUFFER AND SET UNIFORM VALUES IN GLSL
    // ########################################################################

    for (auto i = 0; i < 2; ++i)
    {
        if (sceneView->shadowMappingInProgress())
        {
            normalCurvesTubeShaderOIT->bindProgram("TubeFilteringShadowMap3D");
        }
        else
        {
            normalCurvesTubeShaderOIT->bindProgram((i == 0) ? "TubeFilteringShadow3D"
                                                            : "TubeFilteringOIT3D");
        }

        normalCurvesTubeShaderOIT->setUniformValue(
                "pToWorldZParams", sceneView->pressureToWorldZParameters());

//        normalCurvesTubeShaderOIT->setUniformValue(
//                "worldZPos",
//                worldZPos);
        normalCurvesTubeShaderOIT->setUniformValue(
                "mvpMatrix", *(sceneView->getModelViewProjectionMatrix()));
        CHECK_GL_ERROR;
        normalCurvesTubeShaderOIT->setUniformValue(
                "cameraPosition", sceneView->getCamera()->getOrigin());
        CHECK_GL_ERROR;

        // Lighting direction from scene view.
        normalCurvesTubeShaderOIT->setUniformValue(
                "lightDirection", sceneView->getLightDirection());
        CHECK_GL_ERROR;

        normalCurvesTubeShaderOIT->setUniformValue("bboxMax", maxBBox);
        normalCurvesTubeShaderOIT->setUniformValue("bboxMin", minBBox);

        transferFunctionNCShading->getTexture()->bindToTextureUnit(
                transferFunctionNCShadingTexUnit);
        normalCurvesTubeShaderOIT->setUniformValue(
                "transferFunction", transferFunctionNCShadingTexUnit);

        normalCurvesTubeShaderOIT->setUniformValue(
                "tfMinimum", transferFunctionNCShading->getMinimumValue());
        normalCurvesTubeShaderOIT->setUniformValue(
                "tfMaximum", transferFunctionNCShading->getMaximumValue());

        normalCurvesTubeShaderOIT->setUniformValue(
                "normalized", false);

        normalCurvesTubeShaderOIT->setUniformValue("tubeRadius", normalCurvesTubeRadius);
        CHECK_GL_ERROR;

        float shadowHeightWorldZ = sceneView->worldZfromPressure(shadowHeight);
        normalCurvesTubeShaderOIT->setUniformValue("shadowColor", shadowColor);
        normalCurvesTubeShaderOIT->setUniformValue("shadowHeight",
                                                   shadowHeightWorldZ);

        normalCurvesTubeShaderOIT->setUniformValue("useTFPFilter", useTFPTransferFunction);
        normalCurvesTubeShaderOIT->setUniformValue("useABZFilter", useABZTransferFunction);
        normalCurvesTubeShaderOIT->setUniformValue("useFSFilter", useFSTransferFunction);
        normalCurvesTubeShaderOIT->setUniformValue("useBreadthFilter", useBreadthTransferFunction);

        if (transferFunctionTFP)
        {
            transferFunctionTFP->getTexture()->bindToTextureUnit(transferFunctionTFPTexUnit);
            normalCurvesTubeShaderOIT->setUniformValue("tfTFP", transferFunctionTFPTexUnit);
            CHECK_GL_ERROR;

            normalCurvesTubeShaderOIT->setUniformValue("tfTFPMinMax",
                                                       QVector2D(transferFunctionTFP->getMinimumValue(),
                                                                 transferFunctionTFP->getMaximumValue()));
        }

        if (transferFunctionABZ)
        {
            transferFunctionABZ->getTexture()->bindToTextureUnit(transferFunctionABZTexUnit);
            normalCurvesTubeShaderOIT->setUniformValue("tfABZ", transferFunctionABZTexUnit);
            CHECK_GL_ERROR;

            normalCurvesTubeShaderOIT->setUniformValue("tfABZMinMax",
                                                       QVector2D(transferFunctionABZ->getMinimumValue(),
                                                                 transferFunctionABZ->getMaximumValue()));
        }

        if (transferFunctionFS)
        {
            transferFunctionFS->getTexture()->bindToTextureUnit(transferFunctionFSTexUnit);
            normalCurvesTubeShaderOIT->setUniformValue("tfFS", transferFunctionFSTexUnit);
            CHECK_GL_ERROR;

            normalCurvesTubeShaderOIT->setUniformValue("tfFSMinMax",
                                                       QVector2D(transferFunctionFS->getMinimumValue(),
                                                                 transferFunctionFS->getMaximumValue()));
        }

        if (transferFunctionBreadth)
        {
            transferFunctionBreadth->getTexture()->bindToTextureUnit(transferFunctionBreadthTexUnit);
            normalCurvesTubeShaderOIT->setUniformValue("tfBreadth",
                                                       transferFunctionBreadthTexUnit);
            CHECK_GL_ERROR;
            normalCurvesTubeShaderOIT->setUniformValue(
                    "tfBreadthMinMax",
                    QVector2D(transferFunctionBreadth->getMinimumValue(),
                              transferFunctionBreadth->getMaximumValue()));
        }

        normalCurvesTubeShaderOIT->setUniformValue(
                "inShadowMappingMode", sceneView->shadowMappingInProgress());

        normalCurvesTubeShaderOIT->setUniformValue(
                "shadingMode", ncShadingMode);

        sceneView->getShadowMap()->bindToTextureUnit(
                static_cast<GLuint>(sceneView->getShadowMapTexUnit()));
        normalCurvesTubeShaderOIT->setUniformValue(
                "shadowMap", sceneView->getShadowMapTexUnit());
        CHECK_GL_ERROR;

        sceneView->setOITUniforms(normalCurvesTubeShaderOIT);
        // ########################################################################
        // ### GET RESOURCE MANAGER AND UPLOAD DATA TO VERTEX BUFFER
        // ########################################################################

        const QString vboIDs = QString("normal_curves_vbo_shading#%1").arg(myID);
        uploadVec2ToVertexBuffer(alphaShading2DNormalCurves, vboIDs,
                                 &vbo2DNormalCurvesShading, sceneView);

        //front vertices
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                     ibo2DNormalCurves->getIndexBufferObject());
        CHECK_GL_ERROR;
        glBindBuffer(GL_ARRAY_BUFFER,
                     vbo2DNormalCurves->getVertexBufferObject());
        CHECK_GL_ERROR;

        int normalCurveVertexSize = sizeof(Geometry::NormalCurveVertex);
        long bytePosition = 0;

        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, normalCurveVertexSize,
                              nullptr);
        CHECK_GL_ERROR;

        bytePosition += 9 * sizeof(float); // tfp, 9 because of start and end points  2 * (QVector3D)
        glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, normalCurveVertexSize,
                              (const GLvoid *) bytePosition);
        CHECK_GL_ERROR;

        bytePosition += sizeof(float); // abz
        glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, normalCurveVertexSize,
                              (const GLvoid *) bytePosition);
        CHECK_GL_ERROR;

        bytePosition += sizeof(float); // strength
        glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, normalCurveVertexSize,
                              (const GLvoid *) bytePosition);
        CHECK_GL_ERROR;

        bytePosition += sizeof(float); // type
        glVertexAttribPointer(4, 1, GL_FLOAT, GL_FALSE, normalCurveVertexSize,
                              (const GLvoid *) bytePosition);
        CHECK_GL_ERROR;

        bytePosition += sizeof(float); // breadth
        glVertexAttribPointer(5, 1, GL_FLOAT, GL_FALSE, normalCurveVertexSize,
                              (const GLvoid *) bytePosition);
        CHECK_GL_ERROR;

        glBindBuffer(GL_ARRAY_BUFFER, vbo2DNormalCurvesShading->getVertexBufferObject());
        CHECK_GL_ERROR;
        glVertexAttribPointer(6, 1, GL_FLOAT, GL_FALSE, sizeof(QVector2D),
                              nullptr);
        CHECK_GL_ERROR;

        glVertexAttribPointer(7, 1, GL_FLOAT, GL_FALSE, sizeof(QVector2D),
                              (const GLvoid *) sizeof(float));
        CHECK_GL_ERROR;

        glEnableVertexAttribArray(0);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(1);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(2);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(3);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(4);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(5);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(6);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(7);
        CHECK_GL_ERROR;

//        u_int32_t restartIndex = std::numeric_limits<u_int32_t>::max();
        glPrimitiveRestartIndex(restartIndex);
        glEnable(GL_PRIMITIVE_RESTART);

        glEnable(GL_CULL_FACE);
        CHECK_GL_ERROR;
        glCullFace(GL_FRONT);

        glPolygonMode(GL_BACK, GL_FILL);
        CHECK_GL_ERROR;
        glDrawElements(GL_LINE_STRIP_ADJACENCY, numNormalCurveSegments,
                       GL_UNSIGNED_INT, nullptr);
        CHECK_GL_ERROR;

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        CHECK_GL_ERROR;
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        CHECK_GL_ERROR;
        glDisable(GL_CULL_FACE);
        CHECK_GL_ERROR;
        glDisable(GL_PRIMITIVE_RESTART);
    }
}


void MFrontDetectionActor::triggerAsynchronous3DFrontsRequest()
{
    if (variables.size() < 1 || getViews().empty() || isComputing
            || !detectionVariable->hasData()
            || !windUVar->hasData()
            || !windVVar->hasData()
            || !zVar->hasData()
            )
    { return; }

    detectionVariable = static_cast<MNWP3DVolumeActorVariable *>
    (variables.at(detectionVariableIndex));
    windUVar = static_cast<MNWP3DVolumeActorVariable *>
    (variables.at(windUVariableIndex));
    windVVar = static_cast<MNWP3DVolumeActorVariable *>
    (variables.at(windVVariableIndex));
    zVar = static_cast<MNWP3DVolumeActorVariable *>
    (variables.at(zVariableIndex));

    if (detectionVariable->grid == nullptr)
    { return; }

    if (!detectionVariable->pendingRequests.empty()
            || !windUVar->pendingRequests.empty()
            || !windVVar->pendingRequests.empty()
            || !zVar->pendingRequests.empty()
            )
    { return; }

    isComputing = true;
    suppressUpdates = true;

    // Freeze front detection settings while computing front surface and normal
    // curves. Will be unfreezed once the request is executed and
    // asynchronousFrontLinesAvailable.
    actorPropertiesSupGroup->setEnabled(false);
    applySettingsClickProperty->setEnabled(false);


    MDataRequestHelper rh =
            detectionVariable->constructAsynchronousDataRequest();

    rh.insert("FRONTS_ISOVALUE", QString::number(fleIsoValue));
    rh.insert("FRONTS_WINDU_VAR", windUVar->variableName);
    rh.insert("FRONTS_WINDV_VAR", windVVar->variableName);
    rh.insert("FRONTS_Z_VAR", zVar->variableName);
    rh.insert("FPMA_DISTANCE", QString::number(fpmaDistanceValue_km));
    rh.insert("GEOPOT_HEIGHT", QString::number(useGeopotentialHeight));

    fleSource->setInputSource(detectionVariable->dataSource);
    tfpSource->setInputSource(detectionVariable->dataSource);
    abzSource->setInputSource(detectionVariable->dataSource);
    partialDerivFilter->setInputSource(detectionVariable->dataSource);

    if (computeOnGPU)
    {
        frontDetection3DSourceGPU->setFLESource(fleSource.get());
        frontDetection3DSourceGPU->setTFPSource(tfpSource.get());
        frontDetection3DSourceGPU->setABZSource(abzSource.get());
        frontDetection3DSourceGPU->setDetectionVariableSource(
                detectionVariable->dataSource);
        frontDetection3DSourceGPU->setDetectionVariablePartialDeriveSource(
                partialDerivFilter.get());

        frontDetection3DSourceGPU->setZSource(zVar->dataSource);

        frontDetection3DSourceGPU->setWindUSource(windUVar->dataSource);
        frontDetection3DSourceGPU->setWindVSource(windVVar->dataSource);


        frontDetection3DFilter->setInputSource(frontDetection3DSourceGPU.get());
        // Request values
        if (enableSelection)
        {
            rh.insert("FRONT_TFP_FILTER", QString::number(selectTFP));
            rh.insert("FRONT_STRENGTH_FILTER", QString::number(selectStrength));
            rh.insert("FRONT_BOTTOMP_FILTER", QString::number(selectBottomPressure));
            rh.insert("FRONT_TOPP_FILTER", QString::number(selectTopPressure));
            rh.insert("FRONT_TYPE_FILTER", QString::number(selectFrontTypeMode));
        }
    }
    else
    {
        rh.insert("SAVE_NORMALCURVES", QString::number(render3DNC));
        frontDetection3DSourceCPU->setFLESource(fleSource.get());
        frontDetection3DSourceCPU->setTFPSource(tfpSource.get());
        frontDetection3DSourceCPU->setABZSource(abzSource.get());
        frontDetection3DSourceCPU->setDetectionVariableSource(
                detectionVariable->dataSource);
        frontDetection3DSourceCPU->setDetectionVariablePartialDeriveSource(
                partialDerivFilter.get());

        frontDetection3DSourceCPU->setZSource(zVar->dataSource);

        frontDetection3DSourceCPU->setWindUSource(windUVar->dataSource);
        frontDetection3DSourceCPU->setWindVSource(windVVar->dataSource);

        frontDetection3DFilter->setInputSource(frontDetection3DSourceCPU.get());

        if (enableSelection)
        {
            rh.insert("FRONT_TFP_FILTER", QString::number(selectTFP));
            rh.insert("FRONT_STRENGTH_FILTER", QString::number(selectStrength));
            rh.insert("FRONT_BOTTOMP_FILTER", QString::number(selectBottomPressure));
            rh.insert("FRONT_TOPP_FILTER", QString::number(selectTopPressure));
            rh.insert("FRONT_TYPE_FILTER", QString::number(selectFrontTypeMode));
        }
    }
    frontDetection3DFilter->requestData(rh.request());
}


void MFrontDetectionActor::triggerAsynchronous2DFrontsRequest()
{
    // Return if fronts are being computed.
    if (variables.size() < 1 || getViews().empty() || isComputing
            || !detectionVariable->hasData()
            || !windUVar->hasData()
            || !windVVar->hasData()
            )
    { return; }

    detectionVariable = static_cast<MNWP3DVolumeActorVariable *>
    (variables.at(detectionVariableIndex));
    windUVar = static_cast<MNWP3DVolumeActorVariable *>
    (variables.at(windUVariableIndex));
    windVVar = static_cast<MNWP3DVolumeActorVariable *>
    (variables.at(windVVariableIndex));

    if (detectionVariable->grid == nullptr)
    { return; }

    if (!detectionVariable->pendingRequests.empty()
            || !windUVar->pendingRequests.empty()
            || !windVVar->pendingRequests.empty()
            || !zVar->pendingRequests.empty()
            )
    { return; }


    isComputing = true;
    suppressUpdates = true;

    // Freeze front detection settings while computing front lines and normal
    // curves. Will be unfreezed once the request is executed and
    // asynchronousFrontLinesAvailable.
    actorPropertiesSupGroup->setEnabled(false);
    applySettingsClickProperty->setEnabled(false);

    MDataRequestHelper rh =
            detectionVariable->constructAsynchronousDataRequest();

    rh.insert("FRONTS_ISOVALUE", QString::number(fleIsoValue));
    rh.insert("FRONTS_WINDU_VAR", windUVar->variableName);
    rh.insert("FRONTS_WINDV_VAR", windVVar->variableName);
    rh.insert("FRONTS_ELEVATION", QString::number(frontElevation2d_hPa));
    rh.insert("FPMA_DISTANCE", QString::number(fpmaDistanceValue_km));

    fleSource->setInputSource(detectionVariable->dataSource);
    tfpSource->setInputSource(detectionVariable->dataSource);
    abzSource->setInputSource(detectionVariable->dataSource);
    partialDerivFilter->setInputSource(detectionVariable->dataSource);

    frontDetection2DSource->setFLESource(fleSource.get());
    frontDetection2DSource->setTFPSource(tfpSource.get());
    frontDetection2DSource->setABZSource(abzSource.get());
    frontDetection2DSource->setDetectionVariableSource(
            detectionVariable->dataSource);
    frontDetection2DSource->setDetectionVariablePartialDeriveSource(
            partialDerivFilter.get());
    frontDetection2DSource->setWindUSource(windUVar->dataSource);
    frontDetection2DSource->setWindVSource(windVVar->dataSource);


    frontDetection2DSource->requestData(rh.request());
}


void MFrontDetectionActor::filter3DNormalCurves()
{
    if (!frontSurfaceSelection)
    { return; }

    // Determine seed points grid spacing.
    const float gridSpaceLon = seedPointSpacing;
    const float gridSpaceLat = seedPointSpacing;
    const float gridSpaceHeight = seedPointSpacingZ;

    float dataMinZ, dataMaxZ;
    float westLon, eastLon, southLat, northLat;

    if (bBoxConnection->getBoundingBox() == nullptr)
    {
        dataMaxZ = detectionVariable->grid->getBottomDataVolumePressure_hPa();
        dataMinZ = detectionVariable->grid->getTopDataVolumePressure_hPa();
        westLon = detectionVariable->grid->getWestDataVolumeCorner_lon();
        eastLon = detectionVariable->grid->getEastDataVolumeCorner_lon();
        southLat = detectionVariable->grid->getSouthDataVolumeCorner_lat();
        northLat = detectionVariable->grid->getNorthDataVolumeCorner_lat();
    }
    else
    {
        dataMaxZ = bBoxConnection->bottomPressure_hPa();
        dataMinZ = bBoxConnection->topPressure_hPa();
        westLon = bBoxConnection->westLon();
        eastLon = bBoxConnection->eastLon();
        southLat = bBoxConnection->southLat();
        northLat = bBoxConnection->northLat();
    }

    if ((dataMinZ * dataMaxZ * gridSpaceLon * gridSpaceLat * gridSpaceHeight) <= 0)
    { return; }

    // Compute data extent in lon, lat and height domain.
    const float dataExtentLon = std::abs(eastLon - westLon);
    const float dataExtentLat = std::abs(northLat - southLat);
    const float dataExtentHeight = std::abs(dataMaxZ - dataMinZ);

    const uint32_t numCellsLon = dataExtentLon / gridSpaceLon + 1;
    const uint32_t numCellsLat = dataExtentLat / gridSpaceLat + 1;
    const uint32_t numCellsHeight = dataExtentHeight / gridSpaceHeight + 1;

    QVector<GLint> ghostGrid(numCellsLon * numCellsLat * numCellsHeight, 0);

    QVector3D bboxMin(westLon, southLat, dataMinZ);
    QVector3D bboxMax(eastLon, northLat, dataMaxZ);
    QVector3D boxExtent(dataExtentLon, dataExtentLat, dataExtentHeight);

    // normal curve filtering:
    QVector<Geometry::NormalCurveVertex> normalCurveVertexFiltered;
    QVector<u_int32_t> normalCurveIndexFiltered;
    u_int32_t i = 0;

    for (uint32_t k = 0; k < normalCurves3D->getNumNormalCurves(); ++k)
    {
        Geometry::NormalCurve normalCurve = normalCurves3D->getNormalCurve(k);
        if (normalCurve.positions.empty())
        { continue; }
        QVector3D startPosition = normalCurve.positions.first();

        // compute world z from pressure
//        position.setZ(sceneView->worldZfromPressure(position.z()));

        // check if start position of normal curve is valid and within the
        // current bounding box
        if (startPosition.x() == qNaN || std::isnan(startPosition.x()))
        { continue; }

        if (startPosition.x() < bboxMin.x() || startPosition.x() > bboxMax.x()
                || startPosition.y() < bboxMin.y() || startPosition.y() > bboxMax.y()
                || startPosition.z() < bboxMin.z() || startPosition.z() > bboxMax.z())
        { continue; }

        // compute indices
        QVector3D normTexCoords = (startPosition - bboxMin) / boxExtent;

        int texCoordX = int(normTexCoords.x() * (numCellsLon - 1));
        int texCoordY = int(normTexCoords.y() * (numCellsLat - 1));
        int texCoordZ = int(normTexCoords.z() * (numCellsHeight - 1));

        const int cellIndex = INDEX3zyx(texCoordZ, texCoordY, texCoordX, numCellsLat, numCellsLon);

        // critical section
        int ghostVisited = ghostGrid[cellIndex];
//        const float MIN_OPACITY = 0.1;

        if (!ghostVisited)
        {
            // compute alpha value at current vertex position
            if (normalCurve.tfp < 0)
            { continue; }

            QVector3D start = normalCurve.positions.first();
            QVector3D end = normalCurve.positions.last();

            for (int p = 0; p < normalCurve.positions.size(); p++)
            {
                normalCurveIndexFiltered.append(i);
                Geometry::NormalCurveVertex segment;
                segment.position = normalCurve.positions.at(p);
                segment.start = start;
                segment.end = end;
                segment.tfp = normalCurve.tfp;
                segment.abz = normalCurve.abz;
                segment.strength = normalCurve.strength;
                segment.type = normalCurve.type;
                segment.breadth = normalCurve.breadth;
                normalCurveVertexFiltered.append(segment);
                i++;
            }
            normalCurveIndexFiltered.append(restartIndex);
            ghostGrid[cellIndex] = 1;
        }
    }
    // change format from QVector to array pointer
    uint32_t numNormalCurveSegments = normalCurveVertexFiltered.size();
    Geometry::NormalCurveVertex *normalCurveSegment =
            new Geometry::NormalCurveVertex[numNormalCurveSegments];

    for (uint32_t k = 0; k < numNormalCurveSegments; k++)
    {
        normalCurveSegment[k] = normalCurveVertexFiltered.at(k);
    }

    uint32_t numRestartIndex = normalCurveIndexFiltered.size();
    uint32_t *restartIndex = new uint32_t[numRestartIndex];

    for (uint32_t k = 0; k < numRestartIndex; k++)
    {
        restartIndex[k] = normalCurveIndexFiltered.at(k);
    }

//    normalCurves3D->releaseVertexBuffer();
//    normalCurves3D->releaseIndexBuffer();

    normalCurves3D->setNormalCurveSegments(numNormalCurveSegments,
                                           normalCurveSegment,
                                           numRestartIndex,
                                           restartIndex);

    vbo3DNormalCurves = normalCurves3D->getVertexBuffer();
    ibo3DNormalCurves = normalCurves3D->getIndexBuffer();
}


void MFrontDetectionActor::filter2DNormalCurves()
{
    if (!frontLineSelection
            || num2DFrontVertices < 1)
    { return; }

    // Determine seed points grid spacing.
    const float gridSpaceLon = seedPointSpacing;
    const float gridSpaceLat = seedPointSpacing;

    if ((gridSpaceLon * gridSpaceLat) <= 0)
    { return; }

    float westLon, eastLon, southLat, northLat;

    if (bBoxConnection->getBoundingBox() == nullptr)
    {
        westLon = detectionVariable->grid->getWestDataVolumeCorner_lon();
        eastLon = detectionVariable->grid->getEastDataVolumeCorner_lon();
        southLat = detectionVariable->grid->getSouthDataVolumeCorner_lat();
        northLat = detectionVariable->grid->getNorthDataVolumeCorner_lat();
    }
    else
    {
        westLon = bBoxConnection->westLon();
        eastLon = bBoxConnection->eastLon();
        southLat = bBoxConnection->southLat();
        northLat = bBoxConnection->northLat();
    }

    // Compute data extent in lon, lat and height domain.
    const float dataExtentLon = std::abs(eastLon - westLon);
    const float dataExtentLat = std::abs(northLat - southLat);

    const uint32_t numCellsLon = dataExtentLon / gridSpaceLon + 1;
    const uint32_t numCellsLat = dataExtentLat / gridSpaceLat + 1;

    QVector<GLint> ghostGrid(numCellsLon * numCellsLat, 0);

    QVector2D bboxMin(westLon, southLat);
    QVector2D bboxMax(eastLon, northLat);
    QVector2D boxExtent(dataExtentLon, dataExtentLat);

    // normal curve filtering:
    QVector<Geometry::NormalCurveVertex> normalCurveVertexFiltered;
    QVector<u_int32_t> normalCurveIndexFiltered;
    u_int32_t i = 0;

    for (uint32_t k = 0; k < normalCurves2D->getNumNormalCurves(); ++k)
    {
        Geometry::NormalCurve normalCurve = normalCurves2D->getNormalCurve(k);
        if (normalCurve.positions.empty())
        { continue; }
        QVector3D position = normalCurve.positions.first();

        // check if start position of normal curve is valid and within the
        // current bounding box
        if (position.x() == qNaN || std::isnan(position.x()))
        { continue; }

        if (position.x() < bboxMin.x() || position.x() > bboxMax.x()
                || position.y() < bboxMin.y() || position.y() > bboxMax.y())
        { continue; }

        // compute indices
        QVector2D normTexCoords = (QVector2D(position.x(), position.y()) - bboxMin) / boxExtent;

        int texCoordX = int(normTexCoords.x() * (numCellsLon - 1));
        int texCoordY = int(normTexCoords.y() * (numCellsLat - 1));

        const int cellIndex = INDEX2yx(texCoordY, texCoordX, numCellsLon);

        // critical section
        int ghostVisited = ghostGrid[cellIndex];

        if (!ghostVisited)
        {
            // compute alpha value at current vertex position
            if (normalCurve.tfp < 0)
            { continue; }

            QVector3D start = normalCurve.positions.first();
            QVector3D end = normalCurve.positions.last();

            for (int p = 0; p < normalCurve.positions.size(); p++)
            {
                normalCurveIndexFiltered.append(i);
                Geometry::NormalCurveVertex segment;
                segment.position = normalCurve.positions.at(p);
                segment.start = start;
                segment.end = end;
                segment.tfp = normalCurve.tfp;
                segment.abz = normalCurve.abz;
                segment.strength = normalCurve.strength;
                segment.type = normalCurve.type;
                segment.breadth = normalCurve.breadth;
                normalCurveVertexFiltered.append(segment);
                i++;
            }
            normalCurveIndexFiltered.append(restartIndex);
            ghostGrid[cellIndex] = 1;
        }
    }
    // change format from QVector to array pointer
    uint32_t numNormalCurveSegments = normalCurveVertexFiltered.size();
    Geometry::NormalCurveVertex *normalCurveSegment =
            new Geometry::NormalCurveVertex[numNormalCurveSegments];

    for (uint32_t k = 0; k < numNormalCurveSegments; k++)
    {
        normalCurveSegment[k] = normalCurveVertexFiltered.at(k);
    }

    uint32_t numRestartIndex = normalCurveIndexFiltered.size();
    uint32_t *restartIndex = new uint32_t[numRestartIndex];

    for (uint32_t k = 0; k < numRestartIndex; k++)
    {
        restartIndex[k] = normalCurveIndexFiltered.at(k);
    }

//    normalCurves2D->releaseVertexBuffer();
//    normalCurves2D->releaseIndexBuffer();

    normalCurves2D->setNormalCurveSegments(numNormalCurveSegments,
                                           normalCurveSegment,
                                           numRestartIndex,
                                           restartIndex);

    vbo2DNormalCurves = normalCurves2D->getVertexBuffer();
    ibo2DNormalCurves = normalCurves2D->getIndexBuffer();
}


void MFrontDetectionActor::setNCFilterTransferFunctionFromProperty(
        NormalCurvesFilterSettings *filter)
{
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    QString tfName = properties->getEnumItem(filter->transferFunctionProperty);

    if (tfName == "None")
    {
        filter->transferFunction = nullptr;
        return;
    }

    // Find the selected transfer function in the list of actors from the
    // resources manager. Not very efficient, but works well enough for the
    // small number of actors at the moment..
    for (MActor *actor: glRM->getActors())
    {
        if (MTransferFunction1D * tf = dynamic_cast<MTransferFunction1D *>(actor))
        {
            if (tf->transferFunctionName() == tfName)
            {
                filter->transferFunction = tf;
                return;
            }
        }
    }
}


void MFrontDetectionActor::refreshEnumsProperties(MNWPActorVariable *var)
{
    Q_UNUSED(var);
}


QString MFrontDetectionActor::filterTypeToString(filterType type)
{
    {
        switch (type)
        {
        case ABSOLUTE_CHANGE:
            return "absoluteChangeWithinFrontalZone";
        case CHANGE_PER_100KM:
            return "changePer100km";
        case VALUE_AT_VERTEX:
            return "valueAtFrontline";
        }
        return "changePer100km";
    }
}


MFrontDetectionActor::filterType MFrontDetectionActor::stringToFilterType(
        QString type)
{
    if (type == "absoluteChangeWithinFrontalZone")
    {
        return ABSOLUTE_CHANGE;
    }
    else if (type == "changePer100km")
    {
        return CHANGE_PER_100KM;
    }
    else if (type == "valueAtFrontline")
    {
        return VALUE_AT_VERTEX;
    }
    else
    {
        return CHANGE_PER_100KM;
    }
}


double MFrontDetectionActor::distanceBetweenCoordinates(QVector2D wp1, QVector2D wp2)
{
    // Reference (assuming the earth is a sphere):
    // http://www.codeproject.com/Articles/22488/Distance-using-Longitiude-and-latitude-using-c

    double lon1 = wp1.x();
    double lon2 = wp2.x();
    double lat1 = wp1.y();
    double lat2 = wp2.y();

    double PI = 4.0 * atan(1.0);
    double dlat1 = lat1 * (PI / 180);

    double dlon1 = lon1 * (PI / 180);
    double dlat2 = lat2 * (PI / 180);
    double dlon2 = lon2 * (PI / 180);

    double dlon = dlon1 - dlon2;
    double dLat = dlat1 - dlat2;

    double aHarv = pow(sin(dLat / 2.0), 2.0)
            + cos(dlat1) * cos(dlat2) * pow(sin(dlon / 2), 2);
    double cHarv = 2 * atan2(sqrt(aHarv), sqrt(1.0 - aHarv));

    // Earth radius (IUGG value for the equatorial radius of the earth):
    // 6378.137 km.
    const double earth = 6378.137;
    double distance = earth * cHarv;
    return distance;
}


void MFrontDetectionActor::runStatisticalAnalysis(int sigDigits, int histogramDisplayMode)
{
    QChartView *chartViewTFP = createHistogram(tfpStats, sigDigits, QString("TFP"),
                                               QString("Distribution of thermal front parameter (TFP)"));
    QChartView *chartViewStrength = createHistogram(strengthStats, sigDigits, QString("Frontal strength"),
                                                    QString("Distribution of frontal strength"));
    QChartView *chartViewSlope = createHistogram(slopeStats, sigDigits, QString("Slope"),
                                                 QString("Distribution of frontal slope"),
                                                 false);
    QChartView *chartViewArea = createHistogram(areaStats, sigDigits, QString("Area"),
                                                QString("Distribution of frontal area"),
                                                false);


    auto *widget = new QWidget();

    auto *layout = new QVBoxLayout();
    layout->addWidget(chartViewTFP);
    layout->addWidget(chartViewStrength);
    layout->addWidget(chartViewSlope);
    layout->addWidget(chartViewArea);
    widget->setLayout(layout);
    widget->resize(600, 600);
    widget->show();
}


QChartView *MFrontDetectionActor::createHistogram(const QVector<float> &data, int sigDigits, QString varName,
                                                  QString longName, bool autoBin)
{
    float minValue = *std::min_element(data.constBegin(), data.constEnd());
    float maxValue = *std::max_element(data.constBegin(), data.constEnd());

    int histMin = std::floor(minValue);
    int histMax = std::ceil(maxValue);

    int nbins = (histMax - histMin);

    if (autoBin)
    {
        nbins = (histMax - histMin) * int(std::pow(10, sigDigits));
    }

    float nbinsStepSize = (float(histMax) - float(histMin)) / float(nbins);

    QVector<int> binSizes = QVector<int>(nbins);

    for (float v: data)
    {
        auto lowerB = float(histMin);
        float upperB = float(histMin) + nbinsStepSize;
        for (int i = 0; i < nbins; i++)
        {
            if (v >= lowerB && v < upperB)
                binSizes[i] = binSizes[i] + 1;
            lowerB += nbinsStepSize;
            upperB += nbinsStepSize;
        }

    }

    auto *set0 = new QBarSet(std::move(varName));

    int maxYAxis = 0;

    for (auto v: binSizes)
    {
        *set0 << v;
        if (v > maxYAxis) maxYAxis = v;
    }

    auto *series = new QBarSeries();
    series->append(set0);

    auto *chart = new QChart();
    chart->addSeries(series);
    chart->setTitle(longName);
    chart->setAnimationOptions(QChart::SeriesAnimations);

    QStringList categories;

    float categories_corr = float(nbinsStepSize) / 2.;
    auto cat = float(histMin);

    for (int i = 0; i < nbins; i++)
    {
        categories << QString::number(cat + categories_corr);
        cat += nbinsStepSize;
    }
    auto *axisX = new QBarCategoryAxis();
    axisX->append(categories);
    chart->addAxis(axisX, Qt::AlignBottom);
    series->attachAxis(axisX);

    auto *axisY = new QValueAxis();
    axisY->setRange(0, maxYAxis);
    chart->addAxis(axisY, Qt::AlignLeft);
    series->attachAxis(axisY);

    chart->legend()->setVisible(true);
    chart->legend()->setAlignment(Qt::AlignBottom);

    auto *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);

    return chartView;
}


void MFrontDetectionActor::triggerFrontAttributeAnalysis()
{
    pythonTSSActor->clearStatistic();
    int mem = detectionVariable->getEnsembleMember();
    QDateTime datetime = detectionVariable->getSynchronizationControl()->validDateTime();
    pythonTSSActor->initFrontMetaAttributes(mem, datetime);

    for (uint32_t j = 0; j < frontTrianglePatches->getNumPatches(); j++)
    {
        auto p = frontTrianglePatches->getTrianglePatch(j);
        QMap<QString, float> metrics = calcMetricOfFrontalPatch(p);
        pythonTSSActor->addFrontPatchAttributes(metrics);
        pythonTSSActor->addTfpStats(tfpStats);
        pythonTSSActor->addStrengthStats(strengthStats);
        pythonTSSActor->addAreaStats(areaStats);
        pythonTSSActor->addSlopeStats(slopeStats);
    }
}
bool MFrontDetectionActor::synchronizationEvent(MSynchronizationType syncType, QVector<QVariant> data) {
  return autoCompute;
}
void MFrontDetectionActor::synchronizeWith(MSyncControl *sync, bool updateGUIProperties) {
  sync->registerSynchronizedClass(this);
  synchronizationControl = sync;

}
