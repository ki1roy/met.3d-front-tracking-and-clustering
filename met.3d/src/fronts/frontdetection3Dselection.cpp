/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2017 Marc Rautenhaus
**  Copyright 2017      Michael Kern
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "frontdetection3Dselection.h"

// standard library imports

// related third party imports
#include <omp.h>

// local application imports
#include <log4cplus/loggingmacros.h>

#define COMPUTE_PARALLEL
#define MEASURE_CPU_TIME
#define qNaN (std::numeric_limits<float>::quiet_NaN())

using namespace Met3D;


MFrontDetection3DSelection::MFrontDetection3DSelection()
        : M3DFrontFilterSource()
{
}


void MFrontDetection3DSelection::setInputSource(MFrontDetection3DSourceCPU *s)
{
    frontDetection3DSource = s;
    registerInputSource(frontDetection3DSource);
    enablePassThrough(frontDetection3DSource);
}


M3DFrontSelection *MFrontDetection3DSelection::produceData(MDataRequest request)
{
    assert(frontDetection3DSource != nullptr);

    MDataRequestHelper rh(request);

    const float tfpThreshold = rh.value("FRONT_TFP_FILTER").toFloat();
    const float strengthThreshold = rh.value("FRONT_STRENGTH_FILTER").toFloat();
    const float bottomPThreshold = rh.value("FRONT_BOTTOMP_FILTER").toFloat();
    const float topPThreshold = rh.value("FRONT_TOPP_FILTER").toFloat();
    const int frontType = rh.value("FRONT_TYPE_FILTER").toInt();


    rh.removeAll(locallyRequiredKeys());

    M3DFrontSelection *raw3DFronts = frontDetection3DSource->getData(rh.request());

    MTriangleMeshSelection *rawFrontSurfaces = raw3DFronts->getTriangleMeshSelection();
    MNormalCurvesSelection *rawNormalCurves = raw3DFronts->getNormalCurvesSelection();

    // filter triangles according to tfp:
    QVector<Geometry::MTriangle> *triangleSelection = new QVector<Geometry::MTriangle>;
    for (int i = 0; i < rawFrontSurfaces->getNumTriangles(); i++)
    {
        if (rawFrontSurfaces->getVertex(rawFrontSurfaces->getTriangle(i).indices[0]).tfp > tfpThreshold
                && rawFrontSurfaces->getVertex(rawFrontSurfaces->getTriangle(i).indices[1]).tfp > tfpThreshold
                && rawFrontSurfaces->getVertex(rawFrontSurfaces->getTriangle(i).indices[2]).tfp > tfpThreshold
                && rawFrontSurfaces->getVertex(rawFrontSurfaces->getTriangle(i).indices[0]).strength > strengthThreshold
                && rawFrontSurfaces->getVertex(rawFrontSurfaces->getTriangle(i).indices[1]).strength > strengthThreshold
                && rawFrontSurfaces->getVertex(rawFrontSurfaces->getTriangle(i).indices[2]).strength > strengthThreshold
                && rawFrontSurfaces->getVertex(rawFrontSurfaces->getTriangle(i).indices[0]).position.z()
                        < bottomPThreshold
                && rawFrontSurfaces->getVertex(rawFrontSurfaces->getTriangle(i).indices[1]).position.z()
                        < bottomPThreshold
                && rawFrontSurfaces->getVertex(rawFrontSurfaces->getTriangle(i).indices[2]).position.z()
                        < bottomPThreshold
                && rawFrontSurfaces->getVertex(rawFrontSurfaces->getTriangle(i).indices[0]).position.z() > topPThreshold
                && rawFrontSurfaces->getVertex(rawFrontSurfaces->getTriangle(i).indices[1]).position.z() > topPThreshold
                && rawFrontSurfaces->getVertex(rawFrontSurfaces->getTriangle(i).indices[2]).position.z()
                        > topPThreshold)
        {
            switch (frontType)
            {
            case 0:
                triangleSelection->append(rawFrontSurfaces->getTriangle(i));
                break;
            case 1:
                if (rawFrontSurfaces->getVertex(rawFrontSurfaces->getTriangle(i).indices[0]).type < 0.5
                        && rawFrontSurfaces->getVertex(rawFrontSurfaces->getTriangle(i).indices[1]).type < 0.5
                        && rawFrontSurfaces->getVertex(rawFrontSurfaces->getTriangle(i).indices[2]).type < 0.5)
                {
                    triangleSelection->append(rawFrontSurfaces->getTriangle(i));
                }
                break;
            case 2:
                if (rawFrontSurfaces->getVertex(rawFrontSurfaces->getTriangle(i).indices[0]).type > 0.5
                        && rawFrontSurfaces->getVertex(rawFrontSurfaces->getTriangle(i).indices[1]).type > 0.5
                        && rawFrontSurfaces->getVertex(rawFrontSurfaces->getTriangle(i).indices[2]).type > 0.5)
                {
                    triangleSelection->append(rawFrontSurfaces->getTriangle(i));
                }
                break;
            default:
                break;
            }

        }
    }


#ifdef MEASURE_CPU_TIME
    auto startESN = std::chrono::system_clock::now();
#endif

    // Compute element surrounding node (ESN)

    // Get the number of triangles.
    int numTriangles = triangleSelection->size();
    // Create a list of a list of integer to save the (ESN).
    QVector<QVector<int>> esn;
    esn.resize(rawFrontSurfaces->getNumVertices());

    // Loop over triangles and get nodes of each triangle.
    for (int i = 0; i < numTriangles; i++)
    {
        // For each of the three triangle nodes, append the ESN at the position of the triangle node with the
        // current triangle index.
        esn[triangleSelection->at(i).indices[0]].append(i);
        esn[triangleSelection->at(i).indices[1]].append(i);
        esn[triangleSelection->at(i).indices[2]].append(i);
    }


#ifdef MEASURE_CPU_TIME
    auto endESN = std::chrono::system_clock::now();
    auto durationESN = std::chrono::duration_cast<std::chrono::milliseconds>(endESN - startESN);
    LOG4CPLUS_DEBUG(mlog, "[ESN] \t->done in " << durationESN.count() << "ms");
#endif


#ifdef MEASURE_CPU_TIME
    auto startESE = std::chrono::system_clock::now();
#endif

    // Create element surround elements (ESE)
    // Create a list of a list of integer to save ESE
    QVector<QVector<int>> ese;
    ese.resize(numTriangles);

    QVector<bool> visitElement(numTriangles);
    visitElement.fill(false);

    QVector<int> esn1;
    QVector<int> esn2;
    QVector<int> esn3;


    // loop over triangles and get nodes of each triangle
    for (int i = 0; i < numTriangles; i++)
    {
        QVector<int> eseInternal;
        Geometry::MTriangle testTriangle = triangleSelection->at(i);

        esn1 = esn[testTriangle.indices[0]];
        esn2 = esn[testTriangle.indices[1]];
        esn3 = esn[testTriangle.indices[2]];

        for (int n: esn1)
        {
            if (n != i) visitElement[n] = true;
        }

        for (int n: esn2)
        {
            if (n != i)
            {
                if (visitElement[n])
                    eseInternal.append(n);
                else
                    visitElement[n] = true;
            }
        }

        for (int n: esn3)
        {
            if (n != i)
            {
                if (visitElement[n])
                    eseInternal.append(n);
            }
        }
        visitElement.fill(false);
        ese[i].append(eseInternal);
    }


#ifdef MEASURE_CPU_TIME
    auto endESE = std::chrono::system_clock::now();
    auto durationESE = std::chrono::duration_cast<std::chrono::milliseconds>(endESE - startESE);
    LOG4CPLUS_DEBUG(mlog, "[ESE] \t->done in " << durationESE.count() << "ms");
#endif


// Create patch of connected element (pct)
#ifdef MEASURE_CPU_TIME
    auto startPCE = std::chrono::system_clock::now();
#endif

    visitElement.fill(false);

    // patch triangle indices
    QVector<QVector<int>> patchedTriangleIndices;

    int largestPatchIndex = 0;
    int largestPatchSize = 0;
    int currentPatchIndex = 0;

    // loop over ese and create connected triangle patch
    for (int i = 0; i < numTriangles; i++)
    {
        QVector<int> currentPatchIndices;

        if (!visitElement.at(i))
        {
            currentPatchIndices.append(i);
            visitElement[i] = true;

            QVector<int> loopList;

            for (auto n: ese[i])
            {
                loopList.push_back(n);
            }

            while (!loopList.empty())
            {
                int first = loopList.takeFirst();
                if (!visitElement.at(first))
                {
                    for (auto m: ese[first])
                    {
                        loopList.push_back(m);
                    }

                    currentPatchIndices.append(first);
                    visitElement[first] = true;
                }
            }
            patchedTriangleIndices.append(currentPatchIndices);
            if (currentPatchIndices.size() > largestPatchSize)
            {
                largestPatchSize = currentPatchIndices.size();
                largestPatchIndex = currentPatchIndex;
            }
            currentPatchIndex++;
        }
    }

    LOG4CPLUS_DEBUG(mlog, "Number of patches: " << patchedTriangleIndices.size());

#ifdef MEASURE_CPU_TIME
    auto endPCE = std::chrono::system_clock::now();
    auto durationBCE = std::chrono::duration_cast<std::chrono::milliseconds>(endPCE - startPCE);
    LOG4CPLUS_DEBUG(mlog, "[BSE] \t->done in " << durationBCE.count() << "ms");
#endif


    MTriangleMeshSelection *frontSurfacesSelection = new MTriangleMeshSelection(
            rawFrontSurfaces->getNumVertices(),
            patchedTriangleIndices.at(largestPatchIndex).size());

    // set triangles
    int i = 0;
    for (int n: patchedTriangleIndices.at(largestPatchIndex))
    {
        frontSurfacesSelection->setTriangle(i, triangleSelection->at(
                n));
        i++;
    }

    for (int j = 0; j < rawFrontSurfaces->getNumVertices(); j++)
    {
        frontSurfacesSelection->setVertex(j, rawFrontSurfaces->getVertex(j));
    }
    

    M3DFrontSelection *selected3DFronts = new M3DFrontSelection;

    // raw3DFronts->setTriangleMeshSelection(rawFrontSurfaces);
    selected3DFronts->setTriangleMeshSelection(frontSurfacesSelection);
    selected3DFronts->setNormalCurvesSelection(rawNormalCurves);

    frontDetection3DSource->releaseData(raw3DFronts);
    return selected3DFronts;
}


MTask *MFrontDetection3DSelection::createTaskGraph(MDataRequest request)
{
    assert(frontDetection3DSource != nullptr);

    MTask *task = new MTask(request, this);
    MDataRequestHelper rh(request);

    rh.removeAll(locallyRequiredKeys());

    task->addParent(frontDetection3DSource->getTaskGraph(rh.request()));

    return task;
}


const QStringList MFrontDetection3DSelection::locallyRequiredKeys()
{
    return (QStringList() << "FRONT_TFP_FILTER"
                          << "FRONT_STRENGTH_FILTER"
                          << "FRONT_BOTTOMP_FILTER"
                          << "FRONT_TOPP_FILTER"
                          << "FRONT_TYPE_FILTER");
}
