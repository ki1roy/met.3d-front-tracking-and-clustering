/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2017 Marc Rautenhaus
**  Copyright 2017      Michael Kern
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "frontdetection3Dfilter.h"

// standard library imports

// related third party imports
#include <omp.h>

// local application imports
#include <log4cplus/loggingmacros.h>

#define COMPUTE_PARALLEL
#define MEASURE_CPU_TIME
#define qNaN (std::numeric_limits<float>::quiet_NaN())

using namespace Met3D;

MFrontDetection3DFilter::MFrontDetection3DFilter()
        : M3DFrontFilterSource()
{
}


void MFrontDetection3DFilter::setInputSource(MFrontDetection3DSource* s)
{
    frontDetection3DSource = s;
    registerInputSource(frontDetection3DSource);
    enablePassThrough(frontDetection3DSource);
}


M3DFrontSelection* MFrontDetection3DFilter::produceData(MDataRequest request)
{
    assert(frontDetection3DSource);

    MDataRequestHelper rh(request);

    const float tfpThreshold = rh.value("FRONT_TFP_FILTER").toFloat();
    const float strengthThreshold = rh.value("FRONT_STRENGTH_FILTER").toFloat();
    const float bottomPThreshold = rh.value("FRONT_BOTTOMP_FILTER").toFloat();
    const float topPThreshold = rh.value("FRONT_TOPP_FILTER").toFloat();
    const int frontType = rh.value("FRONT_TYPE_FILTER").toInt();


    rh.removeAll(locallyRequiredKeys());

    M3DFrontSelection* raw3DFronts;
    MTriangleMeshSelection *rawFrontSurfaces;
    MNormalCurvesSelection *rawNormalCurves;

    raw3DFronts = frontDetection3DSource->getData(rh.request());

    rawFrontSurfaces = raw3DFronts->getTriangleMeshSelection();
    rawNormalCurves = raw3DFronts->getNormalCurvesSelection();

    // filter triangles according to tfp, strength, heigth, warm and cold front:
    QVector<Geometry::MTriangle>* triangleSelection = new QVector<Geometry::MTriangle>;
    for (uint32_t i = 0; i < rawFrontSurfaces->getNumTriangles(); i++)
    {
        Geometry::FrontMeshVertex v0 = rawFrontSurfaces->getVertex(rawFrontSurfaces->getTriangle(i).indices[0]);
        Geometry::FrontMeshVertex v1 = rawFrontSurfaces->getVertex(rawFrontSurfaces->getTriangle(i).indices[1]);
        Geometry::FrontMeshVertex v2 = rawFrontSurfaces->getVertex(rawFrontSurfaces->getTriangle(i).indices[2]);
        if (v0.tfp > tfpThreshold && v1.tfp > tfpThreshold && v2.tfp > tfpThreshold
                && (v0.strength / v0.breadth * 100) > strengthThreshold
                && (v1.strength / v1.breadth * 100) > strengthThreshold
                && (v2.strength / v2.breadth * 100) > strengthThreshold
                && v0.position.z() < bottomPThreshold && v1.position.z() < bottomPThreshold && v2.position.z() < bottomPThreshold
                && v0.position.z() > topPThreshold && v1.position.z() > topPThreshold && v2.position.z() > topPThreshold)
        {
            switch (frontType) {
            case 0:
                triangleSelection->append(rawFrontSurfaces->getTriangle(i));
                break;
            case 1:
                if (rawFrontSurfaces->getVertex(rawFrontSurfaces->getTriangle(i).indices[0]).type < 0.5
                        && rawFrontSurfaces->getVertex(rawFrontSurfaces->getTriangle(i).indices[1]).type < 0.5
                        && rawFrontSurfaces->getVertex(rawFrontSurfaces->getTriangle(i).indices[2]).type < 0.5)
                {
                    triangleSelection->append(rawFrontSurfaces->getTriangle(i));
                }
                break;
            case 2:
                if (rawFrontSurfaces->getVertex(rawFrontSurfaces->getTriangle(i).indices[0]).type > 0.5
                        && rawFrontSurfaces->getVertex(rawFrontSurfaces->getTriangle(i).indices[1]).type > 0.5
                        && rawFrontSurfaces->getVertex(rawFrontSurfaces->getTriangle(i).indices[2]).type > 0.5)
                {
                    triangleSelection->append(rawFrontSurfaces->getTriangle(i));
                }
                break;
            default:
                break;
            }

        }
    }

    // set filtered frontal triangles
    MTriangleMeshSelection *frontSurfacesFiltered = new MTriangleMeshSelection(
                 rawFrontSurfaces->getNumVertices(),
                 triangleSelection->size());

    for (int i = 0; i < triangleSelection->size(); i++)
    {
        frontSurfacesFiltered->setTriangle(i, triangleSelection->at(i));
    }

    for (uint32_t i = 0; i < rawFrontSurfaces->getNumVertices(); i++)
    {
        frontSurfacesFiltered->setVertex(i, rawFrontSurfaces->getVertex(i));
    }

    M3DFrontSelection *filtered3DFronts = new M3DFrontSelection;

    filtered3DFronts->setTriangleMeshSelection(frontSurfacesFiltered);
    filtered3DFronts->setNormalCurvesSelection(rawNormalCurves);


// Create element surrounding node (esn)
#ifdef MEASURE_CPU_TIME
    auto startESN = std::chrono::system_clock::now();
#endif

    int numTriangles = triangleSelection->size();
    // create array of esn
    QVector<QVector<int>> esn;
    esn.resize(rawFrontSurfaces->getNumVertices());
    Geometry::MTriangle testTriangle;
    // loop over triangles and get nodes of each triangle
    for (int i=0; i < numTriangles; i++)
    {
        testTriangle = triangleSelection->at(i);
        // fill esn with triangle for each of triangle node index
        esn[testTriangle.indices[0]].append(i);
        esn[testTriangle.indices[1]].append(i);
        esn[testTriangle.indices[2]].append(i);
    }


#ifdef MEASURE_CPU_TIME
    auto endESN = std::chrono::system_clock::now();
    auto durationESN = std::chrono::duration_cast<std::chrono::milliseconds>(endESN - startESN);
    LOG4CPLUS_DEBUG(mlog, "[ESN] \t->done in " << durationESN.count() << "ms");
#endif


// Create element surround elements (ese)
#ifdef MEASURE_CPU_TIME
    auto startESE = std::chrono::system_clock::now();
#endif
    // create array of ese
    QVector<QVector<int>> ese;
    ese.resize(numTriangles);

    QVector<bool> visitElement(numTriangles);
    visitElement.fill(false);

    QVector<int> esn1;
    QVector<int> esn2;
    QVector<int> esn3;


    // loop over triangles and get nodes of each triangle
    for (int i=0; i < numTriangles; i++)
    {
        QVector<int> eseInternal;
        testTriangle = triangleSelection->at(i);

        esn1 = esn[testTriangle.indices[0]];
        esn2 = esn[testTriangle.indices[1]];
        esn3 = esn[testTriangle.indices[2]];

        // LOG4CPLUS_DEBUG(mlog, "[ESE] \in loop number: " << i <<  " of " << numTriangles <<", ESN1: " << esn1.size() << " ESN2: " << esn2.size()<< " ESN3: " << esn3.size());

        for (int n : esn1)
        {
            if (n != i) visitElement[n] = true;
        }

        for (int n : esn2)
        {
            if (n != i)
            {
                if (visitElement[n])
                    eseInternal.append(n);
                else
                    visitElement[n] = true;
            }
        }

        for (int n : esn3)
        {
            if (n != i)
            {
                if (visitElement[n])
                    eseInternal.append(n);
            }
        }
        visitElement.fill(false);
        ese[i].append(eseInternal);
    }


#ifdef MEASURE_CPU_TIME
    auto endESE = std::chrono::system_clock::now();
    auto durationESE = std::chrono::duration_cast<std::chrono::milliseconds>(endESE - startESE);
    LOG4CPLUS_DEBUG(mlog, "[ESE] \t->done in " << durationESE.count() << "ms");
#endif


// Create patch of connected element (pct)
#ifdef MEASURE_CPU_TIME
    auto startPCE = std::chrono::system_clock::now();
#endif

    visitElement.fill(false);

    // patch triangle indices
    QVector<QVector<int>> patchedTriangleIndices;

    int largestPatchIndex = 0;
    int largestPatchSize = 0;
    int currentPatchIndex = 0;

    // loop over ese and create connected triangle patch
    for (int i=0; i < numTriangles; i++)
    {
        QVector<int> currentPatchIndices;

        if (!visitElement.at(i))
        {
            currentPatchIndices.append(i);
            visitElement[i] = true;

            QVector<int> loopList;

            for (auto n : ese[i])
            {
                loopList.push_back(n);
            }

            while (loopList.size())
            {
                int first = loopList.takeFirst();
                if (!visitElement.at(first))
                {
                    for (auto m : ese[first])
                    {
                        loopList.push_back(m);
                    }

                    currentPatchIndices.append(first);
                    visitElement[first] = true;
                }
            }
            patchedTriangleIndices.append(currentPatchIndices);
            if (currentPatchIndices.size() > largestPatchSize)
            {
                largestPatchSize = currentPatchIndices.size();
                largestPatchIndex = currentPatchIndex;
            }
            currentPatchIndex++;
        }
    }

    LOG4CPLUS_DEBUG(mlog, "Number of patches: " << patchedTriangleIndices.size());


#ifdef MEASURE_CPU_TIME
    auto endPCE = std::chrono::system_clock::now();
    auto durationBCE = std::chrono::duration_cast<std::chrono::milliseconds>(endPCE - startPCE);
    LOG4CPLUS_DEBUG(mlog, "[BSE] \t->done in " << durationBCE.count() << "ms");
#endif

    MTrianglePatches* frontTrianglePatches = new MTrianglePatches;



    for (QVector<int> p : patchedTriangleIndices)
    {
        int numTriangles = p.size();
        MTriangleSelection* singleTriangleSelection = new MTriangleSelection(numTriangles);

        for (int i = 0; i < numTriangles; i++)
        {
            singleTriangleSelection->setTriangle(i, frontSurfacesFiltered->getTriangle(p.at(i)));
        }
        frontTrianglePatches->setTrianglePatch(singleTriangleSelection);
    }


    filtered3DFronts->setTrianglePatches(frontTrianglePatches);

    frontDetection3DSource->releaseData(raw3DFronts);
    return filtered3DFronts;
}


MTask* MFrontDetection3DFilter::createTaskGraph(MDataRequest request)
{
    assert(frontDetection3DSource);

    MTask* task =  new MTask(request, this);
    MDataRequestHelper rh(request);

    rh.removeAll(locallyRequiredKeys());

    task->addParent(frontDetection3DSource->getTaskGraph(rh.request()));
    return task;
}


const QStringList MFrontDetection3DFilter::locallyRequiredKeys()
{
    return (QStringList() << "FRONT_TFP_FILTER"
                          << "FRONT_STRENGTH_FILTER"
                          << "FRONT_BOTTOMP_FILTER"
                          << "FRONT_TOPP_FILTER"
                          << "FRONT_TYPE_FILTER");
}
