/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2017 Marc Rautenhaus
**  Copyright 2017      Michael Kern
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "frontdetection3Dsource.h"
#include "util/mmarchingcubes.h"
#include <unistd.h>

// standard library imports

// related third party imports
#include <omp.h>

// local application imports
#include <log4cplus/loggingmacros.h>

#define COMPUTE_PARALLEL
#define MEASURE_CPU_TIME
#define qNaN (std::numeric_limits<float>::quiet_NaN())

using namespace Met3D;

MFrontDetection3DSource::MFrontDetection3DSource()
        : M3DFrontFilterSource()
{
}


void MFrontDetection3DSource::setFLESource(MFrontLocationEquationSource *s)
{
    fleSource = s;
    registerInputSource(fleSource);
    enablePassThrough(fleSource);
}


void MFrontDetection3DSource::setTFPSource(MThermalFrontParameterSource *s)
{
    tfpSource = s;
    registerInputSource(tfpSource);
    enablePassThrough(tfpSource);
}


void MFrontDetection3DSource::setABZSource(MAdjacentBaroclinicZoneSource *s)
{
    abzSource = s;
    registerInputSource(abzSource);
    enablePassThrough(abzSource);
}


void MFrontDetection3DSource::setDetectionVariableSource(MWeatherPredictionDataSource *s)
{
    detectionVariableSource = s;
    registerInputSource(detectionVariableSource);
    enablePassThrough(detectionVariableSource);
}


void MFrontDetection3DSource::setDetectionVariablePartialDeriveSource(
        MPartialDerivativeFilter *s)
{
    detectionVarPartialDerivativeSource = s;
    registerInputSource(detectionVarPartialDerivativeSource);
    enablePassThrough(detectionVarPartialDerivativeSource);
}


void MFrontDetection3DSource::setWindUSource(MWeatherPredictionDataSource *s)
{
    windUSource = s;
    registerInputSource(windUSource);
    enablePassThrough(windUSource);
}


void MFrontDetection3DSource::setWindVSource(MWeatherPredictionDataSource *s)
{
    windVSource = s;
    registerInputSource(windVSource);
    enablePassThrough(windVSource);
}


void MFrontDetection3DSource::setZSource(MWeatherPredictionDataSource *s)
{
    zSource = s;
    registerInputSource(zSource);
    enablePassThrough(zSource);
}


/**
 * @brief MFrontDetection3DSourceCPU::MFrontDetection3DSourceCPU
 * Pipeline element to compute normal curve integration on CPU.
 */
MFrontDetection3DSourceCPU::MFrontDetection3DSourceCPU()
        : MFrontDetection3DSource()
{
}


//public methods
MTask* MFrontDetection3DSourceCPU::createTaskGraph(MDataRequest request)
{
    assert(fleSource != nullptr);
    assert(tfpSource != nullptr);
    assert(abzSource != nullptr);
    assert(detectionVariableSource != nullptr);
    assert(detectionVarPartialDerivativeSource != nullptr);
    assert(windUSource != nullptr);
    assert(windVSource != nullptr);
    assert(zSource != nullptr);

    MTask* task =  new MTask(request, this);
    MDataRequestHelper rh(request);

    const QString windUVar = rh.value("FRONTS_WINDU_VAR");
    const QString windVVar = rh.value("FRONTS_WINDV_VAR");
    const QString zVar = rh.value("FRONTS_Z_VAR");

    rh.removeAll(locallyRequiredKeys());

    task->addParent(fleSource->getTaskGraph(rh.request()));
    rh.remove("FPMA_DISTANCE");
    task->addParent(tfpSource->getTaskGraph(rh.request()));
    task->addParent(abzSource->getTaskGraph(rh.request()));

    task->addParent(detectionVariableSource->getTaskGraph(rh.request()));
    const int DLON = MPartialDerivativeProperties::DLON;
    const int DLAT = MPartialDerivativeProperties::DLAT;
    rh.insert("PARTIALDERIVATIVE", DLON);
    task->addParent(detectionVarPartialDerivativeSource->getTaskGraph(rh.request()));
    rh.insert("PARTIALDERIVATIVE", DLAT);
    task->addParent(detectionVarPartialDerivativeSource->getTaskGraph(rh.request()));
    rh.remove("PARTIALDERIVATIVE");

    rh.insert("VARIABLE", windUVar);
    task->addParent(windUSource->getTaskGraph(rh.request()));
    rh.insert("VARIABLE", windVVar);
    task->addParent(windVSource->getTaskGraph(rh.request()));
    rh.insert("VARIABLE", zVar);
    task->addParent(zSource->getTaskGraph(rh.request()));

    return task;
}


M3DFrontSelection* MFrontDetection3DSourceCPU::produceData(MDataRequest request)
{
    assert(fleSource != nullptr);
    assert(tfpSource != nullptr);
    assert(abzSource != nullptr);
    assert(detectionVariableSource != nullptr);
    assert(detectionVarPartialDerivativeSource != nullptr);
    assert(windUSource != nullptr);
    assert(windVSource != nullptr);
    assert(zSource != nullptr);

    MDataRequestHelper rh(request);

    const float isovalue = rh.value("FRONTS_ISOVALUE").toFloat();
    const QString windUVar = rh.value("FRONTS_WINDU_VAR");
    const QString windVVar = rh.value("FRONTS_WINDV_VAR");
    const QString zVar = rh.value("FRONTS_Z_VAR");
    const bool useGeopot = rh.value("GEOPOT_HEIGHT").toInt();
    const bool saveNormalCurves = bool(rh.value("SAVE_NORMALCURVES").toInt());

    rh.removeAll(locallyRequiredKeys());

    MStructuredGrid* fleGrid = fleSource->getData(rh.request());
    rh.remove("FPMA_DISTANCE");
    MStructuredGrid* tfpGrid = tfpSource->getData(rh.request());
    MStructuredGrid* abzGrid = abzSource->getData(rh.request());

    MStructuredGrid* detectionVarGrid = detectionVariableSource->getData(rh.request());
    const int DLON = MPartialDerivativeProperties::DLON;
    const int DLAT = MPartialDerivativeProperties::DLAT;
    rh.insert("PARTIALDERIVATIVE", DLON);
    MStructuredGrid* dDetectionVarDXGrid =
            detectionVarPartialDerivativeSource->getData(rh.request());
    rh.insert("PARTIALDERIVATIVE", DLAT);
    MStructuredGrid* dDetectionVarDYGrid =
            detectionVarPartialDerivativeSource->getData(rh.request());
    rh.remove("PARTIALDERIVATIVE");

    rh.insert("VARIABLE", windUVar);
    MStructuredGrid* windUGrid = windUSource->getData(rh.request());
    rh.insert("VARIABLE", windVVar);
    MStructuredGrid* windVGrid = windVSource->getData(rh.request());
    rh.insert("VARIABLE", zVar);
    MStructuredGrid* zGrid = zSource->getData(rh.request());

    // 1) Create voxel cells
    const int nx = fleGrid->getNumLons() - 1;
    const int ny = fleGrid->getNumLats() - 1;
    const int nz = fleGrid->getNumLevels() - 1;

    //const uint32_t numVoxelCells = nx * ny * nz;

    LOG4CPLUS_DEBUG(mlog, QString("[0] Number of voxel cells: %1 x %2 x %3").arg(nx).arg(ny).arg(nz).toStdString());

    // QVector max size is 2GB (2000000000 bytes)
    // Here we check if we can compute mc or if our data is too large
    u_int32_t numEdges = (3 * fleGrid->getNumValues()
                    - fleGrid->getNumLons() * fleGrid->getNumLevels()
                    - fleGrid->getNumLons() * fleGrid->getNumLats()
                    - fleGrid->getNumLats() * fleGrid->getNumLevels());
    u_int32_t sizefloatQVector3d = numEdges * 3 * 4;
    u_int32_t maxSizeQVector = 2000000000;
    if (sizefloatQVector3d > maxSizeQVector)
    {
        LOG4CPLUS_DEBUG(mlog, "Your data set is too large to compute 3D fronts");
        return new M3DFrontSelection();
    }

    LOG4CPLUS_DEBUG(mlog, "[1] Compute voxels and isosurface triangle geometry...");

    MMarchingCubes mc(fleGrid, zGrid, useGeopot);
    mc.computeMeshOnCPU(isovalue);

    LOG4CPLUS_DEBUG(mlog, "[1] \t->done.");

    QVector<Geometry::MTriangle>* triangles = mc.getFlattenTriangles();
    QVector<QVector3D>* positions = mc.getFlattenInterPoints();
    QVector<QVector3D>* normals   = mc.getFlattenInterNormals();
    QVector<QVector3D>* normalsZ  = mc.getFlattenInterNormalsZ();

    int p = positions->size();
    LOG4CPLUS_DEBUG(
                mlog,
                QString("[2] Compute integration values and create mesh of %1 values...").arg(p).toStdString());
#ifdef MEASURE_CPU_TIME
    auto start2 = std::chrono::system_clock::now();
#endif

    MTriangleMeshSelection *rawFrontSurfaces = new MTriangleMeshSelection(
                 positions->size(),
                 triangles->size());

    MNormalCurvesSelection *rawNormalCurves;
    if (saveNormalCurves)
    {
        rawNormalCurves = new MNormalCurvesSelection(positions->size());
    }
    else
    {
        rawNormalCurves = new MNormalCurvesSelection(0);
    }

    const float minValue = fleGrid->min();

#ifdef COMPUTE_PARALLEL
#pragma omp parallel for
#endif
    for (auto k = 0; k < positions->size(); ++k)
    {
        // get current position
        QVector3D position = positions->at(k);

        // Check if position is valid
        if (position.x() == qNaN) { continue; }

        // initialize front mesh vertex
        Geometry::FrontMeshVertex frontVertex;

        // get all relevant values at the position
        const float tfp = tfpGrid->interpolateValue(position);
        const float abz = abzGrid->interpolateValue(position);

        // compute front type (warm or cold)
        QVector2D gradTheta(dDetectionVarDXGrid->interpolateValue(position),
                            dDetectionVarDYGrid->interpolateValue(position));
        gradTheta.normalize();
        QVector2D wind(windUGrid->interpolateValue(position),
                       windVGrid->interpolateValue(position));
        wind.normalize();
        const float type = float(QVector2D::dotProduct(-gradTheta, wind) >= 0);

        // compute frontal slope
        // the frontal slope is the tangent of the angle between normalZ and the a xy-level
        QVector3D normalZ = normalsZ->at(k);
//        float x = std::abs(normalZ.z())/std::sqrt(normalZ.x() * normalZ.x() +
//                                                  normalZ.y() * normalZ.y() +
//                                                  normalZ.z() * normalZ.z());
//        float slope1 = abs(x)/std::sqrt(1-(x*x));

        const float lengthXY = std::sqrt(normalZ.x() * normalZ.x() + normalZ.y() * normalZ.y());
        const float lengthZ = -normalZ.z();
        const float slope = lengthXY / lengthZ;

        // compute normal curves
        Geometry::NormalCurve nc = integrateAlongThermalGradient(
                fleGrid, dDetectionVarDXGrid, dDetectionVarDYGrid,
                position, tfp, minValue);

        // compute frontal strength:
        QVector3D ncEnd = nc.positions.last();
        float strength = detectionVarGrid->interpolateValue(nc.positions.first()) -
                detectionVarGrid->interpolateValue(ncEnd);

        // all needed values are computed, fill front vertex
        frontVertex.position = position;
        frontVertex.normal = normals->at(k);
        frontVertex.normalZ = normalZ;
        frontVertex.nCEnd = nc.positions.last();
        frontVertex.tfp = tfp;
        frontVertex.abz = abz;
        frontVertex.strength = strength;
        frontVertex.type = type;
        frontVertex.breadth = nc.breadth;
        frontVertex.slope = slope;

        // all needed values are computed, fill normal curve vertices
        nc.tfp = tfp;
        nc.abz = abz;
        nc.strength = strength;
        nc.type = type;

        // set vertices to raw frontal surfaces and raw normal curves
        rawFrontSurfaces->setVertex(k, frontVertex);
        if (saveNormalCurves)
        {
            rawNormalCurves->setNormalCurve(k, nc);

        }
    }

#ifdef MEASURE_CPU_TIME
    auto end2 = std::chrono::system_clock::now();
    auto elapsed2 = std::chrono::duration_cast<std::chrono::milliseconds>(end2 - start2);
    LOG4CPLUS_DEBUG(mlog, "[2] \t->done in " << elapsed2.count() << "ms");
#endif


#ifdef COMPUTE_PARALLEL
#pragma omp parallel for
#endif

    // set triangles
    for (int i = 0; i < triangles->size(); i++)
    {
        rawFrontSurfaces->setTriangle(i, triangles->at(
                                          i));
    }

    fleSource->releaseData(fleGrid);
    tfpSource->releaseData(tfpGrid);
    abzSource->releaseData(abzGrid);
    detectionVariableSource->releaseData(detectionVarGrid);
    detectionVarPartialDerivativeSource->releaseData(dDetectionVarDXGrid);
    detectionVarPartialDerivativeSource->releaseData(dDetectionVarDYGrid);
    windUSource->releaseData(windUGrid);
    windVSource->releaseData(windVGrid);
    zSource->releaseData(zGrid);

    M3DFrontSelection *raw3DFronts = new M3DFrontSelection;

    raw3DFronts->setTriangleMeshSelection(rawFrontSurfaces);
    raw3DFronts->setNormalCurvesSelection(rawNormalCurves);
    return raw3DFronts;
}


//private methods
const QStringList MFrontDetection3DSourceCPU::locallyRequiredKeys()
{
    return (QStringList() << "FRONTS_ISOVALUE"
                          << "FRONTS_WINDU_VAR"
                          << "FRONTS_WINDV_VAR"
                          << "FRONTS_Z_VAR"
                          << "GEOPOT_HEIGHT"
                          << "SAVE_NORMALCURVES");
}


Geometry::NormalCurve MFrontDetection3DSourceCPU::integrateAlongThermalGradient(
        MStructuredGrid* fleGrid,
        MStructuredGrid* ddxGrid,
        MStructuredGrid* ddyGrid,
        QVector3D startPosition,
        const float tfp,
        const float minValue)
{
    Geometry::NormalCurve normalCurve;

    const float deltaLatKM = 111.2; //km
    const float intStepSize = fleGrid->getDeltaLon();
    const int maxNumIterations = 3000 / (deltaLatKM * intStepSize);

    float normalCurveLength_km = 0; // KM

    QVector3D currentPos = startPosition;
    QVector3D gradient(0, 0, 0);
    QVector3D prevPos;

    float westBoundary = ddxGrid->getNorthWestTopDataVolumeCorner_lonlatp().x();
    float northBoundary = ddxGrid->getNorthWestTopDataVolumeCorner_lonlatp().y();
    float eastBoundary = ddxGrid->getSouthEastBottomDataVolumeCorner_lonlatp().x();
    float southBoundary = ddxGrid->getSouthEastBottomDataVolumeCorner_lonlatp().y();

    int direction = (tfp < 0) ? -1 : 1;

    normalCurve.positions.append(startPosition);

    for (auto cc = 0; cc < maxNumIterations; ++cc)
    {
        gradient.setX(ddxGrid->interpolateValue(currentPos));
        gradient.setY(ddyGrid->interpolateValue(currentPos));
        gradient.normalize();

        prevPos = currentPos;
        currentPos -= gradient * intStepSize * direction;

        // if next position out of boundary or in an area where
        // fle is not defined return normal curve vertices.
        if(currentPos.x() < westBoundary || currentPos.x() > eastBoundary
         ||currentPos.y() < southBoundary || currentPos.y() > northBoundary
         || (fleGrid->interpolateValue(currentPos) < minValue))
        {
            normalCurve.breadth = normalCurveLength_km;
            return normalCurve;
        }

        // check if next position is out of frontal zone, that is, if the
        // fle field is larger or equal to zero
        float locator = fleGrid->interpolateValue(currentPos);

        if (locator <= 0.)
        {
            // compute a bisection correction
            currentPos = bisectionCorrection(currentPos,
                                             prevPos,
                                             fleGrid);

            normalCurve.positions.push_back(currentPos);

            // calculate length of normal curve segment and add to total length
            QVector3D diffPos = currentPos - prevPos;
            float deltaLonKM = deltaLatKM * std::cos(currentPos.y() / 180.0 * M_PI);
            diffPos.setX(diffPos.x() * deltaLonKM);
            diffPos.setY(diffPos.y() * deltaLatKM);
            diffPos.setZ(0);

            normalCurveLength_km += diffPos.length();
            normalCurve.breadth = normalCurveLength_km;

            return normalCurve;

        }

        // add next vertex to normal curve
        normalCurve.positions.push_back(currentPos);

        // calculate length of normal curve segment and add to total length
        QVector3D diffPos = currentPos - prevPos;
        float deltaLonKM = deltaLatKM * std::cos(currentPos.y() / 180.0 * M_PI);
        diffPos.setX(diffPos.x() * deltaLonKM);
        diffPos.setY(diffPos.y() * deltaLatKM);
        diffPos.setZ(0);
        normalCurveLength_km += diffPos.length();
    }

    // return normal curve if max number of iterations is reached
    normalCurve.breadth = normalCurveLength_km;
    return normalCurve;
}


// Correct the position of any detected iso-surface by using the
// bisection algorithm.
QVector3D MFrontDetection3DSourceCPU::bisectionCorrection(QVector3D position,
                              QVector3D prevPosition,
                              MStructuredGrid* fleGrid)
{
    QVector3D centerPosition;
    const int numBisectionSteps = 5;
    float locatorCenter = 0.0;

    for (int i = 0; i < numBisectionSteps; ++i)
    {
        centerPosition = (position + prevPosition) / 2.0;
        locatorCenter = fleGrid->interpolateValue(centerPosition);

        if (locatorCenter <= 0.)
        {
            position = centerPosition;
        }
        else
        {
            prevPosition = centerPosition;
        }
    }
    centerPosition = (position + prevPosition) / 2.0;
    return centerPosition;
}


/**
 * @brief MFrontDetection3DSourceGPU::MFrontDetection3DSourceGPU
 * Pipeline element to compute normal curve integration on GPU.
 */
MFrontDetection3DSourceGPU::MFrontDetection3DSourceGPU()
        : MFrontDetection3DSource()
{
    pbot    = 1050.; // hPa
    ptop    = 20.;
    logpbot = log(pbot);
    zbot    = 0.;
    ztop    = 36.;
    slopePtoZ = (ztop - zbot) / (log(ptop) - log(pbot));
}


// public methods
MTask* MFrontDetection3DSourceGPU::createTaskGraph(MDataRequest request)
{
    assert(fleSource != nullptr);
    assert(tfpSource != nullptr);
    assert(abzSource != nullptr);
    assert(detectionVariableSource != nullptr);
    assert(detectionVarPartialDerivativeSource != nullptr);
    assert(windUSource != nullptr);
    assert(windVSource != nullptr);
    assert(zSource != nullptr);

    MTask* task =  new MTask(request, this);
    task->setGPUTask();
    MDataRequestHelper rh(request);

    const QString windUVar = rh.value("FRONTS_WINDU_VAR");
    const QString windVVar = rh.value("FRONTS_WINDV_VAR");
    const QString zVar = rh.value("FRONTS_Z_VAR");

    rh.removeAll(locallyRequiredKeys());

    task->addParent(fleSource->getTaskGraph(rh.request()));
    task->addParent(tfpSource->getTaskGraph(rh.request()));
    task->addParent(abzSource->getTaskGraph(rh.request()));

    task->addParent(detectionVariableSource->getTaskGraph(rh.request()));
    const int DLON = MPartialDerivativeProperties::DLON;
    const int DLAT = MPartialDerivativeProperties::DLAT;
    rh.insert("PARTIALDERIVATIVE", DLON);
    task->addParent(detectionVarPartialDerivativeSource->getTaskGraph(rh.request()));
    rh.insert("PARTIALDERIVATIVE", DLAT);
    task->addParent(detectionVarPartialDerivativeSource->getTaskGraph(rh.request()));
    rh.remove("PARTIALDERIVATIVE");

    rh.insert("VARIABLE", windUVar);
    task->addParent(windUSource->getTaskGraph(rh.request()));
    rh.insert("VARIABLE", windVVar);
    task->addParent(windVSource->getTaskGraph(rh.request()));
    rh.insert("VARIABLE", zVar);
    task->addParent(zSource->getTaskGraph(rh.request()));

    if (!normalCurvesComputeShader)
    {
        // compile shader
        MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
        glRM->makeCurrent();

        GLint maxWorkGroupInvocations; //, maxWorkGroupCount, maxWorkGroupSize;

        glGetIntegerv(GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS, &maxWorkGroupInvocations); CHECK_GL_ERROR;
        //glGetIntegerv(GL_MAX_COMPUTE_WORK_GROUP_COUNT, &maxWorkGroupCount); CHECK_GL_ERROR;
        //glGetIntegerv(GL_MAX_COMPUTE_WORK_GROUP_SIZE, &maxWorkGroupSize); CHECK_GL_ERROR;

        bool reloadShader = glRM->generateEffectProgram("normal_curves_compute_shader",
                                                        normalCurvesComputeShader);

        if (reloadShader)
        {
            if (!normalCurvesComputeShader->compileFromFile_Met3DHome("src/glsl/normal_curves_compute_shader.fx.glsl"))
            {
                throw std::exception();
            }
        }
    }
    return task;
}


M3DFrontSelection* MFrontDetection3DSourceGPU::produceData(MDataRequest request)
{
    assert(fleSource != nullptr);
    assert(tfpSource != nullptr);
    assert(abzSource != nullptr);
    assert(detectionVariableSource != nullptr);
    assert(detectionVarPartialDerivativeSource != nullptr);
    assert(windUSource != nullptr);
    assert(windVSource != nullptr);
    assert(zSource != nullptr);

    MDataRequestHelper rh(request);

    const float isovalue = rh.value("FRONTS_ISOVALUE").toFloat();
    const QString windUVar = rh.value("FRONTS_WINDU_VAR");
    const QString windVVar = rh.value("FRONTS_WINDV_VAR");
    const QString zVar = rh.value("FRONTS_Z_VAR");
    const bool useGeopot = rh.value("GEOPOT_HEIGHT").toInt();

    rh.removeAll(locallyRequiredKeys());

    MStructuredGrid* fleGrid = fleSource->getData(rh.request());
    MStructuredGrid* tfpGrid = tfpSource->getData(rh.request());
    MStructuredGrid* abzGrid = abzSource->getData(rh.request());

    MStructuredGrid* detectionVarGrid = detectionVariableSource->getData(rh.request());
    const int DLON = MPartialDerivativeProperties::DLON;
    const int DLAT = MPartialDerivativeProperties::DLAT;
    rh.insert("PARTIALDERIVATIVE", DLON);
    MStructuredGrid* dDetectionVarDXGrid =
            detectionVarPartialDerivativeSource->getData(rh.request());
    rh.insert("PARTIALDERIVATIVE", DLAT);
    MStructuredGrid* dDetectionVarDYGrid =
            detectionVarPartialDerivativeSource->getData(rh.request());
    rh.remove("PARTIALDERIVATIVE");

    rh.insert("VARIABLE", windUVar);
    MStructuredGrid* windUGrid = windUSource->getData(rh.request());
    rh.insert("VARIABLE", windVVar);
    MStructuredGrid* windVGrid = windVSource->getData(rh.request());
    rh.insert("VARIABLE", zVar);
    MStructuredGrid* zGrid = zSource->getData(rh.request());

    // 1) Create voxel cells
    const int nx = fleGrid->getNumLons() - 1;
    const int ny = fleGrid->getNumLats() - 1;
    const int nz = fleGrid->getNumLevels() - 1;

    // set ptop and p bottom for compute shader
    pbot = detectionVarGrid->getTopDataVolumePressure_hPa();
    pbot = detectionVarGrid->getBottomDataVolumePressure_hPa();
    logpbot = log(pbot);
    slopePtoZ = (ztop - zbot) / (log(ptop) - log(pbot));

    //const uint32_t numVoxelCells = nx * ny * nz;

    LOG4CPLUS_DEBUG(mlog, QString("[0] Number of voxel cells: %1 x %2 x %3").arg(nx).arg(ny).arg(nz).toStdString());

    // QVector max size is 2GB (2000000000 bytes)
    // Here we check if we can compute mc or if our data is too large
    u_int32_t numEdges = (3 * fleGrid->getNumValues()
                    - fleGrid->getNumLons() * fleGrid->getNumLevels()
                    - fleGrid->getNumLons() * fleGrid->getNumLats()
                    - fleGrid->getNumLats() * fleGrid->getNumLevels());
    u_int32_t sizefloatQVector3d = numEdges * 3 * 4;
    u_int32_t maxSizeQVector = 2000000000;
    if (sizefloatQVector3d > maxSizeQVector)
    {
        LOG4CPLUS_DEBUG(mlog, "Your data set is too large to compute 3D fronts");
        return new M3DFrontSelection();
    }

    LOG4CPLUS_DEBUG(mlog, "[1] Compute voxels and isosurface triangle geometry...");

    MMarchingCubes mc(fleGrid, zGrid, useGeopot);
    mc.computeMeshOnCPU(isovalue);

    LOG4CPLUS_DEBUG(mlog, "[1] \t->done.");

    QVector<Geometry::MTriangle>* triangles = mc.getFlattenTriangles();
    QVector<QVector3D>* positions = mc.getFlattenInterPoints();
    QVector<QVector3D>* normals   = mc.getFlattenInterNormals();
    QVector<QVector3D>* normalsZ  = mc.getFlattenInterNormalsZ();

    int p = positions->size();
    LOG4CPLUS_DEBUG(
                mlog,
                QString("[2] Compute integration values and create mesh of %1 values...").arg(p).toStdString());
#ifdef MEASURE_CPU_TIME
    auto start2 = std::chrono::system_clock::now();
#endif

    int numNormalCurves = positions->size();
    QVector<QVector4D> posTFP(numNormalCurves, QVector4D(0, 0, 0, 0));

    for (int i = 0; i < numNormalCurves; i++)
    {
        posTFP[i].setX(positions->at(i).x());
        posTFP[i].setY(positions->at(i).y());
        posTFP[i].setZ(positions->at(i).z());
        posTFP[i].setW(tfpGrid->interpolateValue(positions->at(i)));
    }

    float westBoundary = fleGrid->getNorthWestTopDataVolumeCorner_lonlatp().x();
    float northBoundary = fleGrid->getNorthWestTopDataVolumeCorner_lonlatp().y();
    float eastBoundary = fleGrid->getSouthEastBottomDataVolumeCorner_lonlatp().x();
    float southBoundary = fleGrid->getSouthEastBottomDataVolumeCorner_lonlatp().y();
    QVector2D minBBox = QVector2D(westBoundary, southBoundary);
    QVector2D maxBBox = QVector2D(eastBoundary, northBoundary);

    QVector<QVector4D> endPos(numNormalCurves, QVector4D(0, 0, 0, 0));

    std::vector<QVector4D> endPosGPU(numNormalCurves);

    const float intStepSize = detectionVarGrid->getDeltaLon();
    const float minValue = fleGrid->min();

    if (numNormalCurves == 0)
    {
        LOG4CPLUS_ERROR(mlog, "Warning: could not find any normal curve init "
                              "points");

        fleSource->releaseData(fleGrid);
        tfpSource->releaseData(tfpGrid);
        abzSource->releaseData(abzGrid);
        detectionVariableSource->releaseData(detectionVarGrid);
        detectionVarPartialDerivativeSource->releaseData(dDetectionVarDXGrid);
        detectionVarPartialDerivativeSource->releaseData(dDetectionVarDYGrid);
        windUSource->releaseData(windUGrid);
        windVSource->releaseData(windVGrid);
        zSource->releaseData(zGrid);

        auto rawFrontSurfaces = new MTriangleMeshSelection(0, 0);
        auto rawNormalCurves = new MNormalCurvesSelection(0);
        M3DFrontSelection *raw3DFronts = new M3DFrontSelection;
        raw3DFronts->setTriangleMeshSelection(rawFrontSurfaces);
        raw3DFronts->setNormalCurvesSelection(rawNormalCurves);
        return raw3DFronts;
    }

    else
    {
        std::vector<QList<QString>> normalCompSubroutines;

        normalCompSubroutines.resize(MVerticalLevelType::SIZE_LEVELTYPES);

        normalCompSubroutines[PRESSURE_LEVELS_3D]
                << "samplePressureLevel"
                << "pressureLevelGradient";

        normalCompSubroutines[HYBRID_SIGMA_PRESSURE_3D]
                << "sampleHybridLevel"
                << "hybridLevelGradient";

        normalCompSubroutines[AUXILIARY_PRESSURE_3D]
                << "sampleAuxiliaryPressure"
                << "auxiliaryPressureGradient";

        QMatrix4x4 mvpMatrix(0,0,0,0,
                             0,0,0,0,
                             0,0,0,0,
                             0,0,0,0);

        QVector2D pToWorldZParams = QVector2D(logpbot, slopePtoZ);

        // upload start position
        const QString ssboNCCurvesID =
                QString("nc_integration_ssbo_%1").arg(getID());
        MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
        glRM->makeCurrent();

        auto ssboNCStart =  dynamic_cast<GL::MShaderStorageBufferObject *>(
                    glRM->getGPUItem(ssboNCCurvesID));

        if(ssboNCStart)
        {
            ssboNCStart->updateSize(numNormalCurves);
            ssboNCStart->upload(posTFP.data(), GL_DYNAMIC_COPY);
        }
        else
        {
            ssboNCStart =  new GL::MShaderStorageBufferObject(
                        ssboNCCurvesID, sizeof(QVector4D), numNormalCurves);
            if (glRM->tryStoreGPUItem(ssboNCStart))
            {
                // Upload normal curve properties
                ssboNCStart->upload(posTFP.data(), GL_DYNAMIC_COPY);
            }
            else
            {
                LOG4CPLUS_WARN(mlog, "WARNING: cannot store buffer for normal curves"
                                     " in GPU memory, skipping normal curves computation.");

                delete ssboNCStart;

                fleSource->releaseData(fleGrid);
                tfpSource->releaseData(tfpGrid);
                abzSource->releaseData(abzGrid);
                detectionVariableSource->releaseData(detectionVarGrid);
                detectionVarPartialDerivativeSource->releaseData(dDetectionVarDXGrid);
                detectionVarPartialDerivativeSource->releaseData(dDetectionVarDYGrid);
                windUSource->releaseData(windUGrid);
                windVSource->releaseData(windVGrid);
                zSource->releaseData(zGrid);

                auto rawFrontSurfaces = new MTriangleMeshSelection(0, 0);
                auto rawNormalCurves = new MNormalCurvesSelection(0);
                M3DFrontSelection *raw3DFronts = new M3DFrontSelection;
                raw3DFronts->setTriangleMeshSelection(rawFrontSurfaces);
                raw3DFronts->setNormalCurvesSelection(rawNormalCurves);
                return raw3DFronts;
            }
        }


        // ssbo for result values
        const QString ssboNCEndID =
                QString("nc_end_ssbo_%1").arg(getID());

        auto ssboNCEnd =  dynamic_cast<GL::MShaderStorageBufferObject *>(
                    glRM->getGPUItem(ssboNCEndID));

        if(ssboNCEnd)
        {
            ssboNCEnd->updateSize(numNormalCurves);
            ssboNCEnd->upload(endPos.data(), GL_DYNAMIC_COPY);
        }
        else
        {
            ssboNCEnd =  new GL::MShaderStorageBufferObject(
                        ssboNCEndID, sizeof(QVector4D), numNormalCurves);
            if (glRM->tryStoreGPUItem(ssboNCEnd))
            {
                // Upload normal curve properties
                ssboNCEnd->upload(endPos.data(), GL_DYNAMIC_COPY);
            }
            else
            {
                LOG4CPLUS_WARN(mlog, "WARNING: cannot store buffer for normal curves"
                                     " in GPU memory, skipping normal curves computation.");

                delete ssboNCEnd;

                fleSource->releaseData(fleGrid);
                tfpSource->releaseData(tfpGrid);
                abzSource->releaseData(abzGrid);
                detectionVariableSource->releaseData(detectionVarGrid);
                detectionVarPartialDerivativeSource->releaseData(dDetectionVarDXGrid);
                detectionVarPartialDerivativeSource->releaseData(dDetectionVarDYGrid);
                windUSource->releaseData(windUGrid);
                windVSource->releaseData(windVGrid);
                zSource->releaseData(zGrid);

                auto rawFrontSurfaces = new MTriangleMeshSelection(0, 0);
                auto rawNormalCurves = new MNormalCurvesSelection(0);
                M3DFrontSelection *raw3DFronts = new M3DFrontSelection;
                raw3DFronts->setTriangleMeshSelection(rawFrontSurfaces);
                raw3DFronts->setNormalCurvesSelection(rawNormalCurves);
                return raw3DFronts;
            }
        }


        normalCurvesComputeShader->bindProgram("SingleIntegration");
        normalCurvesComputeShader->setUniformValue("pToWorldZParams",
                                      pToWorldZParams);

        uint shiftTextureUnit = 0;

        setVarSpecificShaderVars(normalCurvesComputeShader, fleGrid, "dataExtent",
                                 "dataVolume",
                                 "pressureTable", "surfacePressure",
                                 "hybridCoefficients", "lonLatLevAxes",
                                 "pressureTexCoordTable2D", "minMaxAccel3D",
                                 "flagsVolume", "auxPressureField3D_hPa",
                                 shiftTextureUnit);
        shiftTextureUnit += 9;

        setVarSpecificShaderVars(normalCurvesComputeShader, dDetectionVarDXGrid, "dataExtentDVgradX",
                                 "dataVolumeDVgradX",
                                 "pressureTableDVgradX", "surfacePressureDVgradX",
                                 "hybridCoefficientsDVgradX", "lonLatLevAxesDVgradX",
                                 "pressureTexCoordTable2DDVgradX", "minMaxAccel3DDVgradX",
                                 "flagsVolumeDVgradX", "auxPressureField3DDVgradX_hPa",
                                 shiftTextureUnit);

        shiftTextureUnit += 9;

        setVarSpecificShaderVars(normalCurvesComputeShader, dDetectionVarDYGrid, "dataExtentDVgradY",
                                 "dataVolumeDVgradY",
                                 "pressureTableDVgradY", "surfacePressureDVgradY",
                                 "hybridCoefficientsDVgradY", "lonLatLevAxesDVgradY",
                                 "pressureTexCoordTable2DDVgradY", "minMaxAccel3DDVgradY",
                                 "flagsVolumeDVgradY", "auxPressureField3DDVgradY_hPa",
                                 shiftTextureUnit);

        normalCurvesComputeShader->setUniformSubroutineByName(
                GL_COMPUTE_SHADER,
                normalCompSubroutines[detectionVarGrid->getLevelType()]);

        normalCurvesComputeShader->setUniformValue(
                "integrationStepSize", intStepSize); CHECK_GL_ERROR;
        normalCurvesComputeShader->setUniformValue(
                "bisectionSteps", GLint(5)); CHECK_GL_ERROR;

        normalCurvesComputeShader->setUniformValue("isoValueStop", isovalue); CHECK_GL_ERROR;
        normalCurvesComputeShader->setUniformValue("minValue", minValue); CHECK_GL_ERROR;

        normalCurvesComputeShader->setUniformValue("numNormalCurves", numNormalCurves); CHECK_GL_ERROR;
        normalCurvesComputeShader->setUniformValue("minBBox", minBBox); CHECK_GL_ERROR;
        normalCurvesComputeShader->setUniformValue("maxBBox", maxBBox); CHECK_GL_ERROR;


        ssboNCStart->bindToIndex(0);

        ssboNCEnd->bindToIndex(1);

        //int dispatchX = maxNumIterations / 32 + 1;

        glDispatchCompute(numNormalCurves, 1, 1);
        glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
        //sleep(10);//sleeps for 10 second

        glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssboNCEnd->getBufferObject()); CHECK_GL_ERROR;

        GLint bufMask = GL_MAP_READ_BIT;
        QVector4D* verticesGPU = (QVector4D*)
                glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0,
                numNormalCurves * sizeof(QVector4D), bufMask); CHECK_GL_ERROR;
        for (GLuint i = 0; i < GLuint(numNormalCurves); ++i)
        {
            endPosGPU[i] = verticesGPU[i];
        }

        glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0); CHECK_GL_ERROR;
        normalCurvesComputeShader.reset();
    }


    MTriangleMeshSelection *rawFrontSurfaces = new MTriangleMeshSelection(
                 positions->size(),
                 triangles->size());

    MNormalCurvesSelection *rawNormalCurves = new MNormalCurvesSelection(0);

#ifdef COMPUTE_PARALLEL
#pragma omp parallel for
#endif
    // set triangles
    for (int i = 0; i < triangles->size(); i++)
    {
        rawFrontSurfaces->setTriangle(i, triangles->at(
                i));
    }

    float lon_min, lon_max, lat_min, lat_max;
    lon_min = detectionVarGrid->getLon(0);
    lon_max = detectionVarGrid->getLon(detectionVarGrid->getNumLons() - 1);
    lat_min = detectionVarGrid->getLat(0);
    lat_max = detectionVarGrid->getLat(detectionVarGrid->getNumLats() - 1);

#ifdef COMPUTE_PARALLEL
#pragma omp parallel for
#endif
    for (auto k = 0; k < positions->size(); ++k)
    {
        // get current position
        QVector3D position = positions->at(k);

        // Check if position is valid
        if (position.x() == qNaN) { continue; }

        // initialize front mesh vertex
        Geometry::FrontMeshVertex frontVertex;

        // get all relevant values at the position
        const float tfp = tfpGrid->interpolateValue(position);
        const float abz = abzGrid->interpolateValue(position);

        // compute front type (warm or cold)
        QVector2D gradTheta(dDetectionVarDXGrid->interpolateValue(position),
                            dDetectionVarDYGrid->interpolateValue(position));
        gradTheta.normalize();
        QVector2D wind(windUGrid->interpolateValue(position),
                       windVGrid->interpolateValue(position));
        wind.normalize();
        const float type = float(QVector2D::dotProduct(-gradTheta, wind) >= 0);

        // compute frontal slope
        QVector3D normalZ = normalsZ->at(k);
        const float lengthXY = std::sqrt(normalZ.x() * normalZ.x() + normalZ.y() * normalZ.y());
        const float lengthZ = -normalZ.z();
        const float slope = lengthXY / lengthZ;


        // compute frontal strength:
        QVector3D ncEnd = QVector3D(clamp(endPosGPU[k].x(), lon_min, lon_max),
                                    clamp(endPosGPU[k].y(), lat_min, lat_max),
                                    position.z());


        float strength = detectionVarGrid->interpolateValue(position) -
                detectionVarGrid->interpolateValue(ncEnd);

        // all needed values are computed, fill front vertex
        frontVertex.position = position;
        frontVertex.normal = normals->at(k);
        frontVertex.normalZ = normalZ;
        frontVertex.nCEnd = ncEnd;
        frontVertex.tfp = tfp;
        frontVertex.abz = abz;
        frontVertex.strength = strength;
        frontVertex.type = type;
        frontVertex.breadth = endPosGPU[k].w();
        frontVertex.slope = slope;
        rawFrontSurfaces->setVertex(k, frontVertex);

    }


    LOG4CPLUS_DEBUG(mlog, "[2] \t->done.");
#ifdef MEASURE_CPU_TIME
    auto end2 = std::chrono::system_clock::now();
    auto elapsed2 = std::chrono::duration_cast<std::chrono::milliseconds>(end2 - start2);
    LOG4CPLUS_DEBUG(mlog, "[2] \t->done in " << elapsed2.count() << "ms");
#endif

    fleSource->releaseData(fleGrid);
    tfpSource->releaseData(tfpGrid);
    abzSource->releaseData(abzGrid);
    detectionVariableSource->releaseData(detectionVarGrid);
    detectionVarPartialDerivativeSource->releaseData(dDetectionVarDXGrid);
    detectionVarPartialDerivativeSource->releaseData(dDetectionVarDYGrid);
    windUSource->releaseData(windUGrid);
    windVSource->releaseData(windVGrid);
    zSource->releaseData(zGrid);

    normalCurvesComputeShader.reset();

    M3DFrontSelection *raw3DFronts = new M3DFrontSelection;
    raw3DFronts->setTriangleMeshSelection(rawFrontSurfaces);
    raw3DFronts->setNormalCurvesSelection(rawNormalCurves);
    return raw3DFronts;
}


//private methods
const QStringList MFrontDetection3DSourceGPU::locallyRequiredKeys()
{
    return (QStringList() << "FRONTS_ISOVALUE"
                          << "FRONTS_WINDU_VAR"
                          << "FRONTS_WINDV_VAR"
                          << "FRONTS_Z_VAR"
                          << "GEOPOT_HEIGHT");
}


void MFrontDetection3DSourceGPU::setVarSpecificShaderVars(
        std::shared_ptr<GL::MShaderEffect>& shader,
        MStructuredGrid* grid,
        const QString& structName,
        const QString& volumeName,
        const QString& pressureTableName,
        const QString& surfacePressureName,
        const QString& hybridCoeffName,
        const QString& lonLatLevAxesName,
        const QString& pressureTexCoordTable2DName,
        const QString& minMaxAccelStructure3DName,
        const QString& dataFlagsVolumeName,
        const QString& auxPressureField3DName,
        uint shiftTextureUnit)
{
    // Reset optional textures to avoid draw errors.
    // =============================================

    GL::MTexture *textureDummy1D = new GL::MTexture(GL_TEXTURE_1D, GL_ALPHA32F_ARB, 1);
    GL::MTexture *textureDummy2D = new GL::MTexture(GL_TEXTURE_2D, GL_ALPHA32F_ARB, 1, 1);
    GL::MTexture *textureDummy3D = new GL::MTexture(GL_TEXTURE_3D, GL_ALPHA32F_ARB, 1, 1, 1);

    GLuint textureUnitUnusedTextures = 0;
    // glGenTextures(1, &textureUnitUnusedTextures);
    // 1D textures...
    textureDummy1D->bindToTextureUnit(textureUnitUnusedTextures);
    shader->setUniformValue(pressureTableName, textureUnitUnusedTextures); CHECK_GL_ERROR;
    shader->setUniformValue(hybridCoeffName, textureUnitUnusedTextures); CHECK_GL_ERROR;

    // 2D textures...
    textureDummy2D->bindToTextureUnit(textureUnitUnusedTextures);
    shader->setUniformValue(surfacePressureName, textureUnitUnusedTextures); CHECK_GL_ERROR;
#ifdef ENABLE_HYBRID_PRESSURETEXCOORDTABLE
    shader->setUniformValue(pressureTexCoordTable2DName, textureUnitUnusedTextures); CHECK_GL_ERROR;
#endif

    // 3D textures...
    textureDummy3D->bindToTextureUnit(textureUnitUnusedTextures);
    shader->setUniformValue(dataFlagsVolumeName, textureUnitUnusedTextures); CHECK_GL_ERROR;
    shader->setUniformValue(auxPressureField3DName, textureUnitUnusedTextures); CHECK_GL_ERROR;

    // Bind textures and set uniforms.
    // ===============================

    // Bind volume data

    // bind detection var
    GL::MTexture* textureFleGrid = grid->getTexture();
    GLuint textureUnitFleGrid = 1 + shiftTextureUnit;
    // glGenTextures(1, &textureUnitFleGrid);
    textureFleGrid->bindToTextureUnit(textureUnitFleGrid);
    shader->setUniformValue(volumeName, textureUnitFleGrid);

    shader->setUniformValue(structName + ".tfMinimum", 0.); CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".tfMaximum", 0.); CHECK_GL_ERROR;

    // bind detection grid (lon, lat, lev)
    GL::MTexture* textureDataField = grid->getLonLatLevTexture();
    GLuint textureUnitDataField = 2 + shiftTextureUnit;
    // glGenTextures(1, &textureUnitDataField);
    textureDataField->bindToTextureUnit(textureUnitDataField);
    shader->setUniformValue(lonLatLevAxesName, textureUnitDataField);

#ifdef ENABLE_RAYCASTER_ACCELERATION
    // Bind acceleration grid.
    GLuint textureUnitMinMaxAccelStructure = 3 + shiftTextureUnit;
    // glGenTextures(1, &textureUnitMinMaxAccelStructure);
    grid->getMinMaxAccelTexture3D()->bindToTextureUnit(
                textureUnitMinMaxAccelStructure);
    shader->setUniformValue(minMaxAccelStructure3DName,
                            textureUnitMinMaxAccelStructure); CHECK_GL_ERROR;
#endif

    if (grid->getFlagsTexture() != nullptr)
    {
        // The data flags texture will only be valid if the grid contains
        // a flags field and this actor's render mode requests the flags
        // bitfield.
        GLuint textureUnitDataFlags = 4 + shiftTextureUnit;
        // glGenTextures(1, &textureUnitDataFlags);
        grid->getFlagsTexture()->bindToTextureUnit(textureUnitDataFlags); CHECK_GL_ERROR;
        shader->setUniformValue("flagsVolume", textureUnitDataFlags);
    }

    // Set uniforms specific to data var level type.
    // =============================================

    QVector3D dataNWCrnr = grid->getNorthWestTopDataVolumeCorner_lonlatp();
    dataNWCrnr.setZ(worldZfromPressure(dataNWCrnr.z()));
    QVector3D dataSECrnr = grid->getSouthEastBottomDataVolumeCorner_lonlatp();
    dataSECrnr.setZ(worldZfromPressure(dataSECrnr.z()));

    if (grid->getLevelType() == PRESSURE_LEVELS_3D)
    {
        shader->setUniformValue(structName + ".levelType", GLint(0)); CHECK_GL_ERROR;

        MRegularLonLatStructuredPressureGrid *pgrid =
                            dynamic_cast<MRegularLonLatStructuredPressureGrid*>(grid);

        // Bind pressure to texture coordinate LUT.
        GL::MTexture* texturePressureTexCoordTable = pgrid->getPressureTexCoordTexture1D();
        GLuint textureUnitPressureTexCoordTable = 5 + shiftTextureUnit;
        // glGenTextures(1, &textureUnitPressureTexCoordTable);
        texturePressureTexCoordTable->bindToTextureUnit(
                    textureUnitPressureTexCoordTable); CHECK_GL_ERROR;

        // Helper variables for texture coordinate LUT.
        const GLint nPTable = texturePressureTexCoordTable->getWidth();
        const GLfloat deltaZ_PTable = abs(dataSECrnr.z() - dataNWCrnr.z()) / (nPTable - 1);
        const GLfloat upperPTableBoundary = dataNWCrnr.z() + deltaZ_PTable / 2.0f;
        const GLfloat vertPTableExtent = abs(dataNWCrnr.z() - dataSECrnr.z()) + deltaZ_PTable;

        shader->setUniformValue(structName + ".upperPTableBoundary", upperPTableBoundary); CHECK_GL_ERROR;
        shader->setUniformValue(structName + ".vertPTableExtent", vertPTableExtent); CHECK_GL_ERROR;
    }

    else if (grid->getLevelType() == LOG_PRESSURE_LEVELS_3D)
    {
        shader->setUniformValue(structName + ".levelType", GLint(2)); CHECK_GL_ERROR;
    }

    else if (grid->getLevelType() == HYBRID_SIGMA_PRESSURE_3D)
    {
        shader->setUniformValue(structName + ".levelType", GLint(1)); CHECK_GL_ERROR;

        // bind detection var
        MLonLatHybridSigmaPressureGrid *hgrid =
                dynamic_cast<MLonLatHybridSigmaPressureGrid*>(grid);
                // Bind pressure to texture coordinate LUT.
        GL::MTexture* textureHybridCoefficients = hgrid->getHybridCoeffTexture();

        // Bind hybrid coefficients
        GLuint textureUnitHybridCoefficients = 6 + shiftTextureUnit;
        // glGenTextures(1, &textureUnitHybridCoefficients);
        textureHybridCoefficients->bindToTextureUnit(textureUnitHybridCoefficients);
        shader->setUniformValue(hybridCoeffName,
                                textureUnitHybridCoefficients); CHECK_GL_ERROR;

        // Bind surface pressure
        GL::MTexture* textureSurfacePressure = hgrid->getSurfacePressureGrid()->getTexture();
        GLuint textureUnitSurfacePressure = 7 + shiftTextureUnit;
        // glGenTextures(1, &textureUnitSurfacePressure);
        textureSurfacePressure->bindToTextureUnit(textureUnitSurfacePressure);
        shader->setUniformValue(
                surfacePressureName,
                textureUnitSurfacePressure); CHECK_GL_ERROR;

#ifdef ENABLE_HYBRID_PRESSURETEXCOORDTABLE

        // Bind pressure to texture coordinate LUT.
        GL::MTexture* texturePressureTexCoordTable = hgrid->getPressureTexCoordTexture2D();
        GLuint textureUnitPressureTexCoordTable = 8 + shiftTextureUnit;
        // glGenTextures(1, &textureUnitPressureTexCoordTable);
        texturePressureTexCoordTable->bindToTextureUnit(
                textureUnitPressureTexCoordTable);
        shader->setUniformValue(
                pressureTexCoordTable2DName,
                textureUnitPressureTexCoordTable); CHECK_GL_ERROR;
#endif
    }

    else if (grid->getLevelType() == AUXILIARY_PRESSURE_3D)
    {
        shader->setUniformValue(structName + ".levelType", GLint(2)); CHECK_GL_ERROR;

        // Bind pressure field.
        MLonLatAuxiliaryPressureGrid *apgrid =
                dynamic_cast<MLonLatAuxiliaryPressureGrid*>(grid);
        GL::MTexture* textureAuxiliaryPressure = apgrid->getAuxiliaryPressureFieldGrid()->getTexture();
        GLuint textureUnitAuxiliaryPressure = 9 + shiftTextureUnit;
        // glGenTextures(1, &textureUnitAuxiliaryPressure);
        textureAuxiliaryPressure->bindToTextureUnit(textureUnitAuxiliaryPressure);
        shader->setUniformValue(auxPressureField3DName,
                                textureUnitAuxiliaryPressure); CHECK_GL_ERROR;
    }

    // Precompute data extent variables and store in uniform struct.
    // =============================================================
    const GLfloat westernBoundary = dataNWCrnr.x() - grid->getDeltaLon() / 2.0f;
    const GLfloat eastWestExtent = dataSECrnr.x() - dataNWCrnr.x() + grid->getDeltaLon();
    const GLfloat northernBoundary = dataNWCrnr.y() + grid->getDeltaLat() / 2.0f;
    const GLfloat northSouthExtent = dataNWCrnr.y() - dataSECrnr.y() + grid->getDeltaLat();

    const GLint nLon = grid->getNumLons();
    const GLint nLat = grid->getNumLats();
    const GLint nLev = grid->getNumLevels();
    const GLfloat deltaLnP = std::abs(dataSECrnr.z() - dataNWCrnr.z()) / (nLev-1);
    const GLfloat upperBoundary = dataNWCrnr.z() + deltaLnP /2.0f;
    const GLfloat verticalExtent = abs(dataNWCrnr.z() - dataSECrnr.z()) + deltaLnP;

    // Assume that lat/lon spacing is the same.
    shader->setUniformValue(structName + ".deltaLat", grid->getDeltaLat()); CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".deltaLon", grid->getDeltaLon()); CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".dataSECrnr", dataSECrnr); CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".dataNWCrnr", dataNWCrnr); CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".westernBoundary", westernBoundary); CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".eastWestExtent", eastWestExtent); CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".northernBoundary", northernBoundary); CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".northSouthExtent", northSouthExtent); CHECK_GL_ERROR;
    shader->setUniformValue(
            structName + ".gridIsCyclicInLongitude",
            grid->gridIsCyclicInLongitude()); CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".nLon", nLon); CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".nLat", nLat); CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".nLev", nLev); CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".deltaLnP", deltaLnP); CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".upperBoundary", upperBoundary); CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".verticalExtent", verticalExtent); CHECK_GL_ERROR;
}


float MFrontDetection3DSourceGPU::worldZfromPressure(float p_hPa)
{
    return worldZfromPressure(p_hPa, logpbot, slopePtoZ);
}


float MFrontDetection3DSourceGPU::worldZfromPressure(
        float p_hPa, float log_pBottom_hPa, float deltaZ_deltaLogP)
{
    return (log(p_hPa)-log_pBottom_hPa) * deltaZ_deltaLogP;
}



