/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MTRANSFERFUNCTIONSELECTIONDIALOG_H
#define MTRANSFERFUNCTIONSELECTIONDIALOG_H

// standard library imports

// related third party imports
#include <QDialog>
#include <QListWidgetItem>
#include <QPushButton>

// local application imports
#include "gxfw/colourmap.h"
#include "mtransferfunctionitemwidget.h"

namespace Ui
{
class MTransferFunctionSelectionDialog;
}

namespace Met3D
{

/**
 * Dialog to select a colourmap from a MColourmapPool.
 * It presents a list of colourmaps with name and preview.
 */
class MTransferFunctionSelectionDialog : public QDialog
{
Q_OBJECT

public:
    explicit MTransferFunctionSelectionDialog(int numSteps, QWidget *parent = nullptr);

    ~MTransferFunctionSelectionDialog() override;

    /**
     * @return The last selected colourmap of the widget, or an empty string if none was selected.
     */
    QString getSelectedColourmap()
    { return selectedColourmap; }

public slots:

    /**
     * Slot for the filter field in UI.
     * Called by the filter text field when it is edited.
     * @param text New filter text
     */
    void onFilterEdited(const QString& text);

    /**
     * Called from UI whenever an item is selected in the list of colourmaps.
     * @param from The old selected list item
     * @param to The new selected list item
     */
    void onSelected(QListWidgetItem *to, QListWidgetItem *from);

    /**
     * Called from UI when an item was double clicked in the list of colourmaps.
     * This should confirm the item and close the dialog as accepted.
     * @param item The selected item
     */
    void onItemConfirmed(QListWidgetItem *item);

    /**
     * Called from UI whenever a button was pressed in the dialog button box.
     * @param button The pressed button
     */
    void onButtonClicked(QAbstractButton* button);

private:
    /**
     * Add a colourmap to the list of colourmaps in the UI
     * @param name Name of the colourmap
     * @param map Colourmap object
     */
    void addColourmap(const QString& name, MColourmap *map);

    void refreshColourmapList();

    Ui::MTransferFunctionSelectionDialog *ui;

    MColourmapPool *colourmaps;

    QMap<QString, QListWidgetItem *> items;

    QMap<QString, MTransferFunctionItemWidget *> itemWidgets;

    QString selectedColourmap;

    int steps;

    QPushButton *importButton;
};

}


#endif // MTRANSFERFUNCTIONSELECTIONDIALOG_H
