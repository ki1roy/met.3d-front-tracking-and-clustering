/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022 Marc Rautenhaus
**  Copyright 2022 Andreas Beckert
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "variableselectiondialog.h"
#include "ui_variableselectiondialog.h"

// standard library imports

// related third party imports

// local application imports
#include "gxfw/mglresourcesmanager.h"
#include "gxfw/msystemcontrol.h"
#include "gxfw/mscenecontrol.h"
#include "gxfw/actorcreationdialog.h"
#include "mainwindow.h"


namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MVariableSelectionDialog::MVariableSelectionDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MVariableSelectionDialog)
{
    ui->setupUi(this);
    ui->availableVariables->setSelectionMode(QAbstractItemView::MultiSelection);
    ui->availableVariables->setSelectionBehavior(QAbstractItemView::SelectRows);

    connect(ui->selectAllButton, SIGNAL(clicked()),
            this, SLOT(selectAllVariables()));
    connect(ui->selectNoneButton, SIGNAL(clicked()),
            this, SLOT(selectNoVariables()));
}


MVariableSelectionDialog::~MVariableSelectionDialog()
{
    delete ui;
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MVariableSelectionDialog::setAvailableVariables(
        QList<QString> variables)
{
    ui->availableVariables->clear();

    foreach (QString v, variables)
        ui->availableVariables->addItem(v);
}


void MVariableSelectionDialog::setSelectedVariables(QList<QString> variables)
{
    // For each available member in the list, check if it is contained in the
    // set "members" and set its selection accordingly.
    for (int row = 0; row < ui->availableVariables->count(); row++)
    {
        QListWidgetItem *currentItem = ui->availableVariables->item(row);
        QString currentVariable = currentItem->text();
        currentItem->setSelected(variables.contains(currentVariable));
    }
}


QList<QString> MVariableSelectionDialog::getSelectedVariables()
{
    QList<QString> selectedVariables;

    foreach (QListWidgetItem *currentItem, ui->availableVariables->selectedItems())
    {
        QString currentVariable = currentItem->text();
        selectedVariables.append(currentVariable);
    }

    return selectedVariables;
}


/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

void MVariableSelectionDialog::selectAllVariables()
{
    ui->availableVariables->selectAll();
}


void MVariableSelectionDialog::selectNoVariables()
{
    ui->availableVariables->clearSelection();
}


} // namespace Met3D
