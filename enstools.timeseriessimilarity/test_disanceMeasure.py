#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 16 14:17:51 2021

@author: kakaufmann
"""
import pytest
import numpy as np
import enstools.timeseriessimilarity.distanceMeasure as dm
from enstools.timeseriessimilarity.wrappers import euclidean_wrapper
import xarray as xr


def test_intervalRequest():
    arr = np.ones(10)
    arr[2:5] = 0
    toTest = dm.intervalRequest(arr, 0.5)[0]
    assert toTest[0] == 2 
    assert toTest[1] == 5
    
    arr[6:7] = 0
    toTest = dm.intervalRequest(arr, 0.5)
    assert toTest[0][0] == 2
    assert toTest[0][1] == 5
    assert toTest[1][0] == 6
    assert toTest[1][1] == 7
    
    arr = np.zeros(10)
    toTest = dm.intervalRequest(arr, 0.5)[0]
    assert toTest[0] == 0
    assert toTest[1] == 10
    
    arr = np.ones(10)
    arr[1:4] = 0.5
    toTest = dm.intervalRequest(arr, 0.5) #shuld be empty
    assert not toTest
    
    arr = np.ones(10)
    arr[1:2] = 0
    toTest = dm.intervalRequest(arr, 0.5)[0]
    assert toTest[0] == 1
    assert toTest[1] == 2
    
    arr = np.ones(10)
    arr[-1] = 0
    toTest = dm.intervalRequest(arr, 0.5)[0]
    assert toTest[0] == 9
    assert toTest[1] == 10

def test_intervalRequest_nan():
    arr = np.ones(10)
    arr[:2] = np.nan
    arr[2:5] = 0
    toTest = dm.intervalRequest(arr, 0.5)
    assert len(toTest) == 1
    toTest = toTest[0]
    assert toTest[0] == 2 
    assert toTest[1] == 5
    
    arr = np.ones(10)
    arr[5:] = np.nan
    arr[2:5] = 0
    toTest = dm.intervalRequest(arr, 0.5)
    assert len(toTest) == 1
    toTest = toTest[0]
    assert toTest[0] == 2 
    assert toTest[1] == 5
    
#%%
def test_array_ensemble_window_search_normal():
    oneMember = np.ones(25)
    testData = np.vstack([oneMember*0, oneMember*1, oneMember*2, oneMember*3, oneMember*4])
    reference = np.ones(5)
    #simple test, works on expected "normal" data
    toTest = dm.array_ensemble_window_search(reference, testData, euclidean_wrapper, stepSize=1, wSize=None, useDerivative=False, members=None)
    check_distance_dataset(toTest)
    check_distance_values(toTest, 21)

def test_array_ensemble_window_search_derivative():
    #works with the derivative
    oneMember = np.ones(25)
    testData = np.vstack([oneMember*0, oneMember*1, oneMember*2, oneMember*3, oneMember*4])
    reference = np.ones(5)
    
    toTest = dm.array_ensemble_window_search(reference, testData, euclidean_wrapper, stepSize=1, wSize=None, useDerivative=True, members=None)
    check_distance_dataset(toTest)    
    assert toTest["interval"].shape[0] == 21
    assert 21 not in toTest["interval"].values
    
    for i in toTest["ens"].values:
        realResults = np.zeros(21)
        assert np.array_equal(toTest.isel(ens = i)["distance"], realResults)
    
def test_array_ensemble_window_search_ensNames():
    #works with given names, even when they are strings
    oneMember = np.ones(25)
    testData = np.vstack([oneMember*0, oneMember*1, oneMember*2, oneMember*3, oneMember*4])
    reference = np.ones(5)
    names = ["a","b","c","d", "e"]
    
    toTest = dm.array_ensemble_window_search(reference, testData, euclidean_wrapper, stepSize=1, wSize=None, useDerivative=False, members=names)
    check_distance_dataset(toTest)
    
    assert toTest["interval"].shape[0] == 21
    assert 21 not in toTest["interval"].values
    for i,n in enumerate(toTest["ens"].values):
        assert n == names[i]
        realResults = np.array([np.sqrt((i-1)**2 *5)]*21)
        assert np.array_equal(toTest.isel(ens = i)["distance"], realResults)
    
def test_array_ensemble_window_search_stepSize():
    #works with stepSize != 1
    oneMember = np.ones(25)
    testData = np.vstack([oneMember*0, oneMember*1, oneMember*2, oneMember*3, oneMember*4])
    reference = np.ones(5)
    
    toTest = dm.array_ensemble_window_search(reference, testData, euclidean_wrapper, stepSize=2, wSize=None, useDerivative=False, members=None)
    check_distance_dataset(toTest)
    check_distance_values(toTest,10)
    

def test_array_ensemble_window_search_wSize():
    reference = np.ones(6)
    oneMember = np.ones(25)
    testData = np.vstack([oneMember*0, oneMember*1, oneMember*2, oneMember*3, oneMember*4])
    
    toTest = dm.array_ensemble_window_search(reference, testData, euclidean_wrapper, stepSize=1, wSize= 5, useDerivative=False, members=None)
    check_distance_dataset(toTest)   
    check_distance_values(toTest, 21)
   
     
def test_array_ensemble_window_search_nan():
    oneMember = np.ones(25)
    testData = np.vstack([oneMember*0, oneMember*1, oneMember*2, oneMember*3, oneMember*4])
    reference = np.ones(5)
    testData[0, :5] = np.nan
    
    toTest = dm.array_ensemble_window_search(reference, testData, euclidean_wrapper, stepSize=1, wSize=None, useDerivative=False, members=None)
    check_distance_dataset(toTest)
    
    assert toTest["interval"].shape[0] == 21
    assert 21 not in toTest["interval"].values
    for i in toTest["ens"].values:
        realResults = np.array([np.sqrt((i-1)**2 *5)]*21)
        if i == 0:
            realResults[:5]=np.nan
        assert np.array_equal(toTest.isel(ens = i)["distance"], realResults, equal_nan = True)
    
#%%
def check_distance_dataset(testDataset):
    assert type(testDataset) is type(xr.Dataset())
    for var in ["interval", "ens", "distance", "offset", "timeStretch"]:
        assert var in testDataset
    
    assert "interval" in testDataset.dims
    assert "ens" in testDataset.dims
    
def check_distance_values(testDataset, endvalue):
    assert testDataset["interval"].shape[0] == endvalue
    #assert endvalue not in testDataset["interval"].values
    
    for i in testDataset["ens"].values:
        realResults = np.array([np.sqrt((i-1)**2 *5)]*endvalue)
        assert np.array_equal(testDataset.isel(ens = i)["distance"], realResults)
    
#%%
def test_dataset_ensemble_window_search_normal():
    oneMember = np.ones(25)
    testData = np.vstack([oneMember*0, oneMember*1, oneMember*2, oneMember*3, oneMember*4])
    reference = np.ones(5)
    testSet = xr.Dataset()
    testSet["T"] = xr.DataArray(testData, coords =[np.arange(5), np.arange(25)], dims=["ens","time"])
    #simple test, works on expected "normal" data
    toTest = dm.dataset_ensemble_window_search(reference, testSet,"T", euclidean_wrapper, stepSize=1, wSize=None, useDerivative=False)
    check_distance_dataset(toTest)
    check_distance_values(toTest, 21)

def test_dataset_ensemble_window_search_derivative():
    #works with the derivative
    oneMember = np.ones(25)
    testData = np.vstack([oneMember*0, oneMember*1, oneMember*2, oneMember*3, oneMember*4])
    reference = np.ones(5)
    testSet = xr.Dataset()
    testSet["T"] = xr.DataArray(testData, coords =[np.arange(5), np.arange(25)], dims=["ens","time"])
    
    toTest = dm.dataset_ensemble_window_search(reference, testSet,"T", euclidean_wrapper, stepSize=1, wSize=None, useDerivative=True)
    check_distance_dataset(toTest)    
    assert toTest["interval"].shape[0] == 21
    assert 21 not in toTest["interval"].values
    
    for i in toTest["ens"].values:
        realResults = np.zeros(21)
        assert np.array_equal(toTest.isel(ens = i)["distance"], realResults)
    
def test_dataset_ensemble_window_search_ensNames():
    #works with given names, even when they are strings
    oneMember = np.ones(25)
    testData = np.vstack([oneMember*0, oneMember*1, oneMember*2, oneMember*3, oneMember*4])
    reference = np.ones(5)
    names = ["a","b","c","d", "e"]
    testSet = xr.Dataset()
    testSet["T"] = xr.DataArray(testData, coords =[names, np.arange(25)], dims=["ens","time"])
    
    toTest = dm.dataset_ensemble_window_search(reference, testSet,"T", euclidean_wrapper, stepSize=1, wSize=None, useDerivative=False)
    check_distance_dataset(toTest)
    
    assert toTest["interval"].shape[0] == 21
    assert 21 not in toTest["interval"].values
    for i,n in enumerate(toTest["ens"].values):
        assert n == names[i]
        realResults = np.array([np.sqrt((i-1)**2 *5)]*21)
        assert np.array_equal(toTest.isel(ens = i)["distance"], realResults)
    
def test_dataset_ensemble_window_search_stepSize():
    #works with stepSize != 1
    oneMember = np.ones(25)
    testData = np.vstack([oneMember*0, oneMember*1, oneMember*2, oneMember*3, oneMember*4])
    reference = np.ones(5)
    testSet = xr.Dataset()
    testSet["T"] = xr.DataArray(testData, coords =[np.arange(5), np.arange(25)], dims=["ens","time"])
    
    toTest = dm.dataset_ensemble_window_search(reference, testSet,"T", euclidean_wrapper, stepSize=2, wSize=None, useDerivative=False)
    check_distance_dataset(toTest)
    check_distance_values(toTest,10)
    

def test_dataset_ensemble_window_search_wSize():
    reference = np.ones(6)
    oneMember = np.ones(25)
    testData = np.vstack([oneMember*0, oneMember*1, oneMember*2, oneMember*3, oneMember*4])
    testSet = xr.Dataset()
    testSet["T"] = xr.DataArray(testData, coords =[np.arange(5), np.arange(25)], dims=["ens","time"])
    
    toTest = dm.dataset_ensemble_window_search(reference, testSet,"T", euclidean_wrapper, stepSize=1, wSize= 5, useDerivative=False)
    check_distance_dataset(toTest)   
    check_distance_values(toTest, 21)
   
     
def test_dataset_ensemble_window_search_nan():
    oneMember = np.ones(25)
    testData = np.vstack([oneMember*0, oneMember*1, oneMember*2, oneMember*3, oneMember*4])
    reference = np.ones(5)
    testData[0, :5] = np.nan
    testSet = xr.Dataset()
    testSet["T"] = xr.DataArray(testData, coords =[np.arange(5), np.arange(25)], dims=["ens","time"])
    
    toTest = dm.dataset_ensemble_window_search(reference, testSet,"T", euclidean_wrapper, stepSize=1, wSize=None, useDerivative=False)
    check_distance_dataset(toTest)
    
    assert toTest["interval"].shape[0] == 21
    assert 21 not in toTest["interval"].values
    for i in toTest["ens"].values:
        realResults = np.array([np.sqrt((i-1)**2 *5)]*21)
        if i == 0:
            realResults[:5]=np.nan
        assert np.array_equal(toTest.isel(ens = i)["distance"], realResults, equal_nan = True)
        
#%%
def create_simple_dist_dataset():
    dsTestDist = xr.Dataset()
    fAssign = lambda arr: xr.DataArray(arr,coords= [np.arange(5), np.arange(20)], dims = ["ens", "interval"])
    dsTestDist["offset"] = fAssign(np.zeros((5,20)))
    dsTestDist["timeStretch"] = fAssign(np.zeros((5,20)))
    dsTestDist["distance"] = fAssign(np.ones((5,20)))
    return dsTestDist


def test_get_windows_of_interest_normal():
    dsTestDist = create_simple_dist_dataset()
    dsTestDist.isel(ens = 2)["distance"][2:8] = 0
    dsToTest = dm.get_windows_of_interest(dsTestDist, 0.5)
    assert type(dsToTest) is type(xr.Dataset())
    for var in ["ens","distance", "relativeDistance", "offset", "timeStretch", "timeOffset", "window"]:
        assert var in dsToTest
    
    assert dsToTest["window"].size == 1
    assert dsToTest["ens"].values.item() == 2
    assert dsToTest["timeOffset"].values.item() == 2
    assert dsToTest["relativeDistance"].values.item() == 0
    
    dsTestDist.isel(ens = 2)["distance"][2:4] = 0.5
    dsTestDist.isel(ens = 2)["distance"][5:8] = 0.5
    dsToTest = dm.get_windows_of_interest(dsTestDist, 0.5)
    assert type(dsToTest) is type(xr.Dataset())
    for var in ["ens","distance", "relativeDistance", "offset", "timeStretch", "timeOffset", "window"]:
        assert var in dsToTest
    
    assert dsToTest["window"].size == 1
    assert dsToTest["ens"].values.item() == 2
    assert dsToTest["timeOffset"].values.item() == 4
    assert dsToTest["relativeDistance"].values.item() == 0

    
def test_get_windows_of_interest_twoPoints():
    dsTestDist = create_simple_dist_dataset()
    dsTestDist.isel(ens = 3)["distance"][2:8] = 0.5
    dsTestDist.isel(ens = 3)["distance"][3] = 0
    dsTestDist.isel(ens = 3)["distance"][6] = 0
    dsToTest = dm.get_windows_of_interest(dsTestDist, 0.7)
    assert type(dsToTest) is type(xr.Dataset())
    for var in ["ens","distance", "relativeDistance", "offset", "timeStretch", "timeOffset", "window"]:
        assert var in dsToTest
    
    assert dsToTest["window"].size == 1
    assert dsToTest["ens"].values.item() == 3
    assert dsToTest["timeOffset"].values.item() == 3
    assert dsToTest["relativeDistance"].values.item() == 0
 
    dsTestDist.isel(ens = 0)["distance"][:4] = 0.3
    dsToTest = dm.get_windows_of_interest(dsTestDist,0.7)
    assert type(dsToTest) is type(xr.Dataset())
    for var in ["ens","distance", "relativeDistance", "offset", "timeStretch", "timeOffset", "window"]:
        assert var in dsToTest
    
    assert dsToTest["window"].size == 2
    assert np.array_equal(dsToTest["ens"].values, np.array([0,3])) 
    assert np.array_equal(dsToTest["timeOffset"].values, np.array([0,3]))
    assert np.array_equal(dsToTest["relativeDistance"].values, np.array([0.3,0]))

def test_get_windows_of_interest_relDist():
    dsTestDist = create_simple_dist_dataset()
    dsTestDist["distance"] = xr.DataArray(np.ones((5,20))*30,coords= [np.arange(5), np.arange(20)], dims = ["ens", "interval"])
    dsTestDist.isel(ens = 2)["distance"][2:8] = 10
    dsToTest = dm.get_windows_of_interest(dsTestDist, 0.5)
    assert type(dsToTest) is type(xr.Dataset())
    for var in ["ens","distance", "relativeDistance", "offset", "timeStretch", "timeOffset", "window"]:
        assert var in dsToTest
    
    assert dsToTest["window"].size == 1
    assert dsToTest["ens"].values.item() == 2
    assert dsToTest["timeOffset"].values.item() == 2
    assert dsToTest["relativeDistance"].values.item() == 1/3

    
def test_get_windows_of_interest_nans():
    dsTestDist = create_simple_dist_dataset()
    dsTestDist.isel(ens = 3)["distance"][:3] = np.nan
    dsTestDist.isel(ens = 2)["distance"][8:] = np.nan
    dsTestDist.isel(ens = 2)["distance"][2:8] = 0
    dsToTest = dm.get_windows_of_interest(dsTestDist, 0.5)
    assert type(dsToTest) is type(xr.Dataset())
    for var in ["ens","distance", "relativeDistance", "offset", "timeStretch", "timeOffset", "window"]:
        assert var in dsToTest
    
    assert dsToTest["window"].size == 1
    assert dsToTest["ens"].values.item() == 2
    assert dsToTest["timeOffset"].values.item() == 2
    assert dsToTest["relativeDistance"].values.item() == 0

    
def test_get_windows_of_interest_weigted():
    dsTestDist = create_simple_dist_dataset()
    dsTestDist.isel(ens = 2)["distance"][2:8] = 0
    dsTestDist["weightedDistance"] = xr.DataArray(np.ones((5,20)),coords= [np.arange(5), np.arange(20)], dims = ["ens", "interval"])
    dsTestDist.isel(ens = 4)["weightedDistance"][5:] = 0
    dsToTest = dm.get_windows_of_interest(dsTestDist, 0.5)
    assert type(dsToTest) is type(xr.Dataset())
    for var in ["ens","distance", "relativeDistance", "offset", "timeStretch", "timeOffset", "window"]:
        assert var in dsToTest
    
    assert dsToTest["window"].size == 1
    assert dsToTest["ens"].values.item() == 4
    assert dsToTest["timeOffset"].values.item() == 5
    assert dsToTest["relativeDistance"].values.item() == 0
    
#%%
def check_weighted_distance(dsTestDist, newDist, resultVector):
    assert np.array_equal(newDist, dsTestDist["weightedDistance"].values)
    for i in dsTestDist.ens.values:
        assert np.array_equal(dsTestDist.isel(ens = i)["weightedDistance"].values, resultVector)
        
    
def test_weight_the_attributes_dist():
    dsTestDist = create_simple_dist_dataset()
    for i in dsTestDist.ens.values:
        dsTestDist.isel(ens = i)["distance"] = dsTestDist.isel(ens = i)["distance"] * (i+1)
    
    newDist = dm.weight_the_attributes(dsTestDist) #wDist = 100, rest 0
    check_weighted_distance(dsTestDist, newDist, np.ones(20))
        
    newDist = dm.weight_the_attributes(dsTestDist, 2) #wDist = 2, rest 0
    check_weighted_distance(dsTestDist, newDist, np.ones(20))
        
    arr = np.stack([np.arange(20)]*5)
    dsTestDist["distance"] = xr.DataArray(arr,coords= [np.arange(5), np.arange(20)], dims = ["ens", "interval"])
    
    newDist = dm.weight_the_attributes(dsTestDist) #wDist = 100, rest 0
    result = np.arange(20)/19
    check_weighted_distance(dsTestDist, newDist, result)
  
        
def test_weight_the_attributes_time():
    dsTestDist = create_simple_dist_dataset()
    
    newDist = dm.weight_the_attributes(dsTestDist, 0, (10,1)) #wDist = 100, rest 0
    result = np.array([abs(i-10)/10 for i in np.arange(20)])
    check_weighted_distance(dsTestDist, newDist, result)
        
    newDist = dm.weight_the_attributes(dsTestDist, 1, (10,2)) 
    result = np.array([(1 + abs(i-10)*2)/21 for i in np.arange(20)])
    result = result/max(result)   
    check_weighted_distance(dsTestDist, newDist, result)
        
        
def test_weight_the_attributes_offset():
    dsTestDist = create_simple_dist_dataset()
    #newDist = dm.weight_the_attributes(dsTestDist,0, wOffset = 1)
    #assert np.isnan(newDist).all()
    
    dsTestDist["offset"] = xr.DataArray(np.ones((5,20)),coords= [np.arange(5), np.arange(20)], dims = ["ens", "interval"])
    for i in dsTestDist.ens.values:
        dsTestDist.isel(ens = i)["offset"] = dsTestDist.isel(ens = i)["offset"]*i
        
    newDist = dm.weight_the_attributes(dsTestDist,0, wOffset = 1) #wDist = 0, wOffset = 1
    check_weighted_distance(dsTestDist, newDist, np.ones(20))
        
    newDist = dm.weight_the_attributes(dsTestDist, 1, wOffset = 2) 
    check_weighted_distance(dsTestDist, newDist, np.ones(20))
        
    arr = np.stack([np.arange(20)]*5)
    dsTestDist["offset"] = xr.DataArray(arr,coords= [np.arange(5), np.arange(20)], dims = ["ens", "interval"])
    newDist = dm.weight_the_attributes(dsTestDist, 1, wOffset = 2) 
    assert np.array_equal(newDist, dsTestDist["weightedDistance"].values)
    for i in dsTestDist.ens.values:
        result = (arr[i]*2+1)/39
        assert np.array_equal(dsTestDist.isel(ens = i)["weightedDistance"].values, result)


def test_weight_the_attributes_stretch():
    dsTestDist = create_simple_dist_dataset()
    #newDist = dm.weight_the_attributes(dsTestDist,0, wTimeStretch = 1)
    #assert np.isnan(newDist).all()
    
    dsTestDist["timeStretch"] = xr.DataArray(np.ones((5,20)),coords= [np.arange(5), np.arange(20)], dims = ["ens", "interval"])
    for i in dsTestDist.ens.values:
        dsTestDist.isel(ens = i)["timeStretch"] = dsTestDist.isel(ens = i)["timeStretch"]*i
        
    newDist = dm.weight_the_attributes(dsTestDist,0, wTimeStretch = 1) #wDist = 0
    check_weighted_distance(dsTestDist, newDist, np.ones(20))
        
    newDist = dm.weight_the_attributes(dsTestDist, 1, wTimeStretch = 2) 
    check_weighted_distance(dsTestDist, newDist, np.ones(20))
        
    arr = np.stack([np.arange(20)]*5)
    dsTestDist["timeStretch"] = xr.DataArray(arr,coords= [np.arange(5), np.arange(20)], dims = ["ens", "interval"])
    newDist = dm.weight_the_attributes(dsTestDist, 1, wTimeStretch = 2) 
    assert np.array_equal(newDist, dsTestDist["weightedDistance"].values)
    for i in dsTestDist.ens.values:
        result = (arr[i]*2+1)/39
        assert np.array_equal(dsTestDist.isel(ens = i)["weightedDistance"].values, result)
        
#%%
def create_test_window_set():
    dsTest = xr.Dataset()
    fAssign = lambda arr: xr.DataArray(arr, coords= [np.arange(10)], dims = ["window"])
    #arr = list(zip(np.arange(5),np.arange(5)))
    #arr = np.reshape(arr, 10)
    
    dsTest["ens"] = fAssign(np.array([0,0,1,1,2,2,3,3,4,4]))
    dsTest["distance"] = fAssign(np.ones(10))
    dsTest["relativeDistance"] = fAssign(np.ones(10))
    dsTest["offset"] = fAssign(np.zeros(10))
    dsTest["timeStretch"] = fAssign(np.zeros(10))
    dsTest["timeOffset"] = fAssign([4,4,3,3,2,2,1,1,0,0])
    return dsTest
    

def test_get_order():
    dsTestWindows = create_test_window_set()
    o = dm.get_order(dsTestWindows,"invalid", np.arange(5))
    assert ~np.isnan(o).any()
    do = dict(o)
    for i in range(5):
        assert i in do
        assert do[i] == i #this is a test for implicit ordering. Change that, when the implicit order changes
    
    for index, var in enumerate(["relativeDistance","distance","offset","timeOffset","timeStretch"]):
        dsTestWindows = create_test_window_set()
        o = dm.get_order(dsTestWindows,index, np.arange(5))
        assert ~np.isnan(o).any()
        do = dict(o)
        for i in range(5):
            assert i in do
            if var == "timeOffset":
                assert do[i] == 4-i
            else:
                assert do[i] == i #this is a test for implicit ordering. Change that, when the implicit order changes
    
        dsTestWindows[var] = xr.DataArray(np.array([4,6,3,6,2,6,1,6,0,6]), coords= [np.arange(10)], dims = ["window"])
        o = dm.get_order(dsTestWindows,index,np.arange(5))
        assert ~np.isnan(o).any()
        do = dict(o)
        for i,v in enumerate(do):
            assert v == 4-i
        
        dsTestWindows[var] = xr.DataArray(np.array([3,6,4,6,0,6,1,6,2,6]), coords= [np.arange(10)], dims = ["window"])
        goal = {0:3, 1:4, 2:0, 3:1, 4:2}
        o = dm.get_order(dsTestWindows,index,np.arange(5))
        assert ~np.isnan(o).any()
        do = dict(o)
        for _,v in enumerate(do):
            assert do[v] == goal[v]        
    
        
def test_get_order_members():
    dsTestWindows = create_test_window_set()
    o = dm.get_order(dsTestWindows,"invalid", np.arange(7))#add members at the end
    assert ~np.isnan(o).any()
    do = dict(o)
    for i in range(7):
        assert i in do
        assert do[i] == i 
        
    dsTestWindows["dist"] = xr.DataArray(np.array([4,6,3,6,2,6,1,6,0,6]), coords= [np.arange(10)], dims = ["window"])
    dsTestWindows["ens"] = xr.DataArray(np.array([0,0,2,2,3,3,4,4,5,5]), coords= [np.arange(10)], dims = ["window"]) #insert 1 with get_order
    o = dm.get_order(dsTestWindows, "dist", np.arange(6)) #add member in the middle
    assert np.array_equal(np.array(o), np.array([(0,0),(2,1),(3,2),(4,3),(5,4),(1,5)]))
    
    dsTestWindows["ens"] = xr.DataArray(np.array([1.,1.,2.,2.,3.,3.,4.,4.,5.,5.]), 
                 coords= [np.arange(10)], dims = ["window"]) #the members are names
    members = np.array([1.,2.,3.,4.,5.])
    o = dm.get_order(dsTestWindows, "ens", members)
    for i, v in enumerate(o):
        assert v[0] == members[i]
        assert v[1] == i
        
    dsTestWindows["ens"] = xr.DataArray(np.array(["a","a","b","b","c","c","d","d","e","e"]), 
                 coords= [np.arange(10)], dims = ["window"]) #the members are names
    members = np.array(["a","b","c","d","e"])
    o = dm.get_order(dsTestWindows, "ens", members) #TODO: This leads to no abs for string error. Fix it
    for i, v in enumerate(o):
        assert v[0] == members[i]
        assert v[1] == i

def test_get_order_refMember():
    dsTestWindows = create_test_window_set()
    o = dm.get_order(dsTestWindows,"invalid", np.arange(5), 4)
    assert ~np.isnan(o).any()
    do = dict(o)
    assert 4 in do
    assert do[4] == 0
    for i in range(4):
        assert i in do
        assert do[i] == i+1
        
    dsTestWindows["dist"] = xr.DataArray(np.array([4,6,3,6,2,6,1,6,0,6]), coords= [np.arange(10)], dims = ["window"])
    o = dm.get_order(dsTestWindows, "dist", np.arange(5), 3) #add member in the middle
    assert np.array_equal(np.array(o), np.array([(3,0),(0,1),(1,2),(2,3),(4,4)]))