# enstools.timeseriessimilarity installation guide


## Create a new conda enviroment and install required packages

1. Create a new conda enviroment with python version 3.9: 
```
conda create -n enstools python=3.9
```

2. Switch to the new enviroment to install required packages:
```
conda activate enstools
```

3. Install required packages via conda:
```
conda install -c conda-forge numpy numba xarray dask distributed cloudpickle toolz pint nose scikit-learn eccodes cartopy decorator multipledispatch cffi cachey cdo python-cdo git netcdf4 h5py hdf5plugin appdirs pytest dtw-python h5netcdf
```


##  Install enstools 

1. Create a new folder where you want to install the the timeseriessimilarity analysis
```
mkdir tssanalysis && cd tssanalysis
```

2. Clone the enstools git repository:
```
git clone https://github.com/wavestoweather/enstools.git
```

3. Add the enstools package PATH to the conda enviroment. This can be done by creating a .pth file in the conda python site-packages folder. 

```
touch place/where/anaconda/is/anaconda3/envs/enstools/lib/python3.9/site-packages/enstools.pth

```

4. Open the *enstools.pth* file and add the enstools package PATH, which should look like: ***/home/some/path/tssanalysis/enstools/***


## Install Lagranto (optional)
- This step is optional for those who want to use the Python Lagranto package. If not intalled, Pandas will be used to read trajectories. 
1. Navigate to the *tssanalysis* folder and clone the Lagranto git repository:
```
git clone https://git.iac.ethz.ch/atmosdyn/Lagranto.git
```

3. Add the Lagranto **src** PATH to the conda enviroment. This can be done by creating a .pth file in the conda python site-packages folder. 

```
touch place/where/anaconda/is/anaconda3/envs/enstools/lib/python3.9/site-packages/lagranto.pth

```

4. Open the *lagranto.pth* file and add the lagranto package **src** PATH, which should look like: ***/home/some/path/tssanalysis/lagranto/src***


## Install enstools.timeseriessimilarity

1. Navigate to the *tssanalysis* folder and clone the enstools.timeseriessimilarity git repository:
```
git clone https://gitlab.com/vda.internal/enstools.timeseriessimilarity.git
```


# Compabilities and requirements
- Matplotlib v3.4 or above
- Python version 3.6-3.9, not working with python version 3.10 or below 

*Package written on Python version  3.9*
