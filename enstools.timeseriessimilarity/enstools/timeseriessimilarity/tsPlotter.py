#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 10 14:18:36 2021

@author: kakaufmann; andreas beckert
"""
#import xarray as xr #tsDataset, is a xr.Dataset
import numpy as np 
import matplotlib.pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg

from . import plotterFunctions as f
from . import getterFunctions as gf

#TODO: highlight the reference member in a maxtrix or horizon ensemble-view
#TODO: spaghettie and color don't allign anymore, why? -> in visualize the distance, maybe it would be best to use another instance for that...
#TODO: find a better version to deal with Horizon padding, like changing Horizon to work on a axis. -> subfigure
#TODO: paint Reference, with time window as green lines, reference from ensemble member
#TODO: Paint_members_of_interest
        
def subfig2ax(func):
    return lambda ax,*args,**kv: func(ax,*args,**kv) if isinstance(ax,plt.Axes) else func(ax.subplots(),*args,**kv)

#%% Class
class tsPlotter:
    """This class will visualize the steps of the time series similarity analysis
     
    More information about Horizon graph is here:
            https://waterprogramming.wordpress.com/2020/08/04/how-to-make-horizon-plots-in-python/
    This was used as a base for our version.
    More information about matplotlib is here:
            https://matplotlib.org/
    More information about Datasets here:
            http://xarray.pydata.org/en/stable/user-guide/data-structures.html#dataset
        """
    def __init__(self, tsDataset, dataName, tickDist, scale=None, colors="RdBu_r", coreSize=(15,15), outerHeight=5,
                 textSize=12, horizonBase="min"):
        """create an instance of tsPlotter

        Parameter
        ---------
        tsDataset: xarray.Dataset
            an instance of an xarray Dataset, containing the data as a variable of
            time and ensemble member. More information about Datasets here:
                http://xarray.pydata.org/en/stable/user-guide/data-structures.html#dataset
            beside the data that will be displyed a variable, dimensions with the 
            names "ens" and "time" are expected. All time series should have the same length.
        dataName: string
            the name with which one can accsess the data in the dataset, when using
            the notation dataset["name"], the name of the variable. 
        scale: tupel
            the minimal and maximal values of the data to be shown. The first 
            entry is the minimal value and the second entry is the maxmum. 
            If not set, the scale will be centered around zero, with the maximum 
            absolute value in both the positive and negative.
        colors: string
            the name of the matplotlib colorbar. The colors used for the heatmap. 
            The default is "RdBu_r", where blue are the negative numbers, 0 is 
            white and red are positive numbers.
        coreSize: tupel of int or float
            The core of the visualisation is the view in the middle, showing each 
            member of the ensemble in detail, either with a heatmap or horizon plot.
            This parameter sets the size of that view, like the matplotlib figuresize.
            Default is (15,15).
        outerHeight: int or float
            The size of the views above and below the core view. The width is the same
            as the core-width, but the height can be choosen independently with this.
            Default is 5.
        
        Returns
        -------
        tsPlotter:
            instance of the class initialized with the given dataset and name of 
            the time series
        """
        assert ("time" in tsDataset) and ("ens" in tsDataset) and (dataName in tsDataset)
        self.__dsTimeSeries = tsDataset
        self.__VarToAnalyse = dataName
        if scale is None:
            #self.__scale = self.__get_y_bound()
            self.__scale = gf.get_y_bound(self.__get_current_values())
        else:
            self.__scale = scale

        self.__xTicks, self.__tickLabel = self.__get_ticks(tickDist)
        self.__tickDistance = tickDist
        self.__color = colors
        self.__coreSize = coreSize
        self.__height = outerHeight
        self.__textSize = textSize
        self.__horizonBase = horizonBase
            
        #create a map from name to index in  the "ens" variable. This way we can use isel() whenever we need sel().
        self.__nameIndexMap = dict()
        for index, name in enumerate(tsDataset["ens"].values):
            self.__nameIndexMap[name] = index
            

    #%% visualizer

    def visualize_the_ensemble(self, view = 1, title = None):
        """ Creates a matplotlib figure with two views on the ensemble.
        
        The first view shows each time series against the time, all in one axis and with 
        the same color. Below that is a larger view that depics every member in more
        detail either with a heatmap, or a horizon graph. The heatmap is the default.
        
        Parameters
        ----------
        view : int (0,1,2)
            to differenciate the views of each time series in the ensemble. 
            0 is the horizon plot, 1 is the heatmap. default is one.
        title: string
            the title of the resulting figure. Will be generated when not given.
        
        Returns
        -------
        list of matplotlib supfigures.
        """
        
        #carefull, not the ticks need to be in the range(0,len(time))
        #need 2 subfigures: spaghettie and core (=heatmap or horizon) (1x height+1xcore_height)
        #fig = plt.figure(figsize = (self.__coreSize[0], self.__coreSize[1]+ self.__height), constrained_layout = True )
        fig = f.generate_figure(self.__coreSize[0], self.__coreSize[1], self.__height, 2)
        if title is None: #set default title, that is somewhat meaningfull
            title = "Ensemble: " + self.__VarToAnalyse
        
        fig.suptitle(title)
        
# =============================================================================
#         views = fig.subfigures(2, 1,True,hspace = 0, height_ratios = 
#                                [self.__height/(self.__coreSize[1]+ self.__height) ,
#                                 self.__coreSize[1]/(self.__coreSize[1]+ self.__height)])
# =============================================================================
        views = f.generate_subfigures(fig, 2, self.__coreSize[1], self.__height)
        #----------------------------------------------------------------------
        self.__paint_the_ensemble_spaghetti(views[0])
        
        if view == 0:  # Horizon-view
            self.__paint_the_ensemble_horizon(views[1])
            
        else:
            self.__paint_the_ensemble_matrix(views[1])

        return views


    def visualize_the_ensemble_cpp(self, view=1, title=None):
        """creates a matplotlib figure with two views on the ensemble.

        The first view shows each time series against the time, all in one axis and with
        the same color. Below that is a larger view that depicts every member in more
        detail either with a heatmap, or a horizon graph. The heatmap is the default.

        Parameters
        ----------
        view : int (0,1,2)
            to differentiate the views of each time series in the ensemble.
            0 is the horizon plot, 1 is the heatmap. default is one.
        title: string
            the title of the resulting figure. Will be generated when not given.

        Returns
        -------
        list of matplotlib subfigures.
        """

        # carefull, not the ticks need to be in the range(0,len(time))
        # need 2 subfigures: spaghettie and core (=heatmap or horizon) (1x height+1xcore_height)
        # fig = plt.figure(figsize = (self.__coreSize[0], self.__coreSize[1]+ self.__height), constrained_layout = True )
        fig = f.generate_figure(self.__coreSize[0], self.__coreSize[1], self.__height, 2)
        if title is None:  # set default title, that is somewhat meaningfull
            title = "Ensemble: " + self.__VarToAnalyse

        fig.suptitle(title)

        # =============================================================================
        #         views = fig.subfigures(2, 1,True,hspace = 0, height_ratios =
        #                                [self.__height/(self.__coreSize[1]+ self.__height) ,
        #                                 self.__coreSize[1]/(self.__coreSize[1]+ self.__height)])
        # =============================================================================
        views = f.generate_subfigures(fig, 2, self.__coreSize[1], self.__height)
        # ----------------------------------------------------------------------
        self.__paint_the_ensemble_spaghetti(views[0])

        if view == 0:  # Horizon-view
            self.__paint_the_ensemble_horizon(views[1])

        else:
            self.__paint_the_ensemble_matrix(views[1])

        # see: https://stackoverflow.com/questions/59685600/capturing-python-windowed-output-from-c-embedded-interpreter
        # fig.canvas.draw()
        # w, h = fig.canvas.get_width_height()  # width and height of the canvas
        # buf = fig.canvas.tostring_argb()  # a byte string of type uint8
        canvas = plt.get_current_fig_manager().canvas

        agg = canvas.switch_backends(FigureCanvasAgg)
        agg.draw()
        buf, (w, h) = agg.print_to_buffer()

        return w, h, buf

    def visualize_with_reference(self, reference, view=1, title=None,
                                 referenceTimeWindow=None, referenceInEnsemble=False):
        """creates a matplotlib figure with two views on the ensemble and a close view on
        a selected reference.
        
        The first view shows each time series against the time, all in one axis and with 
        the same color, exept the reference. Below that is a larger view that depics 
        every member in more detail. There are two different methods for that, 
        first a matrix view, where the value is color coded, which is default. 
        The second method is a horizon graph.
        Underneath that the reference is shown alone and in more detail.
        
        Parameters
        ----------
        reference: list/array of float, or name of an ensemble member
            the reference to compare the time series of the ensemble with. 
            This will be highlighted in the spaghetti view of the ensemble with red.
            If it is a member of the ensemble or a cutout of such a time series,
            this can be indicated with referenceInEnsemble, the whole member will be displayed
            and in the single view for the reference below, the time of the cutout will be
            highlighted in red as well. Therefor a referenceTimeWindow should be given
        view: int (0,1,2)
            to differentiate the views of each time series in the ensemble.
            0 is the horizon plot, 1 is the matrix view. default is one.
        title: string
            the title of the resulting figure. Will be generated when not given.
        referenceTimeWindow: 2-tuple
            the indices of the start and end of the reference in time. 
            This is used to plot the reference at the right time or highlight a 
            certain window (reference window) in a reference member. Unless the 
            reference is a member of the ensemble, the length of the reference 
            should be the same, as the time covered by this tuple.
            If not given the reference will be shown at the beginning of the time axis.
        referenceInEnsemble: boolean
            change the interpretation of the reference parameter. If True, then the 
            reference will be seen as a single integer, the member which is used
            as a reference member. When this parameter is False, the reference
            needs to be list of values or array.
        
        Returns
        -------
        List of matplotlib supfigures.
        """
        # fig = plt.figure(figsize = (self.__coreSize[0],self.__coreSize[1]+2*self.__height), constrained_layout = True)
        fig = f.generate_figure(self.__coreSize[0], self.__coreSize[1], self.__height, 3)
        if title is None:  # set a somewhat useful default title
            title = "Ensemble: " + self.__VarToAnalyse + "\n with the reference."
        fig.suptitle(title)
        
# =============================================================================
#         views = fig.subfigures(3, 1,True,hspace = 0, height_ratios = [
#             self.__height/(self.__coreSize[1] +2 * self.__height),
#             self.__coreSize[1]/(self.__coreSize[1] + 2 * self.__height), 
#             self.__height/(self.__coreSize[1] + 2 * self.__height) ])
# =============================================================================
        views = f.generate_subfigures(fig, 3, self.__coreSize[1], self.__height)
        # ----------------------------------------------------------------------
        referenceLabel = None
        if referenceInEnsemble:
            # this way the reference is always a ts. I don't have to check again.
            referenceLabel = reference
            reference = gf.get_time_series(self.__dsTimeSeries, self.__VarToAnalyse, self.__nameIndexMap[reference])

        self.__show_reference(views[0], views[2], reference, referenceLabel, referenceTimeWindow)

        if view == 0:  # horizon
            self.__paint_the_ensemble_horizon(views[1])
          
        else:  # shadow is also ensemble, since there is no shadow now
            self.__paint_the_ensemble_matrix(views[1])
                             
        return views

    # TODO: Idea, change the color for this view, too.
    def visualize_the_distance_ts(self, dsDistanceTimeSeries, view=1, title=None):
        """ Creates a figure to show the distance time series, like they are generated
        from the analysis of the given time series dataset. 
        
        The view is the same, as in visualize_the_ensemble, as it does the same.
        But the time axis is the same as for the time series dataset used to generate
        the instance.
        
        Parameters
        ----------
        dsDistanceTimeSeries: xarray.Dataset
            The dataset containing the distance information of the ensemble to the reference.
            It has to contain the dimensions "ens" and "interval",
            The variable "distance" or "weightedDistance", "timeOffset", "offset" and "timeStretch"
        view : int (0,1,2)
            to differentiate the views of each time series in the ensemble.
            0 is the horizon plot, 1 is the matrix view. default is one.
        title: string
            the title of the resulting figure. Will be generated when not given.
        
        Returns
        -------
        list of matplotlib subfigures.
        """
        # fig = plt.figure(figsize = (self.__coreSize[0], self.__coreSize[1]+ self.__height), constrained_layout = True )
        fig = f.generate_figure(self.__coreSize[0], self.__coreSize[1], self.__height, 2)
        if title is None:  # set default title, that is somewhat meaningfull
            title = "Distance ensemble " 
        
        fig.suptitle(title)
        
# =============================================================================
#         views = fig.subfigures(2, 1,True,hspace = 0, height_ratios = 
#                                [self.__height/(self.__coreSize[1]+ self.__height) ,
#                                 self.__coreSize[1]/(self.__coreSize[1]+ self.__height)])
# =============================================================================
        views = f.generate_subfigures(fig, 2, self.__coreSize[1], self.__height)
        # ----------------------------------------------------------------------
        var = "weightedDistance" if "weightedDistance" in dsDistanceTimeSeries else "distance"
        data = gf.get_data_array(dsDistanceTimeSeries, var)
        _, scale = gf.get_y_bound(data)
        
        ax1 = views[0].subplots()
        f.paint_spaghetti(ax1, data, var)
        f.regulate_lims(ax1, (0,scale), (0,self.__dsTimeSeries.sizes["time"]))
        f.regulate_xTicks(ax1, self.__tickLabel, self.__xTicks)
        
        if view == 0:  # Horizon-view
            f.paint_horizon(views[1], data, cmap=self.__color,
                            vmin=0, vmax=scale,
                            xticks=self.__xTicks, xlabels=self.__tickLabel,
                            ylabels=gf.get_ylabels(self.__dsTimeSeries),
                            horizon_base=self.__horizonBase)
            axs = views[1].get_axes()
            axs[-1].set_xlim((0, self.__dsTimeSeries.sizes["time"]))
            
        else:
            ax2 = views[1].subplots()
            ax2.set_xlim((0, self.__dsTimeSeries.sizes["time"]))
            f.paint_heatmap_ensemble(ax2, data, cmap= self.__color, 
                                     vmin = 0, vmax = scale, 
                                     xticks = self.__xTicks, xlabels= self.__tickLabel,
                                     ylabels= gf.get_ylabels(self.__dsTimeSeries))
 
        return views


    # TODO: let the user choose the distance ("wheighted, relative, normal")
    def visualize_the_windows(self, reference, dsWindows, view=1, title=None,
                              referenceTimeWindow=None, referenceInEnsemble=False):
        """Given a set of Windows, this creates a view, where the windows are highlighted
        while visulazing the ensemble.
        
        The figure is similar to the one obtained by visualize_with_reference.
        First a spaghetti plot of the whole ensemble with the reference in red.
        The ensemble in the chosen view under that with the windows with minimal
        distance to the reference shown as a grey scale over the ensemble. 
        The grey scale depends on the distance to the reference, the darker the color,
        the closer the window is to the reference. 
        At the bottom is the reference in more detail.
        
        Parameters
        ----------
        reference: list/array of float, or int
            the reference to compare the time series of the ensemble with. 
            This will be highlighted in the spaghetti view of the ensemble with red.
            If it is a member of the ensemble or a cutout of such a time series,
            this can be indicated with referenceInEnsemble, the whole member will be displayed
            and in the single view for the reference below, the time of the cutout will be
            highlighted in red as well. Therefor a referenceTimeWindow should be given
        dsWindows: xarray.Dataset
            The windows with their distances, like a result of DistanceMessurer.get_windows_of_interest.
            In the core view (matrix or horizon) the windows will be shown as a black overlay or a 
            black line, respectively. The transparency depends on the distance to the reference.
            The Overlay is than it the same time, where the window was detected.
            The weightedDistance is chosen as distance, when possible.
        view: int (0,1,2)
            to differentiate the views of each time series in the ensemble.
            0 is the horizon plot, 1 is the matrix view. default is one.
        title: string
            the title of the resulting figure. Will be generated when not given.
        referenceTimeWindow: 2-tuple
            the indices of the start and end of the reference in time. The end is 
            exclusive, like the index [a:b].
            This is used to plot the reference at the right time or highlight a 
            certain window (reference window) in a reference member. Unless the 
            reference is a member of the ensemble, the length of the reference 
            should be the same, as the time covered by this tuple.
            If not given the reference will be shown at the beginning of the time axis.
        referenceInEnsemble: boolean
            change the interpretation of the reference parameter. If True, then the 
            reference will be seen as a single integer, the member which is used
            as a reference member. When this parameter is False, the reference
            needs to be list of values or array.
        
        Returns
        -------
        list of matplotlib supfigures.

        """
        # fig = plt.figure(figsize = (self.__coreSize[0],self.__coreSize[1]+2*self.__height), constrained_layout = True)
        fig = f.generate_figure(self.__coreSize[0], self.__coreSize[1], self.__height, 3)
        if title is None:  # set a default title, since none is given
            title = "Ensemble: " + self.__VarToAnalyse + "\n windows with a difference lower than a threshold"
            
        fig.suptitle(title)
        
# =============================================================================
#         views = fig.subfigures(3, 1,True,hspace = 0, height_ratios = [
#             self.__height/(self.__coreSize[1] +2 * self.__height),
#             self.__coreSize[1]/(self.__coreSize[1] + 2 * self.__height), 
#             self.__height/(self.__coreSize[1] + 2 * self.__height) ])
# =============================================================================
        views = f.generate_subfigures(fig, 3, self.__coreSize[1], self.__height)
        # ----------------------------------------------------------------------
        # reference and the spaghettie of the ensemble: no change here, because we don't draw the distance in here
        referenceLabel = None
        if referenceInEnsemble:
            # this way the reference is always a ts. I don't have to check again.
            referenceLabel = reference
            reference = gf.get_time_series(self.__dsTimeSeries, self.__VarToAnalyse, self.__nameIndexMap[reference])

        self.__show_reference(views[0], views[2], reference, referenceLabel, referenceTimeWindow)

        if referenceTimeWindow is not None:  # need at least the length of the windows, since the windows only contain the starting point
            wSize = referenceTimeWindow[1] - referenceTimeWindow[0]
        else:
            # when no length is dictated by an optimal window, the length of the reference is extracted
            # this means, when the windows have another length than the reference, this must be indicated with an interval
            wSize = len(reference)

        mask = self.__generate_shadow_mask(dsWindows, wSize, self.__nameIndexMap)

        if view == 0:  # horizon
            self.__paint_important_windows_horizon(views[1], mask)
        else:
            self.__paint_important_windows_matrix(views[1], mask, not (view == 2))

        return views

    def visualize_with_new_order(self, reference, dsWindows, newOrder, view=1, title=None, referenceTimeWindow=None,
                                 referenceInEnsemble=False, shadow=True):
        """Given a set of Windows, this creates a view, where the windows are highlighted
        while visualizing the ensemble and the members are sorted according to a given order.
        
        The figure is similar to the one optained by visualize_the_windows, but the
        order of the members is changed, and labled accordingly.
        
        Parameters
        ----------
        reference: list/array of float, or int
            the reference to compare the time series of the ensemble with. 
            This will be highlighted in the spaghetti view of the ensemble with red.
            If it is a member of the ensemble or a cut-out of such a time series,
            this can be indicated with referenceInEnsemble, the whole member will be displayed
            and in the single view for the reference below, the time of the cutout will be
            highlighted in red as well. Therefor a referenceTimeWindow should be given
        dsWindows: xarray.Dataset
            The windows with their distances, like a result of DistanceMessurer.get_windows_of_interest.
            In the core view (matrix or horizon) the windows will be shown as a black overlay or a 
            black line, respectively. The transparency depends on the distance to the reference.
            The Overlay is than it the same time, where the window was detected.
            The weightedDistance is choosen as distance, when possible.
        newOrder: list of tupel
            Each pair contains first the member name, second is the distance to the reference
            the position in the list indicates the new order, the member containing
            the reference is in index 0, the next closest is in index 1, the last index
            contains the the number of the member with the least similarity. That
            is still true, when the reference is not a part of a member. 
        view: int (0,1,2)
            to differentiate the views of each time series in the ensemble.
            0 is the horizon plot, 1 is the matrix view. default is one.
        title: string
            the title of the resulting figure. Will be generated when not given.
        referenceTimeWindow: 2-tuple
            the indices of the start and end of the reference in time. 
            This is used to plot the reference at the right time or highlight a 
            certain window (reference window) in a reference member. Unless the 
            reference is a member of the ensemble, the length of the reference 
            should be the same, as the time covered by this tupel.
            If not given the reference will be shown at the begining of the time axis.
        referenceInEnsemble: boolean
            change the interpretation of the reference parameter. If True, then the 
            reference will be seen as a single integer, the member which is used
            as a reference member. When this parameter is False, the reference
            needs to be list of values or array.
        
        Returns
        -------
        list of subfigures.
        """

        # fig = plt.figure(figsize = (self.__coreSize[0],self.__coreSize[1]+2*self.__height), constrained_layout = True)
        fig = f.generate_figure(self.__coreSize[0], self.__coreSize[1], self.__height, 3)
        if title is None:  # set a default title, since none is given
            title = "Ensemble: " + self.__VarToAnalyse + "\n sorted according to the distance to the reference"

        fig.suptitle(title)

        # =============================================================================
        #         views = fig.subfigures(3, 1,True,hspace = 0, height_ratios = [
        #             self.__height/(self.__coreSize[1] +2 * self.__height),
        #             self.__coreSize[1]/(self.__coreSize[1] + 2 * self.__height),
        #             self.__height/(self.__coreSize[1] + 2 * self.__height) ])
        # =============================================================================
        views = f.generate_subfigures(fig, 3, self.__coreSize[1], self.__height)
        # ----------------------------------------------------------------------
        referenceLabel = None
        if referenceInEnsemble:
            # this way the reference is always a ts. I don't have to check again.
            referenceLabel = reference
            reference = gf.get_time_series(self.__dsTimeSeries, self.__VarToAnalyse, self.__nameIndexMap[reference])

        self.__show_reference(views[0], views[2], reference, referenceLabel, referenceTimeWindow)

        if referenceTimeWindow is not None:  # need at least the length of the windows, since the windows only contain the starting point
            wSize = referenceTimeWindow[1] - referenceTimeWindow[0]
        else:
            # when no length is dictated by an optimal window, the length of the reference is extracted
            # this means, when the windows have another length than the reference, this must be indicated with an interval
            wSize = len(reference)

        # get a mapping for a name to a new position according to newOrder.
        # reverse the ordering, those with position 0 should have the last possible number
        indizes = dict(newOrder)
        for k in indizes.keys():
            indizes[k] = len(newOrder) - 1 - indizes[k]

        mask = self.__generate_shadow_mask(dsWindows, wSize, indizes)
        if view == 0:  # horizon
            self.__paint_ordered_windows_horizon(views[1], mask, newOrder,
                                                 referenceTimeWindow, shadow=shadow)

        else:
            self.__paint_ordered_windows_matrix(views[1], mask, newOrder, referenceTimeWindow,
                                                colored=not (view == 2), shadow=shadow)

        return views

    def visualize_with_order(self, order, view=1, title=None, ranges=None):
        """ Creates a matplotlib figure with two views on the ensemble.

        The first view shows each time series against the time, all in one axis and with
        the same color. Below that is a larger view that depics every member in more
        detail either with a heatmap, or a horizon graph. The heatmap is the default.

        Parameters
        ----------
        order: list of tupel
            Each pair contains first the member name, second is the new ordered position

        view : int (0,1,2)
            to differenciate the views of each time series in the ensemble.
            0 is the horizon plot, 1 is the heatmap. default is one.
        title: string
            the title of the resulting figure. Will be generated when not given.

        Returns
        -------
        list of matplotlib supfigures.
        """

        fig = f.generate_figure(self.__coreSize[0], self.__coreSize[1], self.__height, 2)
        if title is None:  # set default title, that is somewhat meaningfull
            title = "Ensemble: " + self.__VarToAnalyse

        fig.suptitle(title)

        # =============================================================================
        #         views = fig.subfigures(2, 1,True,hspace = 0, height_ratios =
        #                                [self.__height/(self.__coreSize[1]+ self.__height) ,
        #                                 self.__coreSize[1]/(self.__coreSize[1]+ self.__height)])
        # =============================================================================
        views = f.generate_subfigures(fig, 2, self.__coreSize[1], self.__height)
        # ----------------------------------------------------------------------
        self.__paint_the_ensemble_spaghetti(views[0])

        if view == 0:  # Horizon-view
            self.__paint_the_ensemble_horizon_order(views[1], order, ranges)

        else:
            self.__paint_the_ensemble_matrix_order(views[1], order)

        return views

    # :................................................................................................................:
    # %% helper

    def __get_current_values(self): 
        """Uses the data of the object to get a ndArray of the data.
        
        Parameter
        ---------
        horizon: boolean
            The data needs to be reorganized for the horizon view to use them properly
        
        Returns
        -------
        numpy ndArray:
            the data with the ensemble members in the first dimension and the time
            in the second dimension.
        """
        data = gf.get_data_array(self.__dsTimeSeries, self.__VarToAnalyse)
        # data = np.flip(data, 0)

        return data
    

    def __get_ticks(self, tickDistance):
        """Uses the tick distance and get the labels and idizes of the time ticks, 
        since they are in time
        
        Parameters
        ----------
        tickDistance: int
            The number of time steps between the shown labels, to make each lable
            readable.
        
        Returns
        -------
        list of Indizes
        list of Times, in datetime64[D]
        """
        
# =============================================================================
#         ticks = np.arange(0,len(self.__dsTimeSeries.time),tickDistance)
#         
#         labels = self.__dsTimeSeries.time[ticks].values
#         labels =  np.array([d for d in labels]).astype('datetime64[D]')
#         return ticks, labels
# =============================================================================
        #return gf.get_x_label_stuff(self.__dsTimeSeries.time.values, tickDistance)
        return gf.get_xticks(self.__dsTimeSeries, tickDistance) , gf.get_xlabels(self.__dsTimeSeries, tickDistance)
        
    
    def __get_y_bound(self):
        """get the maximal absolute number for each time series to set the limits
        in the y-axis. This bounds will be used, when no bounds are given.
        Ignores Nans.
        
        Returns
        -------
        tuple: 
            first value is minus the absolute maxima, the second value is the 
            absolute maxima. They are equidistant from 0 in both minus and plus.
        """
        
        return gf.get_y_bound(self.__get_current_values())

    
    def __generate_shadow_mask(self, dsWindows, wSize, indizes):
         """Generates a mask with zvalues between 0 and 1 at places where "interesting Windows" 
         would be.
         
         Has the same size, as the data. Values, other than zero, depend on the relativeDistance
         of the window to the reference at that point. The last window to be looked at, has priority.
         
         Parameters
         ----------
         dsWindows: xarray.Dataset
             The Windows of interest, containing the information about the distance and
             position of the windows.
         wSize: int
             The length of one window, since dsWindows only has a staring point for each window
         indizes: dictionary
             With the labels of the members as a key, get the index of them in the 
             original data. This ensures the mask has the same ordering as wished.
             
         Returns
         -------
         ndArray: the mask, values of 0 or between 0 and 1.
         """
         
         s = np.shape(self.__get_current_values())
         return gf.generate_mask(s, dsWindows, wSize, indizes)
    
    
    #only used by the spaghettie methods
    def __set_spaghetti_axis(self, ax):
        """sets the predefined specs of each spaghetti view. That are: black dotted 
        grid, right xticks, adjusts the ylim and xlim. 
        Parameter
        ---------
        ax: matplotlib axis object
            the axis, where the spaghetti plot is. 
        """
        #f.make_background_grid(ax)

        ylims = self.__scale
        xlims = (0, len(self.__dsTimeSeries.time) - 1)
        #f.regulate_lims(ax, ylims, xlims)
        
        #xticks, xlabels = self.__get_ticks(self.__tickDistance)
        xticks = self.__xTicks
        xlabels = self.__tickLabel
        #f.regulate_xTicks(ax,labels, ticks)
        
        f.regulate_spaghetti_axes(ax, ylims, xlims, xticks, xlabels)

    
    #TODO: name is not unambiguous
    #TODO: TimeWindow, mark it green lines?
    def __show_reference(self, axsAll, axsRef, reference, referenceLabel = None, referenceTimeWindow = None):
        """Changes the visualization of the reference in both the Spaghettie ensemble view
        as well as in the single view for the reference, depending on the Informaion about the Reference.
        
        Prameters
        ---------
        axs0: matplotlib axis Object, or subfigure
            The axis, where the ensemble should be shown, with the reference highlighted in red.
        axis1: matplotlib axis Object or subfigure
            The axis, where the reference is shown in more detail.
        reference: Name of the reference Member, or array-like
            Either the reference directly, when it is an array like, or where to find
            the reference to be displayed.
        referenceInEnsemble: bool
            If True, the reference will be interpreted as the name of the member in the ensemble
            else the reference is expected to be array-like
        referenceTimeWindow: tuple of int
            The time steps, where the reference is found. When the reference is part
            of the ensemble, this says, where in time of the already given member the 
            reference is. If the reference is a array like, this will only effect, 
            if the reference is shown with a little offset in time, according to 
            this value. 
        
        """

        assert isinstance(reference, np.ndarray) or isinstance(reference, list)
        #just to save space, since this has to be done in nearly all vis methods. This Deals with all Information given by these Variables

        if referenceLabel is not None:
            axsRef.supylabel("member: "+str(referenceLabel)+ "\n reference")

            #reference = gf.get_time_series(self.__dsTimeSeries, self.__VarToAnalyse, self.__nameIndexMap[reference])

            #the whole ts is red in the view for all ts, not just the window
            self.__paint_the_ensemble_spaghetti(axsAll, reference)
        else: #reference is not a member
            axsRef.supylabel("reference")        

            self.__paint_the_ensemble_spaghetti(axsAll, reference = reference, referenceTimeWindow = referenceTimeWindow)
        
        if not isinstance(axsRef, plt.Axes) :
            axsRef = axsRef.subplots()
    
        self.__paint_reference(axsRef, reference, referenceTimeWindow)


#:.............................................................................:
    #%% painter

     #TODO: reference can be part of the ensemble, than we want the member displayed, it can be just a piece, than it should be displayed at the right time, 
     #   if there is no time, than we can do it like this. I had in mind: just give it the ts of the member, and than it is fine
     #   this should be easyer to understand.
    def __paint_the_ensemble_spaghetti(self, subfig, reference = None, membersOfInterest = None, referenceTimeWindow = None): 
        """Creates a simple spaghetti plot of all members in the time series in the given axis. 
        
        When a reference is given, it is plotted slightly bigger in red.
        (When members of interests are given, they are plotted in blue instead 
        of black like the rest.)
        
        Parameter
        ---------
        ax: matplotlib axis object or subfigure
            The axis, where the reference should be displayed.
        reference: array-like or none
            none, when there is no reference to be shown. When it is an array-like, it is visualized in the 
            plot with a red line, which is a bit larger than the rest. Does not have to be the same lenght
            than the ensemble members.
        membersOf Interest: list of Integers
            The members, which are to be displayed differently than the rest. They will be shown in blue
            and a bit larger than the rest of the ensemble. Can be none, when no member should be 
            highlighted.
        referenceTimeWindow: tupel of integer
            The first value is the start point in time of the reference. So the reference will start after
            referenceTimeWindow[0] time steps and end after referenceTimeWindow[1] time steps.
            The end is exlusive
            It can be used to shift the position of the reference to fit the knowledge over the
            time, where the reference should fit. 
        
        """

        #if not isinstance(ax, plt.Axes) :
        #    ax = subfig.subplots()
       
        data = self.__get_current_values()
        #----------------------------------------------------------------------
        #f.paint_spaghetti(subfig, data, self.__VarToAnalyse) 
                        #self.__nameIndexMap[membersOfInterest] if membersOfInterest is not None else None)
        f.paint_ensemble_spaghetti(subfig, data, vmin = self.__scale[0], 
                                   vmax = self.__scale[1], xticks = self.__xTicks,
                                   xlabels = self.__tickLabel, 
                                   ylabels = gf.get_ylabels(self.__dsTimeSeries),
                                   varName = self.__VarToAnalyse)
            
        if reference is not None:
            f.paint_reference(subfig, reference, window = referenceTimeWindow)
        
        # set the limit for the y-axis, so it is universal and easy to read
        #ax = subfig.get_axes()[0]
        #self.__set_spaghetti_axis(ax)
    
    def __paint_reference(self,ax, reference, window = None):
        """Creates a view of the reference in a single axis.
        
        Parameters
        ----------
        ax: matplotlib axis object or subfigure
            The axis, where to plot
        reference: array-like
            The reference to plot, when it is as long as the time of one ensemble member,
            then it is seen as the member of the reference and window is used to 
            highlight the reference-window position in red.
        window: tuple of ints, optional
            The first value is the index of the start-point, while the second value
            is the index of the end point in time of the reference window.
            When the reference is not a member, and window is none, the reference
            will be plottend in the beginning
        """
# =============================================================================
#         if not isinstance(ax, plt.Axes) :
#             ax = ax.subplots()
# =============================================================================
            
        #use that instead of time, so we can align the axis to core more easily.
        #time = range(self.__dsTimeSeries.sizes["time"]) 
        #----------------------------------------------------------------------
        f.paint_reference(ax, reference, window)
        f.regulate_spaghetti_axes(ax, self.__scale, (0,self.__dsTimeSeries.time.shape[0]), 
                                  xticks = self.__xTicks, xlabels = self.__tickLabel)
        self.__set_spaghetti_axis(ax)

        #%% Heatmap

    def __paint_the_ensemble_matrix(self, subfig):
        """Creates a view of the whole ensemble, using a heatmap, with pcolor
        from matplotlib on the given axis or subfigure. 
        
        Parameters
        ----------
        ax: matplotlib axis object or subfigure
            the axis, where the matrix view should be displayed. When it is a subfigure,
            the axis will be derived.
        """
        # if not isinstance(ax, plt.Axes) :
        #    ax = ax.subplots()

        data = self.__get_current_values()
        # we want the number of member to be on the y-axis, if it is ordered differently,
        # transpose the data, so it is ordered correctly

        # use xTicks, those are the positions in the time array, that we want to use
        f.paint_heatmap_ensemble(subfig, data, cmap=self.__color, vmin=self.__scale[0],
                                 vmax=self.__scale[1], xticks=self.__xTicks,
                                 xlabels=self.__tickLabel,
                                 ylabels=gf.get_ylabels(self.__dsTimeSeries))

    def __paint_the_ensemble_matrix_order(self, subfig, order):
        """Creates a view of the whole ensemble, using a heatmap, with pcolor
        from matplotlib on the given axis or subfigure.

        Parameters
        ----------
        ax: matplotlib axis object or subfigure
            the axis, where the matrix view should be displayed. When it is a subfigure,
            the axis will be derived.
        """
        # if not isinstance(ax, plt.Axes) :
        #    ax = ax.subplots()

        new_order = [o[0] for o in order]
        data = self.__get_current_values()
        data = data[new_order, :]
        # we want the number of member to be on the y-axis, if it is ordered differently,
        # transpose the data, so it is ordered correctly

        # use xTicks, those are the positions in the time array, that we want to use
        f.paint_heatmap_ensemble(subfig, data, cmap=self.__color, vmin=self.__scale[0],
                                 vmax=self.__scale[1], xticks=self.__xTicks,
                                 xlabels=self.__tickLabel,
                                 ylabels=new_order)

    def __paint_important_windows_matrix (self, subfig, mask, colored = True):
        """Paints the ensemble in matrix view to the ax, with the mask as overlay.
        
        Parameters
        ----------
        ax: matplotlib axis object or subfigure
            The axis, where the ensemble should be shown. If it is a subfigure,
            the axis will be derived.
        mask: ndArray
            A mask of zero, where nothing extra is shown and values between 0 and 1
            for transparency, where something should be seen. The gray-scale is used
            to show them. 
        colored: boolean
            If True, the ensemble will be shown in color, underneath the mask, when 
            False, only the mask is shown.
        """            
        
        if colored:
            self.__paint_the_ensemble_matrix(subfig)
            f.generate_heatmap_shadow(subfig, mask)
        else:
           #we want the number of member to be on the y axis, if it is ordered differently, transpose the data, so it is ordered correctly
           f.paint_heatmap_ensemble(subfig, mask, cmap = "Greys", vmin = 0, vmax = 1, 
                                    xticks = self.__xTicks, xlabels = self.__tickLabel,
                                    ylabels = gf.get_ylabels(self.__dsTimeSeries))

            
    def __paint_ordered_windows_matrix(self, subfig, mask, newOrder, referenceTimeWindow = None, colored = True, shadow = True):
        """Paints the ensemble in matrix view on axis, overlaying with mask, while 
        reordering the member according to newOrder.
        
        Parameters
        ----------
        ax: matplotlib axis object or subfigure
            The axis, where the ensemble should be shown.
        mask: ndArray
            A mask of zero, where nothing extra is shown and values between 0 and 1
            for transparency, where something should be seen. 
        newOrder: list of tupel
            Each pair contains first the member name, second is the distance to the reference
            the position in the list indicates the new order, the member containing
            the reference is in index 0, the next closest is in index 1, the last index
            contains the the number of the member with the least similarity. That
            is still true, when the reference is not a part of a member. 
        referenceTimeWindow: tupel of int
            The location of the reference window in time, to highlight where
            a good winow should be. That will happen with a thin green line over
            every member. It is also used to show the windows, by giving the 
            length of the reference window. The first value is the index, of the 
            start of the reference window, the second value is the index of the 
            end of the reference.
        colored: boolean
            If True, the ensemble will be shown in color, underneath the mask, when 
            False, only the mask is shown.
        shadow: boolean
            Weather to display the windows as a dark shadow over the time series (mask),
            or not. When the shadow is false, shadow view has nothing to show.
        """        
            
        label = [i[0] for i in newOrder]
        label.reverse()

        if colored:
            dataReordered = gf.get_ordered_data(self.__dsTimeSeries, self.__VarToAnalyse, 
                                       newOrder, self.__nameIndexMap)

            f.paint_heatmap_ensemble(subfig, np.flip(dataReordered), cmap = self.__color, vmin = self.__scale[0],
                               vmax = self.__scale[1], xticks = self.__xTicks,
                               xlabels = self.__tickLabel, ylabels = label)
            if shadow:
                f.generate_heatmap_shadow(subfig, mask)

        if shadow and not colored:             
            f.paint_heatmap_ensemble(subfig, mask, cmap = "Greys", vmin = 0, 
                                   vmax = 1, xticks = self.__xTicks, 
                                   xlabels = self.__tickLabel, ylabels = label)
            
        if referenceTimeWindow is not None:
            for ax in subfig.get_axes():
                f.indicate_window(ax, referenceTimeWindow[0], referenceTimeWindow[1], 'g')

        
        #%% Horizons
        
    def __paint_the_ensemble_horizon(self, subfig, ranges=None):
        """Creates a view of the whole ensemble, using the horizon plot on the given figure.
        
        It also labels the view and the members.
        
        Parameter
        ---------
        subfigure: matplotlib figure
            the figure on which to didplay the horizon plot.
        """
        data = self.__get_current_values()
        #want the data to have the first at the bottom
        
        f.paint_horizon(subfig, data, cmap=self.__color, vmin=self.__scale[0],
                        vmax=self.__scale[1], xticks=self.__xTicks,
                        xlabels=self.__tickLabel,
                        ylabels=np.flip(gf.get_ylabels(self.__dsTimeSeries)),
                        horizon_base=self.__horizonBase, ranges=ranges)

    def __paint_the_ensemble_horizon_order(self, subfig, order, ranges=None):
        """Creates a view of the whole ensemble, using the horizon plot on the given figure.

        It also labels the view and the members.

        Parameter
        ---------
        subfigure: matplotlib figure
            the figure on which to didplay the horizon plot.
        """
        new_order = [o[0] for o in order]
        data = self.__get_current_values()
        data = data[new_order, :]

        # want the data to have the first at the bottom

        f.paint_horizon(subfig, data, cmap=self.__color, vmin=self.__scale[0],
                        vmax=self.__scale[1], xticks=self.__xTicks,
                        xlabels=self.__tickLabel,
                        ylabels=np.flip(new_order),
                        horizon_base=self.__horizonBase, ranges=ranges)

    def __paint_important_windows_horizon (self, subfig, mask, colored = True):  
        """Paints the ensemble in matrix view to the ax, with the mask as overlay.
        
        Parameters
        ----------
        ax: matplotlib axis object or subfigure
            The axis, where the ensemble should be shown. If it is a subfigure,
            the axis will be derived.
        mask: ndArray
            A mask of zero, where nothing extra is shown and values between 0 and 1
            for transparency, where something should be seen. The gray-scale is used
            to show them. 
        colored: boolean
            If True, the ensemble will be shown in color, underneath the mask, when 
            False, only the mask is shown.
        """            
        
        if colored:
            self.__paint_the_ensemble_horizon(subfig)
            f.generate_horizon_shadow(subfig, mask)  
        else: 
            f.paint_horizon(subfig, mask, cmap = "Greys", vmin = 0, vmax = 1, 
                            xticks = self.__xTicks, xlabels = self.__tickLabel,
                            ylabels = np.flip(gf.get_ylabels(self.__dsTimeSeries)),
                            horizon_base=self.__horizonBase)

    def __paint_ordered_windows_horizon(self, subfig, mask, newOrder, referenceTimeWindow = None, colored = True, shadow = True):
         """Paints the ensemble in horizon view on axis, overlaying with mask, while 
         reordering the member according to newOrder.
         
         Parameters
         ----------
         ax: matplotlib axis object or subfigure
             The axis, where the ensemble should be shown.
         mask: ndArray
             A mask of zero, where nothing extra is shown and values between 0 and 1
             for transparency, where something should be seen. 
         newOrder: list of tupel
             Each pair contains first the member name, second is the distance to the reference
             the position in the list indicates the new order, the member containing
             the reference is in index 0, the next closest is in index 1, the last index
             contains the the number of the member with the least similarity. That
             is still true, when the reference is not a part of a member. 
         referenceTimeWindow: tuple of int
             The location of the reference window in time, to highlight where
             a good window should be. That will happen with a thin green line over
             every member. It is also used to show the windows, by giving the 
             length of the reference window. The first value is the index, of the 
             start of the reference window, the second value is the index of the 
             end of the reference.
         colored: boolean
             If True, the ensemble will be shown in color, underneath the mask, when 
             False, only the mask is shown.
         shadow: boolean
             Weather to display the windows as a dark shadow over the time series (mask),
             or not. When the shadow is false, shadow view has nothing to show.
         """        
             
         label = [i[0] for i in newOrder]

         if colored:
             dataReordered = gf.get_ordered_data(self.__dsTimeSeries, self.__VarToAnalyse, 
                                        newOrder, self.__nameIndexMap)
            
             f.paint_horizon(subfig, np.flip(dataReordered), cmap = self.__color, vmin = self.__scale[0],
                             vmax = self.__scale[1], xticks = self.__xTicks,
                             xlabels = self.__tickLabel,
                             ylabels = label,
                             horizon_base=self.__horizonBase)

             if shadow:
                 f.generate_horizon_shadow(subfig, mask)
                 
         if shadow and not colored:
             f.paint_horizon(subfig, mask, "Greys", 0,1 , self.__xTicks,
                             self.__tickLabel, label,
                             horizon_base=self.__horizonBase)
             
         if referenceTimeWindow is not None:
             for ax in subfig.get_axes():
                 f.indicate_window(ax, referenceTimeWindow[0], referenceTimeWindow[1], 'g')

#------------------------------------------------------------------------------