#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 22 10:53:59 2022

@author: Alex Hoffmann
"""
class Plotter:
    """Class to hold and execute getter and plotter functions, using a give set 
    of parameters and values. 
    
    Attributes
    ----------
    entries : dictionary
        List of parameter and their values to use in the painter function
    dataset : container
        The container for the data. Access it throu a list of getter functions
        to allow for different possible containers.
    getters : list of functions
        different functions, that return a dictionary with a key and a value.
        Each getter function gets only the data container (dataset) as parameter.
        After execution, the key serves as a parameter in the painter function.
    painter : function
        The function actually resposible for plotting, it uses the entries attribute
        to fill in the parameter of the function.
    """
    def __init__(self,dataset=None,getters=None,painter=None):
        self.entries = {}#add preprocess accumulator?
        self.dataset = dataset
        self.getters = [] if getters is None else getters
        self.painter = painter
        
        
    def set_getterMethods(self,funcs):
        """Set a new list of getter methods for the object.
        Parameters
        ----------
        funcs : list of functions
            The new list of getter methods. They take only the data container
            as parameter and return a key, value pair. 
        """
        self.getters = funcs
        
        
    def add_getterMethod(self,func):
        """Add a new getter method to the list of getter methdos for this object.
        
        Parameters
        ----------
        func : function
            The additional getter method. It takes only the data container as input
            and returns a pair/ list of pairs of keys and values.
        """
        self.getters.append(func)
        
        
    def set_painterMethod(self,painter):
        """Set a new function as the painter function.
        
        Parameters
        ----------
        painter : function
            The function actually resposible for plotting the data. It gets 
            the dictionary of entries as input. 
        """
        self.painter = painter
        
        
    def set_Dataset(self,ds):
        """Set the data container.
        
        Parameters
        ----------
        ds : data container
            The new container holding the data. The access to the data is made
            with getter functions unziping the data or the plotter function 
            interpreting it.
        """
        self.dataset = ds
        
        
    def isReady(self):
        """Checks weather all nesseccary attributes of the objects are set, so
        the plotter can be executed.
        
        Returns
        -------
        boolean:
            True, when all nesseccary attributes are not None. False, otherwise.
        """
        return self.dataset is not None and bool(self.getters) and self.painter is not None
   
    
    def processData(self):
        """ Executes the getters and fills entries with the returned key, value
        pairs.
        """
        self.entries = {}
        for getter in self.getters:
            self.entries.update(getter(self.dataset))#make getters able to access some kind of preprocessed data?


    def paintPlot(self):
        """Executes the plotter function with the help of entries.
        """
        self.painter(**self.entries)
    
    
    def paintData(self):
        """First processes the data, using the getter functions and then executes 
        the painter function to visualize the data.
        """
        self.processData()
        self.paintPlot()