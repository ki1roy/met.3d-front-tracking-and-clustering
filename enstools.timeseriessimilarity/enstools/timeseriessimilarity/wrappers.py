#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  3 16:27:56 2021

@author: kakaufmann
"""
import numpy as np
import dtw
import tslearn.metrics as tsm
# TODO: should this be named wrapper instead?
# TODO: check ob es refactoring braucht, ist recht lang, sieht kompliziert aus,
#       braucht sehr lange zum berechnen, mach es schneller
# TODO: debug somethings wrong


def lcss(a, b, t):
    """Calculates the longest common subsequence on real sequences between the
    time series a and b with the threshold t for equality between points.
    
    Two points in both sequences are considered equal, when their absolute difference
    is smaller than or equal to the threshold. The path, which is the mapping
    from one time series to another, that creates this longest common subsequence 
    is also returned as a second value.
    
    Parameters
    ----------
    a: array like
        one of the time series to be compared
    b: array like
        one of the time series to be compared
    t: int/float
        Threshold, at which two points of the series are considered equal
    
    Returns
    -------
    float
        The distance in a range from 0 to 1 and the alignment. 
    2D array-like
        The alignment is a series of tuple representing the index number in a and b.
        example: 
            dist,lcsPath = lcss(a,b,t)
            len(lcsPath) - len(reference) # the inserts and deletes in the reference
            len(lcsPath) - len(query) # the number of inserts and deletes in the query sequence
    
    """
    c = np.ndarray(
        (len(a) + 1, len(b) + 1))  # the alignment matrix, where the counts of possible subsequences are saved
    for i in range(len(a) + 1):
        c[i, 0] = 0
    for i in range(len(b) + 1):
        c[0, i] = 0
    for i in range(1, len(a) + 1):
        for j in range(1, len(b) + 1):
            m = a[i - 1] - b[j - 1]
            if abs(m) <= t:
                c[i, j] = c[i - 1, j - 1] + 1
            else:
                c[i, j] = max(c[i, j - 1], c[i - 1, j])
    # calc the path---------------------------------------------------------------------------#
    print(c)
    i = len(a)
    j = len(b)
    lcsPath = []  # don't know the length yet.
    while i > 0 and j > 0:
        top = c[i - 1, j]
        left = c[i, j - 1]
        diag = c[i - 1, j - 1]
        lcsPath.append((i - 1, j - 1))
        if (diag == left == top) or (max([top, left, diag]) == diag):  # no insert, no delete
            i = i - 1
            j = j - 1
        elif left >= top:  # prefer insert in b instead of in a
            j = j - 1
        else:
            i = i - 1
    while i > 0:
        lcsPath.append((i, 0))
        i -= 1
    while j > 0:
        lcsPath.append((0, j))
        j -= 1

    lcsPath = np.array(lcsPath)

    return c[len(a), len(b)], lcsPath


def dtw_wrapper(reference, query, **kwargs):
    """Uses the dtw-package to calculate a distance between the reference and the 
    query, changes the return, so that just the distance and an integer, standing
    for the change in the time direction is returned.
    
    The used dtw libary is here: https://dynamictimewarping.github.io/py-api/html/
    
    Parameter
    ---------
    reference: Array-like
        One of the time series to be compared. In the dtw call, it will be the second
        variable
    query: array-like
        One of the time series to be compared. It will be the first argument in the
        dtw call
    
    Returns
    -------
    (float, integer)
        the distance is the first value of the returned tuple, the second is the
        number of changes done to the warping path compared to a point to point match
    """
    query = removeNans(query)
    if query.size == 0:
        return np.NaN, np.NaN

    temp = dtw.dtw(query, reference, **kwargs)
    return temp.distance, (2 * len(temp.index1) - len(query) - len(reference))


def dtw_tslearn_wrapper(reference, query, **kwargs):
    """Uses the soft_dtw from the tslearn package to calculate a distance between the reference and the
    query, changes the return, so that just the distance and an integer, standing
    for the change in the time direction is returned.

    The used tslearn libary is here: https://tslearn.readthedocs.io/en/stable/user_guide/dtw.html

    Parameter
    ---------
    reference: Array-like
        One of the time series to be compared. In the dtw call, it will be the second
        variable
    query: array-like
        One of the time series to be compared. It will be the first argument in the
        dtw call

    Returns
    -------
    (float, integer)
        the distance is the first value of the returned tuple, the second is the
        number of changes done to the warping path compared to a point to point match
    """
    query = removeNans(query)
    if query.size == 0:
        return np.NaN, np.NaN

    optimal_path, distance = tsm.dtw_path(query, reference, **kwargs)
    # temp = dtw.dtw(query, reference, **kwargs)
    return distance, (2 * len(optimal_path) - len(query) - len(reference))


def soft_dtw_tslearn_wrapper(reference, query, **kwargs):
    """Uses the dtw from the tslearn package to calculate a distance between the reference and the
    query, changes the return, so that just the distance and an integer, standing
    for the change in the time direction is returned.

    The used tslearn libary is here: https://tslearn.readthedocs.io/en/stable/user_guide/dtw.html

    Parameter
    ---------
    reference: Array-like
        One of the time series to be compared. In the dtw call, it will be the second
        variable
    query: array-like
        One of the time series to be compared. It will be the first argument in the
        dtw call

    Returns
    -------
    (float, integer)
        the distance is the first value of the returned tuple, the second is the
        number of changes done to the warping path compared to a point to point match
    """
    query = removeNans(query)
    if query.size == 0:
        return np.NaN, np.NaN

    distance = tsm.soft_dtw(query, reference, **kwargs)
    # temp = dtw.dtw(query, reference, **kwargs)
    return distance, 0


def lcss_wrapper(reference, query, **kwargs):
    """Uses the longest common subsequence on real time series to calculate a distance
    between the reference and the query, changes the return, so that just the distance 
    and an integer, standing for the change in the time direction is returned.
    
        
    Parameter
    ---------
    reference: Array-like
        One of the time series to be compared. In the lcss call, it will be the second
        variable
    query: array-like
        One of the time series to be compared. It will be the first argument in the
        lcss call
    
    Returns
    -------
    (float, integer)
        the distance is the first value of the returnd tupel, the second is the 
        number of changes done to the warping path compared to an point to point match
    """
    # TODO: maybe remove the second value, still thinking about it
    temp = lcss(query, reference, **kwargs)
    return 1 - temp[0] / min(len(reference), len(query)), (2 * len(temp[1]) - len(query) - len(reference))


def euclidean_wrapper(reference, query):
    """Euclidian distance Messure for time series
    
    compares time series by calculating the square sum, divided by the number of
    points. Also provides a second value, 0, for the time stretch is not allowed.
    
    Parameter
    ---------
    reference: array
        one of the time series to be compared.
    query: array
        the second time series to be compared, needs to have the same length as the
        reference
    
    Retrun
    ------
    float
        Square sum of the point distances of the both input time series
    """
    assert (reference.shape == query.shape), "Reference must have the same length.(%s,%s)" % (
        reference.shape, query.shape)

    return np.linalg.norm(reference - query), 0


def eucl_3D_wrapper(reference, query):
    """ Euclidean distance to calculate the difference between two 3D time series.
    
    Parameter
    ---------
    reference: 3D array-like
        one of the time series to be compared.
    query: 3D array-like
        the second time series to be compared, needs to have the same length as the
        reference
    Retrun
    ------
    float
        Square sum of the point distances of the both input time series
    """
    dist = 0
    for i in range(len(reference)):
        n = (reference[i, 0] - query[i, 0]) ** 2 + (reference[i, 1] - query[i, 1]) ** 2 + (
                    reference[i, 2] - query[i, 2]) ** 2
        dist += n

    return dist / len(reference), 0


# TODO: the mean as a return value is not enough, var is missing
def standardize(timeSeries):
    """Standardize the time series by substracting the mean and deviding with 
    the standard deviation.
        
    Parameter
    ---------
    timeSeries: array-like
        The time series to be normalized
    
    Return
    ------
    array-like
        the time series normalized.
    float
        the mean of the original time series
    """
    m = timeSeries.mean()

    return (timeSeries - m) / np.std(timeSeries), m


def norm_with_mean(timeSeries):
    """ Normalises the time series by substracting its mean, keep the standard 
    deviation
        
    Parameter
    ---------
    timeSeries: array-like
        The time series to be normalized
    
    Return
    ------
    array-like
        the time series normalized.
    float
        the mean of the original time series
    """

    return timeSeries - timeSeries.mean(), timeSeries.mean()


# TODO: the max alone is not enough, need to return the min, too
def normalize(timeSeries):
    """ Normalize the time series to be between 0 and 1
    
    Parameter
    ---------
    timeSeries: array-like
        the time series to be normalized
    
    Return
    ------
    array-like
        the normalized time series
    float
        the former maximum
    """

    return (timeSeries - min(timeSeries)) / (min(timeSeries) - max(timeSeries))


def removeNans(timeSeries):
    """ Given a time series, this method just removes every nan and returns a 
    time series, that is shorter.
    
    Parameter
    ---------
    timeSeries: array like
        the time series that should be manioulated
    
    Returns
    -------
    array like
        the shorter version of the input time series, without nans
    """

    return timeSeries[~np.isnan(timeSeries)]
