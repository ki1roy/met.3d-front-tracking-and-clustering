# -*- coding: utf-8 -*-
"""
Last Modified on November 2022
@author: Ingra Barbosa | Git: @ingrabarbosa
"""

import numpy as np
import warnings

#%% 
def vapour_pressure(P, QV):
    """ Calculates the partial pressure of water vapour. 
    
    Args:
        P (array): pressure in Pa.
        QV (array): Specific humidity in kg/kg.
    
    Returns:
        vp (array): vapor pressure in Pa. 
    """
    #e = mixing ratio of the molar masses of water and dry air (Rdry/Rvap)
    e = 0.621981                                               
    vp = P*QV/(e*(1 + QV*(1/e-1)))
    
    return vp

def saturation_vapour_pressure(QV, phase='water', T=None):
    """ Calculates the saturation of the partial pressure of water vapour.
    
    Args:
        QV (array): Specific humidity in kg/kg.
        T (array or None): temperature in Kelvin. If None T = 283 K.
        phase (str): 'water', 'ice' or 'mixed', water and ice phase components 
        calculated with the Tetens formula. Defaults to 'water'.
        
    Returns:
        esat (array): saturation vapour pressure in Pa units.
        
    Credit: 
        Formulas were adapted from the IFS DOCUMENTATION (Cy45r1), June 2018
    """
    
    warnings.filterwarnings('ignore')        # divisions
    
    Tice = 250.16       # T in Kelvin, 
    T0 = 273.16         # triple point of water in Kelvin, 
    a1 = 611.21         # a1 in Pa

    if T is None:
        T = np.full(QV.shape, 283)
    
    esatfunc = lambda a3, a4: a1*np.exp(a3*(T - T0) / (T - a4))
    
    if phase == 'mixed':
        is_ice = T <= Tice
        is_water = T >= T0
        #mixed = np.logical_or(~alpha1, ~alpha3)
        
        a = np.where(is_ice, 0, np.where(is_water, 1, ((T-Tice)/(T0-Tice))**2))

        assert(np.all(a>=0) or np.all(a=np.nan))
        assert(np.all(a<=1) or np.all(a=np.nan))

        esatw = esatfunc(17.502, 32.19)
        esati = esatfunc(22.587, - 0.7)

        esat = a*esatw + (1-a)*esati
        
    elif phase == 'water':
        esat = esatfunc(17.502, 32.19)
        
    elif phase == 'ice':
        esat = esatfunc(22.587, - 0.7)
   
    return esat

#%%
def relative_humidity(P, QV, phase ='water', T=None):
    """ Calculates relative humidity.
    
    Args:
        P (array): pressure in Pa.
        QV (array): Specific humidity in kg/kg.
        T (array or None): temperature in Kelvin. If None T = 283 K.
        phase (str): 'water', 'ice' or 'mixed', water and ice phase components
        calculated with the Tetens formula. Defaults to 'water'.
        
    Returns:
        RH (array): relative humidity in percentage. Can present supersaturation with respect to ice.
    """
    
    RH = 100*vapour_pressure(P, QV)/saturation_vapour_pressure(QV, phase=phase, T=T)

    return RH

def saturation(P, QV, phase ='water', T=None):
    """ Calculates saturation.
    
    Args:
        P (array): pressure in Pa.
        QV (array): Specific humidity in kg/kg.
        T (array or None): temperature in Kelvin. If None T = 283 K.
        phase (str): 'water', 'ice' or 'mixed', water and ice phase components
        calculated with the Tetens formula. Defaults to 'water'.

    Returns:
        SAT (array): saturation in percentage.
    """
    
    RH = relative_humidity(P, QV, phase=phase, T=T)
    SAT = np.where(RH > 95, RH, np.nan)

    if np.isnan(np.nanmax(SAT)):
        print('No trajectories reach saturation point')  
        
    return SAT
    
def cumulative_specific_humidity(QV):
    """ Calculates cummulative specific humidity.
    
    Args:
        QV (array): Specific humidity in kg/kg.
       
    Returns:
        cum_QV (array): cummulative humidity in kg/kg.
    """
    
    cum_QV = np.cumsum(QV, axis=1)
            
    return cum_QV

def diff_time_specific_humidity(QV, timestep=1):
    """ Calculates cummulative specific humidity.
    
    Args:
        QV (array): Specific humidity in kg/kg.
        timestep (int): timestep. Defaults to 1. 
        
    Returns:
        QV_diff (array): difference from all specific humidity data to the 
        specific humidity at timestep t in kg/kg.
    """
    
    QV_diff = QV - QV[:,:timestep]

    return QV_diff

def threshold_relative_humidity(P, QV, phase='water', T=None, thr=None):
    """ Calculates all relative humidity data above a certain threshhold.
    
    Args:
        P (array): pressure in Pa.
        QV (array): Specific humidity in kg/kg.
        T (array or None): temperature in Kelvin. If None T = 283 K.
        phase (str): 'water', 'ice' or 'mixed', water and ice phase components
        calculated with the Tetens formula. Defaults to 'water'.
        thr (array or None): threshhold value for each ensamble in kg/kg. If None
        thr = half the mean of each trajectory.
        
    Returns:
        RH_threshold (array): relative humidity data above threshold.
    """
    
    RH = relative_humidity(P, QV, phase=phase, T=T)
    
    if thr == None:
        thr = np.mean(RH, axis=1)/2
        
    RH_threshold = np.where(RH.T > thr, RH.T, np.nan).T
                
    return RH_threshold

def survival_rate(QV):
    """ Calculates survival rate of pathogen according to the cummulative 
    specific humidity.
    
    Args:
        QV (array): Specific humidity in kg/kg.
        
    Returns:
        surv (array): survival rate.
    """
    
    cum_QV = cumulative_specific_humidity(QV)
    surv = np.exp(cum_QV*-0.005)

    return surv

def survival_material(P, QV, t, phase='water', T=None, timestep=1): 
    """ Calculates survival rate of the material according to the cummulative 
    specific humidity and timestep.
    
    Args:
        P (array): pressure in Pa.
        QV (array): Specific humidity in kg/kg.
        phase (str): 'water', 'ice' or 'mixed', water and ice phase components
        calculated with the Tetens formula. Defaults to 'water'.
        T (array or None): temperature in Kelvin. If None T = 283 K.
        timestep (int) = timestep. Defaults to 1. 
        
    Returns:
        surv_mt (array): survival rate of material.
    """
    
    RH = relative_humidity(P, QV, phase=phase, T=T)
    time = np.array(t, dtype='datetime64[m]')
    
    surv_mt_function = lambda it: np.exp(-0.064*timestep*it*RH[:,it])
    surv_mt = surv_mt_function(np.arange(len(time)))
    
    return surv_mt

def survival_uv(P, QV, phase='water', T=None, theta = 60, cte = 1):  
    """ Calculates survival rate of the material according to the relative 
    humidity and UV incidence.
    
    Args:
        P (array): pressure in Pa.
        QV (array): Specific humidity in kg/kg.
        phase (str): 'water', 'ice' or 'mixed', water phase components calculated 
        with the Tetens formula. Defaults to 'water'.
        T (array or None): temperature in Kelvin. If None T = 283 K.
        theta (array): solar zenith angle. Defaults to 60°.
        cte (float): constant for scaling from solar zenith angle to UV dose. Defaults to 1.
        
    Returns:
        surv_uv (array): survival rate of pathogen.
    """
     
    RH = relative_humidity(P, QV, phase=phase, T=T)
    cum_uv = cte*np.cos(theta)                           # sum(cte*cos(theta)) when theta varies

    m = np.mean(RH, axis=1)/2                            # threshold = mean/2 for now 
    surv_uv = np.where(RH.T > m, np.exp(-2*cum_uv*RH.T), np.exp(-1*cum_uv*RH.T)).T
        
    return surv_uv

def point_of_last_saturation(P, QV, phase ='water', T=None):
    """ Filter data array and return the values after the 
        point of last saturation (PSL).
    
    Args:
        P (array): pressure in Pa.
        QV (array): Specific humidity in kg/kg.
        phase (str): 'water', 'ice' or 'mixed', water phase components calculated 
        with the Tetens formula. Defaults to 'water'.
        T (array or None): temperature in Kelvin. If None T = 283 K.
        
    Returns:
        PLS (array): point of last saturation.        
    """

    def first_cond(bool_mask, axis=0):
        idx = np.argwhere(bool_mask)
        _, index = np.unique(idx[:, axis], return_index=True)
        return idx[index]

    RH = relative_humidity(P, QV, phase, T)
    SAT = saturation(P, QV, phase, T)

    indices = first_cond(~np.isnan(SAT), axis=0)
    PLS_values = np.array([SAT[i,j] for i, j in indices])

    idx_time = indices[:,1]

    def outer_comparison(a,b,cmp):
        ad,bd = len(a.shape),len(b.shape)
        a_cast=np.tile(a,(*b.shape,1)).reshape(*b.shape,*a.shape)
        b_cast=np.tile(b,(*a.shape,1)).reshape(*a.shape,*b.shape)
        a_cast = np.transpose(a_cast,(*bd+np.arange(ad),*np.arange(bd)))
        return cmp(a_cast,b_cast)

    time=np.arange(SAT[0].size)

    bool_mask = outer_comparison(idx_time, time, np.less)

    PLS = np.where(bool_mask, RH[indices[:,0]], np.nan)
    SAT[indices[:,0]] = PLS
    PLS = SAT

    return PLS