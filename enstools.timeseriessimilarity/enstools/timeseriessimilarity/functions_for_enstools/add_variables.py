# -*- coding: utf-8 -*-
"""
Last Modified on November 2022
@author: Ingra Barbosa | Git: @ingrabarbosa
"""

import numpy as np
from read_trajectory import Met3d2xarray, lagranto2xarray, nc2xarray
import humidity_functions as husf

#%%
def concatenate_vars(filename, loaddir, dim_tra, dim_time, phase = 'water', savedir=None, 
                    auxvariables=None, aux_files=None, save_file=False, for_enstools=True):

    """ Concatenate calculated humidity auxiliary data to the dataset and convert to netcdf if wanted.
        Att: QV is assumed to be kg/kg, P Pascal and T Kelvin, if this is not the case, convert the 
        variables to the forementioned units before using the function. 

    Args:
        filename (str): filename with trajectory data (txt/ascci from Met.3D, .4 from Lagranto, .nc).
        loaddir (str): directory path to file.
        dim_tra (string): names of dimention of trajectory.
        dim_time (string): names of dimention of time.
        savedir (str): directory path in which file will be saved. Defaults to None.
        auxvariables (list or None): list of desired auxiliary variables. Defaults to None.
        aux_files (list): list of files containing auxiliary variables in .4 format. Defaults to None.
        phase (str): 'water', 'ice' or 'mixed', water and ice phase components calculated with the
        Tetens formula. Defaults to 'water'.
        save_file (bool): saves xarray to file. Defaults to False.
        for_enstools (bool): if 'True' changes compatability to enstools, if 'False' mantains file format.
        Defaults to True.
        
    Returns:s
        xds (xarray): Dataset.
    """

    if filename.endswith('.nc'):
        xds = nc2xarray(filename, dim_tra, dim_time, loaddir, for_enstools)
    
    elif filename.endswith('.4'):
        xds = lagranto2xarray(filename, dim_tra, dim_time, loaddir, aux_files, for_enstools)
            
    elif filename.endswith('.txt'):
        xds = Met3d2xarray(filename, loaddir, ['QV'])
    
    if for_enstools:
        dim_tra = 'ens'
        dim_time = 'time'
    
    var_f = lambda v, th: np.where(xds[v].values == th, np.nan, xds[v].values)

    QV = var_f('QV', -1.7599316e-35)
    P = var_f('P', 6821176.)

    try:    
        T = var_f('T', -7.9223500e-33)
    except:
        T = np.full(QV.shape, 283)
    
    xds = xds.assign({'QV' : ([dim_tra, dim_time], QV),
                           'P' : ([dim_tra, dim_time], P),
                           'T' : ([dim_tra, dim_time], T)})

    xds.QV.attrs['units'] = 'kg/kg'
    xds.P.attrs['units'] = 'Pa'  
    xds.T.attrs['units'] = 'K'  
  
    print('QV is assumed to be kg/kg, P Pascal and T Kelvin')

    xds = select_aux_variables(xds, dim_tra, dim_time, auxvariables, P, QV, T, phase)
   
    if save_file:
        if savedir:
            loaddir = savedir
        fname = loaddir + 'NEW_' + filename
        xds.to_netcdf(fname)
        print('Saved file found in: %s' %fname)
    
    return xds.load()
    

def select_aux_variables(xds, dim_tra, dim_time, auxvariables, P, QV, T, phase):
    """ Assign auxiliary variables to the dataset.

    Args:
        dim_tra (string): names of dimention of trajectory.
        dim_time (string): names of dimention of time.
        auxvariables (list or None): list of desired auxiliary variables. Defaults to None.
        
    Returns:s
        xds (xarray): Dataset.
    """

    t = xds[dim_time]

    for v in auxvariables:                 
        if v == 'SAT' or v == 'saturation':
            hus_f = husf.saturation(P, QV, phase = phase, T = T)

        elif v =='RH' or v =='relative_humidity':
            hus_f = husf.relative_humidity(P, QV, phase = phase, T = T)  
        
        elif v =='pls' or v =='PLS':
            hus_f = husf.point_of_last_saturation(P, QV, phase = phase, T = T)

        elif v =='QV_diff': 
            hus_f = husf.diff_time_specific_humidity(QV, timestep=1)  

        elif v =='surv_mt':
            hus_f = husf.survival_material(P, QV, t, timestep=1)  # hardcoded for now 
               
        elif v =='cum_QV':
            hus_f = husf.cumulative_specific_humidity(QV)

        elif v =='surv_QV':
            hus_f = husf.survival_rate(QV)

        elif v =='th_RH':
            hus_f = husf.threshold_relative_humidity(P, QV, phase = phase, T = T) 

        elif v =='surv_uv':
            hus_f = husf.survival_uv(P, QV, phase = phase, T = T, theta = 60, cte = 1) # hardcoded for now 

        else:
            print('Auxiliary variable %s not available. Verify name or presence of variable.' %v)
                
        xds = xds.assign({v : ([dim_tra, dim_time], hus_f)})  

    return xds