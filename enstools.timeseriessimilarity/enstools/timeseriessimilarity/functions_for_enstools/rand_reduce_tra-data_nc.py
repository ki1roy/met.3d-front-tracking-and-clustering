#script to reduce trajectory amount
#Authors: Alexander Hoffamnn and Alan Schumacher (alan.schumacher@uni-hamburg.de)
import xarray as xr
import numpy as np
import config_lagranto_converter as config
from lagranto2netcdf import convert2doubletime, labelVariables

ds_t = xr.open_dataset("your_dataset.nc") # choose the trajectory dataset (netcdf) you want to reduce
ds_t = convert2doubletime(ds_t)
labelVariables(ds_t)

#Works only in Runtime, because we need the dataset
#Filter the Trajectory with NAN Values in it and return the time and trajectory
def filterNaNIndex(ds,varName):
    var = np.array(ds[varName])
    #only look at trajectory and time dimension discard ensemble
    var = np.isnan(var)[0]
    time = np.argwhere(~np.any(var,axis=0))[:,0]
    traj = np.argwhere(~np.any(var,axis=1))[:,0]
    return traj,time 

traj,time = filterNaNIndex(ds_t,"pressure")
ds_t = ds_t.isel(trajectory=traj)
#Indexer
#seed
np.random.seed(2)

#reduce trajectory to a number of your choice, here 1000
ind_tra = xr.DataArray(np.random.choice(ds_t.trajectory.size, 1000, replace=False), dims = ["trajectory"])
ds_t = ds_t.isel(trajectory=ind_tra)      
#ds_n = sortCronological(ds_t)
ds_t.to_netcdf("your_dataset.nc")
