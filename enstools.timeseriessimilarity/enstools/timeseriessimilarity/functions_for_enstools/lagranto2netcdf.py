import numpy as np 
import netCDF4 as nc4
import xarray as xr
import config_lagranto_converter as config

#This programm convert Trajectory data.
#Input: lagranto-file(.4) --> Output: netcdf-file(.nc)
#The choosen formatting is for the visualsation in Met3D.
#The time-array must be in ascending order, so the synchronization Panel of Met3D can select the trajectories. 
#https://collaboration.cen.uni-hamburg.de/display/Met3D/Trajectory+data+in+NetCDF+format

#Authors: Alexander Hoffamnn and Alan Schumacher (alan.schumacher@uni-hamburg.de)

#----------------------|main program|---------------------------------------
def convert_lagranto2netcdf(name, newname=None, loaddir="", savedir="",verbose=False):
    xds = load_lagranto(name, loaddir)
    if verbose: print("loaded lagranto dataset. now converting format...")
    xds = assembled_conversion(xds)
    if verbose: print("converted to proper format for Met3d. exporting to netcdf...")
    output_netcdf(xds, name if newname is None else newname, savedir)
    if verbose: print("done.")

#modular implementation
def load_lagranto(name,loaddir=""):
    #constructing file path
    p = loaddir
    p += name
    p += ".4"
    lds = nc4.Dataset(p, mode='r')
    xds = xr.open_dataset(xr.backends.NetCDF4DataStore(lds))
    return xds

def assembled_conversion(xds):
    xds = reshapeDatasetTrajectory(xds)
    labelVariables(xds)
    xds = sortCronological(xds)
    return xds

def output_netcdf(xds, name, savedir=""):
    xds.to_netcdf(savedir + name + ".nc")
#--------------------------------------------------------------------

#"year-month-day hour:min:00"
def convert2Datetime(ds):
    #convert date integers into string because of datetime format "year-month-day"
    datestr = str(ds.attrs["ref_year"])+"-"+str(ds.attrs["ref_month"]).zfill(2)+"-"+str(ds.attrs["ref_day"]).zfill(2)
    date = np.datetime64(datestr)
    #offset by starttime of recording
    date += np.timedelta64(ds.attrs["ref_hour"],"h")
    date += np.timedelta64(ds.attrs["ref_min"],"m")
    #attention! if your format comes in fractions of hours it's going to get rounded
    delta = np.array([np.timedelta64(d,'h') for d in ds["time"].values.astype(int)])
    date+=delta
    return date

#permute the time-array in ascending order
def sortCronological(ds):
    t = np.array(ds.coords["time"])
    ind = np.argsort(t)
    return ds.isel(time=ind) 

#time needs to be double for Met3D
#String manipulation to get the Format for Met3D
def convert2doubletime(ds):
    startdate = np.amin(ds.coords["time"])
    enddate = np.amax(ds.coords["time"])
    #replace enddate with startdate to get positive time Values
    diff = np.array(ds.coords["time"] - startdate) 
    diff = diff.astype("timedelta64[h]").astype("double")
    ds_n = ds.assign_coords(time=("time",diff))
    
    #remove T from startdate and enddate
    startdate = np.datetime_as_string(startdate, "s")[:-9] + " " + np.datetime_as_string(startdate, "s")[-8:]
    enddate = np.datetime_as_string(enddate, "s")[:-9] + " " + np.datetime_as_string(enddate, "s")[-8:]
    unit = "hours"
    
    #assign startdate and enddate 
    unit += " since " + startdate
    print(unit)
    ds_n.coords["time"].attrs["units"] = unit
    ds_n.coords["time"].attrs["trajectory_starttime"] = enddate
    #ICON model inittime
    ds_n.coords["time"].attrs["forecast_inittime"] = "2021-06-27 00:00:00"
    return ds_n

def reshapeDatasetTrajectory(ds):
    date = convert2Datetime(ds)
    #time is a variable and must be a coordinate for Met3D
    ds_n = ds.drop("time")
    ds_n = ds_n.rename_dims({"ntim":"time","ntra":"trajectory"})
    ds_n = ds_n.expand_dims("ensemble")
    ds_n = ds_n.assign_coords(time=("time",date))
    ds_n = convert2doubletime(ds_n)
     #the order of dimension is critical for Met3d -> else segmantation fault
    ds_n = ds_n.transpose("ensemble","trajectory","time")
    ds_n.attrs["featureType"] = "trajectory"
    return ds_n

#for future Version: checks and conversions for units + assert errors for missing units
#(non of our files shoud be missing their units, but here we are!)
def labelVariables(ds):
    naming = lambda var, stname, lnname: var.attrs.update({'standard_name': stname , 'long_name': lnname})
    naming(ds.coords['time'],'time','time')
    naming(ds['lon'],'longitude','longitude')
    naming(ds['lat'],'latitude','latitude')
    naming(ds['z'],'altitude','altitude')
    if 'P' in ds:
        naming(ds['P'],'air_pressure','pressure')
        ds['P'].attrs.update({'units':'Pa'})
    if "QV" in ds:
        ds["QV"].attrs.update({"units":"kg/kg"})
    for varname in ds:           
        if varname in config.variable_map:
            ds[varname].attrs["auxiliary_data"] = "yes"
            naming(ds[varname],*config.variable_map[varname])
