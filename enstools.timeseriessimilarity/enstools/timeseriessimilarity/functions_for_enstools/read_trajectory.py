# -*- coding: utf-8 -*-
"""
Last Modified on November 2022
@author: Ingra Barbosa | Git: @ingrabarbosa
"""

import netCDF4 
#import lagranto as lg
import datetime 
import xarray as xr
import numpy as np
import pandas as pd
import lagranto2netcdf

#%%
def Met3d2xarray_with_lagranto(filename, loaddir=None, auxvariables=None):
    """ Read trajectory pathlines and auxiliary data along trajectories from original Met.3D 
       otputed txt-file and write into a new txt-file that can be read by lagranto.
       Further conversion to xarray dataset.
    
    Args:
        filename (str): filename with trajectory data (txt/ascci from Met.3D, .4 from Lagranto).
        loaddir (str): directory path to file. Defaults to None.
        auxvariables (list): auxiliary data of interest in the file (e.g. p, hus, wap, ...). Defaults to None.
        
    Returns:
        xds (xarray): Dataset containing trajectory data.
    """
    
    if loaddir == None:
        loaddir=''
            
    f1 = loaddir + filename
   
    f2 = loaddir + "EDITED_" + filename 

    def txt_file_edit():
        """Re-format parts of the original trajectory data into the format required by the Lagranto lib"""
        with open(f1,"rt") as f:
            with open(f2,"wt") as fw:
                lines = f.readlines()
                
                for line in lines[:4]:
                    line = line.replace('time [h]', 'time')
                    fw.write(line)
                
                for line in lines[4:]:
                    if len(line) == 1:
                        fw.write(line)
                    else:
                        h = float(line[2:6])
                        hours = int(h)
                        minutes = (h*60) % 60
                        hh2mm = "%d.%02d" % (hours, minutes)
                        line = line.replace(line[2:6], hh2mm[:4])
                        line = line.replace('-999.99', 'nan    ')
                        line = line.replace('-9.99e+11', 'nan      ')
                        fw.write(line)

        return f2

    def ascii_trajs():
        """ - ascii_trajs   ->  use the Lagranto lib to read trajectory data from txt-file"""
        f2 = txt_file_edit()    
        trajs = lg.Tra()
        trajs.load_ascii(f2)
        
        return trajs

    def get_time_vars():
        """ - get_time_vars ->  get time data to convert to the right format (hours)"""

        trajs = ascii_trajs()
        startdate = trajs.initial  #start date may be prior to end date
        timestep = trajs.ntime

        return startdate, timestep
    
    #------------------------------------------------------------------
    trajs = ascii_trajs()
    startdate, timestep = get_time_vars()
        
    steps = np.arange(trajs.ntime)*trajs.duration/(trajs.ntime-1)
    time = np.array([datetime.timedelta(minutes = s) for s in steps])
    time += startdate 
    
    trajs['time'] = np.broadcast_to(time, (trajs.ntra, *time.shape))
    
    time = trajs['time'][0] #dtype='datetime64[s]'

    lon = trajs['lon']
    lat = trajs['lat']
   
    ens = np.arange(0, len(lon), 1)

    #create xarray dataset 
    xds = xr.Dataset({"lat" : (["ens","time"], lat),
                       "lon" : (["ens","time"], lon),  }, 
                 coords = {
                       "ens": ens,
                       "time":time,})
    
    #add variables to dataset
    for v in variables:
        try:
            xds = xds.assign({v : (["ens","time"], trajs[v])})
        except:
            continue
    return xds


#%% Met.3D txt/ascii file to xarray
def Met3d2xarray(filename, loaddir=None, auxvariables=None):
    """ Read trajectory pathlines and auxiliary data along trajectories from original Met.3D 
       otputed txt-file and write into a new txt-file that can be read by lagranto.
       Further conversion to xarray object.
    
    Args:
        filename (str): filename with trajectory data (txt/ascci from Met.3D, .4 from Lagranto).
        loaddir (str): directory path to file. Defaults to None.
        auxvariables (list): auxiliary data of interest in the file (e.g. p, hus, wap, ...). Defaults to None.
        
    Returns:
        xds (xarray): Dataset containing trajectory data.
    """
    
    if loaddir == None:
        loaddir=''
            
    f1 = loaddir + filename

    with open(f1,"rt") as f:
        firstline = f.readline().rstrip()
        dateobj = firstline.split(' ')[2]
        
    df = pd.read_csv(f1, sep=r'\s{2,}', engine='python', header=1)  
    df = df.drop(index=0)
    df = df.replace(-999.99, np.nan)
    df = df.replace(-9.990000e+11, np.nan)
    
    dims = np.array(np.where(df['time [h]'] =='0.00'))
    nt = dims[0][1]                         # number of timesteps
    ntj = int(df.shape[0]/nt)               # number of trajectories
    
    lat = np.array(df['lat']).reshape(ntj, nt)
    lon = np.array(df['lon']).reshape(ntj, nt)
    p = np.array(df['p']).reshape(ntj, nt) * 100          # hPa to Pa
    time = np.array(df['time [h]']).reshape(ntj, nt)[0]
        
    startdate = datetime.datetime.strptime(dateobj,"%Y%m%d_%H%M")
    enddate = startdate + datetime.timedelta(hours=float(time[-1]))
    timestep= datetime.timedelta(hours=(float(time[1])-float(time[0]))) 
    time = np.arange(startdate, enddate+timestep, timestep, dtype='M8[m]')

    ens = np.arange(0, ntj, 1)
    
    #create xarray dataset 
    xds = xr.Dataset({"lat" : (["ens","time"], lat),
                         "lon" : (["ens","time"], lon),
                         "P" : (["ens","time"], p)},
                    coords = {
                         "ens": ens,
                         "time": time})
    
    for v in auxvariables:
        if v == 'hus' or v == 'QV':
            xds = xds.assign({'QV' : (["ens","time"], np.array(df['hus']).reshape(ntj, nt))})
        else:
            try:
                xds = xds.assign({v : (["ens","time"], np.array(df[v]).reshape(ntj, nt))})
            except:
                print('Make sure the auxiliary variable %s name is the same as in the original file' %v)

    return xds 

#%%
def lagranto2xarray(filename, dim_tra, dim_time, loaddir=None, aux_files=None, for_enstools=True):
    """ Read trajectory pathlines and auxiliary data along trajectories from .4 files. 
    
    Args:
        filename (str): filename with trajectory data (format .4 from Lagranto).
        dim_tra (string): names of dimention of trajectory.
        dim_time (string): names of dimention of time.
        loaddir (str): directory path to file. Defaults to None.
        aux_files (list): files containing auxiliary variables from the ensemble (e.g. P, QV, T,...). Defaults to None.
        for_enstools (bool): if 'True' changes compatability to enstools, 'False' mantains file format. Defaults to True.

    Returns:
        xds (xarray): Dataset containing trajectory data.
    """
    
    if loaddir == None:
        loaddir=''
        
    f = loaddir + filename
    
    lds = netCDF4.Dataset(f, mode="r") 
    xds = xr.open_dataset(xr.backends.NetCDF4DataStore(lds))    
    
    if aux_files:
        for file in aux_files:  
            f = loaddir + file
            lds = netCDF4.Dataset(f, mode="r") 
            aux_xds = xr.open_dataset(xr.backends.NetCDF4DataStore(lds)) 
            assert xds.merge(aux_xds), "Non-compatible values in merged sets"
            xds = xds.merge(aux_xds)#, compat="override")
            
    date = lagranto2netcdf.convert2Datetime(xds)

    xds = xds.transpose(dim_tra, dim_time)

    if for_enstools:
        ds_n = xds.drop("time")
        ds_n = ds_n.rename_dims({dim_time:"time", dim_tra:"ens"})
        ds_n = ds_n.assign_coords(time=("time", date))
        xds = ds_n.assign_coords(ens=("ens", ds_n['ens'].values))

    if 'QV' in xds.variables:
        xds['QV'] = xds['QV']/10e5                        #mg/kg -> kg/kg
        print('The units of QV were converted from mg/kg to kg/kg')

    return xds


#%%
def nc2xarray(filename, dim_tra, dim_time, loaddir=None, for_enstools=True):
    """ Read trajectory pathlines and auxiliary data along trajectories from .nc files. 
    
    Args:
        filename (str): filename with trajectory data in .nc format.
        dim_tra (string): names of dimention of trajectory.
        dim_time (string): names of dimention of time.
        loaddir (str): directory path to file. Defaults to None.
        for_enstools (bool): if 'True' changes compatability to enstools, 'False' mantains file format.
        Defaults to True.

    Returns:
        xds (xarray): Dataset containing trajectory data.
    """
    
    if loaddir == None:
        loaddir=''
        
    f = loaddir + filename 
    
    lds = netCDF4.Dataset(f, mode="r") 
    lds = xr.open_dataset(xr.backends.NetCDF4DataStore(lds)) 
    
    xds = lds.load()
    
    if 'ensemble' in xds.dims:
        xds = xr.concat([xds.sel(ensemble=h) for h in range(len(xds.ensemble))], dim=dim_tra)
    
    if xds.pressure.attrs['units'] == 'hPa':
       xds['pressure'] = xds.pressure*100
       xds.pressure.attrs['units'] = 'Pa'
       print('The units of pressure were converted from hPa to Pa')
    xds = xds.rename_vars({"pressure":"P"})

    if xds.QV.attrs['units'] == 'g/kg':
       xds['QV'] = xds.QV/1000
       xds.QV.attrs['units'] = 'kg/kg'
       print('The units of QV were converted from g/kg to kg/kg')

    if for_enstools:
        xds = xds.rename_dims({dim_tra:"ens"})

    return xds