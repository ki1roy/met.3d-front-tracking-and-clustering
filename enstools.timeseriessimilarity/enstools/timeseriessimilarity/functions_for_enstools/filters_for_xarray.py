"""
Last Modified on November 2022
@author: Ingra Barbosa | Git: @ingrabarbosa
"""

import numpy as np
import xarray as xr
from numpy import logical_and, logical_or 

def threshold_xarray(xds, dim_tra, variable, th):
    """ Drop trajectories which all the values of a trajectory are bellow a threshold[0]
        or has a single value above a threshold[-1] for a particular variable.

    Args:
        xds (xarray.Dataset): dataset to be filtered.
        dim_tra (string): names of dimention of trajectory.
        variable (DataArray or np.array): variables which will be used to filter the data
        th (list): bottom and upper limits to be filtered. Trajectories with all values
        bellow the bottom limit and a any value above the upper limit will be removed. 

    Returns:
        xds (xarray.Dataset): filtered Dataset.
    """

    var = xds[variable].values              # still structered as data array
    ens_n = xds[dim_tra].values

    section1 = np.isnan(var)
    section2 = var<=th[0]
    section3 = var>=th[1]

    section2 = np.logical_or(section2, section1)
    section3 = np.logical_and(section3, ~section1)

    idx1 = np.argwhere(section2.all(axis=1))
    idx2 = np.argwhere(section3.any(axis=1))
    idx = np.concatenate((idx1, idx2), axis=0)

    bool_mask = np.array([False if i in idx else True for i in ens_n])

    xds = xds.where(xr.DataArray(bool_mask, dims=(dim_tra)), drop=True)

    return xds

#%%
def index_from_dataset(xds, dim_tra, PLS=False):
    """ Index of PLS fit accordingly inside the length of the dataset. 
        Att: needs SAT as a variable if 'PLS' == True. 

    Args:
        xds (xarray.Dataset): dataset to be filtered.
        dim_tra (string): names of dimention of trajectory.
        PLS (bool): if True fills PLS index in matrix. Defaults to False.

    Returns:
        index_fill (array)
    """

    ens_n = xds[dim_tra].values.size
    index_fill = np.full((ens_n, 2), (0, 0))
    index_fill[:,0]= np.arange(ens_n)

    if PLS:
        SAT = xds.SAT.values

        idx = np.argwhere(~np.isnan(SAT))
        _, index = np.unique(idx[:, 0], return_index=True)
        indices = idx[index]
        
        index_fill[indices[:,0]] = indices

    return index_fill

def divide_xarray_from_value(xds, dim_tra, variable, th, index_divide=None):
    """ Divide the timeseries according to a trajectory first value bellow or 
        above a threshold for a particular variable in a particular index. 

    Args:
        xds (xarray.Dataset): dataset to be filtered.
        dim_tra (string): names of dimention of trajectory.
        variable (str): variables which will be used to filter the data
        th (float): threshold value to divide the Dataset.  
        index_divide (int, array, str or None): index or indices of the values to 
        compare with the th. If 'PLS' uses the value of the index of the PLS.
        Defaults to None and it compares to index 0. 
        
    Returns:
        xds_lower (xarray.Dataset): filtered Dataset of values bellow the threshold.
        xds_higher (xarray.Dataset): filtered Dataset of values above the threshold.
    """
        
    indices =  index_from_dataset(xds, dim_tra)

    if type(index_divide) == int:
        indices[:,1] = np.full(len(indices), index_divide)

    elif index_divide == 'PLS':
        indices =  index_from_dataset(xds, dim_tra, PLS=True)

    elif index_divide.size > 1:
        assert len(index_divide) == len(indices), 'Array size not compatible with xarray'
        indices[:,1] = index_divide

    else:
        print('ensure index_divide is compatible with function')

    bool_mask = np.array([True if xds[variable][i,j] > th else False for i, j in indices])
    xds_higher  = xds.where(xr.DataArray(bool_mask, dims=(dim_tra)), drop=True)
    xds_lower = xds.where(xr.DataArray(~bool_mask, dims=(dim_tra)), drop=True)

    return xds_lower, xds_higher

#%%
def coordinates_window(xds, dim_tra, dim_time, lat_limits=[-90,90], lon_limits=[-180, 180]):
    """ Selects data that ends inside the coordinate window.
    
    Args:
        xds (xarray.Dataset): Dataset to be filtered. 
        dim_tra (string): names of dimention of trajectory.
        dim_time (string): names of dimention of time.
        lat_limits (list): lower and upper limits of the latitudes.
        lon_limits (list): lower and upper limits of the longitudes.  

    Returns:
        xds (xaray.Dataset): selected Dataset.
    """

    lat = np.array(xds['lat'].values).reshape((xds.dims[dim_tra], xds.dims[dim_time]))[:,:1]
    lon = np.array(xds['lon'].values).reshape((xds.dims[dim_tra], xds.dims[dim_time]))[:,:1]

    m1 = logical_and(lat_limits[0]<lat,lat<lat_limits[1])
    m2 = logical_and(lon_limits[0]<lon,lon<lon_limits[1])
    m = logical_and(m1, m2)
    #m is used to select indices dim(trajectory)
    m_tra = np.any(m, axis=1)

    xds = xds.where(xr.DataArray(m_tra, dims=(dim_tra)), drop=True)

    return xds

#%%
def drop_non_saturated_trajectories(xds, dim_tra, dim_time):
    """ Removes all trajectories that do not reach the saturation point. 
        The original Dataset must have the variable saturation (SAT). 
    
    Args:
        xds (xarray.Dataset): Dataset to be filtered.
        dim_tra (string): names of dimention of trajectory.
        dim_time (string): names of dimention of time.
        
    Returns:
        xds (xarray.Dataset): filtered dataset.
    """
    
    SAT = xds.SAT.values
    idx = np.argwhere(np.isnan(SAT).all(axis=1))[:,0]
    ens_n = xds[dim_tra].values

    bool_mask = np.array([False if i in idx else True for i in ens_n])
    xds = xds.where(xr.DataArray(bool_mask, dims=(dim_tra)), drop=True)

    return xds