#config
#dictonary for the variables
#used by "lagranto2netcdf.py" in the funktion "labelVariables"

variable_map={}
variable_map['T' ]= ("temperature","temperature")
variable_map["QV"] = ("specific_humidity","specific humidity")
variable_map["RH"] = ("relative_humdity","relative humidity")
variable_map["SAT"] = ("saturation","saturation")
