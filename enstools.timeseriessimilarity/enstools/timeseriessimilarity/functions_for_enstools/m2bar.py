#when you only have the altitude in m and need pressure levels
#Met3D only works with: "levels of constant pressure", "hybrid sigma-pressure levels" or ...
# "arbitrary model levels with an additionally provided auxiliary 3d pressure field" in the vertical.
import numpy as np
import numpy

class pLevel:
    def __init__(self,values,mode):
        if mode == 'Pa':
            self.pa = np.array(values)
            self.hpa = self.pa / 100.0
            self.m = convert_hpa2m(self.hpa)
        elif mode == 'hPa':
            self.hpa = np.array(values)
            self.pa = self.hpa * 100.0
            self.m = convert_hpa2m(self.hpa)
        elif mode == 'm':
            self.m = np.array(values)
            self.hpa = convert_m2hpa(self.m)
            self.pa = self.hpa *100.0
        else:
            raise ValueError('Invalid Pressure mode flag!')

    def findPa(self,value,strict= True):
        assert not strict or value in self.pa,'pa Value not found!'
        if strict:
            return numpy.argwhere(self.pa==value)[0][0]
        else:
            return numpy.argmin(numpy.absolute( self.pa-value))

    def findhPa(self,value,strict= True):
        assert not strict or value in self.hpa,'hpa Value not found!'
        if strict:
            return numpy.argwhere(self.hpa==value)[0][0]
        else:
            return numpy.argmin(numpy.absolute( self.hpa-value))
    
    def findM(self,value,strict= True):
        assert not strict or value in self.hpa,'meter Value not found!'
        if strict:
            return numpy.argwhere(self.m==value)[0][0]
        else:
            return numpy.argmin(numpy.absolute( self.m-value))


#this funktion requires height above sea level 
#Barometric formula with lapse rate equal to zero
#m->hPa
def convert_m2hpa(arr):
    g = 9.8
    M = 0.0289
    R = 8.3144
    T = 288.16
    p = 1013.25 
    return p*np.exp(-arr*(g*M)/(T*R))

def convert_hpa2m(arr):
    g = 9.8
    M = 0.0289
    R = 8.3144
    T = 288.16
    p = 1013.25 
    return -np.log(arr/p)*((T*R)/(g*M))
