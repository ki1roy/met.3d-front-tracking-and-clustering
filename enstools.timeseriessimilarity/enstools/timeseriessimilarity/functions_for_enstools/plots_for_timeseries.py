"""
Last Modified on November 2022
@author: Ingra Barbosa | Git: @ingrabarbosa
"""

import numpy as np
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import matplotlib.pyplot as plt

def map_trajectories(xds, dim_tra, dim_time, filename=None, to_png=False):
    """ Map the trajectories.
    
    Args:
        xds (xarray.Dataset): Dataset to be filtered. 
        dim_tra (string): names of dimention of trajectory.
        dim_time (string): names of dimention of time.
        filename (string): name of the file. Defaults to None.
        to_png (bool): saves the image as .png. Defaults to False.
    """

    date = f'{xds.ref_day}.{xds.ref_month}.{xds.ref_year}'

    def getRuns(arr, repeating=False):
        ars = np.roll(arr,1)                                  #shift array by one
        arr[-1],ars[0]=arr[-1] & repeating, ars[0] & repeating
        starts = ~ars & arr                                   #get everyone where the prev. was false and now is true
        ends = ~arr & ars                                     #get everyone where now is false but prev. was true
        return zip(np.nonzero(starts)[0],np.nonzero(ends)[0]) #pair them by order of appearance

    var = xds['lat'].values.reshape((-1, xds.dims[dim_time]))
    interval = np.array([(i,*l) for i, n in enumerate(var<np.inf) for l in getRuns(n)])

    rs = (int(xds.sizes[dim_tra]),int(xds.sizes[dim_time]))
    coord = np.moveaxis(np.stack((xds['lon'].values.reshape(rs), xds['lat'].values.reshape(rs))),0,2)
    print(coord.shape)

    plt.figure(figsize=(13,10))
    ax = plt.axes(projection=ccrs.PlateCarree())

    plt.title(f'Trajectories path of {date}')
    ax.add_feature(cfeature.COASTLINE)
    ax.gridlines(draw_labels=True, color = "gray")

    for i,t in enumerate(coord):
        if np.isin(i, interval[:,0]):                         #uses intervals to only draw relevant data 
            s=interval[np.argwhere(interval[:,0]==i)[0][0],1]
            plt.plot(t[s:,0],t[s:,1])

    if to_png:
        plt.savefig(f'{filename}.png')
        print('Map of selected coordinates image saved')

    plt.show()

#%%
def plot_histograms_with_SAT(xds, variable, filename=None, to_png=False): 
    """ Plot histogram of the variable within the limits of 0-90% as well
    as the saturated (> 95% relative humidity) values. 
    
    Args:
        xds (xarray.Dataset): Dataset to be filtered. 
        variable (string): name of variable.
        filename (string): name of the file. Defaults to None.
        to_png (bool): saves the image as .png. Defaults to False.
    """

    def bins(var, limbin):
        count_bins = []
        c=0
        for ii in range(0, len(limbin)-1):
            b = np.count_nonzero((var >=limbin[ii]) & (var<= limbin[ii+1]))
            c +=b
            count_bins.append(b)
            
        count_bins.append(len(var)*len(var[0])-c)
        return count_bins
    
    qvhist = bins(xds[variable], limbin=[0, 10, 20, 30, 40, 50, 60, 70, 80, 90])
    rqv_hist=['0-10', '10-20', '20-30', '30-40', '40-50', '50-60', '60-70', '70-80', '80-90', '>90']

    date = f'{xds.ref_day}.{xds.ref_month}.{xds.ref_year}'
    fig, axs = plt.subplots(1, 2, figsize=(16, 4))
    fig.suptitle(f'{variable} of {date}')

    sat=np.array(xds.SAT)

    axs[0].bar(rqv_hist, qvhist)
    axs[0].set_title('Distribution')
    axs[1].hist(sat, histtype='stepfilled', range=(95, np.nanmax(sat)))
    axs[1].set_title('Saturated Values')

    for ax in axs:
        ax.set_ylabel("Amount of Timesteps")
        ax.set_xlabel('%')
    
    plt.tight_layout() 

    if to_png:
        plt.savefig(f'{filename}.png')
        print('Histogram image saved')
