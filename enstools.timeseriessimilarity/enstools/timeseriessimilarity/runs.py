#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 24 12:58:01 2021

@author: Alexander Hoffmann
"""

import numpy
import numpy as np

#returns indeces of intervals where arr is True. 
#>>if repeat option disabled will alter input array
#>>doesn't report an all true array(since it never started or ended anywhere)
def getRuns(arr, repeated = False):
    ars = np.roll(arr,1)#shift array by one
    ars[0] = ars[0] & repeated 
    arr[-1] = arr[-1] & repeated
    starts = ~ars & arr #get everyone where the prev. was false and now is true
    ends = ~arr & ars   #get everyone where now is false but prev. was true
    
    return zip(np.nonzero(starts)[0],np.nonzero(ends)[0])#pair them by order of appearance