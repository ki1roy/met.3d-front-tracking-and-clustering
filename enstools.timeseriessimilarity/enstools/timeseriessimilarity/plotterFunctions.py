#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 28 14:45:52 2022

@author: kakaufmann
"""
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable
from matplotlib.ticker import AutoMinorLocator

import numpy as np
from matplotlib import ticker

from .horizon import horizon
from .runs import getRuns
from .getterFunctions import get_horizon_base

#%% figure, subfigure, axes
def generate_figure(width,coreHeight, outerHeight, number):
    """ Generates a matplotlib figure with the size adjusted to the number of views
    
    Parameter
    ---------
    width: float
        the width of the figure.
    coreHeight: float
        the height of the core view
    outerHeight: float
        the height of one outer view
    number: int
        the number of outer views, one core view is assumed.
        
    Returns
    -------
    matplotlib figure object
    """
    assert number >= 0
    assert outerHeight > 0 #might be better with ValueError
    assert coreHeight >= 0 #might be better with ValueError

    return plt.figure(figsize = (width, coreHeight+ number * outerHeight),constrained_layout = True )


def generate_subfigures(fig, number, coreHeight, outerHight):
    """ Generates a list of subfigures on the given figure. 
    
    Parameter
    ---------
    fig : matplotlib figure
        The host figure, where the subfigures are created on
    number: int
        the number of views/ subfigures. Must be at least 2. One core, one outer
    coreHeight: float
        The height of the core view. It is needed to give the subfigures the needed
        space depending on the number of views and the size of the core view and 
        outer view.
    outerHeight: float
        The height of one outer view. It is needed to give each view the needed
        space. 
    
    Returns
    -------
    list of subfigures
    """
    assert number >= 2
    divider = coreHeight + (number -1)* outerHight
    heightRatios = [outerHight/divider, coreHeight/divider]
    addedFigures = [outerHight/divider] * (number - 2)
    heightRatios = heightRatios + addedFigures

    return fig.subfigures(number, 1, True, hspace = 0, height_ratios = heightRatios)


def get_axes(subfig, ysuplabel = None):
    """ Checks if the subfigure is an axes or has allready axes. It returns always
    an axes, uses the first existing one or creating one, when the subfigure is 
    a figure.
    
    Sets ysuplabel as an y_label when subfig is a axes. 
    
    Parameter
    ---------
    subfig: matplotlib figure or axes object
        The subfigure to check
    ysuplabel: string
        y label, either sublabel or normal label on the axes. 
    
    Returns
    -------
    matplotlib axes object
        
    """
    if isinstance(subfig, plt.Axes) :
        ax = subfig
        if ysuplabel is not None: 
            ax.set_ylabel(ysuplabel)
    else: 
        if ysuplabel is not None:
            subfig.supylabel(ysuplabel)
        if not subfig.get_axes():
            ax = subfig.subplots()
        else:
            ax = subfig.get_axes()[0]
        
    return ax

#%% manipulate axes
#only used by heatmap
def generate_colorbar(collection,ax):
    """
    Creates a additional insert axis to a figure, with the 
    same coordinates as the given axis and displayes the colorbar with c there.
    
    Parameter
    ---------
    collection: matplotlib object
        The return value from pcolor, it contains the color and limits for the values.
    ax: matplotlib axis object
        the axis, where the color is used
    """
    #having the colorbar in a single axis ensures, that the view in ax 
    #still has the same amount of space. Helps with alignment
    fig = ax.get_figure()
    axins = inset_axes(ax, width="2%", height="100%", loc='lower left', bbox_to_anchor=(1.05, 0., 1, 1),
                       bbox_transform=ax.transAxes, borderpad = 0)
    fig.colorbar(collection,cax = axins)
    

#only used by heatmap
def regulate_yTicks(ax, ylabels, ticks = None):
    """ Changes the yticks and yticklabels of the given axis.
    Parameter
    ---------
    ax:matplotlib axis object
        The axis, where the ticks should be changed to the labels
    ylabels: list of Strings/labels
        the labels, that should be displayed on the axis.
    ticks: array like
        The location, where the labels are. When None, the length of the labels 
        is used.
    """

    if ticks is None:
        ax.set_yticks(np.arange(len(ylabels)))
    else:
        ax.set_yticks(ticks)
    ax.set_yticklabels(ylabels)


#used by spaghetti and heatmap, Horizon
def regulate_xTicks(ax, xlabels, xticks = None):
    """ Changes the yticks and yticklabels of the given axis.
    Parameter
    ---------
    ax:matplotlib axis object
        The axis, where the ticks should be changed to the labels
    xlabels: list of Strings/labels
        the labels, that should be displayed on the axis.
    xticks: array like
        The location, where the labels are. When None, the length of the labels 
        is used.
    """
    ax.set_xticks(np.arange(len(xlabels))) if xticks is None else ax.set_xticks(xticks)
    ax.set_xticklabels(xlabels, rotation=45, ha='right')
    

#used by _set_spaghetti_axis    
def regulate_lims(ax, ylim, xlim):
    """ On the given axis, the xlims and ylims get adjusted.
    
    Parameter
    ---------
    ax: matplotlib axis object
        the axis, where the lims should be adjusted
    ylim: tuple
        The set y-limit
    xlim:tuple
        The set x-limit
    """
    ax.set_ylim(ylim)
    ax.xaxis.set_minor_locator(AutoMinorLocator())
    ax.set_xlim(xlim)
    

#used by spaghetti
def make_background_grid(ax):
    """ Enables the grid of the given axis, in black with dots.
    
    Parameter
    ---------
    ax:matplotlib axis object
        The axis where the grid should show
    """
    ax.grid(color='k', linestyle=':', linewidth=1)


#used by spaghetti, horizon and heatmap
def indicate_window(ax, start, end, color = 'k'):
    """ Draws two lines on the axis. One at the position of start and the other 
    at the position of end. 
    
    Parameter
    ax: matplotlib axis object
        The manipulated axis
    start: same type as the x values of ax.
        One value on the x-axis of the ax. The start of the window
    end: same type as the x values of ax.
        One value on the x-axis of the ax. The end of the window.
    color: char
        The color of the lines. Default is black.
    """
    ax.axvline(x = start, color =  color, linewidth = 0.5)
    ax.axvline(x = end, color = color, linewidth = 0.5)
    

def regulate_spaghetti_axes(ax, ylims, xlims, xticks, xlabels):
    """ Modifies the axes according to a predefined format, using some helper funciton.
    
    Parameter
    ---------
    ax: matplotlib axes object
        The axes to manipulate
    ylims: tuple of floats
        The minimal and maximal values of the data, that should be displayed on the axes
    xlims: tuple of int
        The first and last point on the time dimension, which should be shown
    xticks: list of ints
        The points in time, where a label should be placed.
    xlabels: list of strings
        The lables to display on the time axis.
    """
    make_background_grid(ax)
    regulate_lims(ax, ylims, xlims)
    regulate_xTicks(ax, xlabels, xticks)
    

#%% plotter    
#TODO: make membersOfInterest work again. numbers as member causes problems, gets interpreted as index, not labels
def paint_spaghetti(subfig, data, varName):#, membersOfInterest = None):
    """ Creates a spaghetti view on ax where one line represents one ensemble member.
    When a member is of special interest, it is shown in blue instead of black
    and slightly bigger. 
    
    Parameter
    --------
    ax: matplotlib axis object
        the axis, where the lines are drawn.
    ensemble: [member, time] numpy ndarray or 2D list
        the Ensemble that should be shown
    varName: string
        the label, that should describe the ylabel of the axes 
    membersOfInterest: list of indices
        indices of these members, that will be displayed in blue, the rest is black. 
    """        
    ax = get_axes(subfig, varName + ", all members")
    #for each member, plot the time series
    for i, ts in enumerate(data): 
# =============================================================================
#         if (membersOfInterest is not None) and (i in membersOfInterest) :
#             #plot only on those time steps, where our ts is defined, usefull for distance ts, which are shorter
#             ax.plot(range(len(ts)), ts,"b",linewidth=1.5)
#         else:
# =============================================================================
            #plot only on those time steps, where our ts is defined, usefull for distance ts, which are shorter
        ax.plot(range(len(ts)), ts,'k',linewidth=1.0)
            
    #subfig.supylabel(varName + ", all members")


def paint_ensemble_spaghetti(subfig, data, vmin, vmax, xticks, xlabels, varName, ylabels = None):
    """ Plots the data as spaghetti plot and makes the subfigure visualy pleasing.
    
    The data is plottet with black lines, without white padding at the beginning
    and a grey grid in the background. The xaxis has ticks and labels as given.
    
    Parameter
    ---------
    subfig: matplotlib figure
        The figure where to plot.
    data: numpy ndArray
        The data to plot, the first dimension is the ensemble member/ trajectory
        the second dimension is the time.
    vmin: float
        The minimal value, that should be shown on the y-axis.
    vmax: float
        The maximal value that should be shown on the y-axis.
    xticks: list of int
        The position/ index, where on the time axis the labels are displayed.
    xlabels: list of string/ datetimes
    
    """
    paint_spaghetti(subfig, data, varName)
    ax = get_axes(subfig)
    regulate_spaghetti_axes(ax, (vmin, vmax), (0, data.shape[1] - 1), xticks, xlabels)


def paint_reference(subfig, reference, window = None):
    """ Draws the reference on the axis. When the reference is as long as the time.
    
    The whole reference is shown in cyan, the time steps between the window are 
    shown in red. Without window and time, the reference is plotted completely
    at the beginning. 
    
    Parameter
    ---------
    ax: matplotlib axis object
        The axis, where the reference is drawn.
    reference: array like
        The data to plot
    time: array like
        The indizes of the time steps. Starting from 0. 
    window: tuple of integer
        The start and end index of time, where the reference is. It is highlighted 
        with black lines at the beginning of the window and the end. 
    """
    
    def paint_red_ribbon(ax, reference, x):
        ax.plot(x, reference, 'r', linewidth=3)
        
    ax = get_axes(subfig)
    #if time is None:
    #    time = range(len(reference))
        
    if window is not None:
        start, end = window 
        time = np.arange(start, end)
        indicate_window(ax, time[0], time[-1])
        if len(reference) < end: #reference is a member of the ensemble, plot the window where it is defined in red
            paint_red_ribbon(ax, reference, time)
        
        else:
            ax.plot(range(len(reference)), reference, 'c',linewidth = 1.0)
            paint_red_ribbon(ax, reference[start:end], time)
            
    else: 
        #no information about where the reference comes from, or where it should fit, just plot it at the beginning
        paint_red_ribbon(ax, reference, range(len(reference)))


def paint_heatmap_ensemble(subfig, data, cmap = "Greys", vmin = 0, vmax = 1, xticks = None, xlabels = None, ylabels = None):
    """ Creates a heatmap with the data on the first axis of the given subfigure.
    And sets the labels accordingly. 
    
    When there is a list of existing axes in the figure, this method assumes 
    that there is a plot on these axes and plottes the heatmap with an alpha 
    value of 0.5. 
    The default values are set to be used for a shadow plot with a mask. 
    Parameter
    ---------
    subfig: matplotlib figure
        The figure, where the heatmap is drawn onto. When there are no axes 
        already, creates a axes for the view and a second axes for the colorbar
        on this figure. Uses the first axes to draw the heatmap on otherwise. 
    data: ndarray
        The data to be visualized. The first dimension is interpreted as the 
        rows and the second dimension as the collums.
    cmap: string
        the name of the colormap matplotlib should use. 
    vmin: float
        the minimal value of the data. It is passed on to matplotlib to ensure
        a tight fit of color to data, so that differences can be seen easily.
    vmax: float
        the maximal value of the data. It is passed on to matplotlib to ensure
        a tight fit of color to data, so that differences can be seen easily.
    xticks: list of integer
        The list of position, where a tick on the x axis should occur.
    xlabels: list of labels
        These labels will be shown at the settes xticks. Therfor, xticks and 
        xlabels must have the same length.
    ylabels: list of labels 
        These labels are set on the y axis of the heatmap. It can contain the 
        names of an ensemble and should have the same length as the first 
        dimension of the data.
    """
    overlay = False
    if isinstance(subfig, plt.Axes):
        ax = subfig
        ax.set_ylabel("Member")
    else: 
        if not subfig.get_axes():
            ax = subfig.subplots()
            subfig.supylabel("Member")
        else:
            ax = subfig.get_axes()[0]
            overlay = True
    
    c = paint_heatmap(ax, data, cmap, vmin, vmax, overlay)

    if not overlay:
        generate_colorbar(c, ax)    
        regulate_xTicks(ax, xlabels, [x + 0.5 for x in xticks])
        regulate_yTicks(ax, ylabels, [y + 0.5 for y in np.arange(len(ylabels))])
         

def generate_horizon_shadow(subfig, data):
    """ Add the shadow of interesting windows to a existing horizon plot.
    
    Parameter
    ---------
    subfig: matplotlib figure
        the figure, where the horizon plot is on
    data: ndarray
        the mask containing the information of the windows for each member. 
        The first dimension is the member, the second the time steps. The values
        are between 0 and 1. zero indicate no window, everything else is a alpha 
        value cooresponding to the distance of that window to the reference. 
    """
    if not subfig.get_axes():
        axs = subfig.subplots()
    else:
        axs = subfig.get_axes()
    
    maxMember = data.shape[0]-1
    for i,oneMember in enumerate(data):
        ax = axs[maxMember-i]
        horizon_shadow_from_mask(ax, oneMember)
        
        
def generate_heatmap_shadow(subfig, data):
    """ Add the shadow of interesting windows to a existing heatmap plot.
    
    Parameter
    ---------
    subfig: matplotlib figure
        the figure, where the horizon plot is on
    data: ndarray
        the mask containing the information of the windows for each member. 
        The first dimension is the member, the second the time steps. The values
        are between 0 and 1. zero indicate no window, everything else is a alpha 
        value cooresponding to the distance of that window to the reference. 
    """
    if isinstance(subfig, plt.Axes) :
        ax = subfig
    else: 
        if not subfig.get_axes():
            ax = subfig.subplots()
        else:
            ax = subfig.get_axes()[0]
        
    _ = paint_heatmap(ax, data, cmap = "Greys", vmin = 0, vmax = 1, overlay = True)


def paint_horizon(subfig, data, cmap, vmin, vmax, xticks, xlabels, ylabels, horizon_base='avg', ranges=None):
    """ Plots the Horizon grapf of the data on the subfigure and sets the labels.
    
    Parameter
    ---------
    subfig: matplotlib figure
        the figure to plot the horizon graph on. Horizon creates as many axes as 
        the size of the first dimension in the data.
    data: ndarray
        The data to be visualized. The first dimension is interpreted as the 
        rows and the second dimension as the collums.
    cmap: string
        the name of the colormap matplotlib should use. 
    vmin: float
        the minimal value of the data. It is passed on to matplotlib to ensure
        a tight fit of color to data, so that differences can be seen easily.
    vmax: float
        the maximal value of the data. It is passed on to matplotlib to ensure
        a tight fit of color to data, so that differences can be seen easily.
    xticks: list of integer
        The list of position, where a tick on the x axis should occur.
    xlabels: list of labels
        These labels will be shown at the settes xticks. Therfor, xticks and 
        xlabels must have the same length.
    ylabels: list of labels 
        These labels are set on the y axis of the heatmap. It can contain the 
        names of an ensemble and should have the same length as the first 
        dimension of the data.
    horizon_base: string
        Optional, default average ('avg'). Adjusts the base of the horizon plot.
    """
    data = np.flip(data, 0)

    if horizon_base == 'min':
        base = vmin
    elif horizon_base == 'max':
        base = vmax
    elif horizon_base == 'avg':
        base = get_horizon_base(vmin, vmax)
    else:
        base = get_horizon_base(vmin, vmax)

    horizon(subfig, data, vmin, vmax, cmap=cmap, base=base, memberlabels=list(ylabels), horizon_base=horizon_base,
            ranges=ranges)
    regulate_xTicks(subfig.get_axes()[-1], xlabels, xticks)

    subfig.supylabel("Member") 
    
    
    
    #%%inside
def paint_heatmap(ax, data, cmap, vmin, vmax, overlay = False):
    """ Creates a heatmap of data, using matplotlib's pcolor.
    
    Parameter
    ---------
    ax: matplotlib axis object
        the axis to draw the heatmap on
    data: numpy ndarray
        the data to use, the first dimension is the members/instances the second
        dimension is the time(steps)
    cmap: string
        the colormap according to matplotlib: 
        https://matplotlib.org/stable/tutorials/colors/colormaps.html
    vmin: number
        the minimal value of the data, start of the colormap
    vmax: number
        the maximal value of the data, end of the colormap
    overlay: boolean
        when true, the data is considered an overlay to something else. As such
        it uses an alpha value of 0.5
        
    Returns
    -------
    matplotlib.collections.Collection
    """
    alpha = 1 if not overlay else 0.5

    return ax.pcolor(data, vmin = vmin, vmax = vmax, cmap = cmap, alpha = alpha)    


def horizon_shadow_from_mask(ax, memberMask):
    """ On a given axes, fill in the spaces that are indicated with the memberMask.
    
    Parameters
    ----------
    ax: matplotlib axes object
        The axes to be manipulated.
    memberMask: array like
        One dimensional array containing information about the windows to be 
        highlighted. The length of the mask is the same as whatever is plotted on
        the axes. The value in the position is the alpha value that should be used.
        When a value greater than 0 is there, the alpha value is used to fill the 
        space to the next position that is zero or the end of the time series. 
        To ensure that the original stuff of the axes can still be seen, the 
        alpha value is again multiplied by 0.5 (for now)
    """
    intervals = getRuns(memberMask > 0)
    b,t = ax.get_ylim()
    for s,e in intervals:
        ax.fill_between([s,e], t,b, alpha = memberMask[s]*0.5, color = "k")

    ax.set_ylim((b,t))
 

#%% preconfig functions
def paint_ensemble_heatmap(data, varName, width, coreHeight, outerHeight, ylims, xticks, xlabels, ylabels, cmap, title = None):
    """Combines several functions for plotting to create a picture of the 
    ensemble data.
    
    There are two views. One at the top showing all members in one Spaghetti plot 
    and one where the member is displayed below each other in a heatmap.
    
    Parameters
    ----------
    data: ndarray
        The data to be visualized. The first dimension is intepreted as the member
        the second dimenstion is the time. 
    varName: string
        The name of the variable, where the data came from. It is used as a ylabel
        on the Spaghetti view and in the defaut title, when none is given. 
    width: float
        the width of the figure, this includes both views.
    coreHeight: float
        the height of the heatmap.
    outerHeight: float
        the height of one Spaghetti-plot.
    ylims: tuple of floats
        The minimal and maximal values of the data, that should be displayed. 
        Also effects the colorbar of the heatmap.
    xticks: list of ints
        The points in time, where a label should be placed.
    xlabels: list of strings
        The lables to display on the time axis.
    ylabels: list of labels 
        These labels are set on the y axis of the heatmap. It can contain the 
        names of an ensemble and should have the same length as the first 
        dimension of the data (ensemble members).
    cmap: string
        the name of the colormap matplotlib should use for the heatmap. 
    title: string
        The potential title of the figure. Will be displayed at the top.       
    """
    
    fig = generate_figure(width,coreHeight, outerHeight, 2)
    if title is None: #set default title, that is somewhat meaningfull
        title = "Ensemble: " + varName
    
    fig.suptitle(title)
    subfigs = generate_subfigures(fig, 2, coreHeight, outerHeight)
    
    paint_ensemble_spaghetti(subfigs[0], data, vmin = ylims[0], vmax = ylims[1], xticks = xticks, 
                               xlabels = xlabels, varName = varName)
    
    paint_heatmap_ensemble(subfigs[1], data, cmap = cmap, vmin = ylims[0], vmax = ylims[1], 
                             xticks = xticks, xlabels = xlabels, ylabels = ylabels)
    
    fig.show()
    

def paint_ensemble_horizon(data, varName, width, coreHeight, outerHeight, ylims, xticks, xlabels, ylabels, cmap,
                           horizon_base='avg', title = None):
    """ Combines several functions for plotting to create a picture of the 
    ensemble data.
    
    There are two views. One at the top showing all members in one Spaghetti plot 
    and one where the member is displayed below each other in a Horizon plot.
    
    Parameters
    ----------
    data: ndarray
        The data to be visualized. The first dimension is intepreted as the member
        the second dimenstion is the time. 
    varName: string
        The name of the variable, where the data came from. It is used as a ylabel
        on the Spaghetti view and in the defaut title, when none is given. 
    width: float
        the width of the figure, this includes both views.
    coreHeight: float
        the height of the Horizon plot.
    outerHeight: float
        the height of one Spaghetti-plot.
    ylims: tuple of floats
        The minimal and maximal values of the data, that should be displayed. 
        Also effects the colorbar of the Horizon plot.
    xticks: list of ints
        The points in time, where a label should be placed.
    xlabels: list of strings
        The lables to display on the time axis.
    ylabels: list of labels 
        These labels are set on the y axis of the Horizon Plot. It can contain the 
        names of an ensemble and should have the same length as the first 
        dimension of the data (ensemble members).
    cmap: string
        the name of the colormap matplotlib should use for the Horizon Plot.
    horizon_base: string
        Optional, default average ('avg'). Adjusts the base of the horizon plot.
    title: string
        The potential title of the figure. Will be displayed at the top.       
    """
    
    fig = generate_figure(width,coreHeight, outerHeight, 2)
    if title is None: #set default title, that is somewhat meaningfull
        title = "Ensemble: " + varName
    
    fig.suptitle(title)
    subfigs = generate_subfigures(fig, 2, coreHeight, outerHeight)
    
    paint_ensemble_spaghetti(subfigs[0], data, vmin = ylims[0], vmax = ylims[1], xticks = xticks, 
                               xlabels = xlabels, varName = varName)
    
    paint_horizon(subfigs[1], data, cmap = cmap, vmin = ylims[0], vmax = ylims[1],
                  xticks = xticks, xlabels = xlabels, ylabels = ylabels, horizon_base=horizon_base)
    
    fig.show()
  
    
def paint_ens_with_reference_heatmap(data, varName, width, coreHeight, outerHeight, ylims, xticks, xlabels, ylabels, cmap, reference, referenceTime, title = None, referenceLabel = None):
    """ Combines several functions for plotting to create a picture of the 
    ensemble data, where one of the members is a potential reference.
    
    There are three views. One at the top showing all members in one Spaghetti 
    plot, one where the member is displayed below each other in a heatmap and 
    one view, where a certain member of the ensemble, a potential reference, is
    shown in more detail alone in its own spaghetti view, below the heatmap.
    
    Parameters
    ----------
    data: ndarray
        The data to be visualized. The first dimension is intepreted as the member
        the second dimenstion is the time. 
    varName: string
        The name of the variable, where the data came from. It is used as a ylabel
        on the Spaghetti view and in the defaut title, when none is given. 
    width: float
        the width of the figure, this includes all views.
    coreHeight: float
        the height of the heatmap.
    outerHeight: float
        the height of one Spaghetti-plot.
    ylims: tuple of floats
        The minimal and maximal values of the data, that should be displayed. 
        Also effects the colorbar of the heatmap.
    xticks: list of ints
        The points in time, where a label should be placed.
    xlabels: list of strings
        The lables to display on the time axis.
    ylabels: list of labels 
        These labels are set on the y axis of the heatmap. It can contain the 
        names of an ensemble and should have the same length as the first 
        dimension of the data (ensemble members).
    cmap: string
        the name of the colormap matplotlib should use for the heatmap. 
    reference: array like
        The time series, that is considered a reference. It will be displayed in red
        in the Spaghetti-plots
    referenceTime: tuple of int
        The start and end index of time, where the reference is. It is highlighted 
        with black lines at the beginning of the window and the end in the close 
        up view of the reference.
    title: string
        The potential title of the figure. Will be displayed at the top.   
    referenceLabel: string
        A string version of the label that belonged to the reference. When given
        it will be used in a y label for the close up view of the reference.
    """
    assert isinstance(reference, np.ndarray) or isinstance(reference, list)
    
    fig = generate_figure(width,coreHeight, outerHeight, 3)
    if title is None: #set default title, that is somewhat meaningfull
        title = "Ensemble: " + varName
    
    fig.suptitle(title)
    subfigs = generate_subfigures(fig, 3, coreHeight, outerHeight)
        
    paint_ensemble_spaghetti(subfigs[0], data, vmin = ylims[0], vmax = ylims[1], 
                             xticks = xticks, xlabels = xlabels, varName = varName)
    if referenceLabel is not None:
        oneYLabel = "member: "+str(referenceLabel)+ "\n reference"
        paint_reference(subfigs[0], reference, window = None)      
    else: #reference is not a member
        oneYLabel = "reference"
        paint_reference(subfigs[0], reference, window = referenceTime)
    
    paint_reference(subfigs[2], reference, window = referenceTime)
    ax = get_axes(subfigs[2], oneYLabel)
    regulate_spaghetti_axes(ax, ylims, (0,len(data[0])), xticks, xlabels)
    
    paint_heatmap_ensemble(subfigs[1], data, cmap = cmap, vmin = ylims[0], 
                           vmax = ylims[1], xticks = xticks, xlabels = xlabels,
                           ylabels = ylabels)
    fig.show()
    
    
def paint_ens_with_reference_horizon(data, varName, width, coreHeight, outerHeight, ylims, xticks, xlabels, ylabels,
                                     cmap, reference, referenceTime, horizon_base='avg', title = None,
                                     referenceLabel = None):
    """ Combines several functions for plotting to create a picture of the 
    ensemble data, where one of the members is a potential reference.
    
    There are three views. One at the top showing all members in one Spaghetti 
    plot, one where the member is displayed below each other in a Horizon Plot and 
    one view, where a certain member of the ensemble, a potential reference, is
    shown in more detail alone in its own spaghetti view, below the Horizon Plot.
    
    Parameters
    ----------
    data: ndarray
        The data to be visualized. The first dimension is intepreted as the member
        the second dimenstion is the time. 
    varName: string
        The name of the variable, where the data came from. It is used as a ylabel
        on the Spaghetti view and in the defaut title, when none is given. 
    width: float
        the width of the figure, this includes all views.
    coreHeight: float
        the height of the Horizon Plot.
    outerHeight: float
        the height of one Spaghetti-plot.
    ylims: tuple of floats
        The minimal and maximal values of the data, that should be displayed. 
        Also effects the colorbar of the Horizon Plot.
    xticks: list of ints
        The points in time, where a label should be placed.
    xlabels: list of strings
        The lables to display on the time axis.
    ylabels: list of labels 
        These labels are set on the y axis of the Horizon Plot. It can contain the 
        names of an ensemble and should have the same length as the first 
        dimension of the data (ensemble members).
    cmap: string
        the name of the colormap matplotlib should use for the Horizon Plot. 
    reference: array like
        The time series, that is considered a reference. It will be displayed in red
        in the Spaghetti-plots
    referenceTime: tuple of int
        The start and end index of time, where the reference is. It is highlighted 
        with black lines at the beginning of the window and the end in the close 
        up view of the reference.
    horizon_base: string
        Optional, default average ('avg'). Adjusts the base of the horizon plot.
    title: string
        The potential title of the figure. Will be displayed at the top.   
    referenceLabel: string
        A string version of the label that belonged to the reference. When given
        it will be used in a y label for the close up view of the reference.
    """
    assert isinstance(reference, np.ndarray) or isinstance(reference, list)
    
    fig = generate_figure(width,coreHeight, outerHeight, 3)
    if title is None: #set default title, that is somewhat meaningfull
        title = "Ensemble: " + varName
    
    fig.suptitle(title)
    subfigs = generate_subfigures(fig, 3, coreHeight, outerHeight)
        
    paint_ensemble_spaghetti(subfigs[0], data, vmin = ylims[0], vmax = ylims[1], 
                             xticks = xticks, xlabels = xlabels, varName = varName)
    if referenceLabel is not None:
        oneYLabel = "member: "+str(referenceLabel)+ "\n reference"
        paint_reference(subfigs[0], reference, window = None)      
    else: #reference is not a member
        oneYLabel = "reference"
        paint_reference(subfigs[0], reference, window = referenceTime)
    
    paint_reference(subfigs[2], reference, window = referenceTime)
    ax = get_axes(subfigs[2], oneYLabel)
    regulate_spaghetti_axes(ax, ylims, (0,len(data[0])), xticks, xlabels)
    
    paint_horizon(subfigs[1], data, cmap = cmap, vmin = ylims[0], vmax = ylims[1],
                  xticks = xticks, xlabels = xlabels, ylabels = ylabels, horizon_base=horizon_base)
    fig.show()
 
    
def paint_ens_windows_heatmap(data, varName, width, coreHeight, outerHeight, ylims, xticks, xlabels, ylabels, cmap, reference, referenceTime, mask, title = None, referenceLabel = None):
    """ Combines several functions for plotting to create a picture of the 
    ensemble data and the distance to a reference, showing the reference as well.
    
    There are three views. One at the top showing all members in one Spaghetti 
    plot, one where the member is displayed below each other in a heatmap, where
    the prts that are similar to the reference are shown as a grey overlay over 
    the data.
    Lastly, there is one view, where a certain member of the ensemble, a 
    potential reference, is shown in more detail alone in its own spaghetti 
    view, below the heatmap.
    
    Parameters
    ----------
    data: ndarray
        The data to be visualized. The first dimension is intepreted as the member
        the second dimenstion is the time. 
    varName: string
        The name of the variable, where the data came from. It is used as a ylabel
        on the Spaghetti view and in the defaut title, when none is given. 
    width: float
        the width of the figure, this includes all views.
    coreHeight: float
        the height of the heatmap.
    outerHeight: float
        the height of one Spaghetti-plot.
    ylims: tuple of floats
        The minimal and maximal values of the data, that should be displayed. 
        Also effects the colorbar of the heatmap.
    xticks: list of ints
        The points in time, where a label should be placed.
    xlabels: list of strings
        The lables to display on the time axis.
    ylabels: list of labels 
        These labels are set on the y axis of the heatmap. It can contain the 
        names of an ensemble and should have the same length as the first 
        dimension of the data (ensemble members).
    cmap: string
        the name of the colormap matplotlib should use for the heatmap. 
    reference: array like
        The time series, that is considered a reference. It will be displayed in red
        in the Spaghetti-plots
    referenceTime: tuple of int
        The start and end index of time, where the reference is. It is highlighted 
        with black lines at the beginning of the window and the end in the close 
        up view of the reference.
    mask: ndarray
        An ndarray with the same dimension as the data. The values range from 0 
        to 1 and represent the distance to the reference for each member and time step.
        It will interpreted as a grey mask over the data, showing which member 
        is similar to the reference for each time step. The darker the color, 
        the more smilar it is. 
    title: string
        The potential title of the figure. Will be displayed at the top.   
    referenceLabel: string
        A string version of the label that belonged to the reference. When given
        it will be used in a y label for the close up view of the reference.
    """
    assert isinstance(reference, np.ndarray) or isinstance(reference, list)
    
    fig = generate_figure(width,coreHeight, outerHeight, 3)
    if title is None: #set default title, that is somewhat meaningfull
        title = "Ensemble: " + varName +"\n windows with a difference lower than a threshold"
    
    fig.suptitle(title)
    subfigs = generate_subfigures(fig, 3, coreHeight, outerHeight)
        
    #self.__show_reference(views[0],views[2], reference, referenceLabel, referenceTimeWindow)
    paint_ensemble_spaghetti(subfigs[0], data, vmin = ylims[0], vmax = ylims[1], xticks = xticks, 
                             xlabels = xlabels, varName = varName)
    if referenceLabel is not None:
        oneYLabel = "member: "+str(referenceLabel)+ "\n reference"
        paint_reference(subfigs[0], reference, window = None)      
    else: #reference is not a member
        oneYLabel = "reference"
        paint_reference(subfigs[0], reference, window = referenceTime)
    
    paint_reference(subfigs[2], reference, window = referenceTime)
    ax = get_axes(subfigs[2], oneYLabel)
    regulate_spaghetti_axes(ax, ylims, (0,len(data[0])), xticks, xlabels)
    
    paint_heatmap_ensemble(subfigs[1], data, cmap = cmap, vmin = ylims[0], vmax = ylims[1], 
                           xticks = xticks, xlabels = xlabels, ylabels = ylabels)
    generate_heatmap_shadow(subfigs[1], mask)
    
    for ax in subfigs[1].get_axes():
        indicate_window(ax, referenceTime[0], referenceTime[1], 'g')
    
    fig.show()
    
    
def paint_ens_windows_horizon(data, varName, width, coreHeight, outerHeight, ylims, xticks, xlabels, ylabels, cmap,
                              reference, referenceTime, mask, horizon_base='avg', title = None, referenceLabel = None):
    """ Combines several functions for plotting to create a picture of the 
    ensemble data and the distance to a reference, showing the reference as well.
    
    There are three views. One at the top showing all members in one Spaghetti 
    plot, one where the member is displayed below each other in a Horizon Plot, 
    where the prts that are similar to the reference are shown as a grey 
    overlay over the data.
    Lastly, there is one view, where a certain member of the ensemble, a 
    potential reference, is shown in more detail alone in its own spaghetti 
    view, below the Horizon Plot.
    
    Parameters
    ----------
    data: ndarray
        The data to be visualized. The first dimension is intepreted as the member
        the second dimenstion is the time. 
    varName: string
        The name of the variable, where the data came from. It is used as a ylabel
        on the Spaghetti view and in the defaut title, when none is given. 
    width: float
        the width of the figure, this includes all views.
    coreHeight: float
        the height of the Horizon Plot.
    outerHeight: float
        the height of one Spaghetti-plot.
    ylims: tuple of floats
        The minimal and maximal values of the data, that should be displayed. 
        Also effects the colorbar of the Horizon Plot.
    xticks: list of ints
        The points in time, where a label should be placed.
    xlabels: list of strings
        The lables to display on the time axis.
    ylabels: list of labels 
        These labels are set on the y axis of the Horizon Plot. It can contain the 
        names of an ensemble and should have the same length as the first 
        dimension of the data (ensemble members).
    cmap: string
        the name of the colormap matplotlib should use for the Horizon Plot.
    reference: array like
        The time series, that is considered a reference. It will be displayed in red
        in the Spaghetti-plots
    referenceTime: tuple of int
        The start and end index of time, where the reference is. It is highlighted 
        with black lines at the beginning of the window and the end in the close 
        up view of the reference.
    mask: ndarray
        An ndarray with the same dimension as the data. The values range from 0 
        to 1 and represent the distance to the reference for each member and time step.
        It will interpreted as a grey mask over the data, showing which member 
        is similar to the reference for each time step. The darker the color, 
        the more smilar it is.
    horizon_base: string
        Optional, default average ('avg'). Adjusts the base of the horizon plot.
    title: string
        The potential title of the figure. Will be displayed at the top.   
    referenceLabel: string
        A string version of the label that belonged to the reference. When given
        it will be used in a y label for the close up view of the reference.
    """
    assert isinstance(reference, np.ndarray) or isinstance(reference, list)
    
    fig = generate_figure(width,coreHeight, outerHeight, 3)
    if title is None: #set default title, that is somewhat meaningfull
        title = "Ensemble: " + varName + "\n windows with a difference lower than a threshold"
    
    fig.suptitle(title)
    subfigs = generate_subfigures(fig, 3, coreHeight, outerHeight)
        
    paint_ensemble_spaghetti(subfigs[0], data, vmin = ylims[0], vmax = ylims[1], xticks = xticks, 
                             xlabels = xlabels, varName = varName)
    if referenceLabel is not None:
        oneYLabel = "member: "+str(referenceLabel)+ "\n reference"
        paint_reference(subfigs[0], reference, window = None)      
    else: #reference is not a member
        oneYLabel = "reference"
        paint_reference(subfigs[0], reference, window = referenceTime)
    
    paint_reference(subfigs[2], reference, window = referenceTime)
    ax = get_axes(subfigs[2], oneYLabel)
    regulate_spaghetti_axes(ax, ylims, (0,len(data[0])), xticks, xlabels)
    
    paint_horizon(subfigs[1], data, cmap = cmap, vmin = ylims[0], vmax = ylims[1],
                  xticks = xticks, xlabels = xlabels, ylabels = ylabels, horizon_base=horizon_base)
    generate_horizon_shadow(subfigs[1], mask)
    
    for ax in subfigs[1].get_axes():
        indicate_window(ax, referenceTime[0], referenceTime[1], 'g')
    
    fig.show()


def adjustVisual(figs, t, remove_x_label=False):
    """
    Adjusts the visual appearance of the given figures, this includes the rotation of the labels and axis lables
    :param figs: figures
    :param t: list of strings for X labels, normally a date and time strong
    :param remove_x_label: remove some x labels if not needed
    :return:
    """
    for fig in figs:
        axs = fig.get_axes()
        if len(axs) > 2:
            axs[-1].set_xticklabels(t)
        else:
            axs[0].set_xticklabels(t)

    if remove_x_label:
        axs = figs[0].get_axes()
        if len(axs) > 2:
            axs[-1].set_xticklabels([])
            ax1_divider = make_axes_locatable(axs[-1])
        else:
            axs[0].set_xticklabels([])
            ax1_divider = make_axes_locatable(axs[0])
        cax1 = ax1_divider.append_axes("left", size="7%", pad="2%")
        cax1.axis("off")


def barplot_distance(ds_dist, order=None, ref_member=None):
    """
    Plots the distances as barplot between ensemble members
    :param ds_dist: distance dataset
    :param order: list of tuples (member number , position)
    :param ref_member: reference member, to which the distance was computed
    :return:
    """
    if  (ref_member is not None) and order:
        order = [i for i in order if i[0] != ref_member]
        distance_values = np.array([ds_dist['ens'].squeeze().data, ds_dist['distance'].squeeze().data])

    elif ref_member is not None:
        distance_values = np.array([np.delete(ds_dist['ens'].squeeze(), ref_mem).data, np.delete(ds_dist['distance'].squeeze(), ref_mem).data])

    else:
        distance_values = np.array([ds_dist['ens'].squeeze().data, ds_dist['distance'].squeeze().data])

    if order:
        sort_idx = [i[0] for i in order]
        distance_values = [distance_values[0][sort_idx], distance_values[1][sort_idx]]

    fig = plt.figure(figsize=(12,5), dpi=500)
    ax = fig.add_axes([0,0,1,1])
    x_values = distance_values[0].astype(int).astype(str)
    y_values = distance_values[1].astype(float)
    ax.bar(x_values, y_values)
    ax.set_xlim([-0.6, len(x_values) - 0.4])
    plt.xlabel("Member")
    plt.ylabel("Distance to reference member")
    plt.show()


def heatmap(data, row_labels, col_labels, title="", ax=None,
            cbar_kw=None, cbarlabel="", **kwargs):
    """
    Create a heatmap from a numpy array and two lists of labels.

    Parameters
    ----------
    data
        A 2D numpy array of shape (M, N).
    row_labels
        A list or array of length M with the labels for the rows.
    col_labels
        A list or array of length N with the labels for the columns.
    ax
        A `matplotlib.axes.Axes` instance to which the heatmap is plotted.  If
        not provided, use current axes or create a new one.  Optional.
    cbar_kw
        A dictionary with arguments to `matplotlib.Figure.colorbar`.  Optional.
    cbarlabel
        The label for the colorbar.  Optional.
    **kwargs
        All other arguments are forwarded to `imshow`.
    """

    if ax is None:
        ax = plt.gca()

    if cbar_kw is None:
        cbar_kw = {}

    # Plot the heatmap
    im = ax.imshow(data, **kwargs)

    # Create colorbar
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.2)

    cax = cax.figure.colorbar(im, cax=cax, **cbar_kw)
    cax.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")

    # Show all ticks and label them with the respective list entries.
    ax.set_xticks(np.arange(data.shape[1]), labels=col_labels)
    ax.set_yticks(np.arange(data.shape[0]), labels=row_labels)
    ax.set_title(title)

    ax.tick_params(axis='y', which='major', pad=10)

    # Let the horizontal axes labeling appear on top.
    #ax.tick_params(top=True, bottom=False,
    #               labeltop=True, labelbottom=False)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=0, ha="center",
             rotation_mode="anchor")

    plt.setp(ax.get_yticklabels(), rotation=90, ha="center",
             rotation_mode="anchor")

    # Turn spines off and create white grid.
    ax.spines[:].set_visible(False)

    ax.set_xticks(np.arange(data.shape[1]+1)-.5, minor=True)
    ax.set_yticks(np.arange(data.shape[0]+1)-.5, minor=True)
    ax.grid(which="minor", color="w", linestyle='-', linewidth=3)
    ax.tick_params(which="minor", bottom=False, left=False)

    return im, cax


def annotate_heatmap(im, data=None, valfmt="{x:.2f}",
                     textcolors=("black", "white"),
                     threshold=None, **textkw):
    """
    A function to annotate a heatmap.

    Parameters
    ----------
    im
        The AxesImage to be labeled.
    data
        Data used to annotate.  If None, the image's data is used.  Optional.
    valfmt
        The format of the annotations inside the heatmap.  This should either
        use the string format method, e.g. "$ {x:.2f}", or be a
        `matplotlib.ticker.Formatter`.  Optional.
    textcolors
        A pair of colors.  The first is used for values below a threshold,
        the second for those above.  Optional.
    threshold
        Value in data units according to which the colors from textcolors are
        applied.  If None (the default) uses the middle of the colormap as
        separation.  Optional.
    **kwargs
        All other arguments are forwarded to each call to `text` used to create
        the text labels.
    """

    if not isinstance(data, (list, np.ndarray)):
        data = im.get_array()

    # Normalize the threshold to the images color range.
    if threshold is not None:
        threshold = im.norm(threshold)
    else:
        threshold = im.norm(data.max())/2.

    # Set default alignment to center, but allow it to be
    # overwritten by textkw.
    kw = dict(horizontalalignment="center",
              verticalalignment="center")
    kw.update(textkw)

    # Get the formatter in case a string is supplied
    if isinstance(valfmt, str):
        valfmt = ticker.StrMethodFormatter(valfmt)

    # Loop over the data and create a `Text` for each "pixel".
    # Change the text's color depending on the data.
    texts = []
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            kw.update(color=textcolors[int(im.norm(data[i, j]) > threshold)])
            text = im.axes.text(j, i, valfmt(data[i, j], None), **kw)
            texts.append(text)

    return texts


def evaluate_silhouette_score(score):
    plt.plot(score.transpose())
    plt.grid(False)
    plt.legend(["Euclidean", "DTW", "Soft-DTW"])
    plt.xticks(np.arange(0, 9, 2), np.arange(2,11,2))
    plt.xlabel("Number of cluster")
    plt.ylabel("Silhouette score")
    plt.tight_layout()