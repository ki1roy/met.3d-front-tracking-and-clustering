import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
from data_transformer import DataTransformer

#TODO: labels for data between 0 and 1 are wrong. Start always from 0. 
#TODO: mode that does not create a figure, but workes on a axis.

class InputError(Exception):
  def __init__(self, value):
    self.value = value
  def __str__(self):
    return np.repeat(self.value)

class Horizon:

  # public methods

  def run(self, x, y, labels, figsize=(15,20), bands=3, colors=("#EF9483","#E02421", "#A90E0A", "#8BBCD4","#2B7ABD","#0050A0")): #  dark red, medium red, light red,dark blue, medium blue, light blue,
    """ Return the entire graph and its plt object

        Look at DataTransformer.transform to see how the data is transformed.

        Keyword arguments:
        x: single array with x values. Distance between neighboring entries have to be the same
        y: two-dimensional array with y values for each entry.
        labels: array with strings, shown as the labels on the y-axis.
        figsize: (a,b) used when creating the figure (optional)
        bands: default is 3
        colors: array with the colors used for the bands. from dark to light blue, then from dark red to light red.

        Requirements:
        len(y[i]) == len(x) for all 0 <= i < len(y)
        len(y[0]) == len(labels)
        len(colors) == 2*bands

        RETURN: plt object
    """

    self.check_valid_params(x,y,labels,figsize,bands,colors) 
    n = len(y[0,:])

    F, axes = plt.subplots(n, 1, figsize=figsize, sharex=True, sharey=True)
    df = DataTransformer(y, bands)

    for i, ax in enumerate(axes.flatten()):
      transformed_x, ybands = df.transform(y[:,i], x)
      for idx,band in enumerate(ybands):
        ax.fill_between(transformed_x[idx],0,band,color=colors[idx])
      self.adjust_visuals_line(x, df, ax, i, n, labels)

    handles = []
    legend_colors=[colors[2],colors[1],colors[0],colors[3],colors[4],colors[5]]
    for c in legend_colors:
      handles.append(self.patch_creator(c))
      
    bandwidths = int(df.max)/bands #int() might cause it to go 0
    if bandwidths == 0:
        bandwidths = df.max/bands
        lowerbounds = np.arange(df.min, df.max, bandwidths)
        labels = [str(int(b))+' - '+str(round(b+bandwidths,4)) for b in lowerbounds]
        
    else:
        lowerbounds = np.arange(int(df.min), int(df.max), bandwidths)
        labels = [str(int(b))+' - '+str(int(b+bandwidths)) for b in lowerbounds]

    labels.reverse() #changed for vis with one vertical space
    F.legend(handles, labels, ncol = 1, loc='center right',bbox_to_anchor = (1.01,0.5), fontsize='x-large')

    return plt


  # private methods
  def patch_creator(self, color):
    patch = mpatches.Rectangle((0, 0), 1, 1, fc=color)
    return patch
  #def set_theme(self,ax):
    # """ hides all ticks and labels on both axes """
    # ax.get_yaxis().set_visible(False)
    # ax.get_xaxis().set_visible(False)

    
  def adjust_visuals_line(self, x, df, ax, i, n, labels):
    """ adjusts the subplot: height, width, labels """
    plt.xlim(0, x[-1])
    plt.ylim(0, df.get_max()/3)
    #self.set_theme(ax)
    ax.set_yticks([])
    ax.set_ylabel(labels[i], rotation = 45)
    ax.set_xticks([])
    
    if i==0:
      ax.set_yticks(np.arange(0, 100, 20))

    
  def check_valid_params(self, x,y,labels, figsize, bands, colors):
    """ checks parameters, throws an InputError if parameters are invalid """

    if bands * 2 != len(colors):
      raise InputError("Number of bands invalid for number of colors")

    if len(y[0,:]) != len(labels):
      raise InputError("Lengths of arrays y and labels are different")

    if len(x) != len(y[:,0]):
      raise InputError("Lengths of arrays x and y are different")
