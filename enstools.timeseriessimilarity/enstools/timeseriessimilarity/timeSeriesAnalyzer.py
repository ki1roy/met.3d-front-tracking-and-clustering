from .dataFunctions import prepare_dataset, compute_rand_score
import xarray as xr
import numpy as np
import pandas as pd
from .plotterFunctions import adjustVisual, barplot_distance, heatmap, annotate_heatmap, \
    evaluate_silhouette_score
import timeseriessimilarity.tsPlotter as tp
import timeseriessimilarity.distanceMeasure as dm
import timeseriessimilarity.clustering as cl
from timeseriessimilarity.getterFunctions import get_xlabels
import matplotlib.pyplot as plt


class TimeSeriesAnalyzer:
    def __init__(self, file_path):
        self.ds = prepare_dataset(xr.open_dataset(file_path))
        self.variable_names = list(self.ds.keys())
        self.plotter = None
        self.time_labels = None
        self.variable = None
        self.cluster = None
        self.tick_distance = None
        self.df_robustness = None
        self.ranges = None

        print("Variables in your dataset:")
        print(*self.variable_names, sep=", ")

    def create_plotter(self, variable, tick_distance=6, variable_range=None, outer_height=3, horizon_base='avg', cmap='plasma'):
        self.tick_distance = tick_distance
        self.time_labels = get_xlabels(self.ds, tick_distance)

        self.variable = variable
        if variable_range is None:
            variable_range = (np.nanmin(self.ds[variable].values), np.nanmax(self.ds[variable].values))
            variable_range = (round(variable_range[0], 1) - 0.1, round(variable_range[1], 1) + 0.1)

        self.plotter = tp.tsPlotter(self.ds, variable, tick_distance, variable_range, cmap, outerHeight=outer_height,
                                    horizonBase=horizon_base)

    def visualize(self, reference_member=0):
        if self.plotter is None:
            print("Please create a plotter before executing this function. You can create a plotter by executing "
                  "this.create_plotter")
        else:
            p = self.plotter.visualize_with_reference(reference_member, view=0, referenceInEnsemble=True, title="")
            adjustVisual(p, self.time_labels, remove_x_label=True)

    def visualize_distance_to_reference(self, distance_wrapper, reference_member=0, time_distance=10):
        ds_dist = dm.dataset_ensemble_window_search(self.ds.sel(ens=reference_member)[self.variable].values, self.ds,
                                                    self.variable, distance_wrapper)

        ds_window = dm.get_windows_of_interest(ds_dist, time_distance)
        o = dm.get_order(ds_window, "dist", self.ds.ens.values, reference_member)
        barplot_distance(ds_dist, ref_member=reference_member, order=o)

        p = self.plotter.visualize_with_order(o, view=0, title="", ranges=self.ranges)
        adjustVisual(p, self.time_labels, True)

    def change_variable(self, variable=None):
        self.variable = variable

    def create_clusterer(self):
        self.cluster = cl.TimeSeriesClustering(self.ds, self.variable)

    def silhouette_score_analysis(self, round_score=2, gamma=0.01):
        if self.cluster is None:
            print("Please create a clusterer before executing this function. You can create a plotter by executing "
                  "this.create_clusterer")
            return

        eval_sil_score = dict()
        score = np.empty((3, 9))

        for i in range(2, 11):
            self.cluster.generate_cluster(n_cluster=i, gamma=gamma)
            eval_sil_score[i] = self.cluster.get_silhouette_score()
            score[:, i - 2] = eval_sil_score[i].score.values

        evaluate_silhouette_score(score)

        print("Silhouette score values:")
        print(score.round(round_score))

    def create_cluster_analysis(self, number_cluster=3, figure_size=(16, 9), dpi=120, gamma=0.01, ranges=None):
        if self.cluster is None:
            print("Please create a clusterer before executing this function. You can create a plotter by executing "
                  "this.create_clusterer")
            return
        self.cluster.generate_cluster(n_cluster=number_cluster, gamma=gamma)
        # self.cluster.plot_cluster(self.tick_distance, figure_size=[16,9],dpi=120)
        self.cluster.plot_cluster1(self.tick_distance, figure_size=figure_size, dpi=dpi)
        self.ranges = ranges

        ds_cluster = self.cluster.get_cluster()
        for m in ds_cluster['method'].values:
            #arr_cl_var = self.cluster.get_cluster().sel({"method": "dtw"}).cluster.values
            arr_cl_var = self.cluster.get_cluster().sel({"method": m}).cluster.values
            index = range(0, len(arr_cl_var))
            cl_order = list(zip(arr_cl_var, index))
            cl_order.sort()
            for i in range(0, len(arr_cl_var)):
                cl_order[i] = (cl_order[i][1], i)

            p = self.plotter.visualize_with_order(cl_order, view=0, title=m, ranges=self.ranges)
            adjustVisual(p, self.time_labels, True)

    def prepare_robustness_analysis(self, variable_names, number_cluster=3, plot_cluster=False, gamma=0.01):
        cluster_of_variables = {}
        for var in variable_names:
            clust_of_variable = cl.TimeSeriesClustering(self.ds, var)
            clust_of_variable.generate_cluster(n_cluster=number_cluster, gamma=gamma)
            cluster_of_variables[var] = clust_of_variable
            if plot_cluster:
                clust_of_variable.plot_cluster(self.tick_distance, figure_size=[16, 9], dpi=120)

        first = True
        df_cluster = xr.Dataset
        for var in variable_names:
            df = cluster_of_variables[var].get_cluster()
            df['method'] = [n + '_' + var for n in df['method'].values]
            if first:
                df_cluster = df
                first = False
            else:
                df_cluster = xr.concat([df_cluster, df], dim='method')

        df_column_names = list(df_cluster['method'].values)
        df_index_names = list(df_cluster['ens'].values)

        dist_array = df_cluster['cluster'].values.transpose()
        df_robustness = pd.DataFrame(data=dist_array, index=df_index_names, columns=df_column_names)

        self.df_robustness = df_robustness.reindex(sorted(df_robustness.columns), axis=1)

    def plot_robustness(self, column_names, column_labels=[], figsize=(7, 6), dpi=250, cmap='viridis'):
        if self.df_robustness is None:
            print("Please prepare cluster robustness before executing this function. "
                  "You can prepare the robustness analysis by executing this.prepare_robustness_analysis")
            return
        r_scores = compute_rand_score(self.df_robustness, column_names)
        r_scores_no_self = r_scores.copy()

        for i in range(0, len(column_names) - 1):
            for j in range(i + 1, len(column_names)):
                r_scores[i, j] = None

        for i in range(0, len(column_names)):
            for j in range(i, len(column_names)):
                r_scores_no_self[i, j] = None

        print("Mean robustness score: " + str(np.nanmean(r_scores_no_self)))

        fig, ax = plt.subplots(figsize=figsize, dpi=dpi)
        ax.grid(False)
        im, cbar = heatmap(r_scores, column_labels, column_labels, ax=ax,
                           cmap=cmap, cbarlabel="Rand score", vmin=0.5, vmax=1)
        texts = annotate_heatmap(im, valfmt="{x:.2f}")

        fig.tight_layout()
        plt.show()

    def get_robustness_column_names(self):
        if self.df_robustness is None:
            print("Please prepare cluster robustness before executing this function. "
                  "You can prepare the robustness analysis by executing this.prepare_robustness_analysis")
            return

        print(list(self.df_robustness.columns.values))


class ParameterRobustness:
    def __init__(self, files_paths, names):
        self.paths = files_paths
        self.names = names
        self.datasets = {}
        self.df_robustness = None

    def add_datasets(self, ens_range):
        for p, n in zip(self.paths, self.names):
            ds = xr.open_dataset(p)
            ds = prepare_dataset(ds)
            self.datasets[n] = ds.sel(ens=ens_range)

    def prepare_robustness_analysis(self, variable_name, number_cluster=3, gamma=.01):
        cluster_of_variables = {}
        for n in self.datasets:
            clust_of_variable = cl.TimeSeriesClustering(self.datasets[n], variable_name)
            clust_of_variable.generate_cluster(n_cluster=number_cluster, gamma=gamma)
            cluster_of_variables[n] = clust_of_variable

        first = True
        df_cluster = xr.Dataset
        for ds_name in self.datasets:
            df = cluster_of_variables[ds_name].get_cluster()
            df['method'] = [n + '_' + ds_name for n in df['method'].values]
            if first:
                df_cluster = df
                first = False
            else:
                df_cluster = xr.concat([df_cluster, df], dim='method')

        df_column_names = list(df_cluster['method'].values)
        df_index_names = list(df_cluster['ens'].values)

        dist_array = df_cluster['cluster'].values.transpose()
        df_robustness = pd.DataFrame(data=dist_array, index=df_index_names, columns=df_column_names)

        self.df_robustness = df_robustness.reindex(sorted(df_robustness.columns), axis=1)

    def plot_robustness(self, column_names, column_labels=[], title="", figsize=(7, 6), dpi=250, cmap='viridis'):
        if self.df_robustness is None:
            print("Please prepare cluster robustness before executing this function. "
                  "You can prepare the robustness analysis by executing this.prepare_robustness_analysis")
            return
        r_scores = compute_rand_score(self.df_robustness, column_names)
        r_scores_no_self = r_scores.copy()

        for i in range(0, len(column_names) - 1):
            for j in range(i + 1, len(column_names)):
                r_scores[i, j] = None

        for i in range(0, len(column_names)):
            for j in range(i, len(column_names)):
                r_scores_no_self[i, j] = None

        print("Mean robustness score: " + str(np.nanmean(r_scores_no_self)))

        if column_labels == []:
            column_labels = column_names

        fig, ax = plt.subplots(figsize=figsize, dpi=dpi)
        ax.grid(False)
        im, cbar = heatmap(r_scores, column_labels, column_labels, title=title, ax=ax,
                           cmap=cmap, cbarlabel="Rand score", vmin=0.5, vmax=1)
        texts = annotate_heatmap(im, valfmt="{x:.2f}")

        fig.tight_layout()
        plt.show()

        return r_scores_no_self

    def get_robustness_column_names(self):
        if self.df_robustness is None:
            print("Please prepare cluster robustness before executing this function. "
                  "You can prepare the robustness analysis by executing this.prepare_robustness_analysis")
            return

        print(list(self.df_robustness.columns.values))