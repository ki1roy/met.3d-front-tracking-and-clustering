import numpy as np
import pandas as pd


def prepare_dataset(ds):
    """
    Prepare dataset for time series analysis and time series clustering.
    the total front area is computed from m^2 to km^2.
    Data coordinate renaming (trajectory to ens)
    The frontal slope is computed and normalized. The frontal slope is 0, if the front is vertical, positive for
    slope towards the frontal zone, negative for slope away from frontal zone.
    :param ds: xarray data set
    :return: manipulated dataset
    """
    ds['totalFrontArea_km2'] = ds['totalFrontArea_m2']/1000000
    ds = ds.rename({"trajectory": "ens"}).sel(ensemble=1).drop("ensemble")

    sz = ds["slope_normal_lengthXY_average"]
    sx = ds["slope_normal_lengthZ_average"]
    ds["slope_angle"] = (np.arccos(sx / (np.sqrt(sx * sx + sz * sz))) * 180 / np.pi - 90) * -1
    ds["slope_normalized"] = ds["slope_angle"].copy()
    ds["slope_normalized"].values = ds["slope_angle"]
    return ds


from sklearn.metrics.cluster import rand_score
def compute_rand_score(df, sens_names):
    robustness_score = np.empty((len(sens_names), len(sens_names)))
    for i in range(0, len(sens_names)):
        for j in range(0, len(sens_names)):
            robustness_score[i,j] = rand_score(df[sens_names[i]], df[sens_names[j]])

    df_robustness_score = pd.DataFrame(robustness_score, index=sens_names, columns=sens_names)
    pd.DataFrame(df_robustness_score, index=sens_names, columns=sens_names)
    return robustness_score


def compute_robustness_score(df, sens_names):
    robustness_score = np.empty((len(sens_names), len(sens_names)))
    for i in range(0, len(sens_names)):
        for j in range(0, len(sens_names)):
            robustness_score[i,j] = 1 - (np.where(df[sens_names[i]] != df[sens_names[j]])[0].size / df.shape[0])

    df_robustness_score = pd.DataFrame(robustness_score, index=sens_names, columns=sens_names)
    pd.DataFrame(df_robustness_score, index=sens_names, columns=sens_names)
    return robustness_score

