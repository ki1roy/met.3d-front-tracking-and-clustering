import numpy as np
import xarray as xr
from matplotlib import pyplot as plt
from tslearn.clustering import TimeSeriesKMeans, silhouette_score
from timeseriessimilarity.getterFunctions import get_xticks, get_xlabels


class TimeSeriesClustering:

    def __init__(self, ds_timeseries, variable_name):
        assert ("time" in ds_timeseries) and ("ens" in ds_timeseries) and (variable_name in ds_timeseries)
        self.__ds_timeseries = ds_timeseries
        self.__ds_cluster = None
        self.__ds_silhouette_score = None
        self.__ds_cluster_center = None
        self.__d_trained_cluster = None
        self.__variable_name = variable_name
        self.__cluster_methods = ["euclidean", "dtw", "softdtw"]
        self.__n_cluster = int(3)
        self.__n_ens_member = self.__ds_timeseries['ens'].size

    def change_variable(self, variable_name):
        assert (variable_name in self.__ds_timeseries)
        self.__variable_name = variable_name

    def change_number_cluster(self, number_cluster):
        assert number_cluster > 0
        self.__n_cluster = int(number_cluster)
        
    def get_cluster(self):
        assert self.__ds_cluster
        return self.__ds_cluster

    def get_silhouette_score(self):
        assert self.__ds_silhouette_score
        return self.__ds_silhouette_score

    def generate_cluster(self, variable_name=None, n_cluster=None, seed=0, max_iter=50, n_init=1,
                         max_iter_barycenter=100, gamma=.01):
        if variable_name and self.__variable_name != variable_name:
            self.change_variable(variable_name)
        if n_cluster:
            self.change_number_cluster(n_cluster)

        data_vars = dict(cluster=(['method', 'ens'],
                                  np.full((len(self.__cluster_methods), self.__n_ens_member), -1, dtype=int)))
        data_vars_coords = dict(method=('method', self.__cluster_methods),
                                ens=('ens', self.__ds_timeseries['ens'].data))
        ds_cluster = xr.Dataset(data_vars=data_vars, coords=data_vars_coords)

        data_score = dict(score=('method', np.full(len(self.__cluster_methods), -1, dtype=float)))
        data_score_coords = dict(method=('method', self.__cluster_methods))

        ds_score = xr.Dataset(data_vars=data_score, coords=data_score_coords)

        data_center = dict(cluster_center=(['method', 'cluster_number', 'time'],
                                           np.full((len(self.__cluster_methods), self.__n_cluster,
                                                    self.__ds_timeseries['time'].size), -1, dtype=float)))
        data_center_coords = dict(method=('method', self.__cluster_methods),
                                  cluster=('cluster', np.arange(0, self.__n_cluster, 1)),
                                  time=('time', self.__ds_timeseries['time'].data))
        ds_cluster_center = xr.Dataset(data_vars=data_center, coords=data_center_coords)

        print("Generate cluster for: " + self.__variable_name)
        np.random.seed(seed)
        # x_train, y_train, X_test, y_test = CachedDatasets().load_dataset("Trace")
        # x_train = x_train[y_train < 4]  # Keep first 3 classes

        x_train = self.__ds_timeseries[self.__variable_name].data
        print("Euclidean k-means")
        method = 'euclidean'
        km = TimeSeriesKMeans(n_clusters=self.__n_cluster, verbose=0, random_state=seed)

        ds_cluster['cluster'].loc[dict(method=method)] = km.fit_predict(x_train)
        ds_score['score'].loc[dict(method=method)] = silhouette_score(x_train, km.labels_,
                                                                                   metric='euclidean')

        ds_cluster_center['cluster_center'].loc[dict(method=method)] = km.cluster_centers_.squeeze()

        # DBA-k-means
        print("DBA k-means")
        method = 'dtw'
        dba_km = TimeSeriesKMeans(n_clusters=self.__n_cluster,
                                  max_iter=max_iter,
                                  n_init=n_init,
                                  metric="dtw",
                                  verbose=0,
                                  max_iter_barycenter=max_iter_barycenter,
                                  random_state=seed)

        ds_cluster['cluster'].loc[dict(method=method)] = dba_km.fit_predict(x_train)
        ds_score['score'].loc[dict(method=method)] = silhouette_score(x_train, dba_km.labels_,
                                                                                   metric='dtw')
        ds_cluster_center['cluster_center'].loc[dict(method=method)] = dba_km.cluster_centers_.squeeze()

        # Soft-DTW-k-means
        print("Soft-DTW k-means")
        method = "softdtw"
        sdtw_km = TimeSeriesKMeans(n_clusters=self.__n_cluster,
                                   max_iter=max_iter,
                                   n_init=n_init,
                                   metric="softdtw",
                                   metric_params={"gamma": gamma},
                                   verbose=0,
                                   max_iter_barycenter=max_iter_barycenter,
                                   random_state=seed)
        ds_cluster['cluster'].loc[dict(method=method)] = sdtw_km.fit_predict(x_train)
        ds_score['score'].loc[dict(method=method)] = silhouette_score(x_train, sdtw_km.labels_, metric='softdtw')
        ds_cluster_center['cluster_center'].loc[dict(method=method)] = sdtw_km.cluster_centers_.squeeze()

        self.__ds_cluster = ds_cluster
        self.__ds_silhouette_score = ds_score
        self.__ds_cluster_center = ds_cluster_center

    def plot_cluster(self, tick_dist, figure_size=[6.4, 4.8], dpi=100, title=''):
        sz = self.__ds_timeseries['time'].size - 1

        y_limit_up = self.__ds_timeseries[self.__variable_name].values.max() + 0.1
        y_limit_down = self.__ds_timeseries[self.__variable_name].values.min() - 0.1

        if y_limit_down < 0:
            y_limit_up = max([abs(y_limit_up), abs(y_limit_up)])
            y_limit_down = -max([abs(y_limit_up), abs(y_limit_up)])

        x_ticks, x_labels = get_xticks(self.__ds_timeseries, tick_dist), get_xlabels(self.__ds_timeseries, tick_dist)

        plt.figure(figsize=figure_size, dpi=dpi)

        i = 0
        for m in self.__cluster_methods:
            for yi in range(0, self.__n_cluster):
                plt.subplot(len(self.__cluster_methods), self.__n_cluster, i * self.__n_cluster + yi + 1)
                for xx in self.__ds_timeseries[self.__variable_name].loc[dict(ens=self.__ds_cluster['cluster'].loc[dict(method=m)] == yi)].values:
                    plt.plot(xx.ravel(), "k-")
                plt.plot(self.__ds_cluster_center['cluster_center'].sel(dict(method=m, cluster_number=yi)).data,
                         "r-")
                plt.xlim(0, sz)
                plt.ylim(y_limit_down, y_limit_up)
                plt.text(0.55, 0.85, 'Cluster %d' % (yi + 1), transform=plt.gca().transAxes)
                if yi == 1:
                    plt.title(
                        m + " $k$-means, silhouette score: %.3f" % self.__ds_silhouette_score['score'].loc[dict(method=m)])
                # labels
                plt.xticks(x_ticks, "")
                plt.grid(color='k', linestyle=':', linewidth=1)
            i += 1

        plt.tight_layout()
        plt.show()

    def plot_cluster1(self, tick_dist, figure_size=[6.4, 4.8], dpi=100, title=''):
        sz = self.__ds_timeseries['time'].size - 1

        y_limit_up = self.__ds_timeseries[self.__variable_name].values.max() + 0.1
        y_limit_down = self.__ds_timeseries[self.__variable_name].values.min() - 0.1

        if y_limit_down < 0:
            y_limit_up = max([abs(y_limit_up), abs(y_limit_up)])
            y_limit_down = -max([abs(y_limit_up), abs(y_limit_up)])

        x_ticks, x_labels = get_xticks(self.__ds_timeseries, tick_dist), get_xlabels(self.__ds_timeseries, tick_dist)

        n_cluster = self.__n_cluster
        n_methods = len(self.__cluster_methods)

        fig, axs = plt.subplots(nrows=n_methods, ncols=n_cluster, figsize=figure_size, dpi=dpi)

        for m in range(0, n_methods):
            for c in range(0, n_cluster):
                m_name = self.__cluster_methods[m]
                for xx in self.__ds_timeseries[self.__variable_name].loc[
                        dict(ens=self.__ds_cluster['cluster'].loc[dict(method=m_name)] == c)].values:
                    axs[m, c].plot(xx.ravel(), "k-", alpha=0.3)


                axs[m, c].plot(self.__ds_cluster_center['cluster_center'].sel(dict(method=m_name, cluster_number=c)).data,
                               color="red", linestyle='dotted', linewidth=3)
                axs[m, c].set_xlim(0, sz)
                axs[m, c].set_ylim(y_limit_down, y_limit_up)
                axs[m, c].grid(color='k', linestyle=':', linewidth=1)

                if m == n_methods - 1:
                    axs[m, c].set_xticks(x_ticks, x_labels)
                    plt.setp(axs[m, c].get_xticklabels(), rotation=45, ha='right',
                             rotation_mode="anchor")
                else:
                    axs[m, c].set_xticks(x_ticks, "")

                if m == 0:
                    axs[m, c].set_title('Cluster %d' % (c + 1))

            # Rotate the tick labels and set their alignment.
        plt.tight_layout()
        plt.show()
