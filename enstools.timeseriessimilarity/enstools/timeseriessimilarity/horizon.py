#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 10 12:05:18 2022

@author: Alexander Hoffmann
"""


import numpy as np
import numpy
#import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.patches import Patch

'''
shows horizon graph of data.
-data: 2d numpy array
-base: value "center" of horizon; defines "neutral" value
-cmap: string name of matplotlib colormap you want to use for color steps
'''


def horizon(fig, data, vmin, vmax, base=0, cmap="bwr", memberlabels=None, horizon_base='avg', ranges=None):
    #extend = np.amax(np.absolute(data - base))
    extend = np.absolute(vmax - base)

    perms = data.shape[0]
    axs = fig.subplots(perms, sharex=True, sharey=True)
    # generating colors for subplots
    cm = mpl.cm.get_cmap(cmap)
    css = cm(np.linspace(0.0, 1.0, 6))

    if horizon_base == 'min':
        cs = list(zip(css[1:], (css[:5])))
        ranges = np.around(np.linspace(base, base + extend, 7), 1)
        threshold = extend / 6
    elif horizon_base == 'max':
        cs = list(zip(css[1:], (css[:5])))
        ranges = np.around(np.linspace(base - extend, base, 7), 1)
        threshold = extend / 6
    elif horizon_base == 'avg':
        if ranges:
            rlength = len(ranges) - 1
            rlengthHalf = int(rlength / 2)
            css = cm(np.linspace(0.0, 1.0, rlength))
            cs = list(zip(css[rlengthHalf:], reversed(css[:rlengthHalf])))
            threshold = extend / rlengthHalf
        else:
            ranges = np.around(np.linspace(base - extend, base + extend, 7), 1)
            cs = list(zip(css[3:], reversed(css[:3])))
            threshold = extend / 3
    else:
        ranges = np.around(np.linspace(base - extend, base + extend, 7), 1)
        cs = list(zip(css[3:], reversed(css[:3])))
        threshold = extend / 3

    legend = [Patch(facecolor=c, label=f"{ranges[i]} - {ranges[i + 1]}") for i, c in enumerate(css)]
    legend.reverse()
    if memberlabels is not None:
        for i, ax in enumerate(axs):
            axHorizon(ax, data[i], threshold, cs, base, memberlabels[i])
    else:
        for i, ax in enumerate(axs):
            axHorizon(ax, data[i], threshold, cs, base)

    # axs[0].legend(handles=legend)
    fig.legend(handles=legend)
    # axs[0].set_xticks(timeticks)
    # axs[-1].set_xticklabels(timelabels, rotation=45, ha='right')
    axs[0].set_xlim((0, data.shape[1] - 1))
    # axs[0].set_adjustable("box",share=True)
    # axs[0].set_aspect(1.,share=True)
    return axs


def axHorizon(a, data, threshold, cs, base=0, label=None):
    window = lambda x, min_lim, max_lim: np.maximum(np.minimum(x, max_lim), min_lim)
    for i, (cp, cn) in enumerate(cs):
        cthresh, nthresh = threshold * i, threshold * (i + 1)
        arr_p = np.absolute(window(data - base, cthresh, nthresh)) - cthresh
        arr_n = np.absolute(window(data - base, -nthresh, -cthresh)) - cthresh
        a.fill_between(np.arange(data.shape[0]), arr_p, 0, color=cp)
        a.fill_between(np.arange(data.shape[0]), arr_n, 0, color=cn)
    a.set_yticks([])
    if label is not None:
        a.set_ylabel(label)
