# Version of the Namespace package "timeseriessimilarity"
__version__ = "2020.11.a1"

from . import distanceMeasure
from . import wrappers

from . import horizon
from . import getterFunctions
#import getterFunctions
#from .runs import *
from . import plotterFunctions
from . import plotter
from . import tsPlotter
