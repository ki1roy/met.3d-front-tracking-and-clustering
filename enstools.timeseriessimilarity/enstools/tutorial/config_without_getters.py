#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 17 11:30:12 2022

@author: kakaufmann
"""
import timeseriessimilarity.getterFunctions as gf
from timeseriessimilarity.plotterFunctions import paint_ensemble_heatmap, paint_ens_windows_heatmap ,paint_ens_with_reference_heatmap
from timeseriessimilarity.plotterFunctions import paint_ensemble_horizon#, paint_ens_with_reference_horizon, paint_ens_windows_horizon
import timeseriessimilarity.plotter as plotter

import timeseriessimilarity.wrappers as wrp
#import numpy as np

#%% set params
#------------------------------------the looks---------------------------------
varName = "SSW_10" #name of the variable holding the time series
title = None #title of the figure, default is Ensemble: varName
width = 15
coreHeight = 15
outerHeight = 5
cmap = "viridis"
tickDistance = 25

#--------------------------------calc. distance--------------------------------
referenceLabel = 31
referenceTime = (25,75)
distFunc = wrp.dtw_wrapper
threshold = 0.2
criteria = "dist"
#%%

ord_heatf = gf.curry(paint_ens_windows_heatmap,varName = varName, title = title, coreHeight = coreHeight, outerHeight = outerHeight, 
             cmap = cmap, width = width, referenceLabel = referenceLabel, referenceTime = referenceTime)
heatmapOrderedPlotter = plotter.Plotter(painter = ord_heatf)
#%%

originalGetters = [gf.curry(gf.get_data_array, varName = varName),        #data,
                   gf.get_ylabels,                                        #ylabels
                   gf.curry(gf.get_ylims,varName = varName),              #ylims
#                   gf.curry(gf.get_xlabels, tickDistance = tickDistance), #xlabels
                   gf.curry(gf.get_xticks, tickDistance = tickDistance),  #xticks
                   gf.curry(gf.get_reference, varName = varName, referenceLabel = referenceLabel)]#reference
 
keys = ["data", "ylabels","ylims" ,"xticks", "reference"]
keyedGetters = [gf.add_keys(f,k) if isinstance(k, tuple) else gf.add_key(f,k) for f,k in zip(originalGetters,keys)]
 
ref_heatf = gf.curry(paint_ens_with_reference_heatmap,varName = varName, title = title, coreHeight = coreHeight, outerHeight = outerHeight, 
             cmap = cmap, width = width, referenceLabel = referenceLabel, referenceTime = referenceTime)
 
heatmapRefPlotter = plotter.Plotter(getters=keyedGetters, painter = ref_heatf)

