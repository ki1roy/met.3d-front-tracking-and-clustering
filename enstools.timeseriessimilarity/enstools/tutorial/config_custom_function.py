#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 14 16:43:44 2022

@author: kakaufmann
"""

import timeseriessimilarity.getterFunctions as gf
#from timeseriessimilarity.plotterFunctions import paint_ensemble_heatmap, paint_ens_with_reference_heatmap, paint_ens_windows_heatmap 
#from timeseriessimilarity.plotterFunctions import paint_ensemble_horizon, paint_ens_with_reference_horizon, paint_ens_windows_horizon
import timeseriessimilarity.plotter as plotter

import timeseriessimilarity.wrappers as wrp


varName = "T"
#title = None #"2018 Firederike"
width = 15
coreHeight = 15
outerHeight = 5
cmap = "magma"
tickDistance = 6
scale = (265,275)

#--------------------------------calc. distance--------------------------------
referenceLabel = 13
referenceTime = (10,21)
distFunc = wrp.euclidean_wrapper
threshold = 10
criteria = "dist"

originalGetters = [gf.curry(gf.get_data_array, varName = varName),       #data
                   gf.get_ylabels,                                        #ylabels
                   gf.curry(gf.get_xlabels, tickDistance = tickDistance), #xlabels
                   gf.curry(gf.get_xticks, tickDistance = tickDistance)]  #xticks

keys = ["data", "ylabels", "xlabels","xticks"]
keyedGetters = [gf.add_keys(f,k) if isinstance(k, tuple) else gf.add_key(f,k) for f,k in zip(originalGetters,keys)]

heatmapPlotter = plotter.Plotter(getters=keyedGetters)
