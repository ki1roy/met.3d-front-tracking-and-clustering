#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  8 09:25:31 2022

@author: kakaufmann
"""
import pytest
import numpy as np
import xarray as xr
import enstools.timeseriessimilarity.getterFunctions as ensf

def create_simple_test_dataset():
    dsTest = xr.Dataset()
    fAssign = lambda arr: xr.DataArray(arr,coords= [np.arange(5), np.arange(20)], dims = ["ens", "interval"])
    dsTest["Test1"] = fAssign(np.zeros((5,20)))
    dsTest["Test2"] = fAssign(np.arange(100).reshape((5,20)))
    dsTest[""] = fAssign(np.ones((5,20)))
    return dsTest

def create_windows_test_dataset():
    #data shape (ens: 5, time: 20)
    dsTest = xr.Dataset()
    fAssign = lambda arr: xr.DataArray(arr,coords= [np.arange(5)], dims = ["window"])
    dsTest["ens"] = fAssign(np.arange(5))
    #dsTest["distance"] = fAssign(np.ones(5)*2)
    dsTest["relativeDistance"] = fAssign(np.ones(5)*0.5)
    #dsTest["offset"] = fAssign(np.ones(5)*3)
    #dsTest["timeStretch"] = fAssign(np.ones(5)*4)
    dsTest["timeOffset"] = fAssign(np.arange(5))
    return dsTest

#%% get_data_array
def test_get_data_array():
    dsTest = create_simple_test_dataset()
    assert np.all(ensf.get_data_array(dsTest, "Test1") == np.zeros((5,20)))
    
    assert np.all(ensf.get_data_array(dsTest, "Test2") == np.arange(100).reshape((5,20)))
    assert np.all(ensf.get_data_array(dsTest, "") == np.ones((5,20)))
    
    
def test_get_data_array_transpose():
    dsTest = create_simple_test_dataset()
    arr = xr.DataArray(np.ones((20,5))*5,coords= [np.arange(20), np.arange(5)], dims = ["interval", "ens"])
    dsTest["Test1"] = arr
    assert np.all(ensf.get_data_array(dsTest, "Test1") == np.ones((5,20))*5)
    
    data = np.broadcast_to(np.arange(1,6)*20,(20,5))
    arr = xr.DataArray(data, coords= [np.arange(20), np.arange(5)], dims = ["interval", "ens"])
    dsTest["Test2"] = arr
    assert np.all(ensf.get_data_array(dsTest, "Test2") == np.transpose(data))
    
    
def test_get_data_array_negative():
    dsTest = create_simple_test_dataset()
    with pytest.raises(KeyError):
        ensf.get_data_array(dsTest, "noName")
        
#%% get_time_series
def test_get_time_series():
    dsTest = create_simple_test_dataset()
    
    assert np.all(ensf.get_time_series(dsTest, "Test2", 0) == np.arange(20))
    assert np.all(ensf.get_time_series(dsTest, "Test2", -1) == np.arange(80,100))
    assert np.all(ensf.get_time_series(dsTest, "Test2", 2) == np.arange(40,60))
    
def test_get_time_series_IndexError():
    dsTest = create_simple_test_dataset()
    with pytest.raises(IndexError):
        ensf.get_time_series(dsTest, "Test2", 13)
        
    with pytest.raises(IndexError):
        ensf.get_time_series(dsTest, "Test1", -13)
        
def test_get_time_series_KeyError():
    dsTest = create_simple_test_dataset()
    with pytest.raises(KeyError):
        ensf.get_time_series(dsTest, "noName", 0)
    
#%% get_ordered_data
def test_order_data():
    dsTest = create_simple_test_dataset()
    data = np.broadcast_to(np.arange(1,6),(20,5))
    data = data.T
    
    dsTest["Test3"] = xr.DataArray(data,coords= [np.arange(5), np.arange(20)], dims = ["ens", "interval"])
    nimap = {0:0, 1:1, 2:2, 3:3, 4:4}
    order = [(0,4),(1,3),(2,2),(3,1),(4,0)]
    toTest = ensf.get_ordered_data(dsTest, "Test3", order, nimap)
    assert np.all(toTest == np.flip(data, 0))
    
    order = list(nimap.items())
    toTest = ensf.get_ordered_data(dsTest, "Test3", order, nimap)
    assert np.all(toTest == data)
    
def test_order_data_mismatched_data_length():
    dsTest = create_simple_test_dataset()
    data = np.broadcast_to(np.arange(1,6),(20,5))
    data = data.T
    
    dsTest["Test3"] = xr.DataArray(data,coords= [np.arange(5), np.arange(20)], dims = ["ens", "interval"])
    nimap = {0:0, 1:1, 2:2, 3:3, 4:4}
    order = [(0,4),(1,3),(2,2),(3,1), (4,0)]
    with pytest.raises(ValueError):
        _ = ensf.get_ordered_data(dsTest, "Test3", order[:-1], nimap)

    nimap.pop(4)
    with pytest.raises(KeyError):
        _ = ensf.get_ordered_data(dsTest, "Test3", order, nimap)
    
    nimap[4] = 0
    order.append((5,5))
    with pytest.raises(ValueError):
        _ = ensf.get_ordered_data(dsTest, "Test3", order, nimap)
        
    
#%% generate_mask

def test_generate_mask():
    dataShape= (5,20)
    wSize = 5
    nimap = {0:0, 1:1, 2:2, 3:3, 4:4}
    dsWindows = create_windows_test_dataset()
    toTest = ensf.generate_mask(dataShape, dsWindows, wSize, nimap)
    trueResult = np.zeros(dataShape)
    for i in range(5):
        trueResult[i,i:i+5] = 0.5 
        
    assert np.all(toTest == trueResult)
    
    dsWindows["weightedDistance"] =  xr.DataArray(np.ones(5)*0.4,coords= [np.arange(5)], dims = ["window"])
    toTest = ensf.generate_mask(dataShape, dsWindows, wSize, nimap)
    trueResult = np.zeros(dataShape)
    for i in range(5):
        trueResult[i,i:i+5] = 0.6
        
    assert np.all(toTest == trueResult)
    

def test_generate_mask_negative():
    dsWindows = create_windows_test_dataset()
    dataShape = (5,20)
    wSize = 5
    nimap = {0:0, 1:1, 2:2, 3:3}
    with pytest.raises(KeyError):
        _ = ensf.generate_mask(dataShape, dsWindows, wSize, nimap)
    
    wSize = 0
    nimap = {0:0, 1:1, 2:2, 3:3, 4:4}
    with pytest.raises(AssertionError):
        _ = ensf.generate_mask(dataShape, dsWindows, wSize, nimap)

#%% get_y_bound

def test_get_y_bound():
    data = np.ones((5,20))
    data[0,1] = 5
    assert ensf.get_y_bound(data) == (-5,5)
    
    data[4,10] = -6
    assert ensf.get_y_bound(data) == (-6,6)
    
    data[3,3] = 6
    assert ensf.get_y_bound(data) == (-6,6)
    
#%% gete_x_label_stuff
def test_get_x_label_stuff():
    testTime = np.arange('2020-02', '2020-03', dtype='datetime64[D]')
    tickDist = 7
    testTicks, testLabels = ensf.get_x_label_stuff(testTime, tickDist)
    assert np.all(testTicks == np.arange(0,29,7))
    assert np.all(testLabels == testTime[testTicks])
    
    
def test_get_x_label_stuff_time_short():
    testTime = np.arange('2020-02', '2020-02-02', dtype='datetime64[D]')
    tickDist = 7
    testTicks, testLabels = ensf.get_x_label_stuff(testTime, tickDist)
    assert np.all(testTicks == np.array([0]))
    assert testLabels == np.array(testTime[0])
    
