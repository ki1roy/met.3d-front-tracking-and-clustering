#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 16 12:39:41 2021

@author: kakaufmann
"""
import pytest
import numpy as np
import enstools.timeseriessimilarity.wrappers as wrp

#@pytest

def test_euclidean_wrapper():
    assert wrp.euclidean_wrapper(np.ones(10),np.zeros(10)) == (np.sqrt(10),0)
    
    a = np.array([np.nan,np.nan,np.nan,np.nan,np.nan,np.nan,np.nan,np.nan,np.nan,np.nan])
    assert np.isnan(wrp.euclidean_wrapper(np.zeros(10), a)[0])
    
    a = np.ones(10)
    a[0] = np.nan
    assert np.isnan(wrp.euclidean_wrapper(np.zeros(10), a)[0])


def test_removeNans():
    arr = np.ones(10)
    assert wrp.removeNans(arr).all() == arr.all()
    
    arr[0] = np.nan
    assert sum(wrp.removeNans(arr)) == sum(arr[1:])
    
    arr[5] = np.nan
    assert sum(wrp.removeNans(arr)) == 8
    
    arr = np.arange(1,11, dtype = float)
    goal = sum(arr)
    goal = goal -arr[7]
    arr[7] = np.nan
    assert sum(wrp.removeNans(arr)) == goal
    
    goal = goal -arr[-1]
    arr[-1] = np.nan
    assert sum(wrp.removeNans(arr)) == goal
    
def test_lcss(): #TODO: thin kmore tests
    arrA = np.ones(6)
    arrA[2:4] = 2
    arrB = np.ones(6)
    arrB[3:5] = 2
    t = 0.5
    result = wrp.lcss(arrA,arrB,t)
    assert result[0] == 5
    realPath =[(5,5),(4,5),(3,4),(2,3),(1,2),(1,1),(0,0)]
    for i,v in enumerate(realPath):
        assert result[1][i][0] == v[0]
        assert result[1][i][1] == v[1]
    
    