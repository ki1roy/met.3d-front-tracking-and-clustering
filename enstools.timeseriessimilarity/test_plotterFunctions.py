#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 28 13:26:27 2022

@author: kakaufmann
"""
import pytest
#import numpy as np
#import xarray as xr
import enstools.timeseriessimilarity.plotterFunctions as ensf

def test_generate_figure():
    with pytest.raises(ValueError):
        ensf.generate_figure(-1, 15, 5, 2)
         
    with pytest.raises(AssertionError):
        ensf.generate_figure(15, 15, 5, -1)
     
    with pytest.raises(AssertionError):
        ensf.generate_figure(15, 15, -1, 2)
     
    with pytest.raises(AssertionError):
        ensf.generate_figure(15, -1, 5, 2)